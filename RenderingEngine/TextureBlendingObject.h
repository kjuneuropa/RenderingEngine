#pragma once
#include "stdafx.h"
#include "Object.h"
#include "TextureBlendingMesh.h"
//#include "Mesh.h"
class CTextureBlendingObject : public CGameObject
{
public:
	CTextureBlendingObject();

	CTextureBlendingObject(ID3D11Device *pd3dDevice);
	virtual ~CTextureBlendingObject();

	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
};

