#include "Renderer.h"
#include "Shader.h"

_component_identifier<CRenderer, CComponent, true> CRenderer::identifier;


CRenderer::CRenderer()
{
	m_ppMeshes = nullptr;
	m_pTexture = nullptr;
	m_pMaterial = nullptr;
	m_bcMeshBoundingCube = AABB();
	m_nMeshes = 1;
	
}


CRenderer::~CRenderer()
{
	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i]) m_ppMeshes[i]->Release();
			m_ppMeshes[i] = nullptr;
		}
		delete[] m_ppMeshes;
	}
	if (m_pMaterial) m_pMaterial->Release();
	if (m_pTexture) m_pTexture->Release();
}


void CRenderer::Initalize(ID3D11Device *pd3dDevice)
{
	m_ppMeshes = new CMesh*[m_nMeshes];
	for (int i = 0; i < m_nMeshes; i++)m_ppMeshes[i] = nullptr;

}

void CRenderer::SetMesh(CMesh *pMesh, int nIndex)
{
	if (m_ppMeshes)
	{
		if (m_ppMeshes[nIndex])
			m_ppMeshes[nIndex]->Release();
		m_ppMeshes[nIndex] = pMesh;
		if (pMesh)
			pMesh->AddRef();
	}

	if (pMesh)
	{
		AABB bcBoundingCube = pMesh->GetBoundingCube();
		m_bcMeshBoundingCube.Union(&bcBoundingCube);
	}
}
void CRenderer::ReplaceTexture(ID3D11ShaderResourceView* pd3dsrvTexture)
{
	if (pd3dsrvTexture)
	{
		m_pTexture->SetTexture(0, pd3dsrvTexture);
	}	
}
void CRenderer::CreateMaterial(XMFLOAT4 xmf4Diffuse, XMFLOAT4 xmf4Ambient, XMFLOAT4 xmf4Specular, XMFLOAT4 xmf4Emissive)
{
	m_pMaterial = new CMaterial;
	m_pMaterial->m_Material.m_xmf4Diffuse = xmf4Diffuse;
	m_pMaterial->m_Material.m_xmf4Ambient = xmf4Ambient;
	m_pMaterial->m_Material.m_xmf4Specular = xmf4Specular;
	m_pMaterial->m_Material.m_xmf4Emissive = xmf4Emissive;
}
void CRenderer::CreateMaterial(CMaterial* pMaterial)
{
	m_pMaterial = pMaterial;
}
void CRenderer::CreateTexture(ID3D11Device *pd3dDevice, wstring strTextureName)
{
	ID3D11SamplerState *pd3dBaseSamplerState = nullptr;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;


	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dBaseSamplerState);

	m_pTexture = new CTexture(1, 1, 0, 0);
	ID3D11ShaderResourceView *pd3dsrvBaseTexture = nullptr; // dirt01d.tga

	 D3DX11CreateShaderResourceViewFromFile(pd3dDevice, strTextureName.c_str(), nullptr, nullptr, &pd3dsrvBaseTexture, nullptr);

	m_pTexture->SetTexture(0, pd3dsrvBaseTexture);
	m_pTexture->SetSampler(0, pd3dBaseSamplerState);
	pd3dsrvBaseTexture->Release();
	pd3dBaseSamplerState->Release();
}
void CRenderer::CreateTexture(ID3D11Device *pd3dDevice, wstring strTextureName, HWND hWnd)
{
	ID3D11SamplerState *pd3dBaseSamplerState = nullptr;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;


	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dBaseSamplerState);



	OPENFILENAME OFN;
	wchar_t str[300];
	wchar_t lpstrFile[MAX_PATH] = L"";

	memset(&OFN, 0, sizeof(OPENFILENAME));
	OFN.lStructSize = sizeof(OPENFILENAME);
	OFN.hwndOwner = hWnd;
	OFN.nFilterIndex = 2;
	OFN.lpstrFilter = L"텍스쳐 파일(*.jpg;*.bmp;*.png;*.dds)\0*.jpg;*.bmp;*.PNG;*.dds\0\0";
	OFN.lpstrFile = lpstrFile;
	OFN.nMaxFile = MAX_PATH;



	// 그냥 바꾸니깐 문제가 생기는데?

	// 텍스쳐 컨테이너에 저장해서
	// 이미 존재하는 것이라면 그냥 쓰고
	// 그게 아니면 가져와서 쓸까?

	if (GetOpenFileName(&OFN) != 0)
	{
		wcout << OFN.lpstrFile << endl;
		ID3D11ShaderResourceView *pd3dsrvTexture = nullptr;
		if (FAILED(D3DX11CreateShaderResourceViewFromFile(pd3dDevice, OFN.lpstrFile, nullptr, nullptr, &pd3dsrvTexture, nullptr)))
		{
		}

		m_pTexture = new CTexture(1, 1, 0, 0);
		//ID3D11ShaderResourceView *pd3dsrvBaseTexture = nullptr; // dirt01d.tga
		// D3DX11CreateShaderResourceViewFromFile(pd3dDevice, strTextureName.c_str(), nullptr, nullptr, &pd3dsrvBaseTexture, nullptr);

		m_pTexture->SetTexture(0, pd3dsrvTexture);
		m_pTexture->SetSampler(0, pd3dBaseSamplerState);
		pd3dsrvTexture->Release();
		pd3dBaseSamplerState->Release();
	}


}
void CRenderer::UpdateMaterial(ID3D11DeviceContext* pd3dDeviceContext)
{
	if (m_pMaterial) CIlluminatedShader::UpdateShaderVariable(pd3dDeviceContext, &m_pMaterial->m_Material);
}
void CRenderer::UpdateTexture(ID3D11DeviceContext* pd3dDeviceContext)
{
	if (m_pTexture) m_pTexture->UpdateShaderVariable(pd3dDeviceContext);
}
void CRenderer::Render(ID3D11DeviceContext* pd3dDeviceContext, CCamera *pCamera)
{
	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i])
			{
				bool bIsVisible = true;
				if (pCamera)
				{
					AABB bcBoundingCube = m_ppMeshes[i]->GetBoundingCube();
					bcBoundingCube.Update(&XMLoadFloat4x4(&m_pTransform->GetWorldMtx()));
					bIsVisible = pCamera->IsInFrustum(&bcBoundingCube);
				}
				if (bIsVisible) m_ppMeshes[i]->Render(pd3dDeviceContext);
			}
		}
	}
}
void CRenderer::Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	UpdateMaterial(pd3dDeviceContext);
	UpdateTexture(pd3dDeviceContext);
	Render(pd3dDeviceContext, pCamera);
}