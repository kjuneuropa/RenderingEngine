// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

//#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
// Windows 헤더 파일:

#include <windows.h>
#include <cstdlib>
#include <memory>
#include <cmath>
#include <tchar.h>
#include  <iostream>
#include <vector>
#include <array>
#include <algorithm>
#include <unordered_map>
#include <map>
#include <functional>


//#include <d3d11.h>
//#include <DXGI.h>
//#include <d3dx11.h>
//#include <xnamath.h>
//#include <d3dcompiler.h>  


#include "include\D3DX11.h"
#include "Include\D3D11.h"
#include "Include\DXGI.h"
#include "Include\xnamath.h"
#include "Include\D3Dcompiler.h"
#include "Include\D3DX10Math.h"
#include <commdlg.h>

//#include <d3dx10.h>


#include <Mmsystem.h>

	//쉐이더 컴파일 함수를 사용하기 위한 헤더 파일
//#include <D3DX10Math.h>	//Direct3D 수학 함수를 사용하기 위한 헤더 파일
//#include <D3D9Types.h>





#pragma commnet (lib, "d3dx11.lib");




//#pragma commnet (lib, "D3D11.lib");
//#pragma commnet (lib, "winmm.lib");
//#pragma commnet (lib, "legacy_stdio_definitions.lib");
////
//#pragma commnet (lib, "d3dx11d.lib");
//#pragma commnet (lib, "d3dcompiler.lib");
//#pragma commnet (lib, "d3dx9.lib");
//#pragma commnet (lib, "d3dx9d.lib");
//#pragma commnet (lib, "dxerr.lib");
//#pragma commnet (lib, "dxguid.lib");
//#pragma commnet (lib, "dxgi.lib");






//#include <directxmath.h>
//#include <DirectXMath.h>
//#include <DirectXPackedVector.h>
//#include <DirectXCollision.h>




// win
//#include <d3d.h>
//#include <d3d11.h>
//#include <d3d11shader.h>
//#include <DirectXMath.h>
//#include <D3DCompiler.h>

//#pragma commnet (lib, "DirectXTex.lib");
//#include "common\DirectXTex.h"

const int WINDOW_WIDTH = 1000;
const int WINDOW_HEIGHT = 600;
const int FRAME_BUFFER_WIDTH = 800;
const int  FRAME_BUFFER_HEIGHT = 600;


#define DIR_FORWARD		0x01
#define DIR_BACKWARD	0x02
#define DIR_LEFT		0x04
#define DIR_RIGHT		0x08
#define DIR_UP			0x10
#define DIR_DOWN		0x20


#define X_OFFSET 0
#define MAX_FPS 2000
#define MAX_TRIANGLES 50000

#define VS_SLOT_CAMERA				0x00
#define VS_SLOT_WORLD_MATRIX			0x01

#define PS_SLOT_COLOR	0x00
#define PS_SLOT_LIGHT			0x00
#define PS_SLOT_MATERIAL		0x01

//텍스쳐와 샘플러 상태를 설정하기 위한 쉐이더의 슬롯 번호를 정의한다. 
#define PS_SLOT_TEXTURE		0x00
#define PS_SLOT_SAMPLER_STATE		0x00



// 컨트롤

#define ID_WND_INSPECTOR 1

#define IDC_EDIT_POS_X 1
#define IDC_EDIT_POS_Y 2
#define IDC_EDIT_POS_Z 3

#define IDC_EDIT_ROT_X 4
#define IDC_EDIT_ROT_Y 5
#define IDC_EDIT_ROT_Z 6

#define IDC_EDIT_SCALE_X 7
#define IDC_EDIT_SCALE_Y 8
#define IDC_EDIT_SCALE_Z 9

#define WM_REPLACE WM_USER+1

enum TRANSFORM
{
	X = 0,
	Y = 1,
	Z = 2
};


enum OBJECT
{
	CUBE=0,
	SPHERE=1,
	PLANE=2,
	PILLAR =3
};

enum SHADER_TYPE
{
	TERRAIN = 0,
	BASIC=1,
	NORMALMAP=2
};

struct VertexType
{
	XMFLOAT3 xmf3Pos;
	XMFLOAT2 xmf2Tex;
};


using namespace std;
//using namespace DirectX;

#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 



//using namespace DirectX;
//using namespace DirectX::PackedVector;

// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
