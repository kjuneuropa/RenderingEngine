#pragma once
#include "stdafx.h"
class CTextManager
{
private:
	wchar_t m_pszCpuUsage[16];
	wchar_t m_pszVideoInfo[16];
	wchar_t m_pszFps[16];
	wchar_t m_pszVertexNum[16];

	vector<wstring> m_vSenteces;

public:
	CTextManager();
	~CTextManager();
	void SetCpuUsage(wchar_t* pCpuUsage){ wcscpy(m_pszCpuUsage, pCpuUsage); }
	void SetVideoInfo(wchar_t* pVideoInfo) { wcscpy(m_pszVideoInfo, pVideoInfo); }
	void SetFps(wchar_t* pFps) { wcscpy(m_pszFps, pFps); }
	void SetVisibleVertexNum(wchar_t* pVertexNum) { wcscpy(m_pszVertexNum, pVertexNum); }


	wchar_t* GetCpuUsage() { return m_pszCpuUsage; }
	wchar_t* GetVideoInfo() { return m_pszCpuUsage; }
	wchar_t* GetFps() { return m_pszCpuUsage; }
	wchar_t* GetVisibleVertexNum() { return m_pszCpuUsage; }

	void SetSentence(int nType, wchar_t* pSentence);
	vector<wstring>& GetSenteces() { return m_vSenteces; }

};
