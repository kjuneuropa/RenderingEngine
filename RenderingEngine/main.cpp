

#include "stdafx.h"
#include "LabProject08.h"
#include "GameFramework.h"
#include "Scroll.h"



#define MAX_LOADSTRING 100


HINSTANCE						ghAppInstance;								
TCHAR							szTitle[MAX_LOADSTRING];					
TCHAR							szWindowClass[MAX_LOADSTRING];			

HWND							g_hViewWnd;
HWND							g_hInspetor;
HWND							g_hTranform;
//CGameFramework					gGameFramework;

HINSTANCE						g_hInstance;
int g_nWindowWidth;
int g_nWindowHeight;


int g_nViewWidth;
int g_nViewHeight;

RECT g_nWindowPos;

void MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK About(HWND, UINT, WPARAM, LPARAM);

LRESULT CALLBACK ScreenProc(HWND, UINT, WPARAM, LPARAM);

LRESULT CALLBACK ComponentProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	MSG msg;
	HACCEL hAccelTable;

	::LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	::LoadString(hInstance, IDC_LABPROJECT03, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// // 응용프로그램 초기화를 수행합니다.
	if (!InitInstance (hInstance, nCmdShow)) 
		return(FALSE);

	hAccelTable = ::LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LABPROJECT03));
	
	while (1) 
	{
		//PeekMessage() 메세지가 있으면 꺼내고, TRUE를 반환한다.
		if (::PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) 
		{
			if (msg.message == WM_QUIT) 
				break;
			if (!::TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
		} 
		else 
		{
			//gGameFramework.FrameAdvance();
			CGameFramework::GetInstance().FrameAdvance();
		}
	}
	//메세제 루프가 종료되면 프레임 워크 객체를 소멸시킨다.
	//gGameFramework.OnDestroy();
	CGameFramework::GetInstance().OnDestroy();
	return((int)msg.wParam);
}


void MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= ::LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LABPROJECT03));
	wcex.hCursor		= ::LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_LABPROJECT03);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= ::LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	
	g_hInstance = hInstance;

	::RegisterClassEx(&wcex);

	wcex.hbrBackground = CreateSolidBrush(RGB(100, 100, 100));
	wcex.lpfnWndProc = ScreenProc;
	wcex.lpszClassName = L"Screen";
	::RegisterClassEx(&wcex);


	//
	//wcex.hbrBackground = (HBRUSH)(COLOR_BTNFACE + 1);
	//wcex.lpfnWndProc = ComponentProc;
	//wcex.lpszClassName = L"Component";
	//::RegisterClassEx(&wcex);


	//return ::RegisterClassEx(&wcex);


}

//다음은 주 윈도우를 생성하고 화면에 보이도록 하는 함수이다.
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	ghAppInstance = hInstance; 

	RECT rc = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
	//RECT rc = { 0, 0, FRAME_BUFFER_WIDTH + 200,FRAME_BUFFER_HEIGHT };
	DWORD dwStyle = WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN;
		//WS_OVERLAPPED | WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU | WS_BORDER;
	AdjustWindowRect(&rc, dwStyle, FALSE);
	HWND hMainWnd = CreateWindow(szWindowClass, szTitle, dwStyle, 0,0,
		                                                        rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, hInstance, nullptr);
	if (!hMainWnd) return(FALSE);
	


	
	g_nViewWidth = FRAME_BUFFER_WIDTH;
	g_nViewHeight = FRAME_BUFFER_HEIGHT;

	rc = { 0, 0, g_nViewWidth, g_nViewHeight };
	
	AdjustWindowRect(&rc, dwStyle, FALSE);
		g_hViewWnd = CreateWindow(TEXT("Screen"), nullptr, WS_CHILD |  WS_VISIBLE |WS_SIZEBOX,
	200, 0, g_nViewWidth, g_nViewHeight, hMainWnd, (HMENU)2, g_hInstance, nullptr);


	CGameFramework::GetInstance().OnCreate(hInstance, hMainWnd, g_hViewWnd, g_nViewWidth, g_nViewHeight);
	//gGameFramework.OnCreate(hInstance, hMainWnd, g_hViewWnd, g_nViewWidth, g_nViewHeight);


	//g_hInspetor = gComponent.Initalize(hMainWnd, g_hInstance, g_nViewWidth, g_nViewHeight);
	//gGameFramework.GetRenderScene()->InitailzeControl();
	
	::ShowWindow(hMainWnd, nCmdShow);
	//::ShowWindow(hMainWnd, SW_SHOWMAXIMIZED);
	::UpdateWindow(hMainWnd);


	return(TRUE);
}
CScroll* m_pScroll = new CScroll();
LRESULT CALLBACK ComponentProc(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	
	static bool g_Mouse = false;
	m_pScroll->BuildObjects(50, 300, 30, 30, 0, 20 , true);
	switch (nMessageID)
	{
	case WM_CREATE:
	{
		
		break;
	}
	case WM_COMMAND:
	{
		// 프레임 워크에 인스펙터가 있기 때문에 프레임워크에서 처리해준다.
		// (다르게 생각하면 이상할 수도 있다.
		//gGameFramework.OnProcessingWindowMessage(hWnd, nMessageID, wParam, lParam);
		CGameFramework::GetInstance().OnProcessingWindowMessage(hWnd, nMessageID, wParam, lParam);
		break;
	}
	case WM_SIZE:
		break;
	case WM_LBUTTONDOWN:
	{
		//cout << LOWORD(lParam) << " " << HIWORD(lParam) << endl;
		if (m_pScroll->CheckInputBox(LOWORD(lParam), HIWORD(lParam)))
		{
			
			g_Mouse = true;
		}
			
		else
			g_Mouse = false;
		
		break;
	}
		
	case WM_LBUTTONUP:
		g_Mouse = false;
		break;
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
		break;
	case WM_MOUSEMOVE:
		if (m_pScroll->CheckInputBox(LOWORD(lParam), HIWORD(lParam)))
		{
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			m_pScroll->SetOnMouse(true);
			if (g_Mouse)
			{
				//m_pScroll->SetScrollPos(pt);
				m_pScroll->m_vPos.x = pt.x;
				m_pScroll->m_vPos.y = pt.y;
				InvalidateRect(hWnd, nullptr, true);
			}
			
			
		}

		else
			m_pScroll->SetOnMouse(false);
		break;
	case WM_KEYDOWN:
		switch (wParam) {
		case VK_RETURN:
			cout << "엔터" << endl;
			m_pScroll->m_vPos.x += 5;
			break;
		}
		break;
	case WM_PAINT:
		hdc = ::BeginPaint(hWnd, &ps);
		cout << m_pScroll->m_vPos.x << " " << m_pScroll->m_vPos.y << endl;
		m_pScroll->Draw(hdc);
		EndPaint(hWnd, &ps);
		break;
	default:
		return(::DefWindowProc(hWnd, nMessageID, wParam, lParam));
	}
	return 0;
}
LRESULT CALLBACK ScreenProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
	/*	 CreateWindow(TEXT("edit"), nullptr, WS_CHILD | WS_BORDER | WS_VISIBLE,
		1000, 10, 200, 100, hWnd, (HMENU)1, g_hInstance, nullptr);*/

		break;
	case WM_SIZE:
		//gGameFramework.OnProcessingWindowMessage(hWnd, message, wParam, lParam);
		CGameFramework::GetInstance().OnProcessingWindowMessage(hWnd, message, wParam, lParam);
		break;
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	//case WM_MOUSEMOVE:
	case WM_KEYDOWN:
	case WM_KEYUP:
		//cout << LOWORD(lParam) << " " << HIWORD(lParam) << endl;
		//gGameFramework.OnProcessingWindowMessage(hWnd, message, wParam, lParam);
		CGameFramework::GetInstance().OnProcessingWindowMessage(hWnd, message, wParam, lParam);
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		switch (wmId)
		{
		case IDM_ABOUT:
			::DialogBox(ghAppInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case ID_GAMEOBJECT_CUBE: // 큐브를 만든다
			//gGameFramework.OnProcessingMenuMessage(hWnd, message, wParam, lParam);
			CGameFramework::GetInstance().OnProcessingMenuMessage(hWnd, message, wParam, lParam);
			break;
		case IDM_EXIT:
			::DestroyWindow(hWnd);
			break;
		default:
			return(::DefWindowProc(hWnd, message, wParam, lParam));
		}
		break;
	case WM_PAINT:
		hdc = ::BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		::PostQuitMessage(0);
		break;
	default:
		return(::DefWindowProc(hWnd, message, wParam, lParam));
	}
	return 0;
}

// 다음은 윈도우 클래스를 등록할 때 설정한 윈도우 프로시져 함수이다
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	static bool g_flag = false;
	switch (message)
	{
	case WM_SIZE:
	{
		if (wParam != SIZE_MINIMIZED) {
			//gGameFramework.ResizeAllWindows(0, 0);
			CGameFramework::GetInstance().ResizeAllWindows(0, 0);


		}
		break;
	}
	case WM_KEYDOWN:
	case WM_KEYUP:
		//gGameFramework.OnProcessingWindowMessage(hWnd, message, wParam, lParam);
		CGameFramework::GetInstance().OnProcessingWindowMessage(hWnd, message, wParam, lParam);
			break;
		case WM_COMMAND:
			wmId = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case IDM_ABOUT:
					::DialogBox(ghAppInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
					break;
				case ID_GAMEOBJECT_SPHERE:
				case ID_GAMEOBJECT_CUBE: // 큐브를 만든다
				case ID_GAMEOBJECT_PLANE:
				case ID_GAMEOBJECT_PILLAR:
				case ID_GAMEOBJECT_Terrain:
					//gGameFramework.OnProcessingMenuMessage(hWnd, message, wParam, lParam);
					CGameFramework::GetInstance().OnProcessingMenuMessage(hWnd, message, wParam, lParam);
					break;
				case IDM_EXIT:
					::DestroyWindow(hWnd);
					break;
				default:
					return(::DefWindowProc(hWnd, message, wParam, lParam));
			}
			break;
		case WM_DESTROY:
			::PostQuitMessage(0);
			break;
		default:
			return(::DefWindowProc(hWnd, message, wParam, lParam));
	}
	return 0;
}

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
		case WM_INITDIALOG:
			return((INT_PTR)TRUE);
		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
			{
				::EndDialog(hDlg, LOWORD(wParam));
				return((INT_PTR)TRUE);
			}
			break;
	}
	return((INT_PTR)FALSE);
}
