#include "Transform.h"
#include "Shader.h"

_component_identifier<CTransform, CComponent, true> CTransform::identifier;

CTransform::CTransform()
{
	XMStoreFloat4x4(&m_xmmtxWorld, XMMatrixIdentity());
	m_xmf3Scale.x = 1.0f;
	m_xmf3Scale.y = 1.0f;
	m_xmf3Scale.z = 1.0f;
	m_xmf4Quaternion.x = 0, m_xmf4Quaternion.y = 0, m_xmf4Quaternion.z = 0, m_xmf4Quaternion.w = 1;
	m_xmf3Pos.x = m_xmf3Pos.y = m_xmf3Pos.z = 0;
	m_xmf3Degree.x = m_xmf3Degree.y = m_xmf3Degree.z = 0.0f;
}


CTransform::~CTransform()
{
}
void CTransform::Initalize(ID3D11Device *pd3dDevice)
{
}
void CTransform::Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	UpdateTransform();
	CShader::UpdateShaderVariable(pd3dDeviceContext, &m_xmmtxWorld);
}
void CTransform::SetPosition(float x, float y, float z)
{
	m_xmmtxWorld._41 = x;
	m_xmmtxWorld._42 = y;
	m_xmmtxWorld._43 = z;
	//UpdateTransform();
}
void CTransform::ScaleFor(float fScale, int nIndex)
{
	XMMATRIX xmmtxScale;

	switch (nIndex)
	{
	case TRANSFORM::X:
	{
		m_xmf3Scale.x = fScale;

		break;
	}
	case TRANSFORM::Y:
	{
		m_xmf3Scale.y = fScale;


		break;
	}
	case TRANSFORM::Z:
	{
		m_xmf3Scale.z = fScale;

		break;
	}
	}
	UpdateTransform();

}

void  CTransform::FromEulerAngles(XMFLOAT3 xmf3Angles)
{
	float fPitch = XMConvertToRadians(xmf3Angles.x);
	float fYaw = XMConvertToRadians(xmf3Angles.y);
	float fRoll = XMConvertToRadians(xmf3Angles.z);

	float fHalfRoll = fRoll * 0.5f;
	float fHalfPitch = fPitch * 0.5f;
	float fHalfYaw = fYaw * 0.5f;

	float fSinRoll = sin(fHalfRoll);
	float fCosRoll = cos(fHalfRoll);

	float fSinPitch = sin(fHalfPitch);
	float fCosPitch = cos(fHalfPitch);

	float fSinYaw = sin(fHalfYaw);
	float fCosYaw = cos(fHalfYaw);

	m_xmf4Quaternion =
	{ fCosYaw * fSinPitch * fCosRoll + fSinYaw * fCosPitch * fSinRoll,
		fSinYaw * fCosPitch * fCosRoll - fCosYaw * fSinPitch * fSinRoll,
		fCosYaw * fCosPitch * fSinRoll - fSinYaw * fSinPitch * fCosRoll,
		fCosYaw * fCosPitch * fCosRoll + fSinYaw * fSinPitch * fSinRoll };


	UpdateTransform();
}
XMFLOAT3 CTransform::ToEulerAngles()
{
	XMFLOAT4 xmf4EulerAngles;

	float fCheck = 2.0f * (-m_xmf4Quaternion.y * m_xmf4Quaternion.z + m_xmf4Quaternion.w * m_xmf4Quaternion.x);
	if (fCheck < -0.995f)
	{
		return XMFLOAT3
		(
			-90.0f,
			0.0f,
			XMConvertToDegrees(-atan2f(2.0f * (m_xmf4Quaternion.x * m_xmf4Quaternion.z - m_xmf4Quaternion.w * m_xmf4Quaternion.y),
				1.0f - 2.0f * (m_xmf4Quaternion.y *m_xmf4Quaternion.y + m_xmf4Quaternion.z * m_xmf4Quaternion.z)))
		);
	}
	if (fCheck > 0.995f)
	{
		return XMFLOAT3
		(
			90.0f,
			0.0f,
			XMConvertToDegrees(atan2f(2.0f * (m_xmf4Quaternion.x * m_xmf4Quaternion.z - m_xmf4Quaternion.w * m_xmf4Quaternion.y), 1.0f - 2.0f * (m_xmf4Quaternion.y * m_xmf4Quaternion.y + m_xmf4Quaternion.z * m_xmf4Quaternion.z)))
		);
	}
	return XMFLOAT3
	(
		XMConvertToDegrees(asinf(fCheck)),
		XMConvertToDegrees(atan2f(2.0f * (m_xmf4Quaternion.x * m_xmf4Quaternion.z + m_xmf4Quaternion.w * m_xmf4Quaternion.y),
			1.0f - 2.0f * (m_xmf4Quaternion.x * m_xmf4Quaternion.x + m_xmf4Quaternion.y * m_xmf4Quaternion.y))),
		XMConvertToDegrees(atan2f(2.0f * (m_xmf4Quaternion.x * m_xmf4Quaternion.y + m_xmf4Quaternion.w * m_xmf4Quaternion.z),
			1.0f - 2.0f * (m_xmf4Quaternion.x * m_xmf4Quaternion.x + m_xmf4Quaternion.z * m_xmf4Quaternion.z)))
	);
}
XMFLOAT4X4 CTransform::CreateRotation(XMFLOAT4 rotation)
{
	float num9 = rotation.x * rotation.x;
	float num8 = rotation.y * rotation.y;
	float num7 = rotation.z * rotation.z;
	float num6 = rotation.x * rotation.y;
	float num5 = rotation.z * rotation.w;
	float num4 = rotation.z * rotation.x;
	float num3 = rotation.y * rotation.w;
	float num2 = rotation.y * rotation.z;
	float num = rotation.x * rotation.w;

	return XMFLOAT4X4(
		1.0f - (2.0f * (num8 + num7)),
		2.0f * (num6 + num5),
		2.0f * (num4 - num3),
		0.0f,
		2.0f * (num6 - num5),
		1.0f - (2.0f * (num7 + num9)),
		2.0f * (num2 + num),
		0.0f,
		2.0f * (num4 + num3),
		2.0f * (num2 - num),
		1.0f - (2.0f * (num8 + num9)),
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		1.0f
	);
}
void CTransform::UpdateTransform()
{
	XMFLOAT4X4 mRotation = CreateRotation(m_xmf4Quaternion);

	//XMFLOAT4X4 mRotation;
	//XMMATRIX xmmtxRotation = XMMatrixRotationQuaternion(XMLoadFloat4(&m_xmf4Quaternion));
	//XMStoreFloat4x4(&mRotation, xmmtxRotation);

	m_xmmtxWorld._11 = m_xmf3Scale.x * mRotation._11;
	m_xmmtxWorld._12 = m_xmf3Scale.x * mRotation._12;
	m_xmmtxWorld._13 = m_xmf3Scale.x * mRotation._13;
	m_xmmtxWorld._14 = 0.0f;


	m_xmmtxWorld._21 = m_xmf3Scale.y * mRotation._21;
	m_xmmtxWorld._22 = m_xmf3Scale.y * mRotation._22;
	m_xmmtxWorld._23 = m_xmf3Scale.y * mRotation._23;
	m_xmmtxWorld._24 = 0.0f;

	m_xmmtxWorld._31 = m_xmf3Scale.z * mRotation._31;
	m_xmmtxWorld._32 = m_xmf3Scale.z * mRotation._32;
	m_xmmtxWorld._33 = m_xmf3Scale.z * mRotation._33;
	m_xmmtxWorld._34 = 0.0f;

	m_xmmtxWorld._41 = m_xmf3Pos.x;
	m_xmmtxWorld._42 = m_xmf3Pos.y;
	m_xmmtxWorld._43 = m_xmf3Pos.z;


	m_xmmtxWorld._44 = 1.0f;
}
void CTransform::OnPrepareRender(XMFLOAT3& xmf3Right, XMFLOAT3& xmf3Up, XMFLOAT3& xmf3Look, XMFLOAT3& xmf3Position)
{
	m_xmmtxWorld._11 = xmf3Right.x;
	m_xmmtxWorld._12 = xmf3Right.y;
	m_xmmtxWorld._13 = xmf3Right.z;
	m_xmmtxWorld._21 = xmf3Up.x;
	m_xmmtxWorld._22 = xmf3Up.y;
	m_xmmtxWorld._23 = xmf3Up.z;
	m_xmmtxWorld._31 = xmf3Look.x;
	m_xmmtxWorld._32 = xmf3Look.y;
	m_xmmtxWorld._33 = xmf3Look.z;
	m_xmmtxWorld._41 = xmf3Position.x;
	m_xmmtxWorld._42 = xmf3Position.y;
	m_xmmtxWorld._43 = xmf3Position.z;
}


void CTransform::Rotate(float fPitch, float fYaw, float fRoll)
{
	//게임 객체를 주어진 각도로 회전한다.
	XMMATRIX xmmtxRotate;
	//XMRotationYawPitchRoll(&mtxRotate, (float)XMConvertToRadians(fYaw), (float)XMConvertToRadians(fPitch), (float)XMConvertToRadians(fRoll));
	xmmtxRotate = XMMatrixRotationRollPitchYaw((float)XMConvertToRadians(fPitch), (float)XMConvertToRadians(fYaw), (float)XMConvertToRadians(fRoll));

	//XMMATRIX xmmtxWorld = XMLoadFloat4x4(&m_xmmtxWorld);
	XMMATRIX xmmtxWorld = XMLoadFloat4x4(&m_xmmtxWorld);
	xmmtxWorld = xmmtxRotate * xmmtxWorld;
	//XMStoreFloat4x4(&m_xmmtxWorld, xmmtxWorld);

	XMStoreFloat4x4(&m_xmmtxWorld, xmmtxWorld);

}
void CTransform::Rotate(XMFLOAT3 *pd3dxvAxis, float fAngle)
{
	//게임 객체를 주어진 회전축을 중심으로 회전한다.
	XMMATRIX xmmtxRotate;
	FXMVECTOR xmvAxis = XMLoadFloat3(pd3dxvAxis);
	xmmtxRotate = XMMatrixRotationAxis(xmvAxis, XMConvertToRadians(fAngle));
	
	XMMATRIX xmmtxWorld = XMLoadFloat4x4(&m_xmmtxWorld);

	xmmtxWorld = xmmtxRotate * xmmtxWorld;
	XMStoreFloat4x4(&m_xmmtxWorld, xmmtxWorld);

}

XMFLOAT3 CTransform::GetLookAt()
{
	//게임 객체를 로컬 z-축 벡터를 반환한다.
	XMFLOAT3 xmf3LookAt = XMFLOAT3(m_xmmtxWorld._31, m_xmmtxWorld._32, m_xmmtxWorld._33);
	XMVECTOR xmvLookAt = XMVector3Normalize(XMLoadFloat3(&xmf3LookAt));
	XMStoreFloat3(&xmf3LookAt, xmvLookAt);

	return(xmf3LookAt);
}
XMFLOAT3 CTransform::GetUp()
{
	//게임 객체를 로컬 y-축 벡터를 반환한다.
	XMFLOAT3 xmf3Up = XMFLOAT3(m_xmmtxWorld._21, m_xmmtxWorld._22, m_xmmtxWorld._23);
	XMVECTOR xmvUp = XMVector3Normalize(XMLoadFloat3(&xmf3Up));
	XMStoreFloat3(&xmf3Up, xmvUp);
	return(xmf3Up);
}
XMFLOAT3 CTransform::GetRight()
{
	//게임 객체를 로컬 x-축 벡터를 반환한다.
	//XMFLOAT3 xmf3Right(m_xmmtxWorld._11, m_xmmtxWorld._12, m_xmmtxWorld._13);
	XMFLOAT3 xmf3Right = XMFLOAT3(m_xmmtxWorld._11, m_xmmtxWorld._12, m_xmmtxWorld._13);
	XMVECTOR xmvRight = XMVector3Normalize(XMLoadFloat3(&xmf3Right));
	XMStoreFloat3(&xmf3Right, xmvRight);
	return(xmf3Right);
}

void CTransform::MoveStrafe(float fDistance)
{
	//게임 객체를 로컬 x-축 방향으로 이동한다.
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Right = GetRight();
	XMVECTOR xmvPosition = XMLoadFloat3(&xmf3Position);
	XMVECTOR xmvRight = XMLoadFloat3(&xmf3Right);


	xmvPosition += fDistance * xmvRight;
	XMStoreFloat3(&xmf3Position, xmvPosition);
	SetPosition(xmf3Position);
}
void CTransform::MoveUp(float fDistance)
{
	//게임 객체를 로컬 y-축 방향으로 이동한다.
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Up = GetUp();
	XMVECTOR xmvPosition = XMLoadFloat3(&xmf3Position);
	XMVECTOR xmvUp = XMLoadFloat3(&xmf3Up);

	//d3dxvPosition += fDistance * d3dxvUp;
	xmvPosition += fDistance * xmvUp;
	XMStoreFloat3(&xmf3Position, xmvPosition);
	SetPosition(xmf3Position);
}
void CTransform::MoveForward(float fDistance)
{
	//게임 객체를 로컬 z-축 방향으로 이동한다.
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3LookAt = GetLookAt();
	XMVECTOR xmvPosition = XMLoadFloat3(&xmf3Position);
	XMVECTOR xmvLookAt = XMLoadFloat3(&xmf3LookAt);


	xmvPosition += fDistance * xmvLookAt;
	XMStoreFloat3(&xmf3Position, xmvPosition);
	SetPosition(xmf3Position);
}