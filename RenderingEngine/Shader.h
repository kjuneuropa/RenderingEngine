#pragma once
#include "stdafx.h"
#include "Object.h"
#include "Camera.h"


#include "CubeMesh.h"
#include "CRotatingObject.h"
#include "Terrain.h"
#include "Player.h"


struct VS_CB_WORLD_MATRIX
{
	XMMATRIX m_xmmtxWorld;
};


struct VS_VB_INSTANCE
{
	XMMATRIX m_d3dxTransform;
	XMFLOAT4 m_d3dxColor;
};

class CShader
{
public:
	CShader();
	virtual ~CShader();

	// 순수 가상함수 
	virtual void BuildObjects(ID3D11Device *pd3dDevice) = 0;
	virtual void CreateShader(ID3D11Device *pd3dDevice) = 0;
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera = nullptr) = 0;
	
	virtual void AnimateObjects(float fTimeElapsed);
	virtual void ReleaseObjects();
	virtual void OnPrepareRender(ID3D11DeviceContext *pd3dDeviceContext);
	virtual void AddTerrainObject(ID3D11Device* pd3dDevice) {}


	
	void CreateVertexShaderFromFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderModel, ID3D11VertexShader **ppd3dVertexShader, D3D11_INPUT_ELEMENT_DESC *pd3dInputLayout, UINT nElements, ID3D11InputLayout **ppd3dVertexLayout);
	void CreatePixelShaderFromFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderModel, ID3D11PixelShader **ppd3dPixelShader);
	
	int GetShaderType() { return m_nShaderType; }
	void RemoveObject(CGameObject* pSelectedObect);
	bool FindObject(int nObjectType);
	void AddObject(CGameObject* pGameObject) { m_vObjects.push_back(pGameObject); }


	static void CreateMesh(ID3D11Device *pd3dDevice);
	static void CreateShaderVariables(ID3D11Device *pd3dDevice);
	static void ReleaseShaderVariables();
	static void UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, XMFLOAT4X4 *pd3dxmtxWorld);

	static CTexture* CreateTexture(ID3D11Device* pd3dDevice);
	static CMaterial* CreateMaterial();
	static CMesh* GetMesh(int nObjectIndex) { return m_vMeshes[nObjectIndex]; }

public:
	virtual CGameObject *PickObjectByRayIntersection(XMFLOAT3 *pd3dxvPickPosition, XMMATRIX *pd3dxmtxView, MESHINTERSECTINFO *pd3dxIntersectInfo);


// 클래스 내부 접근에 대해서만 한정함.
protected:
	ID3D11VertexShader*& GetVertexShader() { return m_pd3dVertexShader; }
	ID3D11PixelShader*& GetPixelShader() { return m_pd3dPixelShader; }
	ID3D11InputLayout*& GetInputLayout() { return m_pd3dVertexLayout; }
	CGameObject* GetGameObject(int nIndex) { return m_vObjects[nIndex]; }
private:
	ID3D11VertexShader *m_pd3dVertexShader;
	ID3D11PixelShader *m_pd3dPixelShader;
	ID3D11InputLayout *m_pd3dVertexLayout;

	vector<CGameObject*> m_vObjects;
	int m_nShaderType;
	bool m_bActive;

	static ID3D11ShaderResourceView* m_d3dDefaultTexture;
	static vector<CMesh*> m_vMeshes;


	//월드 변환 행렬을 위한 상수 버퍼는 하나만 있어도 되므로 정적 멤버로 선언한다.
	static ID3D11Buffer *m_pd3dcbWorldMatrix;
};
class CDiffusedShader : public CShader
{
public:
	CDiffusedShader();
	virtual ~CDiffusedShader();
	virtual void CreateShader(ID3D11Device *pd3dDevice);
};


class CSceneShader : public CShader
{
public:
	CSceneShader();
	virtual ~CSceneShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
	virtual void BuildObjects(ID3D11Device *pd3dDevice);
};

//플레이어를 렌더링하기 위한 쉐이더 클래스이다.
class CPlayerShader : public CDiffusedShader
{
public:
	CPlayerShader();
	virtual ~CPlayerShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera = nullptr);

	CPlayer *GetPlayer(int nIndex = 0) { return((CPlayer *)GetGameObject(0)->GetComponent<CPlayer>()); }
};
class CIlluminatedShader : public CShader
{
public:
	CIlluminatedShader();
	virtual ~CIlluminatedShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);

	//재질을 설정하기 위한 상수 버퍼이다.
	static ID3D11Buffer	 *m_pd3dcbMaterial;

	static void CreateShaderVariables(ID3D11Device *pd3dDevice);
	static void ReleaseShaderVariables();
	//재질을 쉐이더 변수에 설정(연결)하기 위한 함수이다.
	static void UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, MATERIAL *pMaterial);
};



class CTexturedShader : public CShader
{
public:
	CTexturedShader();
	virtual ~CTexturedShader();

	virtual void BuildObjects(ID3D11Device *pd3dDevice, CTexture *pTexture);
	virtual void CreateShader(ID3D11Device *pd3dDevice);

};


class CInstancingShader : public CTexturedShader
{
public:
	CInstancingShader();
	virtual ~CInstancingShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);

public:
	virtual void BuildObjects(ID3D11Device *pd3dDevice, CTerrain *pHeightMapTerrain, CMaterial *pMaterial, CTexture *pTexture, int k);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);

	//다음 멤버변수를 추가한다.
private:
	CTexture *m_pTexture;
	CMaterial *m_pMaterial;
	UINT m_nInstanceBufferStride;
	UINT m_nInstanceBufferOffset;

	ID3D11Buffer *m_pd3dCubeInstanceBuffer;
	ID3D11Buffer *m_pd3dSphereInstanceBuffer;

public:
	ID3D11Buffer *CreateInstanceBuffer(ID3D11Device *pd3dDevice, int nObjects, UINT nBufferStride, void *pBufferData);
};


class CDetailTexturedShader : public CTexturedShader
{
public:
	CDetailTexturedShader();
	virtual ~CDetailTexturedShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
};


class CSkyBoxShader : public CTexturedShader
{
public:
	CSkyBoxShader();
	virtual ~CSkyBoxShader();
	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
	virtual void CreateShader(ID3D11Device *pd3dDevice);
};



class CTexturedIlluminatedShader : public CIlluminatedShader
{
private:
	CTexture *m_pTexture;
	CMaterial *m_pMaterial;
public:
	CTexturedIlluminatedShader();
	virtual ~CTexturedIlluminatedShader();
	virtual void CreateShader(ID3D11Device *pd3dDevice);
	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);

};


class CNormalMapShader : public  CTexturedIlluminatedShader
{
private:

public:
	CNormalMapShader();
	virtual ~CNormalMapShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void AddObject(int nIndex);
	//virtual void AddObject(CGameObject* pSelectedObject);
};

class CDetailTexturedIlluminatedShader : public CTexturedIlluminatedShader
{
public:
	CDetailTexturedIlluminatedShader();
	virtual ~CDetailTexturedIlluminatedShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
};

class CTerrainShader : public CDetailTexturedIlluminatedShader
{
public:
	CTerrainShader();
	virtual ~CTerrainShader();

	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void AddTerrainObject(ID3D11Device* pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
	//CHeightMapTerrain *GetTerrain();
	CTerrain* GetTerrain();
public:

	bool CreateTeraain(ID3D11Device *pd3dDevice);
	ID3D11Texture2D* m_texture;
};


class CRenderToTextureShader : public CShader
{
private:
	CScreenQuadMesh *m_pQuadMesh;
public:
	CRenderToTextureShader();
	virtual ~CRenderToTextureShader();

	virtual void BuildObjects(ID3D11Device *pd3dDevice);//, ID3D11ShaderResourceView* pd3dsrvRTT);
	virtual void CreateShader(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
};

class CAlphaBlendingShader : public CTexturedShader
{
public:
	CAlphaBlendingShader();
	virtual ~CAlphaBlendingShader();

	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void CreateShader(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);

	
protected:
	float m_fTime;
};

