#include "AirplaneMesh.h"


CAirplaneMeshDiffused::CAirplaneMeshDiffused(ID3D11Device *pd3dDevice, float gScreenWidth, float gScreenHeight, float fDepth, XMFLOAT4 d3dxColor) : CMeshDiffused(pd3dDevice)
{
	m_nVertices = 24 * 3;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float fx = gScreenWidth*0.5f, fy = gScreenHeight*0.5f, fz = fDepth*0.5f;

	//비행기 메쉬는 2개의 정점 버퍼(위치 벡터 버퍼와 색상 버퍼)로 구성된다.
	//XMFLOAT3 m_vPositions[24 * 3];

	m_vPositions.resize(m_nVertices);

	float x1 = fx * 0.2f, y1 = fy * 0.2f, x2 = fx * 0.1f, y3 = fy * 0.3f, y2 = ((y1 - (fy - y3)) / x1) * x2 + (fy - y3);
	int i = 0;
	//Upper Plane
	m_vPositions[i++] = XMFLOAT3(0.0f, +(fy + y3), -fz);
	m_vPositions[i++] = XMFLOAT3(+x1, -y1, -fz);
	m_vPositions[i++] = XMFLOAT3(0.0f, 0.0f, -fz);

	m_vPositions[i++] = XMFLOAT3(0.0f, +(fy + y3), -fz);
	m_vPositions[i++] = XMFLOAT3(0.0f, 0.0f, -fz);
	m_vPositions[i++] = XMFLOAT3(-x1, -y1, -fz);

	m_vPositions[i++] = XMFLOAT3(+x2, +y2, -fz);
	m_vPositions[i++] = XMFLOAT3(+fx, -y3, -fz);
	m_vPositions[i++] = XMFLOAT3(+x1, -y1, -fz);

	m_vPositions[i++] = XMFLOAT3(-x2, +y2, -fz);
	m_vPositions[i++] = XMFLOAT3(-x1, -y1, -fz);
	m_vPositions[i++] = XMFLOAT3(-fx, -y3, -fz);

	//Lower Plane
	m_vPositions[i++] = XMFLOAT3(0.0f, +(fy + y3), +fz);
	m_vPositions[i++] = XMFLOAT3(0.0f, 0.0f, +fz);
	m_vPositions[i++] = XMFLOAT3(+x1, -y1, +fz);

	m_vPositions[i++] = XMFLOAT3(0.0f, +(fy + y3), +fz);
	m_vPositions[i++] = XMFLOAT3(-x1, -y1, +fz);
	m_vPositions[i++] = XMFLOAT3(0.0f, 0.0f, +fz);

	m_vPositions[i++] = XMFLOAT3(+x2, +y2, +fz);
	m_vPositions[i++] = XMFLOAT3(+x1, -y1, +fz);
	m_vPositions[i++] = XMFLOAT3(+fx, -y3, +fz);

	m_vPositions[i++] = XMFLOAT3(-x2, +y2, +fz);
	m_vPositions[i++] = XMFLOAT3(-fx, -y3, +fz);
	m_vPositions[i++] = XMFLOAT3(-x1, -y1, +fz);

	//Right Plane
	m_vPositions[i++] = XMFLOAT3(0.0f, +(fy + y3), -fz);
	m_vPositions[i++] = XMFLOAT3(0.0f, +(fy + y3), +fz);
	m_vPositions[i++] = XMFLOAT3(+x2, +y2, -fz);

	m_vPositions[i++] = XMFLOAT3(+x2, +y2, -fz);
	m_vPositions[i++] = XMFLOAT3(0.0f, +(fy + y3), +fz);
	m_vPositions[i++] = XMFLOAT3(+x2, +y2, +fz);

	m_vPositions[i++] = XMFLOAT3(+x2, +y2, -fz);
	m_vPositions[i++] = XMFLOAT3(+x2, +y2, +fz);
	m_vPositions[i++] = XMFLOAT3(+fx, -y3, -fz);

	m_vPositions[i++] = XMFLOAT3(+fx, -y3, -fz);
	m_vPositions[i++] = XMFLOAT3(+x2, +y2, +fz);
	m_vPositions[i++] = XMFLOAT3(+fx, -y3, +fz);

	//Back/Right Plane
	m_vPositions[i++] = XMFLOAT3(+x1, -y1, -fz);
	m_vPositions[i++] = XMFLOAT3(+fx, -y3, -fz);
	m_vPositions[i++] = XMFLOAT3(+fx, -y3, +fz);

	m_vPositions[i++] = XMFLOAT3(+x1, -y1, -fz);
	m_vPositions[i++] = XMFLOAT3(+fx, -y3, +fz);
	m_vPositions[i++] = XMFLOAT3(+x1, -y1, +fz);

	m_vPositions[i++] = XMFLOAT3(0.0f, 0.0f, -fz);
	m_vPositions[i++] = XMFLOAT3(+x1, -y1, -fz);
	m_vPositions[i++] = XMFLOAT3(+x1, -y1, +fz);

	m_vPositions[i++] = XMFLOAT3(0.0f, 0.0f, -fz);
	m_vPositions[i++] = XMFLOAT3(+x1, -y1, +fz);
	m_vPositions[i++] = XMFLOAT3(0.0f, 0.0f, +fz);

	//Left Plane
	m_vPositions[i++] = XMFLOAT3(0.0f, +(fy + y3), +fz);
	m_vPositions[i++] = XMFLOAT3(0.0f, +(fy + y3), -fz);
	m_vPositions[i++] = XMFLOAT3(-x2, +y2, -fz);

	m_vPositions[i++] = XMFLOAT3(0.0f, +(fy + y3), +fz);
	m_vPositions[i++] = XMFLOAT3(-x2, +y2, -fz);
	m_vPositions[i++] = XMFLOAT3(-x2, +y2, +fz);

	m_vPositions[i++] = XMFLOAT3(-x2, +y2, +fz);
	m_vPositions[i++] = XMFLOAT3(-x2, +y2, -fz);
	m_vPositions[i++] = XMFLOAT3(-fx, -y3, -fz);

	m_vPositions[i++] = XMFLOAT3(-x2, +y2, +fz);
	m_vPositions[i++] = XMFLOAT3(-fx, -y3, -fz);
	m_vPositions[i++] = XMFLOAT3(-fx, -y3, +fz);

	//Back/Left Plane
	m_vPositions[i++] = XMFLOAT3(0.0f, 0.0f, -fz);
	m_vPositions[i++] = XMFLOAT3(0.0f, 0.0f, +fz);
	m_vPositions[i++] = XMFLOAT3(-x1, -y1, +fz);

	m_vPositions[i++] = XMFLOAT3(0.0f, 0.0f, -fz);
	m_vPositions[i++] = XMFLOAT3(-x1, -y1, +fz);
	m_vPositions[i++] = XMFLOAT3(-x1, -y1, -fz);

	m_vPositions[i++] = XMFLOAT3(-x1, -y1, -fz);
	m_vPositions[i++] = XMFLOAT3(-x1, -y1, +fz);
	m_vPositions[i++] = XMFLOAT3(-fx, -y3, +fz);

	m_vPositions[i++] = XMFLOAT3(-x1, -y1, -fz);
	m_vPositions[i++] = XMFLOAT3(-fx, -y3, +fz);
	m_vPositions[i++] = XMFLOAT3(-fx, -y3, -fz);

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	XMFLOAT4 pd3dxColors[24 * 3];
	for (int j = 0; j < m_nVertices; j++) pd3dxColors[j] = d3dxColor; //+RANDOM_COLOR;
	pd3dxColors[0] = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	pd3dxColors[3] = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	pd3dxColors[6] = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
	pd3dxColors[9] = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
	pd3dxColors[12] = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	pd3dxColors[15] = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	pd3dxColors[18] = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
	pd3dxColors[21] = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT4) * m_nVertices;
	d3dBufferData.pSysMem = pd3dxColors;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dColorBuffer);

	ID3D11Buffer *ppd3dBuffers[2] = { m_pd3dPositionBuffer, m_pd3dColorBuffer };
	UINT pnBufferStrides[2] = { sizeof(XMFLOAT3), sizeof(XMFLOAT4) };
	UINT pnBufferOffsets[2] = { 0, 0 };
	AssembleToVertexBuffer(2, ppd3dBuffers, pnBufferStrides, pnBufferOffsets);

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fx, -fy, -fz);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fx, +fy, +fz);
}


CAirplaneMeshDiffused::~CAirplaneMeshDiffused()
{
}


