#pragma once
#include "stdafx.h"
#include "Component.h"
#include "Renderer.h"

class CSkyBox : public CRenderer
{
	//public:
	//	typedef _component_identifier<CSkyBox1, CComponent, true> component_identifier_t;
	//private:
	//	static component_identifier_t identifier;
public:
	//DEFINE_COMPONENT(CSkyBox1, CComponent, true);
	CSkyBox();
	~CSkyBox();
	void Initalize(ID3D11Device *pd3dDevice);
	virtual void Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera) override;

	//void LoadSkyBox(ID3D11Device *pd3dDevice, wstring& strSkyBox);
private:
	CTexture* m_pTexture;
};
