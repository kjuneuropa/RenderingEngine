#include "Terrain.h"
#include "Shader.h"


_component_identifier<CTerrain, CRenderer, false> CTerrain::identifier;


CTerrain::CTerrain()
{

}


CTerrain::~CTerrain()
{
}

void CTerrain::Initalize(ID3D11Device *pd3dDevice, wstring& strHeightMap)
{

	/*m_pMaterial = new CMaterial();
	m_pMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	m_pMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(0.9f, 0.9f, 0.9f, 1.0f);
	m_pMaterial->m_Material.m_xmf4Specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f);
	m_pMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);*/



	




	//m_nObjectType = 0; //����
	m_nWidth = 129;
	m_nLength = 129;

	int nBlockWidth = 129;
	int nBlockLength = 129;

	int cxQuadsPerBlock = 129 - 1;
	int czQuadsPerBlock = 129 - 1;

	m_xmvScale = XMFLOAT3(2.0f, 2.0f, 2.0f);

	m_pHeightMap = new CHeightMap(strHeightMap.c_str(), m_nWidth, m_nLength, m_xmvScale);

	long cxBlocks = (m_nWidth - 1) / cxQuadsPerBlock;
	long czBlocks = (m_nLength - 1) / czQuadsPerBlock;
	m_nMeshes = cxBlocks * czBlocks;
	m_ppMeshes = new CMesh*[m_nMeshes];
	for (int i = 0; i < m_nMeshes; i++)m_ppMeshes[i] = nullptr;

	CHeightMapGridMesh *pHeightMapGridMesh = nullptr;

	//m_pParentNode = new QuadTreeNode;

	int nTreeDepth = log2(cxBlocks*czBlocks) / 2;



	//CreateTreeNode(pd3dDevice, m_pParentNode, 0, 0, m_nWidth -1, nBlockWidth, nTreeDepth);

	// Ŀ�Կ�


	for (int z = 0, zStart = 0; z < czBlocks; z++)
	{
		for (int x = 0, xStart = 0; x < cxBlocks; x++)
		{
			xStart = x * (nBlockWidth - 1);
			zStart = z * (nBlockLength - 1);
			pHeightMapGridMesh = new CHeightMapGridMesh(pd3dDevice, xStart, zStart, nBlockWidth, nBlockLength, m_xmvScale, m_pHeightMap);
			SetMesh(pHeightMapGridMesh, x + (z*cxBlocks));
		}
	}
}

void CTerrain::Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{

	UpdateMaterial(pd3dDeviceContext);
	UpdateTexture(pd3dDeviceContext);
	Render(pd3dDeviceContext, pCamera);
}