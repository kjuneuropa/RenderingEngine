#include "TextureBlendingMesh.h"


CTextureBlendingMesh::CTextureBlendingMesh(ID3D11Device *pd3dDevice, float gScreenWidth, float gScreenHeight, float fDepth) :CMesh(pd3dDevice)
{
	m_pd3dTexCoordBuffer1 = nullptr;
	m_pd3dTexCoordBuffer2 = nullptr;

	m_pBlendingTexture= nullptr;

	m_nVertices = 36;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float fx = gScreenWidth*0.5f, fy = gScreenHeight*0.5f, fz = fDepth*0.5f;

	m_vPositions.resize(m_nVertices);
	//생략된 부분은 LabProject13-2의 CCubeMeshTextured 클래스의 생성자 부분과 동일하다.
	XMFLOAT2 pxmf2TexCoords1[36];
	XMFLOAT2 pxmf2TexCoords2[36];
	int i = 0;

	//직육면체의 각 면(삼각형 2개)에 하나의 텍스쳐 이미지 전체가 맵핑되도록 텍스쳐 좌표를 설정한다.
	//
	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 0.0f);
	pxmf2TexCoords1[i++] = XMFLOAT2(1.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);
	//==========================================
	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 1.0f);
	//==========================================

	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);
	//==========================================
	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 1.0f);
	//==========================================
	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);
	//==========================================
	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 1.0f);
	//==========================================
	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);
	//==========================================
	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 1.0f);
	//==========================================
	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);
	//==========================================
	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 1.0f);
	//==========================================
	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);
	//==========================================
	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 0.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords1[i] = XMFLOAT2(1.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords1[i] = XMFLOAT2(0.0f, 1.0f);
	pxmf2TexCoords2[i++] = XMFLOAT2(0.0f, 1.0f);
	//==========================================

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);


	//첫번째 텍스쳐
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * m_nVertices;
	d3dBufferData.pSysMem = pxmf2TexCoords1;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer1);

	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer2);


	//정점은 위치 벡터, 법선 벡터, 텍스쳐 좌표를 갖는다.
	ID3D11Buffer *pd3dBuffers[3] = { m_pd3dPositionBuffer, m_pd3dTexCoordBuffer1, m_pd3dTexCoordBuffer2 };
	UINT pnBufferStrides[3] = { sizeof(XMFLOAT3), sizeof(XMFLOAT2), sizeof(XMFLOAT2) };
	UINT pnBufferOffsets[3] = { 0, 0, 0 };
	AssembleToVertexBuffer(3, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fx, -fy, -fz);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fx, +fy, +fz);
}


void CTextureBlendingMesh::Render(ID3D11DeviceContext *pd3dDeviceContext)
{
	//================================



	//==============================
	pd3dDeviceContext->RSSetState(m_pd3dRasterizerState);
	pd3dDeviceContext->IASetVertexBuffers(m_nSlot, m_nBuffers, m_ppd3dVertexBuffers, m_pnVertexStrides, m_pnVertexOffsets);

	pd3dDeviceContext->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	//스카이 박스를 그리기 위한 샘플러 상태 객체와 깊이 스텐실 상태 객체를 설정한다.
	//m_pWaterTexture->UpdateSamplerShaderVariable(pd3dDeviceContext, 0, 0);


	// 깊이 버퍼 관련
	//pd3dDeviceContext->OMSetDepthStencilState(m_pd3dDepthStencilState, 1);



	/*if (m_pBlendingTexture)
		m_pBlendingTexture->UpdateShaderVariable(pd3dDeviceContext);*/
	pd3dDeviceContext->Draw(m_nVertices, 0);

	pd3dDeviceContext->OMSetDepthStencilState(nullptr, 1);
}

CTextureBlendingMesh::~CTextureBlendingMesh()
{
	if (m_pd3dTexCoordBuffer1) m_pd3dTexCoordBuffer1->Release();
	if (m_pd3dTexCoordBuffer2) m_pd3dTexCoordBuffer2->Release();
}
