#pragma once
#include "Mesh.h"
#include "DiffusedVertex.h"
#include "stdafx.h"
class CCubeMeshDiffused : public CMeshDiffused
{
public:
	CCubeMeshDiffused(ID3D11Device *pd3dDevice, float gScreenWidth = 2.0f, float gScreenHeight = 2.0f, float fDepth = 2.0f, XMFLOAT4 d3dxColor = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f));
	virtual ~CCubeMeshDiffused();
};
