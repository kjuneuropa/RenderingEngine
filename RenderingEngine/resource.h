//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// LabProject.rc에서 사용되고 있습니다.
//
#define IDC_MYICON                      2
#define IDOK_RESET                      3
#define IDD_LABPROJECT_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_LABPROJECT03                107
#define IDI_SMALL                       108
#define IDC_LABPROJECT03                109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_DIALOG_COMPONENT            129
#define IDC_SLIDER_ROT_Y                1000
#define IDC_SLIDER_ROT_X                1002
#define IDC_SLIDER_ROT_Z                1003
#define IDC_STATIC_ANGLE_X              1004
#define IDC_STATIC_ANGLE_Y              1005
#define IDC_STATIC_ANGLE_Z              1006
#define IDC_STATIC_TRANSFORM            1007
#define IDC_STATIC_TRANSFORM2           1008
#define IDC_STATIC_TRANSFORM3           1009
#define IDC_EDIT_POS_X                  1010
#define IDC_EDIT_POS_Y                  1011
#define IDC_EDIT_POS_Z                  1012
#define IDC_STATIC_TRANSFORM4           1013
#define IDC_STATIC_TRANSFORM5           1014
#define IDC_STATIC_SCALE                1015
#define IDC_STATIC_TRANSFORM7           1016
#define IDC_EDIT_SCALE_X                1017
#define IDC_EDIT_SCALE_Y                1018
#define IDC_EDIT_SCALE_Z                1019
#define IDC_STATIC_TRANSFORM8           1020
#define IDC_STATIC_TRANSFORM9           1021
#define IDC_LOAD_TEXTURE                1022
#define IDC_LOAD_NORMAL                 1023
#define IDC_ACTIVE                      1025
#define ID_GAMEOBJECT_CUBE              32771
#define ID_GAMEOBJECT_SPHERE            32772
#define ID_GAMEOBJECT_PLANE             32773
#define ID_GAMEOBJECT_PILLAR            32774
#define ID_GAMEOBJECT_F                 32775
#define ID_GAMEOBJECT_Terrain           32776
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1028
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
