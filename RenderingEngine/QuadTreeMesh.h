#pragma once
#include "stdafx.h"
#include "Mesh.h"
#include "Camera.h"


class CQuadTreeMesh : public CMeshDetailTexturedIlluminated
{
private:
	struct NodeType
	{
		float positionX, positionZ, width;
		int triangleCount;
		ID3D11Buffer *nIndexBuffer;
		ID3D11Buffer **ppd3dVertexBuffers;
		UINT* pnVertexStrides;
		UINT* pnVertexOffsets;
		int nBuffers;
		array<NodeType*, 4> pNodes;
		//NodeType* nodes[4];
	};

	int m_nWidth;
	int m_nLength;
	XMFLOAT3 m_xmvScale;
	XMFLOAT3* m_pxmf3Normals;
	XMFLOAT2* m_pxmf2TexCoords;
	NodeType* m_pParentNode;
	int m_nTriangleCount;
	UINT m_nDrawCount;


public:
	CQuadTreeMesh(ID3D11Device *pd3dDevice, int nWidth, int nLength, XMFLOAT3 pxmf3Scale = XMFLOAT3(1.0f, 1.0f, 1.0f), void *pContext = nullptr);
	~CQuadTreeMesh();
	virtual void Render(CCamera* pCamera, ID3D11DeviceContext *pd3dDeviceContext);
	void RenderNode(CCamera* pCamera, NodeType* pNode, ID3D11DeviceContext *pd3dDeviceContext);

	void CreateQuadTree(ID3D11Device *pd3dDevice);
	void CalculateMeshDimensions(float& fCenterX, float& fCenterZ, float& fMeshWidth);
	void CreateTreeNode(NodeType* pNode, float, float, float, ID3D11Device* pd3dDevice);
	int CountTriangles(float, float, float);
	bool IsTriangleContained(int, float, float, float);
	void ReleaseNode(NodeType* pNode);
};

