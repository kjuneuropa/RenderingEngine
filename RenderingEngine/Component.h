#pragma once
#include "stdafx.h"
#include <fstream>
#include "Texture.h"
#include "Camera.h"





struct null_t {};

template <class _component_t>
struct is_unique_component
{
	typedef typename _component_t::component_identifier_t	identifier_t;
	typedef typename identifier_t::parent_component_t		parent_t;

	enum
	{
		value = std::_If<identifier_t::is_unique, std::true_type, is_unique_component<parent_t>>::type::value
	};
};
template <>
struct is_unique_component<null_t>
{
	enum { value = false };
};

template <class _component_t>
struct get_unique_component
{
	typedef typename _component_t::component_identifier_t	identifier_t;
	typedef typename identifier_t::parent_component_t		parent_t;

	typedef typename std::_If<identifier_t::is_unique, _component_t, typename get_unique_component<parent_t>::type>::type type;
};
template <>
struct get_unique_component<null_t>
{
	typedef null_t type;
};


template <class _component_t>
struct get_component
{
	typedef typename std::_If<
		is_unique_component<_component_t>::value,
		typename get_unique_component<_component_t>::type,
		_component_t>::type type;
};

template <class _component_t, class _parent_component_t, bool _is_unique>
struct _component_identifier
{
	typedef _component_t current_component_t;
	typedef _parent_component_t parent_component_t;

	enum { is_unique = _is_unique };
};





enum ComponentType : unsigned int
{
	ComponentType_Unknown,
	ComponentType_Component,
	ComponentType_Light,
	ComponentType_LineRenderer,
	ComponentType_Renderable,
	ComponentType_Skybox,
	ComponentType_Transform,
	ComponentType_GuiText,
	ComponentType_Terrain1,
	ComponentType_Player,
	ComponentType_TerrainPlayer,
	ComponentType_Renderer
};

class CComponent
{
public:
	static ComponentType getComponentId(void) { return ComponentType_Component; }
	typedef _component_identifier<CComponent, null_t, false> component_identifier_t;
private:
	static component_identifier_t identifier;
private: 
	int m_nType;
public:
	virtual void Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera) = 0;
	void SetType(ComponentType type) { m_nType = type; }
	template <class component_t>
	static ComponentType Type_To_Enum(); 
};









struct Letter
{
	float fLeft, fRight;
	int nSize;
};

struct Font
{
	Letter* pLetters;
	CTexture* pTexture;
};

class CGuiText : public CComponent
{
private:
	

public:
	~CGuiText(){}

	void Initalize(ID3D11Device *pd3dDevice);
	virtual void Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)  override;

	void BuildVertexAarry(void* pVertexs, const wchar_t* pText, float fPosX, float fPosY);
	void  UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext);

	void LoadData(ID3D11Device *pd3dDevice, wstring& strFontFile, wstring& strTextureFile);
	void LoadFont(wstring& strFontFile, Letter* pLetters);
	void LoadTexture(ID3D11Device *pd3dDevice, wstring& strTextureFile, CTexture* pTexture);
	float FromToNDC_X(float fScreenX);
	float FromToNDC_Y(float fScreenY);
private:
	int m_nFontType = 0;
	static vector<Font*> m_Fonts;
	XMFLOAT2 m_xmf2ScreenPos;
	XMFLOAT4 m_xmf4Color;

	//FontType* m_pFontType;
};

//DECLARE_COMPONENT(CComponent);
//DECLARE_COMPONENT(CTransform1);
//DECLARE_COMPONENT(CSkyBox1);

