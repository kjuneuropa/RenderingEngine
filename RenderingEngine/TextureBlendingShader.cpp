#include "TextureBlendingShader.h"


CTextureBlendingShader::CTextureBlendingShader()
{
}


CTextureBlendingShader::~CTextureBlendingShader()
{
}

void CTextureBlendingShader::BuildObjects(ID3D11Device *pd3dDevice, CTexture *pTexture)
{
	//m_nObjects = 1;
	//m_ppObjects = new CGameObject*[m_nObjects];

	m_pTexture = pTexture;
	if (pTexture) pTexture->AddRef();


	CTextureBlendingObject *pTextureBlendingObject = new CTextureBlendingObject(pd3dDevice);
	//m_ppObjects[0] = pTextureBlendingObject;
	//m_ppObjects[0]->SetPosition(XMFLOAT3(1028.0f, 170.0f - 65.0f, 1050.0f + 140.0f));
	//SetPosition(XMFLOAT3(1028.0f, 170.0f - 65.0f, 1050.0f + 140.0f));

}

void CTextureBlendingShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	CShader::OnPrepareRender(pd3dDeviceContext);
	if (m_pTexture) 
		m_pTexture->UpdateShaderVariable(pd3dDeviceContext);
	//m_ppObjects[0]->Render(pd3dDeviceContext,  pCamera);
}


void CTextureBlendingShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD0", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD1", 1, DXGI_FORMAT_R32G32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};


	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Water.fx", "VSTexturedColor", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Water.fx", "PSTexturedColor", "ps_5_0", &GetPixelShader());

}