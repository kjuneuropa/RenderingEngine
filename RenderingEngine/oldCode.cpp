#include "stdafx.h"
#include "Object.h"

class CNewHeightMapTerrain : public CGameObject
{
public:
	CNewHeightMapTerrain(ID3D11Device *pd3dDevice, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale);
	virtual ~CNewHeightMapTerrain();

private:
	//지형의 높이 맵으로 사용할 이미지이다.
	CNewHeightMap *m_pHeightMap;

	//지형의 가로와 세로 크기이다.
	int m_nWidth;
	int m_nLength;


	//지형을 실제로 몇 배 확대할 것인가를 나타내는 스케일 벡터이다.
	XMFLOAT3 m_xmvScale;

public:
	//지형의 실제 높이를 반환한다. 높이 맵의 높이에 스케일을 곱한 값이다.
	float GetHeight(float x, float z, bool bReverseQuad = false) { return(m_pHeightMap->GetHeight(x, z, bReverseQuad) * m_xmvScale.y); }
	XMFLOAT3 GetNormal(float x, float z) { return(m_pHeightMap->GetHeightMapNormal(int(x / m_xmvScale.x), int(z / m_xmvScale.z))); }

	int GetHeightMapWidth() { return(m_pHeightMap->GetHeightMapWidth()); }
	int GetHeightMapLength() { return(m_pHeightMap->GetHeightMapLength()); }

	XMFLOAT3 GetScale() { return(m_xmvScale); }
	//지형의 실제 크기(가로/세로)를 반환한다. 높이 맵의 크기에 스케일을 곱한 값이다.
	float GetWidth() { return(m_nWidth * m_xmvScale.x); }
	float GetLength() { return(m_nLength * m_xmvScale.z); }

	float GetPeakHeight() { return(m_bcMeshBoundingCube.m_xmf3Maximum.y); }

};

class CNewHeightMapGridMesh : public CMeshDetailTexturedIlluminated
{
protected:
	int m_nWidth;
	int m_nLength;
	XMFLOAT3 m_xmvScale;

public:
	//생성자를 다음과 같이 변경한다.
	CNewHeightMapGridMesh(ID3D11Device *pd3dDevice, int nWidth, int nLength, XMFLOAT3 pxmf3Scale = XMFLOAT3(1.0f, 1.0f, 1.0f), void *pContext = nullptr);
	virtual ~CNewHeightMapGridMesh();

	XMFLOAT3 GetScale() { return(m_xmvScale); }
	int GetWidth() { return(m_nWidth); }
	int GetLength() { return(m_nLength); }

	virtual float OnGetHeight(int x, int z, void *pContext);
};






CNewHeightMapGridMesh::CNewHeightMapGridMesh(ID3D11Device *pd3dDevice, int nWidth, int nLength, XMFLOAT3 xmf3Scale, void *pContext) : CMeshDetailTexturedIlluminated(pd3dDevice)
{
	// Calculate the number of vertices in the 3D terrain model.
	m_nVertices = (nWidth - 1) * (nLength - 1) * 6;
	m_nIndices = m_nVertices;
	m_pnIndices = new UINT[m_nIndices];

	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	//unsigned short *pHeightMap = (unsigned short *)pContext;

	CNewHeightMap *pHeightMap = (CNewHeightMap *)pContext;


	m_nWidth = nWidth;
	m_nLength = nLength;
	m_xmvScale = xmf3Scale;


	m_vPositions.resize(m_nVertices);
	XMFLOAT3 *pxmf3Normals = new XMFLOAT3[m_nVertices];
	XMFLOAT2 *pxmf2TexCoords = new XMFLOAT2[m_nVertices];
	//XMFLOAT2 *pxmf2DetailTexCoords = new XMFLOAT2[m_nVertices];



	// Initialize the nIndex into the height map array.
	int nIndex = 0, nIndex1 = 0, nIndex2 = 0, nIndex3 = 0, nIndex4 = 0;
	float tu, tv;

	// Load the 3D terrain model with the height map terrain data.
	// We will be creating 2 triangles for each of the four points in a quad.
	for (int j = 0; j<(nLength - 1); j++)
	{
		for (int i = 0; i<(nWidth - 1); i++)
		{
			// Get the nIndexes to the four points of the quad.
			nIndex1 = (nWidth * j) + i;          // Upper left.
			nIndex2 = (nWidth * j) + (i + 1);      // Upper right.
			nIndex3 = (nWidth * (j + 1)) + i;      // Bottom left.
			nIndex4 = (nWidth * (j + 1)) + (i + 1);  // Bottom right.

													 // Now create two triangles for that quad.
													 // Triangle 1 - Upper left.
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].z;

			pxmf2TexCoords[nIndex].x = 0.0f;
			pxmf2TexCoords[nIndex].y = 0.0f;

			pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].nx;
			pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].ny;
			pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].nz;

			nIndex++;

			// Triangle 1 - Upper right.
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].z;

			pxmf2TexCoords[nIndex].x = 1.0f;
			pxmf2TexCoords[nIndex].y = 0.0f;

			pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].nx;
			pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].ny;
			pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].nz;

			nIndex++;

			// Triangle 1 - Bottom left.
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].z;

			pxmf2TexCoords[nIndex].x = 0.0f;
			pxmf2TexCoords[nIndex].y = 1.0f;

			pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].nx;
			pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].ny;
			pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].nz;
			nIndex++;

			// Triangle 2 - Bottom left.
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].z;

			pxmf2TexCoords[nIndex].x = 0.0f;
			pxmf2TexCoords[nIndex].y = 1.0f;

			pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].nx;
			pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].ny;
			pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].nz;
			nIndex++;

			// Triangle 2 - Upper right.
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].z;

			pxmf2TexCoords[nIndex].x = 1.0f;
			pxmf2TexCoords[nIndex].y = 0.0f;

			pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].nx;
			pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].ny;
			pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].nz;
			nIndex++;

			// Triangle 2 - Bottom right.
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].z;

			pxmf2TexCoords[nIndex].x = 1.0f;
			pxmf2TexCoords[nIndex].y = 1.0f;

			pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].nx;
			pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].ny;
			pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].nz;
			nIndex++;
		}
	}

	//for (int j = 0; j < (nLength - 1); j++)
	//{
	//	for (int i = 0; i < (nWidth - 1); i++)
	//	{
	//		// Get the nIndexes to the four points of the quad.
	//		nIndex1 = (nLength * j) + i;          // Upper left.
	//		nIndex2 = (nLength * j) + (i + 1);      // Upper right.
	//		nIndex3 = (nLength * (j + 1)) + i;      // Bottom left.
	//		nIndex4 = (nLength * (j + 1)) + (i + 1);  // Bottom right.

	//		// Now create two triangles for that quad.
	//		// Upper left.
	//		tv = pHeightMap->m_pHeightMap[nIndex3].tv;

	//		// Modify the texture coordinates to cover the top edge.
	//		if (tv == 1.0f) { tv = 0.0f; }
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].z;

	//		pxmf2TexCoords[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].tu;
	//		pxmf2TexCoords[nIndex].y = tv;

	//		pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].nx;
	//		pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].ny;
	//		pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].nz;

	//		nIndex++;

	//		// Triangle 1 - Upper right.
	//		// Upper right.
	//		tu = pHeightMap->m_pHeightMap[nIndex4].tu;
	//		tv = pHeightMap->m_pHeightMap[nIndex4].tv;

	//		// Modify the texture coordinates to cover the top and right edge.
	//		if (tu == 0.0f) { tu = 1.0f; }
	//		if (tv == 1.0f) { tv = 0.0f; }

	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].z;

	//		pxmf2TexCoords[nIndex].x = tu;
	//		pxmf2TexCoords[nIndex].y = tv;

	//		pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].nx;
	//		pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].ny;
	//		pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].nz;

	//		nIndex++;

	//		// Triangle 1 - Bottom left.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].z;

	//		pxmf2TexCoords[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].tu;
	//		pxmf2TexCoords[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].tv;

	//		pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].nx;
	//		pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].ny;
	//		pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].nz;
	//		nIndex++;

	//		// Triangle 2 - Bottom left.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].z;

	//		pxmf2TexCoords[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].tu;
	//		pxmf2TexCoords[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].tv;

	//		pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].nx;
	//		pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].ny;
	//		pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].nz;
	//		nIndex++;

	//		// Triangle 2 - Upper right.
	//		// Upper right.
	//		tu = pHeightMap->m_pHeightMap[nIndex4].tu;
	//		tv = pHeightMap->m_pHeightMap[nIndex4].tv;

	//		// Modify the texture coordinates to cover the top and right edge.
	//		if (tu == 0.0f) { tu = 1.0f; }
	//		if (tv == 1.0f) { tv = 0.0f; }


	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].z;

	//		pxmf2TexCoords[nIndex].x = tu;
	//		pxmf2TexCoords[nIndex].y = tv;

	//		pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].nx;
	//		pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].ny;
	//		pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].nz;
	//		nIndex++;

	//		// Triangle 2 - Bottom right.
	//		// Bottom right.
	//		tu = pHeightMap->m_pHeightMap[nIndex2].tu;

	//		// Modify the texture coordinates to cover the right edge.
	//		if (tu == 0.0f) { tu = 1.0f; }

	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].z;

	//		pxmf2TexCoords[nIndex].x = tu;
	//		pxmf2TexCoords[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].tv;

	//		pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].nx;
	//		pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].ny;
	//		pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].nz;
	//		nIndex++;
	//	}
	//}

	/*for (int i = 0; i < m_nVertices; ++i)
	m_pnIndices[i] = i;*/



	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferData.pSysMem = pxmf3Normals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dNormalBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * m_nVertices;
	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);

	//d3dBufferData.pSysMem = pxmf2DetailTexCoords;
	//pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dDetailTexCoordBuffer);

	if (pxmf3Normals) delete[] pxmf3Normals;
	if (pxmf2TexCoords) delete[] pxmf2TexCoords;
	//if (pxmf2DetailTexCoords) delete[] pxmf2DetailTexCoords;

	//정점은 위치 벡터, 법선 벡터, 텍스쳐 좌표, 디테일 텍스쳐 좌표를 갖는다.
	ID3D11Buffer *pd3dBuffers[3] = { m_pd3dPositionBuffer, m_pd3dNormalBuffer, m_pd3dTexCoordBuffer };
	UINT pnBufferStrides[3] = { sizeof(XMFLOAT3), sizeof(XMFLOAT3), sizeof(XMFLOAT2) };
	UINT pnBufferOffsets[3] = { 0, 0, 0 };
	AssembleToVertexBuffer(3, pd3dBuffers, pnBufferStrides, pnBufferOffsets);



	//ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	//d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	//d3dBufferDesc.ByteWidth = sizeof(UINT) * m_nIndices;
	//d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	//d3dBufferDesc.CPUAccessFlags = 0;
	//ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	//d3dBufferData.pSysMem = m_pnIndices;
	//pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);




	/*D3D11_RASTERIZER_DESC d3dRasterizerDesc;
	d3dRasterizerDesc.AntialiasedLineEnable = false;
	d3dRasterizerDesc.CullMode = D3D11_CULL_BACK;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = true;
	d3dRasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
	d3dRasterizerDesc.FrontCounterClockwise = false;
	d3dRasterizerDesc.MultisampleEnable = false;
	d3dRasterizerDesc.ScissorEnable = false;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;

	pd3dDevice->CreateRasterizerState(&d3dRasterizerDesc, &m_pd3dRasterizerState);*/



	XNA::AxisAlignedBox pBoundingBox;
	XNA::ComputeBoundingAxisAlignedBoxFromPoints(&pBoundingBox, m_nVertices, &m_vPositions[0], sizeof(XMFLOAT3));


	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(pBoundingBox.Center.x - pBoundingBox.Extents.x,
		pBoundingBox.Center.y - pBoundingBox.Extents.y, pBoundingBox.Center.z - pBoundingBox.Extents.z);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(pBoundingBox.Center.x + pBoundingBox.Extents.x,
		pBoundingBox.Center.y + pBoundingBox.Extents.y, pBoundingBox.Center.z + pBoundingBox.Extents.z);

}

CNewHeightMapGridMesh::~CNewHeightMapGridMesh()
{
}

float CNewHeightMapGridMesh::OnGetHeight(int x, int z, void *pContext)
{
	//높이 맵 객체의 높이 맵 이미지의 픽셀 값을 지형의 높이로 반환한다. 
	CHeightMap *pHeightMap = (CHeightMap *)pContext;
	BYTE *pHeightMapImage = pHeightMap->GetHeightMapImage();
	XMFLOAT3 xmf3Scale = pHeightMap->GetScale();
	int cxTerrain = pHeightMap->GetHeightMapWidth();
	float gScreenHeight = pHeightMapImage[x + (z*cxTerrain)] * xmf3Scale.y;
	return(gScreenHeight);
}


CNewHeightMapTerrain::CNewHeightMapTerrain(ID3D11Device *pd3dDevice, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale) : CGameObject(1)
{
	m_pHeightMap = new CNewHeightMap(pFileName, nWidth, nLength, xmf3Scale);
	m_xmvScale = xmf3Scale;

	CNewHeightMapGridMesh* pHeightMapGridMesh = new CNewHeightMapGridMesh(pd3dDevice, nWidth, nLength, xmf3Scale, m_pHeightMap);
	//CNewHeightMapGridMesh* pHeightMapGridMesh = new CNewHeightMapGridMesh(pd3dDevice, nWidth-1, nLength-1, xmf3Scale, m_pHeightMap);
	SetMesh(pHeightMapGridMesh, 0);

	/*float fCenterX = (pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.x
	+ pHeightMapGridMesh->GetBoundingCube().m_xmf3Minimum.x)/2;
	float fCenterY = (pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.y
	+ pHeightMapGridMesh->GetBoundingCube().m_xmf3Minimum.y)/2;
	float fCenterZ = (pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.z
	+ pHeightMapGridMesh->GetBoundingCube().m_xmf3Minimum.z)/2;





	XMFLOAT3 xmf3Center(fCenterX, fCenterY, fCenterZ);
	XMFLOAT3 xmf3Extent(pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.x - fCenterX,
	pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.y - fCenterY,
	pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.z - fCenterZ);
	CAxisMesh* pAxisMesh = new CAxisMesh(pd3dDevice, xmf3Center, xmf3Extent);
	SetMesh(pAxisMesh,1);*/


}

CNewHeightMapTerrain::~CNewHeightMapTerrain()
{
	if (m_pHeightMap) delete m_pHeightMap;
}

class CNewHeightMap
{
private: // <^오^>
		 //높이 맵 이미지 픽셀(8-비트)들의 이차원 배열이다. 각 픽셀은 0~255의 값을 갖는다.
	BYTE *m_pHeightMapImage;
	//높이 맵 이미지의 가로와 세로 크기이다.
	int m_nWidth;
	int m_nLength;
	//높이 맵을 실제로 몇 배 확대하여 사용할 것인가를 나타내는 스케일 벡터이다.
	XMFLOAT3 m_xmvScale;

public:
	CNewHeightMap(LPCTSTR pFileName, int nWidth, int nLength, XMFLOAT3 xmf3Scale);
	~CNewHeightMap(void);

	//높이 맵 이미지에서 (x, z) 위치의 픽셀 값에 기반한 지형의 높이를 반환한다.
	float GetHeight(float x, float z, bool bReverseQuad = false);
	//높이 맵 이미지에서 (x, z) 위치의 법선 벡터를 반환한다.
	XMFLOAT3 GetHeightMapNormal(int x, int z);
	XMFLOAT3 GetScale() { return(m_xmvScale); }

	BYTE *GetHeightMapImage() { return(m_pHeightMapImage); }
	int GetHeightMapWidth() { return(m_nWidth); }
	int GetHeightMapLength() { return(m_nLength); }

public:
	struct HeightMapType
	{
		float x, y, z;
		float nx, ny, nz;
		float tu, tv;
	};
	void SetTerrainCoordinates();
	void CalculateNormals();
	void CalculateNormalsBitmap();
	void CalculateTextureCoordinatesBitmap();
	HeightMapType* m_pHeightMap;

private:

	float m_fHeightScale;

};

CNewHeightMap::CNewHeightMap(LPCTSTR pFileName, int nWidth, int nLength, XMFLOAT3 xmf3Scale)
{
	m_nWidth = nWidth;
	m_nLength = nLength;
	m_xmvScale = xmf3Scale;


	int nIndex = 0;
	FILE* pFile = nullptr;
	unsigned long long nImageSize, nCount;
	unsigned short* pRawImage;
	m_fHeightScale = 300.0f;





	// Create the float array to hold the height map data.
	m_pHeightMap = new HeightMapType[m_nWidth * m_nLength];

	// Open the 16 bit raw height map file for reading in binary.
	_wfopen_s(&pFile, pFileName, L"rb");

	// Calculate the size of the raw image data.
	nImageSize = m_nWidth * m_nLength;

	// Allocate memory for the raw image data.
	pRawImage = new unsigned short[nImageSize];
	if (!pRawImage)
	{
		exit(0);
	}

	// Read in the raw image data.
	nCount = fread(pRawImage, sizeof(unsigned short), nImageSize, pFile);
	if (nCount != nImageSize)
	{
		exit(0);
	}

	// Close the file.
	fclose(pFile);


	// Copy the image data into the height map array.
	for (int j = 0; j<m_nLength; j++)
	{
		for (int i = 0; i<m_nWidth; i++)
		{
			nIndex = (m_nWidth * j) + i;

			// Store the height at this point in the height map array.
			m_pHeightMap[nIndex].y = static_cast<float>(pRawImage[nIndex]);
		}
	}

	SetTerrainCoordinates();
	CalculateNormals();

	// Release the bitmap image data.
	delete[] pRawImage;






	//FILE* filePtr;
	//int error;
	//unsigned int count;
	//BITMAPFILEHEADER bitmapFileHeader;
	//BITMAPINFOHEADER bitmapInfoHeader;
	//int imageSize, i, j, k, nIndex;
	//unsigned char* bitmapImage;
	//unsigned char height;


	//// Open the height map file in binary.
	// _wfopen_s(&filePtr, pFileName, L"rb");


	//// Read in the file header.
	//count = fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);

	//// Read in the bitmap info header.
	//count = fread(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);

	//// Save the dimensions of the terrain.
	//m_nWidth = bitmapInfoHeader.biWidth;
	//m_nLength = bitmapInfoHeader.biHeight;

	//// Calculate the size of the bitmap image data.
	//imageSize = m_nWidth * m_nLength * 3;

	//// Allocate memory for the bitmap image data.
	//bitmapImage = new unsigned char[imageSize];

	//// Move to the beginning of the bitmap data.
	//fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

	//// Read in the bitmap image data.
	//count = fread(bitmapImage, 1, imageSize, filePtr);

	//// Close the file.
	//error = fclose(filePtr);


	//// Create the structure to hold the height map data.
	//m_pHeightMap = new HeightMapType[m_nWidth * m_nLength];
	//

	//// Initialize the position in the image data buffer.
	//k = 0;



	//// Read the image data into the height map.
	//for (int j = 0; j<m_nLength; j++)
	//{
	//	for (int i = 0; i<m_nWidth; i++)
	//	{
	//		height = bitmapImage[k];

	//		nIndex = (m_nLength * j) + i;

	//		m_pHeightMap[nIndex].x = (float)i;
	//		m_pHeightMap[nIndex].y = (float)height;
	//		m_pHeightMap[nIndex].z = (float)j;

	//		k += 3;
	//	}
	//}

	//// Release the bitmap image data.
	//delete[] bitmapImage;


	//for (int j = 0; j<m_nLength; j++)
	//{
	//	for (int i = 0; i<m_nWidth; i++)
	//	{
	//		m_pHeightMap[(m_nLength * j) + i].y /= 15.0f;
	//	}
	//}


	////SetTerrainCoordinates();
	//CalculateNormalsBitmap();
	//CalculateTextureCoordinatesBitmap();

}
CNewHeightMap::~CNewHeightMap()
{
	if (m_pHeightMapImage) delete[] m_pHeightMapImage;
	if (m_pHeightMap) delete[] m_pHeightMap;
}


void CNewHeightMap::SetTerrainCoordinates()
{
	// Loop through all the elements in the height map array and 
	// adjust their coordinates correctly.

	for (int j = 0; j<m_nLength; j++)
	{
		for (int i = 0; i<m_nWidth; i++)
		{
			int nIndex = (m_nWidth * j) + i;

			// Set the X and Z coordinates.
			m_pHeightMap[nIndex].x = static_cast<float>(i) * 4.0f;
			m_pHeightMap[nIndex].z = -static_cast<float>(j)* 4.0f;

			// Move the terrain depth into the positive range. 
			// For example from (0, -256) to (256, 0).
			m_pHeightMap[nIndex].z += static_cast<float>(m_nLength - 1);

			// Scale the height.
			m_pHeightMap[nIndex].y /= m_fHeightScale;
		}
	}
}
void CNewHeightMap::CalculateTextureCoordinatesBitmap()
{
	int incrementCount, i, j, tuCount, tvCount;
	float incrementValue, tuCoordinate, tvCoordinate;
	const int TEXTURE_REPEAT = 8;

	// Calculate how much to increment the texture coordinates by.
	incrementValue = (float)TEXTURE_REPEAT / (float)m_nWidth;

	// Calculate how many times to repeat the texture.
	incrementCount = m_nWidth / TEXTURE_REPEAT;

	// Initialize the tu and tv coordinate values.
	tuCoordinate = 0.0f;
	tvCoordinate = 1.0f;

	// Initialize the tu and tv coordinate nIndexes.
	tuCount = 0;
	tvCount = 0;

	// Loop through the entire height map and calculate the tu and tv texture coordinates for each vertex.
	for (j = 0; j<m_nLength; j++)
	{
		for (i = 0; i<m_nWidth; i++)
		{
			// Store the texture coordinate in the height map.
			m_pHeightMap[(m_nLength * j) + i].tu = tuCoordinate;
			m_pHeightMap[(m_nLength * j) + i].tv = tvCoordinate;

			// Increment the tu texture coordinate by the increment value and increment the nIndex by one.
			tuCoordinate += incrementValue;
			tuCount++;

			// Check if at the far right end of the texture and if so then start at the beginning again.
			if (tuCount == incrementCount)
			{
				tuCoordinate = 0.0f;
				tuCount = 0;
			}
		}

		// Increment the tv texture coordinate by the increment value and increment the nIndex by one.
		tvCoordinate -= incrementValue;
		tvCount++;

		// Check if at the top of the texture and if so then start at the bottom again.
		if (tvCount == incrementCount)
		{
			tvCoordinate = 1.0f;
			tvCount = 0;
		}
	}

	return;
}
void CNewHeightMap::CalculateNormalsBitmap()
{

	int i, j, nIndex1, nIndex2, nIndex3, nIndex, count = 0;
	float sum[3], length;
	XMFLOAT3 xmf3Vertex1, xmf3Vertex2, xmf3Vertex3, xmf3Vec1, xmf3Vec2, xmf3Sum;
	XMFLOAT3* pxmf3normals;
	XMVECTOR  xmvNormal;

	// Create a temporary array to hold the face normal vectors.
	pxmf3normals = new XMFLOAT3[(m_nLength - 1) * (m_nWidth - 1)];


	// Go through all the faces in the mesh and calculate their normals.
	for (j = 0; j<(m_nLength - 1); j++)
	{
		for (i = 0; i<(m_nWidth - 1); i++)
		{
			nIndex1 = (j * m_nLength) + i;
			nIndex2 = (j *m_nLength) + (i + 1);
			nIndex3 = ((j + 1) * m_nLength) + i;




			// Get three vertices from the face.

			xmf3Vertex1.x = m_pHeightMap[nIndex1].x;
			xmf3Vertex1.y = m_pHeightMap[nIndex1].y;
			xmf3Vertex1.z = m_pHeightMap[nIndex1].z;

			xmf3Vertex2.x = m_pHeightMap[nIndex2].x;
			xmf3Vertex2.y = m_pHeightMap[nIndex2].y;
			xmf3Vertex2.z = m_pHeightMap[nIndex1].z;

			xmf3Vertex3.x = m_pHeightMap[nIndex3].x;
			xmf3Vertex3.y = m_pHeightMap[nIndex3].y;
			xmf3Vertex3.z = m_pHeightMap[nIndex3].z;


			xmf3Vec1.x = xmf3Vertex1.x - xmf3Vertex3.x;
			xmf3Vec1.y = xmf3Vertex1.y - xmf3Vertex3.y;
			xmf3Vec1.z = xmf3Vertex1.z - xmf3Vertex3.z;
			xmf3Vec2.x = xmf3Vertex3.x - xmf3Vertex2.x;
			xmf3Vec2.y = xmf3Vertex3.y - xmf3Vertex2.y;
			xmf3Vec2.z = xmf3Vertex3.z - xmf3Vertex2.z;

			nIndex = (j * (m_nWidth - 1)) + i;

			xmvNormal = XMVector3Cross(XMLoadFloat3(&xmf3Vec1), XMLoadFloat3(&xmf3Vec2));
			xmvNormal = XMVector3Normalize(xmvNormal);
			XMStoreFloat3(&pxmf3normals[nIndex], xmvNormal);
		}
	}

	// Now go through all the vertices and take a sum of the face normals that touch this vertex.
	for (j = 0; j<m_nLength; j++)
	{
		for (i = 0; i< m_nWidth; i++)
		{
			// Initialize the sum.
			xmf3Sum.x = xmf3Sum.y = xmf3Sum.z = 0.0f;

			// Initialize the count.
			count = 0;


			// Bottom left face.
			if (((i - 1) >= 0) && ((j - 1) >= 0))
			{
				nIndex = ((j - 1) * (m_nLength - 1)) + (i - 1);

				xmf3Sum.x += pxmf3normals[nIndex].x;
				xmf3Sum.y += pxmf3normals[nIndex].y;
				xmf3Sum.z += pxmf3normals[nIndex].z;
				count++;
			}

			// Bottom right face.
			if ((i<(m_nWidth - 1)) && ((j - 1) >= 0))
			{
				nIndex = ((j - 1) * (m_nLength - 1)) + i;

				xmf3Sum.x += pxmf3normals[nIndex].x;
				xmf3Sum.y += pxmf3normals[nIndex].y;
				xmf3Sum.z += pxmf3normals[nIndex].z;
				count++;
			}

			// Upper left face.
			if (((i - 1) >= 0) && (j<(m_nLength - 1)))
			{
				nIndex = (j * (m_nWidth - 1)) + (i - 1);

				xmf3Sum.x += pxmf3normals[nIndex].x;
				xmf3Sum.y += pxmf3normals[nIndex].y;
				xmf3Sum.z += pxmf3normals[nIndex].z;
				count++;
			}

			// Upper right face.
			if ((i < (m_nWidth - 1)) && (j < (m_nLength - 1)))
			{
				nIndex = (j * (m_nLength - 1)) + i;

				xmf3Sum.x += pxmf3normals[nIndex].x;
				xmf3Sum.y += pxmf3normals[nIndex].y;
				xmf3Sum.z += pxmf3normals[nIndex].z;
				count++;
			}

			xmf3Sum.x = xmf3Sum.x / static_cast<float>(count);
			xmf3Sum.y = xmf3Sum.y / static_cast<float>(count);
			xmf3Sum.z = xmf3Sum.z / static_cast<float>(count);

			// Calculate the length of this normal.
			XMStoreFloat3(&xmf3Sum, XMVector3Normalize(XMLoadFloat3(&xmf3Sum)));


			// Get an nIndex to the vertex location in the height map array.
			nIndex = (j * m_nWidth) + i;

			// Normalize the final shared normal for this vertex and store it in the height map array.
			m_pHeightMap[nIndex].nx = xmf3Sum.x;
			m_pHeightMap[nIndex].ny = xmf3Sum.y;
			m_pHeightMap[nIndex].nz = xmf3Sum.z;
		}
	}

	// Release the temporary normals.
	delete[]  pxmf3normals;

	//return true;
}
void CNewHeightMap::CalculateNormals()
{
	int i, j, nIndex1, nIndex2, nIndex3, nIndex;
	float sum[3], length;
	XMFLOAT3 xmf3Vertex1, xmf3Vertex2, xmf3Vertex3, xmf3Vec1, xmf3Vec2, xmf3Sum;
	XMFLOAT3* pxmf3normals;
	XMVECTOR  xmvNormal;

	// Create a temporary array to hold the face normal vectors.
	pxmf3normals = new XMFLOAT3[(m_nLength - 1) * (m_nWidth - 1)];


	// Go through all the faces in the mesh and calculate their normals.
	for (j = 0; j<(m_nLength - 1); j++)
	{
		for (i = 0; i<(m_nWidth - 1); i++)
		{
			nIndex1 = ((j + 1) *  m_nWidth) + i;      // Bottom left vertex.
			nIndex2 = ((j + 1) *  m_nWidth) + (i + 1);  // Bottom right vertex.
			nIndex3 = (j *  m_nWidth) + i;          // Upper left vertex.

													// Get three vertices from the face.

			xmf3Vertex1.x = m_pHeightMap[nIndex1].x;
			xmf3Vertex1.y = m_pHeightMap[nIndex1].y;
			xmf3Vertex1.z = m_pHeightMap[nIndex1].z;

			xmf3Vertex2.x = m_pHeightMap[nIndex2].x;
			xmf3Vertex2.y = m_pHeightMap[nIndex2].y;
			xmf3Vertex2.z = m_pHeightMap[nIndex1].z;

			xmf3Vertex3.x = m_pHeightMap[nIndex3].x;
			xmf3Vertex3.y = m_pHeightMap[nIndex3].y;
			xmf3Vertex3.z = m_pHeightMap[nIndex3].z;


			xmf3Vec1.x = xmf3Vertex1.x - xmf3Vertex3.x;
			xmf3Vec1.y = xmf3Vertex1.y - xmf3Vertex3.y;
			xmf3Vec1.z = xmf3Vertex1.z - xmf3Vertex3.z;
			xmf3Vec2.x = xmf3Vertex3.x - xmf3Vertex2.x;
			xmf3Vec2.y = xmf3Vertex3.y - xmf3Vertex2.y;
			xmf3Vec2.z = xmf3Vertex3.z - xmf3Vertex2.z;

			nIndex = (j * (m_nWidth - 1)) + i;

			xmvNormal = XMVector3Cross(XMLoadFloat3(&xmf3Vec1), XMLoadFloat3(&xmf3Vec2));
			xmvNormal = XMVector3Normalize(xmvNormal);
			XMStoreFloat3(&pxmf3normals[nIndex], xmvNormal);
		}
	}

	// Now go through all the vertices and take a sum of the face normals that touch this vertex.
	for (j = 0; j<m_nLength; j++)
	{
		for (i = 0; i< m_nWidth; i++)
		{
			// Initialize the sum.
			xmf3Sum.x = xmf3Sum.y = xmf3Sum.z = 0.0f;

			// Bottom left face.
			if (((i - 1) >= 0) && ((j - 1) >= 0))
			{
				nIndex = ((j - 1) * (m_nWidth - 1)) + (i - 1);

				xmf3Sum.x += pxmf3normals[nIndex].x;
				xmf3Sum.y += pxmf3normals[nIndex].y;
				xmf3Sum.z += pxmf3normals[nIndex].z;
			}

			// Bottom right face.
			if ((i<(m_nWidth - 1)) && ((j - 1) >= 0))
			{
				nIndex = ((j - 1) * (m_nWidth - 1)) + i;

				xmf3Sum.x += pxmf3normals[nIndex].x;
				xmf3Sum.y += pxmf3normals[nIndex].y;
				xmf3Sum.z += pxmf3normals[nIndex].z;
			}

			// Upper left face.
			if (((i - 1) >= 0) && (j<(m_nLength - 1)))
			{
				nIndex = (j * (m_nWidth - 1)) + (i - 1);

				xmf3Sum.x += pxmf3normals[nIndex].x;
				xmf3Sum.y += pxmf3normals[nIndex].y;
				xmf3Sum.z += pxmf3normals[nIndex].z;
			}

			// Upper right face.
			if ((i < (m_nWidth - 1)) && (j < (m_nLength - 1)))
			{
				nIndex = (j * (m_nWidth - 1)) + i;

				xmf3Sum.x += pxmf3normals[nIndex].x;
				xmf3Sum.y += pxmf3normals[nIndex].y;
				xmf3Sum.z += pxmf3normals[nIndex].z;
			}

			// Calculate the length of this normal.
			XMStoreFloat3(&xmf3Sum, XMVector3Normalize(XMLoadFloat3(&xmf3Sum)));


			// Get an nIndex to the vertex location in the height map array.
			nIndex = (j * m_nWidth) + i;

			// Normalize the final shared normal for this vertex and store it in the height map array.
			m_pHeightMap[nIndex].nx = xmf3Sum.x;
			m_pHeightMap[nIndex].ny = xmf3Sum.y;
			m_pHeightMap[nIndex].nz = xmf3Sum.z;
		}
	}

	// Release the temporary normals.
	delete[]  pxmf3normals;

	//return true;
}



XMFLOAT3 CNewHeightMap::GetHeightMapNormal(int x, int z)
{
	//지형의 x-좌표와 z-좌표가 지형(높이 맵)의 범위를 벗어나면 지형의 법선 벡터는 y-축 방향 벡터이다.
	if ((x < 0.0f) || (z < 0.0f) || (x >= m_nWidth) || (z >= m_nLength))
		return(XMFLOAT3(0.0f, 1.0f, 0.0f));

	/*높이 맵에서 (x, z) 좌표의 픽셀 값과 인접한 두 개의 점 (x+1, z), (z, z+1)에 대한 픽셀 값을
	사용하여 법선 벡터를 계산한다.*/
	int nHeightMapIndex = x + (z * m_nWidth);
	int xHeightMapAdd = (x < (m_nWidth - 1)) ? 1 : -1;
	int zHeightMapAdd = (z < (m_nLength - 1)) ? m_nWidth : -(signed)m_nWidth;
	//(x, z), (x+1, z), (z, z+1)의 지형의 높이 값을 구한다.
	float y1 = (float)m_pHeightMapImage[nHeightMapIndex] * m_xmvScale.y;
	float y2 = (float)m_pHeightMapImage[nHeightMapIndex + xHeightMapAdd] * m_xmvScale.y;
	float y3 = (float)m_pHeightMapImage[nHeightMapIndex + zHeightMapAdd] * m_xmvScale.y;

	//vEdge1은 (0, y3, m_vScale.z) - (0, y1, 0) 벡터이다.
	XMFLOAT3 vEdge1 = XMFLOAT3(0.0f, y3 - y1, m_xmvScale.z);
	//vEdge2는 (m_vScale.x, y2, 0) - (0, y1, 0) 벡터이다.
	XMFLOAT3 vEdge2 = XMFLOAT3(m_xmvScale.x, y2 - y1, 0.0f);
	//법선 벡터는 vEdge1과 vEdge2의 외적을 정규화하면 된다.
	XMFLOAT3 vNormal;
	//D3DXVec3Cross(&vNormal, &vEdge1, &vEdge2);
	//D3DXVec3Normalize(&vNormal, &vNormal);
	XMStoreFloat3(&vNormal,
		XMVector3Normalize(XMVector3Cross(XMLoadFloat3(&vEdge1), XMLoadFloat3(&vEdge2))));


	return(vNormal);
}
float CNewHeightMap::GetHeight(float fx, float fz, bool bReverseQuad)
{
	//지형의 좌표 (fx, fz)에서 높이 맵의 좌표를 계산한다.
	fx = fx / m_xmvScale.x;
	fz = fz / m_xmvScale.z;
	//높이 맵의 x-좌표와 z-좌표가 높이 맵의 범위를 벗어나면 지형의 높이는 0이다.
	if ((fx < 0.0f) || (fz < 0.0f) || (fx >= m_nWidth) || (fz >= m_nLength)) return(0.0f);
	//높이 맵의 좌표의 정수 부분과 소수 부분을 계산한다.
	int x = (int)fx, z = (int)fz;
	float fxPercent = fx - x, fzPercent = fz - z;

	float fTopLeft = m_pHeightMapImage[x + (z*m_nWidth)];
	float fTopRight = m_pHeightMapImage[(x + 1) + (z*m_nWidth)];
	float fBottomLeft = m_pHeightMapImage[x + ((z + 1)*m_nWidth)];
	float fBottomRight = m_pHeightMapImage[(x + 1) + ((z + 1)*m_nWidth)];

	if (bReverseQuad)
	{
		/*지형의 삼각형들이 오른쪽에서 왼쪽 방향으로 나열되는 경우이다. <그림 12>의 오른쪽은 (fzPercent < fxPercent)인 경우이다. 이 경우 TopLeft의 픽셀 값은 (fTopLeft = fTopRight + (fBottomLeft - fBottomRight))로 근사한다. <그림 12>의 왼쪽은 (fzPercent ≥ fxPercent)인 경우이다. 이 경우 BottomRight의 픽셀 값은 (fBottomRight = fBottomLeft + (fTopRight - fTopLeft))로 근사한다.*/
		if (fzPercent >= fxPercent)
			fBottomRight = fBottomLeft + (fTopRight - fTopLeft);
		else
			fTopLeft = fTopRight + (fBottomLeft - fBottomRight);
	}
	else
	{
		/*지형의 삼각형들이 왼쪽에서 오른쪽 방향으로 나열되는 경우이다. <그림 13>의 왼쪽은 (fzPercent < (1.0f - fxPercent))인 경우이다.
		이 경우 TopRight의 픽셀 값은 (fTopRight = fTopLeft + (fBottomRight - fBottomLeft))로
		근사한다. <그림 13>의 오른쪽은 (fzPercent ≥ (1.0f - fxPercent))인 경우이다.
		이 경우 BottomLeft의 픽셀 값은 (fBottomLeft = fTopLeft + (fBottomRight - fTopRight))로
		근사한다.*/

		if (fzPercent < (1.0f - fxPercent))
			fTopRight = fTopLeft + (fBottomRight - fBottomLeft);
		else
			fBottomLeft = fTopLeft + (fBottomRight - fTopRight);
	}
	//사각형의 네 점을 보간하여 높이(픽셀 값)를 계산한다.
	float fTopHeight = fTopLeft * (1 - fxPercent) + fTopRight * fxPercent;
	float fBottomHeight = fBottomLeft * (1 - fxPercent) + fBottomRight * fxPercent;
	float gScreenHeight = fBottomHeight * (1 - fzPercent) + fTopHeight * fzPercent;
	return(gScreenHeight);
}
class CQuadTreeMesh : public CMeshDetailTexturedIlluminated
{
private:
	struct NodeType
	{
		float positionX, positionZ, width;
		int triangleCount;
		ID3D11Buffer *nIndexBuffer;
		ID3D11Buffer **ppd3dVertexBuffers;
		UINT* pnVertexStrides;
		UINT* pnVertexOffsets;
		int nBuffers;
		array<NodeType*, 4> pNodes;
		//NodeType* nodes[4];
	};

	int m_nWidth;
	int m_nLength;
	XMFLOAT3 m_xmvScale;
	XMFLOAT3* m_pxmf3Normals;
	XMFLOAT2* m_pxmf2TexCoords;
	NodeType* m_pParentNode;
	int m_nTriangleCount;
	UINT m_nDrawCount;


public:
	CQuadTreeMesh(ID3D11Device *pd3dDevice, int nWidth, int nLength, XMFLOAT3 pxmf3Scale = XMFLOAT3(1.0f, 1.0f, 1.0f), void *pContext = nullptr);
	~CQuadTreeMesh();
	virtual void Render(CCamera* pCamera, ID3D11DeviceContext *pd3dDeviceContext);
	void RenderNode(CCamera* pCamera, NodeType* pNode, ID3D11DeviceContext *pd3dDeviceContext);

	void CreateQuadTree(ID3D11Device *pd3dDevice);
	void CalculateMeshDimensions(float& fCenterX, float& fCenterZ, float& fMeshWidth);
	void CreateTreeNode(NodeType* pNode, float, float, float, ID3D11Device* pd3dDevice);
	int CountTriangles(float, float, float);
	bool IsTriangleContained(int, float, float, float);
	void ReleaseNode(NodeType* pNode);
};
CQuadTreeMesh::CQuadTreeMesh(ID3D11Device *pd3dDevice, int nWidth, int nLength, XMFLOAT3 xmf3Scale, void *pContext) : CMeshDetailTexturedIlluminated(pd3dDevice)
{
	// Calculate the number of vertices in the 3D terrain model.
	m_nDrawCount = 0;
	m_nVertices = (nWidth - 1) * (nLength - 1) * 6;
	//m_nIndices = m_nVertices;
	//m_pnIndices = new UINT[m_nIndices];

	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	//unsigned short *pHeightMap = (unsigned short *)pContext;

	CNewHeightMap *pHeightMap = (CNewHeightMap *)pContext;


	m_nWidth = nWidth;
	m_nLength = nLength;
	m_xmvScale = xmf3Scale;


	m_vPositions.resize(m_nVertices);
	m_pxmf3Normals = new XMFLOAT3[m_nVertices];
	m_pxmf2TexCoords = new XMFLOAT2[m_nVertices];

	// 일단 어떤 한 곳에 저장해두어야 한다.


	// Initialize the nIndex into the height map array.
	int nIndex = 0, nIndex1 = 0, nIndex2 = 0, nIndex3 = 0, nIndex4 = 0;
	float tu, tv;

	// Load the 3D terrain model with the height map terrain data.
	//// We will be creating 2 triangles for each of the four points in a quad.
	//for (int j = 0; j<(nLength - 1); j++)
	//{
	//	for (int i = 0; i<(nWidth - 1); i++)
	//	{
	//		// Get the nIndexes to the four points of the quad.
	//		nIndex1 = (nWidth * j) + i;          // Upper left.
	//		nIndex2 = (nWidth * j) + (i + 1);      // Upper right.
	//		nIndex3 = (nWidth * (j + 1)) + i;      // Bottom left.
	//		nIndex4 = (nWidth * (j + 1)) + (i + 1);  // Bottom right.

	//		// Now create two triangles for that quad.
	//		// Triangle 1 - Upper left.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].z;

	//		m_pxmf2TexCoords[nIndex].x = 0.0f;
	//		m_pxmf2TexCoords[nIndex].y = 0.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].nz;

	//		nIndex++;

	//		// Triangle 1 - Upper right.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].z;

	//		m_pxmf2TexCoords[nIndex].x = 1.0f;
	//		m_pxmf2TexCoords[nIndex].y = 0.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].nz;

	//		nIndex++;

	//		// Triangle 1 - Bottom left.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].z;

	//		m_pxmf2TexCoords[nIndex].x = 0.0f;
	//		m_pxmf2TexCoords[nIndex].y = 1.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].nz;
	//		nIndex++;

	//		// Triangle 2 - Bottom left.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].z;

	//		m_pxmf2TexCoords[nIndex].x = 0.0f;
	//		m_pxmf2TexCoords[nIndex].y = 1.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].nz;
	//		nIndex++;

	//		// Triangle 2 - Upper right.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].z;

	//		m_pxmf2TexCoords[nIndex].x = 1.0f;
	//		m_pxmf2TexCoords[nIndex].y = 0.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].nz;
	//		nIndex++;

	//		// Triangle 2 - Bottom right.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].z;

	//		m_pxmf2TexCoords[nIndex].x = 1.0f;
	//		m_pxmf2TexCoords[nIndex].y = 1.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].nz;
	//		nIndex++;
	//	}
	//}





	for (int j = 0; j<(nLength - 1); j++)
	{
		for (int i = 0; i<(nWidth - 1); i++)
		{
			// Get the nIndexes to the four points of the quad.
			nIndex1 = (nLength * j) + i;          // Upper left.
			nIndex2 = (nLength * j) + (i + 1);      // Upper right.
			nIndex3 = (nLength * (j + 1)) + i;      // Bottom left.
			nIndex4 = (nLength * (j + 1)) + (i + 1);  // Bottom right.

													  // Now create two triangles for that quad.
													  // Upper left.
			tv = pHeightMap->m_pHeightMap[nIndex3].tv;

			// Modify the texture coordinates to cover the top edge.
			if (tv == 1.0f) { tv = 0.0f; }
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].z;

			m_pxmf2TexCoords[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].tu;
			m_pxmf2TexCoords[nIndex].y = tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].nz;

			nIndex++;

			// Triangle 1 - Upper right.
			// Upper right.
			tu = pHeightMap->m_pHeightMap[nIndex4].tu;
			tv = pHeightMap->m_pHeightMap[nIndex4].tv;

			// Modify the texture coordinates to cover the top and right edge.
			if (tu == 0.0f) { tu = 1.0f; }
			if (tv == 1.0f) { tv = 0.0f; }

			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].z;

			m_pxmf2TexCoords[nIndex].x = tu;
			m_pxmf2TexCoords[nIndex].y = tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].nz;

			nIndex++;

			// Triangle 1 - Bottom left.
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].z;

			m_pxmf2TexCoords[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].tu;
			m_pxmf2TexCoords[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].nz;
			nIndex++;

			// Triangle 2 - Bottom left.
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].z;

			m_pxmf2TexCoords[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].tu;
			m_pxmf2TexCoords[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].nz;
			nIndex++;

			// Triangle 2 - Upper right.
			// Upper right.
			tu = pHeightMap->m_pHeightMap[nIndex4].tu;
			tv = pHeightMap->m_pHeightMap[nIndex4].tv;

			// Modify the texture coordinates to cover the top and right edge.
			if (tu == 0.0f) { tu = 1.0f; }
			if (tv == 1.0f) { tv = 0.0f; }


			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].z;

			m_pxmf2TexCoords[nIndex].x = tu;
			m_pxmf2TexCoords[nIndex].y = tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].nz;
			nIndex++;

			// Triangle 2 - Bottom right.
			// Bottom right.
			tu = pHeightMap->m_pHeightMap[nIndex2].tu;

			// Modify the texture coordinates to cover the right edge.
			if (tu == 0.0f) { tu = 1.0f; }

			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].z;

			m_pxmf2TexCoords[nIndex].x = tu;
			m_pxmf2TexCoords[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].nz;
			nIndex++;
		}
	}



	/*for (int i = 0; i < m_nVertices; ++i)
	m_pnIndices[i] = i;*/



	CreateQuadTree(pd3dDevice);



	/*D3D11_RASTERIZER_DESC d3dRasterizerDesc;
	d3dRasterizerDesc.AntialiasedLineEnable = false;
	d3dRasterizerDesc.CullMode = D3D11_CULL_BACK;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = true;
	d3dRasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
	d3dRasterizerDesc.FrontCounterClockwise = false;
	d3dRasterizerDesc.MultisampleEnable = false;
	d3dRasterizerDesc.ScissorEnable = false;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;

	pd3dDevice->CreateRasterizerState(&d3dRasterizerDesc, &m_pd3dRasterizerState);*/



	XNA::AxisAlignedBox pBoundingBox;
	XNA::ComputeBoundingAxisAlignedBoxFromPoints(&pBoundingBox, m_nVertices, &m_vPositions[0], sizeof(XMFLOAT3));


	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(pBoundingBox.Center.x - pBoundingBox.Extents.x,
		pBoundingBox.Center.y - pBoundingBox.Extents.y, pBoundingBox.Center.z - pBoundingBox.Extents.z);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(pBoundingBox.Center.x + pBoundingBox.Extents.x,
		pBoundingBox.Center.y + pBoundingBox.Extents.y, pBoundingBox.Center.z + pBoundingBox.Extents.z);



}

CQuadTreeMesh::~CQuadTreeMesh()
{
	if (m_pParentNode)
	{
		ReleaseNode(m_pParentNode);
		delete m_pParentNode;
	}
}
void CQuadTreeMesh::ReleaseNode(NodeType* pParentNode)
{
	// Recursively go down the tree and release the bottom nodes first.
	for (int i = 0; i<4; i++)
	{
		if (pParentNode->pNodes[i] != 0)
		{
			ReleaseNode(pParentNode->pNodes[i]);
		}
	}

	// Release the vertex buffer for this node.
	if (pParentNode->ppd3dVertexBuffers)
	{
		delete[]pParentNode->ppd3dVertexBuffers;
	}

	// Release the nIndex buffer for this node.
	if (pParentNode->nIndexBuffer)
	{
		pParentNode->nIndexBuffer->Release();
	}

	// Release the four child nodes.
	for (int i = 0; i<4; i++)
	{
		if (pParentNode->pNodes[i])
		{
			delete pParentNode->pNodes[i];
		}
	}

}

void CQuadTreeMesh::Render(CCamera* pCamera, ID3D11DeviceContext *pd3dDeviceContext)
{
	m_nDrawCount = 0;

	// Render each node that is visible starting at the parent node and moving down the tree.

	RenderNode(pCamera, m_pParentNode, pd3dDeviceContext);
	cout << "DrawCount : " << m_nDrawCount << endl;
}
void CQuadTreeMesh::RenderNode(CCamera* pCamera, NodeType* pNode, ID3D11DeviceContext *pd3dDeviceContext)
{
	bool result;
	int count, nIndexCount;
	unsigned int stride, offset;


	// Check to see if the node can be viewed, height doesn't matter in a quad tree.
	AABB bcBoundingCube;
	bcBoundingCube.m_xmf3Maximum = XMFLOAT3(pNode->positionX + pNode->width / 2, 0.0f, pNode->positionZ + pNode->width / 2);
	bcBoundingCube.m_xmf3Minimum = XMFLOAT3(pNode->positionX - pNode->width / 2, 0.0f, pNode->positionZ - pNode->width / 2);
	if (pCamera->IsInFrustum(&bcBoundingCube))
	{
		// If it can be seen then check all four child nodes to see if they can also be seen.
		count = 0;
		for (int i = 0; i < 4; i++)
		{
			if (pNode->pNodes[i] != 0)
			{
				count++;
				RenderNode(pCamera, pNode->pNodes[i], pd3dDeviceContext);
			}
		}
	}
	else
		return;




	// If there were any children nodes then there is no need to continue as parent nodes won't contain any triangles to render.
	if (count != 0)
	{
		return;
	}


	//메쉬의 정점은 여러 개의 정점 버퍼로 표현된다.
	pd3dDeviceContext->IASetVertexBuffers(m_nSlot, 3, pNode->ppd3dVertexBuffers, pNode->pnVertexStrides, pNode->pnVertexOffsets);
	pd3dDeviceContext->IASetIndexBuffer(pNode->nIndexBuffer, m_dxgiIndexFormat, m_nIndexOffset);
	pd3dDeviceContext->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	pd3dDeviceContext->RSSetState(m_pd3dRasterizerState);

	//if (pNode->nIndexBuffer)
	pd3dDeviceContext->DrawIndexed(pNode->triangleCount * 3, 0, 0);




	// Increase the count of the number of polygons that have been rendered during this frame.
	m_nDrawCount += pNode->triangleCount;
}
void CQuadTreeMesh::CreateQuadTree(ID3D11Device *pd3dDevice)
{
	float fCenterX, fCenterZ, fMeshWidth;
	m_nTriangleCount = m_nVertices / 3;
	CalculateMeshDimensions(fCenterX, fCenterZ, fMeshWidth);

	// Create the parent node for the quad tree.
	m_pParentNode = new NodeType;


	// Recursively build the quad tree based on the vertex list data and mesh dimensions.
	CreateTreeNode(m_pParentNode, fCenterX, fCenterZ, fMeshWidth, pd3dDevice);

}
void CQuadTreeMesh::CalculateMeshDimensions(float& fCenterX, float& fCenterZ, float& fMeshWidth)
{
	int i;
	float maxWidth, maxDepth, minWidth, minDepth, width, depth, maxX, maxZ;


	// Initialize the center position of the mesh to zero.
	fCenterX = 0.0f;
	fCenterZ = 0.0f;

	// Sum all the vertices in the mesh.
	for (i = 0; i<m_nVertices; i++)
	{
		fCenterX += m_vPositions[i].x;
		fCenterZ += m_vPositions[i].z;
	}

	// And then divide it by the number of vertices to find the mid-point of the mesh.
	fCenterX = fCenterX / (float)m_nVertices;
	fCenterZ = fCenterZ / (float)m_nVertices;

	// Initialize the maximum and minimum size of the mesh.
	maxWidth = 0.0f;
	maxDepth = 0.0f;

	minWidth = fabsf(m_vPositions[i].x - fCenterX);
	minDepth = fabsf(m_vPositions[i].z - fCenterZ);

	// Go through all the vertices and find the maximum and minimum width and depth of the mesh.
	for (i = 0; i<m_nVertices; i++)
	{
		width = fabsf(m_vPositions[i].x - fCenterX);
		depth = fabsf(m_vPositions[i].z - fCenterZ);

		if (width > maxWidth) { maxWidth = width; }
		if (depth > maxDepth) { maxDepth = depth; }
		if (width < minWidth) { minWidth = width; }
		if (depth < minDepth) { minDepth = depth; }
	}

	// Find the absolute maximum value between the min and max depth and width.
	maxX = (float)max(fabs(minWidth), fabs(maxWidth));
	maxZ = (float)max(fabs(minDepth), fabs(maxDepth));

	// Calculate the maximum diameter of the mesh.
	fMeshWidth = max(maxX, maxZ) * 2.0f;

	return;
}


void CQuadTreeMesh::CreateTreeNode(NodeType* pNode, float positionX, float positionZ, float width, ID3D11Device*  pd3dDevice)
{
	int nNumTriangles, nCount, nVertexCount, nIndex, nVertexIndex;
	float fOffsetX, fOffsetZ;

	unsigned int* indices;
	bool result;



	// Store the node position and size.
	pNode->positionX = positionX;
	pNode->positionZ = positionZ;
	pNode->width = width;
	pNode->pnVertexOffsets = nullptr;
	pNode->nIndexBuffer = nullptr;
	pNode->pnVertexStrides = nullptr;
	pNode->ppd3dVertexBuffers = nullptr;


	// Initialize the triangle nCount to zero for the node.
	pNode->triangleCount = 0;

	// Initialize the vertex and nIndex buffer to null.

	pNode->nIndexBuffer = 0;

	// Initialize the children nodes of this node to null.
	for (int i = 0; i<4; ++i)
		pNode->pNodes[i] = nullptr;

	// Count the number of triangles that are inside this node.
	nNumTriangles = CountTriangles(positionX, positionZ, width);

	// Case 1: If there are no triangles in this node then return as it is empty and requires no processing.
	if (nNumTriangles == 0)
	{
		return;
	}

	// Case 2: If there are too many triangles in this node then split it into four equal sized smaller tree nodes.
	if (nNumTriangles > MAX_TRIANGLES)
	{
		for (int i = 0; i<4; i++)
		{
			// Calculate the position offsets for the new child node.
			fOffsetX = (((i % 2) < 1) ? -1.0f : 1.0f) * (width / 4.0f);
			fOffsetZ = (((i % 4) < 2) ? -1.0f : 1.0f) * (width / 4.0f);

			// See if there are any triangles in the new node.
			nCount = CountTriangles((positionX + fOffsetX), (positionZ + fOffsetZ), (width / 2.0f));
			if (nCount > 0)
			{
				// If there are triangles inside where this new node would be then create the child node.
				pNode->pNodes[i] = new NodeType;

				// Extend the tree starting from this new child node now.
				CreateTreeNode(pNode->pNodes[i], (positionX + fOffsetX), (positionZ + fOffsetZ), (width / 2.0f), pd3dDevice);
			}
		}

		return;
	}

	// Case 3: If this node is not empty and the triangle nCount for it is less than the max then 
	// this node is at the bottom of the tree so create the list of triangles to store in it.
	pNode->triangleCount = nNumTriangles;

	// Calculate the number of vertices.
	nVertexCount = nNumTriangles * 3;

	// Create the vertex array.
	XMFLOAT3 *pxmf3Positions = new XMFLOAT3[nVertexCount];
	XMFLOAT3 *pxmf3Normals = new XMFLOAT3[nVertexCount];
	XMFLOAT2 *pxmf2TexCoords = new XMFLOAT2[nVertexCount];

	// Create the nIndex array.
	indices = new unsigned int[nVertexCount];

	// Initialize the nIndex for this new vertex and nIndex array.
	nIndex = 0;

	// Go through all the triangles in the vertex list.
	for (int i = 0; i<m_nTriangleCount; i++)
	{
		// If the triangle is inside this node then add it to the vertex array.
		result = IsTriangleContained(i, positionX, positionZ, width);
		if (result == true)
		{
			// Calculate the nIndex into the terrain vertex list.
			nVertexIndex = i * 3;

			// Get the three vertices of this triangle from the vertex list.
			pxmf3Positions[nIndex] = m_vPositions[nVertexIndex];
			pxmf2TexCoords[nIndex] = m_pxmf2TexCoords[nVertexIndex];
			pxmf3Normals[nIndex] = m_pxmf3Normals[nVertexIndex];
			indices[nIndex] = nIndex;
			nIndex++;

			nVertexIndex++;
			pxmf3Positions[nIndex] = m_vPositions[nVertexIndex];
			pxmf2TexCoords[nIndex] = m_pxmf2TexCoords[nVertexIndex];
			pxmf3Normals[nIndex] = m_pxmf3Normals[nVertexIndex];
			indices[nIndex] = nIndex;
			nIndex++;

			nVertexIndex++;
			pxmf3Positions[nIndex] = m_vPositions[nVertexIndex];
			pxmf2TexCoords[nIndex] = m_pxmf2TexCoords[nVertexIndex];
			pxmf3Normals[nIndex] = m_pxmf3Normals[nVertexIndex];
			indices[nIndex] = nIndex;
			nIndex++;
		}
	}



	// Now finally create the vertex buffer.

	D3D11_BUFFER_DESC d3dBufferDesc;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ID3D11Buffer *pd3dPositionBuffer = nullptr;
	ID3D11Buffer *pd3dNormalBuffer = nullptr;
	ID3D11Buffer *pd3dTexCoordBuffer = nullptr;

	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * nVertexCount;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;

	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = pxmf3Positions;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &pd3dPositionBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * nVertexCount;
	d3dBufferData.pSysMem = pxmf3Normals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &pd3dNormalBuffer);

	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * nVertexCount;
	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &pd3dTexCoordBuffer);


	ID3D11Buffer *pd3dBuffers[3] = { pd3dPositionBuffer, pd3dNormalBuffer, pd3dTexCoordBuffer };
	UINT pnBufferStrides[3] = { sizeof(XMFLOAT3), sizeof(XMFLOAT3), sizeof(XMFLOAT2) };
	UINT pnBufferOffsets[3] = { 0, 0, 0 };


	pNode->ppd3dVertexBuffers = new ID3D11Buffer*[3];
	pNode->pnVertexStrides = new UINT[3];
	pNode->pnVertexOffsets = new UINT[3];
	pNode->nBuffers = 3;

	for (int i = 0; i < 3; i++)
	{
		pNode->ppd3dVertexBuffers[i] = pd3dBuffers[i];
		pNode->pnVertexStrides[i] = pnBufferStrides[i];
		pNode->pnVertexOffsets[i] = pnBufferOffsets[i];
	}



	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	// Set up the description of the nIndex buffer.
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(unsigned int) *nVertexCount;
	d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	d3dBufferDesc.MiscFlags = 0;
	d3dBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the nIndex data.
	d3dBufferData.pSysMem = indices;
	d3dBufferData.SysMemPitch = 0;
	d3dBufferData.SysMemSlicePitch = 0;

	// Create the nIndex buffer.
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &pNode->nIndexBuffer);

	// Release the vertex and nIndex arrays now that the data is stored in the buffers in the node.
	delete[] pxmf3Positions;
	delete[] pxmf2TexCoords;
	delete[] pxmf3Normals;



	delete[] indices;
	indices = 0;

	return;
}
int CQuadTreeMesh::CountTriangles(float positionX, float positionZ, float width)
{
	int count, i;
	bool result;


	// Initialize the count to zero.
	count = 0;

	// Go through all the triangles in the entire mesh and check which ones should be inside this node.
	for (i = 0; i<m_nTriangleCount; i++)
	{
		// If the triangle is inside the node then increment the count by one.
		result = IsTriangleContained(i, positionX, positionZ, width);
		if (result == true)
		{
			count++;
		}
	}

	return count;
}
bool CQuadTreeMesh::IsTriangleContained(int nIndex, float positionX, float positionZ, float width)
{
	float radius;
	int vertexIndex;
	float x1, z1, x2, z2, x3, z3;
	float minimumX, maximumX, minimumZ, maximumZ;


	// Calculate the radius of this node.
	radius = width / 2.0f;

	// Get the nIndex into the vertex list.
	vertexIndex = nIndex * 3;

	// Get the three vertices of this triangle from the vertex list.
	x1 = m_vPositions[vertexIndex].x;
	z1 = m_vPositions[vertexIndex].z;
	vertexIndex++;

	x2 = m_vPositions[vertexIndex].x;
	z2 = m_vPositions[vertexIndex].z;
	vertexIndex++;

	x3 = m_vPositions[vertexIndex].x;
	z3 = m_vPositions[vertexIndex].z;

	// Check to see if the minimum of the x coordinates of the triangle is inside the node.
	minimumX = min(x1, min(x2, x3));
	if (minimumX > (positionX + radius))
	{
		return false;
	}

	// Check to see if the maximum of the x coordinates of the triangle is inside the node.
	maximumX = max(x1, max(x2, x3));
	if (maximumX < (positionX - radius))
	{
		return false;
	}

	// Check to see if the minimum of the z coordinates of the triangle is inside the node.
	minimumZ = min(z1, min(z2, z3));
	if (minimumZ >(positionZ + radius))
	{
		return false;
	}

	// Check to see if the maximum of the z coordinates of the triangle is inside the node.
	maximumZ = max(z1, max(z2, z3));
	if (maximumZ < (positionZ - radius))
	{
		return false;
	}

	return true;
}

class CQuadTree : public CGameObject
{
private:
	CNewHeightMap *m_pHeightMap;
	XMFLOAT3 m_xmvScale;
public:
	CQuadTree(ID3D11Device *pd3dDevice, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale);
	virtual ~CQuadTree();
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
};


CQuadTree::CQuadTree(ID3D11Device *pd3dDevice, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale) : CGameObject(1)
{
	m_pHeightMap = new CNewHeightMap(pFileName, nWidth, nLength, xmf3Scale);
	m_xmvScale = xmf3Scale;

	//CQuadTreeMesh* pQuadTreeMesh = new CQuadTreeMesh(pd3dDevice, nWidth-1, nLength-1, xmf3Scale, m_pHeightMap);
	CQuadTreeMesh* pQuadTreeMesh = new CQuadTreeMesh(pd3dDevice, nWidth, nLength, xmf3Scale, m_pHeightMap);

	SetMesh(pQuadTreeMesh, 0);
	/*float fCenterX = (pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.x
	+ pHeightMapGridMesh->GetBoundingCube().m_xmf3Minimum.x) / 2;
	float fCenterY = (pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.y
	+ pHeightMapGridMesh->GetBoundingCube().m_xmf3Minimum.y) / 2;
	float fCenterZ = (pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.z
	+ pHeightMapGridMesh->GetBoundingCube().m_xmf3Minimum.z) / 2;





	XMFLOAT3 xmf3Center(fCenterX, fCenterY, fCenterZ);
	XMFLOAT3 xmf3Extent(pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.x - fCenterX,
	pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.y - fCenterY,
	pHeightMapGridMesh->GetBoundingCube().m_xmf3Maximum.z - fCenterZ);
	CAxisMesh* pAxisMesh = new CAxisMesh(pd3dDevice, xmf3Center, xmf3Extent);
	SetMesh(pAxisMesh, 1);*/



}

CQuadTree::~CQuadTree()
{
	if (m_pHeightMap) delete m_pHeightMap;
}


void CQuadTree::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	m_pTransform->UpdateTransform();
	CShader::UpdateShaderVariable(pd3dDeviceContext, &m_pTransform->m_xmmtxWorld);
	if (m_pMaterial)
		CIlluminatedShader::UpdateShaderVariable(pd3dDeviceContext, &m_pMaterial->m_Material);
	//객체의 텍스쳐를 쉐이더 변수에 설정(연결)한다.
	if (m_pTexture) m_pTexture->UpdateShaderVariable(pd3dDeviceContext);
	if (m_ppMeshes)
	{
		//m_ppMeshes[0]
		//if(pCamera->IsInFrustum())
		m_ppMeshes[0]->Render(pCamera, pd3dDeviceContext);

	}
}