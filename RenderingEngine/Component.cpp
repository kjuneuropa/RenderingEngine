#include "Component.h"
#include "Renderer.h"
#include "Terrain.h"
#include "GameFramework.h"
#include "SkyBox.h"
#include "Player.h"




_component_identifier<CComponent, null_t, false> CComponent::identifier;

template <class component_t>
ComponentType CComponent::Type_To_Enum() 
{ 
	return ComponentType_Unknown; 
}
#define REGISTER_COMPONENT(component_t, enumT)  template<>ComponentType CComponent::Type_To_Enum<component_t>() { return enumT; }
// Explicit template instantiation
REGISTER_COMPONENT(CTransform, ComponentType_Transform);
REGISTER_COMPONENT(CSkyBox, ComponentType_Skybox);
REGISTER_COMPONENT(CGuiText, ComponentType_GuiText);
REGISTER_COMPONENT(CPlayer, ComponentType_Renderer);
REGISTER_COMPONENT(CTerrainPlayer, ComponentType_Renderer);
REGISTER_COMPONENT(CTerrain, ComponentType_Renderer);
REGISTER_COMPONENT(CRenderer, ComponentType_Renderer);
//template <class component_t>
//ComponentType CComponent::Type_To_Enum() { return ComponentType_Unknown; }

//#define COMPOMEMT_ID(component_t, enum_T) template<>



vector<Font*>CGuiText::m_Fonts;

void  CGuiText::Initalize(ID3D11Device *pd3dDevice)
{

}
void  CGuiText::Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{

}


void CGuiText::LoadData(ID3D11Device *pd3dDevice, wstring& strFontFile, wstring& strTextureFile)
{
	Letter* pLetters = nullptr;
	CTexture* pTexture = nullptr;
	Font* pFont = new Font;
	LoadFont(strFontFile, pLetters);
	LoadTexture(pd3dDevice, strTextureFile, pTexture);
	
	pFont->pLetters = pLetters;
	pFont->pTexture = pTexture;
	m_Fonts.push_back(pFont);

}
void CGuiText::LoadFont(wstring& strFontFile, Letter* pLetters)
{
	ifstream fin;
	char temp;

	// Create the font spacing buffer.
	 pLetters = new Letter[95];

	// Read in the font size and spacing between chars.
	fin.open(strFontFile);

	// Read in the 95 used ascii characters for text.
	for (int i = 0; i<95; i++)
	{
		fin.get(temp);
		while (temp != ' ')
		{
			fin.get(temp);
		}
		fin.get(temp);
		while (temp != ' ')
		{
			fin.get(temp);
		}

		fin >> pLetters[i].fLeft;
		fin >> pLetters[i].fRight;
		fin >> pLetters[i].nSize;

	}

	fin.close();
}
void CGuiText::LoadTexture(ID3D11Device *pd3dDevice, wstring& strTextureFile, CTexture* pTexture)
{
	ID3D11SamplerState *pd3dBaseSamplerState = nullptr;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;

	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dBaseSamplerState);

	pTexture = new CTexture(1, 1, 0, 0);
	ID3D11ShaderResourceView *pd3dsrvTexture = nullptr; // dirt01d.tga
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, strTextureFile.c_str(), nullptr, nullptr, &pd3dsrvTexture, nullptr);

	pTexture->SetTexture(0, pd3dsrvTexture);
	pTexture->SetSampler(0, pd3dBaseSamplerState);

	pd3dsrvTexture->Release();
	pd3dBaseSamplerState->Release();
}




void CGuiText::BuildVertexAarry(void* pVertexs, const wchar_t* pText, float fPosX, float fPosY)
{
	VertexType* pVertexPtr;
	int nLetterNum, nIndex, nLetter;


	// Coerce the input vertices into a VertexType structure.
	pVertexPtr = static_cast<VertexType*>(pVertexs);


	// Get the number of nLetters in the sentence.
	nLetterNum = wcslen(pText);

	// Initialize the nIndex to the vertex array.
	nIndex = 0;

	// Draw each nLetter onto a quad.
	for (int i = 0; i<nLetterNum; i++)
	{
		nLetter = static_cast<int>(pText[i] - 32);

		// If the nLetter is a space then just move over three pixels.
		if (nLetter == 0)
		{
			fPosX = fPosX + 3.0f;
		}
		else
		{

			// First triangle in quad.
			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX), FromToNDC_Y(fPosY), 0.0f);  // Top left.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_Fonts[m_nFontType]->pLetters[nLetter].fLeft, 0.0f);
			nIndex++;

			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX + m_Fonts[m_nFontType]->pLetters[nLetter].nSize), FromToNDC_Y(fPosY + 16), 0.0f);  // Bottom right.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_Fonts[m_nFontType]->pLetters[nLetter].fRight, 1.0f);
			nIndex++;

			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX), FromToNDC_Y(fPosY + 16), 0.0f);  // Bottom left.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_Fonts[m_nFontType]->pLetters[nLetter].fLeft, 1.0f);
			nIndex++;
			// Second triangle in quad.
			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX), FromToNDC_Y(fPosY), 0.0f);  // Top left.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_Fonts[m_nFontType]->pLetters[nLetter].fLeft, 0.0f);
			nIndex++;

			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX + m_Fonts[m_nFontType]->pLetters[nLetter].nSize), FromToNDC_Y(fPosY), 0.0f);  // Top right.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_Fonts[m_nFontType]->pLetters[nLetter].fRight, 0.0f);
			nIndex++;

			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX + m_Fonts[m_nFontType]->pLetters[nLetter].nSize), FromToNDC_Y(fPosY + 16), 0.0f);  // Bottom right.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_Fonts[m_nFontType]->pLetters[nLetter].fRight, 1.0f);
			nIndex++;


			// Update the x location for drawing by the size of the nLetter and one pixel.
			fPosX = fPosX + m_Fonts[m_nFontType]->pLetters[nLetter].nSize + 1.0f;
		}
	}
}
void  CGuiText::UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext)
{
	if (m_Fonts[m_nFontType]->pTexture)m_Fonts[m_nFontType]->pTexture->UpdateShaderVariable(pd3dDeviceContext);
}
float CGuiText::FromToNDC_X(float fScreenX)
{
	int nWindowWidth = CGameFramework::GetInstance().GetWindowWidth();
	return static_cast<float>(2.0 / nWindowWidth * fScreenX - 1.0f);
}
float CGuiText::FromToNDC_Y(float fScreenY)
{
	int nWindowHeight = CGameFramework::GetInstance().GetWindowHeight();
	return static_cast<float>(-2.0 / nWindowHeight * fScreenY + 1.0f);
}