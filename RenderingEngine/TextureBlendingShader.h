#pragma once

#include "stdafx.h"
#include "Shader.h"
#include "TextureBlendingObject.h"
class CTextureBlendingShader : public CTexturedShader
{
private:
	CTexture *m_pTexture;
public:
	CTextureBlendingShader();
	~CTextureBlendingShader();

	virtual void BuildObjects(ID3D11Device *pd3dDevice, CTexture *pTexture);
	virtual void CreateShader(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
};

