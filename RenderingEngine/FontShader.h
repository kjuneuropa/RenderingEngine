#pragma once
#include "Shader.h"


class CFontShader: public CShader
{
private:
	ID3D11DepthStencilState *m_pd3dDepthStencilState;
	ID3D11BlendState* m_pd3dBlendingState;
public:
	CFontShader();
	virtual ~CFontShader();
	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void CreateShader(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
	void CreateBlendingState(ID3D11Device *pd3dDevice);
	void SetSentence(wchar_t* pFrameStr);
};

