#pragma once
#include "stdafx.h"
class CDiffusedVertex
{
	XMFLOAT3 m_xmf3Position;
	//정점의 색상을 나타내는 멤버 변수(XMFLOAT4 구조체)를 선언한다. 
	XMFLOAT4 m_xmf4Diffuse;
public:
	//생성자와 소멸자를 선언한다.
	CDiffusedVertex(float x, float y, float z, XMFLOAT4 d3dxcDiffuse)
	{
		m_xmf3Position = XMFLOAT3(x, y, z); 
		m_xmf4Diffuse = d3dxcDiffuse; 
	}
	CDiffusedVertex(XMFLOAT3 d3dxvPosition, XMFLOAT4 d3dxcDiffuse)
	{ 
		m_xmf3Position = d3dxvPosition; 
		m_xmf4Diffuse = d3dxcDiffuse; 
	}
	CDiffusedVertex() 
	{ 
		m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		m_xmf4Diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	}
	~CDiffusedVertex() { }

};

