#include "SkyBox.h"
#include "Shader.h"


CSkyBox::CSkyBox()
{
	m_ppMeshes = new CMesh*[m_nMeshes];
	for (int i = 0; i < m_nMeshes; i++)m_ppMeshes[i] = nullptr;
}


CSkyBox::~CSkyBox()
{
}

void CSkyBox::Initalize(ID3D11Device *pd3dDevice)
{

}
void CSkyBox::Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	//스카이 박스 객체의 위치를 카메라의 위치로 변경한다.
	XMFLOAT3 xmf3CameraPos = pCamera->GetPosition();
	//m_pTransform->SetPosition(xmf3CameraPos.x, xmf3CameraPos.y, xmf3CameraPos.z);
	CTransform* pTransform = GetTransform();
	pTransform->SetPosition(xmf3CameraPos.x, xmf3CameraPos.y, xmf3CameraPos.z);

	CShader::UpdateShaderVariable(pd3dDeviceContext, &pTransform->GetWorldMtx());
	//스카이 박스 메쉬(6개의 사각형)를 렌더링한다.

	UpdateTexture(pd3dDeviceContext);
	if (m_ppMeshes && m_ppMeshes[0]) m_ppMeshes[0]->Render(pd3dDeviceContext);
}

