//-----------------------------------------------------------------------------
// File: Scene.h
//-----------------------------------------------------------------------------

#ifndef _SCENE_H_
#define _SCENE_H_

#include "Timer.h"
#include "Shader.h"

#include "CRotatingObject.h"
#include "Mesh.h"
#include "Transform.h"
#include "Inspector.h"
#include "FontShader.h"
#include "Terrain.h"

#define MAX_LIGHTS		4 //4 
#define POINT_LIGHT		1.0f
#define SPOT_LIGHT		2.0f
#define DIRECTIONAL_LIGHT	3.0f
#define VOID 1000.0f

struct LIGHT
{
	XMFLOAT4 m_xmf4Ambient;
	XMFLOAT4 m_xmf4Diffuse;
	XMFLOAT4 m_xmf4Specular;
	XMFLOAT3 m_xmf3Position;
	float m_fRange;
	XMFLOAT3 m_d3dxvDirection;
	float m_nType;
	XMFLOAT3 m_d3dxvAttenuation;
	float m_fFalloff;
	float m_fTheta; //cos(m_fTheta)
	float m_fPhi; //cos(m_fPhi)
	float m_bEnable;
	float padding;
};

//상수 버퍼는 크기가 반드시 16 바이트의 배수가 되어야 한다. 
struct LIGHTS
{
	LIGHT m_pLights[MAX_LIGHTS];
	XMFLOAT4 m_d3dxcGlobalAmbient;
	XMFLOAT4 m_d3dxvCameraPosition;
};

class CScene
{
private:
	//씬은 쉐이더들의 리스트(배열)이다.
	//CShader**		m_ppShaders;
	int				m_nShaders;
	LIGHTS *		m_pLights;
	ID3D11Buffer*	m_pd3dcbLights;
	HWND			m_hInspector;
	CCamera*		m_pCamera;

	static CInspector* m_pInspector;
	vector<CShader*> m_vShaders;
	
public:
    CScene();
    ~CScene();
	void CreateShaderVariables(ID3D11Device *pd3dDevice);
	void UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, LIGHTS *pLights);
	void ReleaseShaderVariables();
	//bool OnProcessingMouseMessage(HWND hWnd, HINSTANCE hInstance, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	bool OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	void OnProcessingMenuMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, ID3D11Device* pd3dDevice);

	void BuildObjects(ID3D11Device *pd3dDevice, ID3D11DeviceContext *pd3dDeviceContext);
	void ReleaseObjects();

	bool ProcessInput();
    void AnimateObjects(float fTimeElapsed);
	void Render(ID3D11DeviceContext*pd3dDeviceContext, CCamera *pCamera);

	static BOOL CALLBACK Dlg_InspectorPorc(HWND hDlg, UINT iMsg,
		WPARAM wParam, LPARAM lParam);

public:
	CGameObject *PickObjectPointedByCursor(int xClient, int yClient);
	void SetCamera(CCamera *pCamera) { m_pCamera = pCamera; }

	bool OnProcessingMouseMessage(HWND hWnd, HINSTANCE hInstance, UINT nMessageID, WPARAM wParam, LPARAM lParam);

	void ActivateNormalMapShader();
	void ActivateBasicShader();


public:
	//CHeightMapTerrain *GetTerrain();
	CTerrain *GetTerrain();

	static CGameObject *m_pSelectedObject;
	static CGameObject *m_pOldSelectedObject;


};

#endif 