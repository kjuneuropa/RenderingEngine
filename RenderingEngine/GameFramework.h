#pragma once

#include "Timer.h"
#include "Scene.h"
#include "Direct3DBase.h"
#include "Cpu.h"
#include "TextManager.h"
#include <memory>
#include "Player.h"

enum
{
	VIDEO_CARD,
	CPU,
	FPS,
	DRAW_VERTEX
};

template<typename T>
void SafeRelease(T& ptr)
{
	if (ptr)
	{
		ptr->Release();
		ptr = nullptr;
	}
}

class AAA : public std::enable_shared_from_this<AAA>
{};

class CGameFramework
{
private:
	static CGameFramework*	m_pInstance;
public:
	static CGameFramework& GetInstance()
	{
		if (m_pInstance == nullptr)
			m_pInstance = new CGameFramework();
		return *m_pInstance;
	}
	CGameFramework();
	~CGameFramework();

	
	bool OnCreate(HINSTANCE hInstance, HWND hMainWnd, HWND hViewWnd, int nViewWidth, int nViewHeight);
	void OnDestroy();



	//디바이스, 스왑 체인, 디바이스 컨텍스트, 디바이스와 관련된 뷰를 생성하는 함수이다.
	//bool CreateRenderTargetView();
	//void SetViewport();
	
	bool CreateDirect3DDisplay();
	void CreateVideoInfo();
    
	//렌더링할 메쉬, 객체를 생성하고 소멸하는 함수이다. 
    void BuildObjects();
    void ReleaseObjects();

	//프레임워크의 핵심(사용자 입력, 애니메이션, 렌더링)을 구성하는 함수이다. 
    void ProcessInput();
    void AnimateObjects();
    void FrameAdvance();

	//윈도우의 메시지(키보드, 마우스 입력)를 처리하는 함수이다. 
	void OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	void OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	void OnProcessingMenuMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	void ResizeAllWindows(int nWidth, int nHeight);
	void RenewDSV(int nWndWidth, int nWndHeight);
	CScene* GetRenderScene() { return m_pScene; }
	int GetWindowWidth() { return m_nWndClientWidth; }
	int GetWindowHeight() { return m_nWndClientHeight; }
	LRESULT CALLBACK OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);

private:
	HINSTANCE					m_hInstance;
	HWND						m_hMainWnd;
	HWND						m_hViewWnd;
	HWND						m_hInspctorWnd;


	int							m_nWndClientWidth;
	int							m_nWndClientHeight;
        
	//디바이스 인터페이스에 대한 포인터이다. 주로 리소스를 생성하기 위하여 필요하다.
	ID3D11Device				*m_pd3dDevice;

	//스왑 체인 인터페이스에 대한 포인터이다. 주로 디스플레이를 제어하기 위하여 필요하다.
	IDXGISwapChain				*m_pDXGISwapChain;

	//렌더 타겟 뷰 인터페이스에 대한 포인터이다. 
	ID3D11RenderTargetView		*m_pd3dRenderTargetView;

	//디바이스 컨텍스트에 대한 포인터이다.주로 파이프라인 설정을 하기 위하여 필요하다.
	ID3D11DeviceContext			*m_pd3dDeviceContext;


	CGameTimer					m_GameTimer;
	CCpu						m_Cpu;
	CScene						*m_pScene;
	CComponent*					m_pComponent;
	CPlayer						*m_pPlayer;
	CTextManager*				m_pTextManager;

	//다음은 프레임 레이트를 주 윈도우의 캡션에 출력하기 위한 문자열이다.
	_TCHAR						m_pszBuffer[50];
	wchar_t						m_pVideoCardName[128];
	int							m_nVideoCardMemory;
private:
	ID3D11Buffer *m_pd3dcbColor;


	//마지막으로 마우스 버튼을 클릭할 때의 마우스 커서의 위치이다.
	POINT	m_ptOldCursorPos;


private:
	ID3D11Texture2D*			m_pd3dDepthStencilBuffer;
	ID3D11DepthStencilView*		m_pd3dDepthStencilView;
	ID3D11ShaderResourceView*	m_pd3dsrvRTT;
	ID3D11RenderTargetView*		m_pd3drtvRTT;
	ID3D11Buffer*				m_pd3dcbPostPrcessIndex;
	
	CTexture*					m_pTexture;
	CRenderToTextureShader*		m_pRenderToTextureShader;
	int m_nBlurIntensity = 1;
	int m_nPostProcess = 0;


public:
	bool CreateRenderTargetDepthStencilView();
	void SetRenderToTexture();
	void SetSentence();
	void CreateRenderToTexture();
	void CreateShaderVariables();
	void UpdateShaderVariable(int nIndex);
	CTextManager* GetTextManager() { return m_pTextManager; }
public:
	CPlayerShader *m_pPlayerShader;
	CShader* m_pSkyBoxShader;

private:
	CCamera *m_pCamera;

};


