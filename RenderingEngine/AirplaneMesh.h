#pragma once
#include "Mesh.h"
#include "stdafx.h"
#include "DiffusedVertex.h"
#include "Vertex.h"
class CAirplaneMeshDiffused : public CMeshDiffused
{
public:
	CAirplaneMeshDiffused(ID3D11Device *pd3dDevice, float gScreenWidth = 20.0f, float gScreenHeight = 20.0f, float fDepth = 4.0f, XMFLOAT4 d3dxColor = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f));
	virtual ~CAirplaneMeshDiffused();
};