#include "FontShader.h"
#include "GameFramework.h"


CFontShader::CFontShader()
{
}


CFontShader::~CFontShader()
{
	CSentenceObject::ReleaseFont();
}

void CFontShader::BuildObjects(ID3D11Device *pd3dDevice)
{
	
	//CGamefrarmwork를 모르는 코드
	
	
	CSentenceObject* pTextObject1 = new CSentenceObject(pd3dDevice, 0, 0, 120, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	CSentenceObject* pTextObject2 = new CSentenceObject(pd3dDevice, 0, 16, 20, XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f));
	CSentenceObject* pTextObject3 = new CSentenceObject(pd3dDevice, 0, 32, 20, XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
	CSentenceObject* pTextObject4 = new CSentenceObject(pd3dDevice, 0, 48, 40, XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	
	//pTextObject1->U
	AddObject(pTextObject1);
	AddObject(pTextObject2);
	AddObject(pTextObject3);
	AddObject(pTextObject4);

	CreateBlendingState(pd3dDevice);




	/*CGameObject* pOb = new CGameObject(1);
	pOb->InsertComponent<CGuiText>();
	wstring strFontFile = L"Font/fontdata.txt";
	wstring strTextureFile = L"Font/font.dds";
	pOb->GetComponent<CGuiText>()->LoadData(pd3dDevice, strFontFile, strTextureFile);*/

	
 }
												
void CFontShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }

	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Font.fx", "VSFont", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Font.fx", "PSFont", "ps_5_0", &GetPixelShader());
}
void CFontShader::CreateBlendingState(ID3D11Device *pd3dDevice)
{
	D3D11_BLEND_DESC d3dBlendDesc;
	ZeroMemory(&d3dBlendDesc, sizeof(d3dBlendDesc));
	d3dBlendDesc.AlphaToCoverageEnable = false;
	d3dBlendDesc.IndependentBlendEnable = false;
	d3dBlendDesc.RenderTarget[0].BlendEnable = true;
	d3dBlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	d3dBlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	d3dBlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	D3D11_COLOR_WRITE_ENABLE_ALPHA;
	D3D11_COLOR_WRITE_ENABLE_ALL;

	pd3dDevice->CreateBlendState(&d3dBlendDesc, &m_pd3dBlendingState);
}
void CFontShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	OnPrepareRender(pd3dDeviceContext);
	float blendFactors[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	auto vSentence =  CGameFramework::GetInstance().GetTextManager()->GetSenteces();
	pd3dDeviceContext->OMSetBlendState(m_pd3dBlendingState, blendFactors, 0xfffffff);
	/*for (int i=0; i<m_vObjects.size(); ++i)
	{
		m_vObjects[i]->Render(pd3dDeviceContext, pCamera, vSentence[i]);
	}*/
		

	pd3dDeviceContext->OMSetDepthStencilState(nullptr, 1);
	pd3dDeviceContext->OMSetBlendState(0, nullptr, 0xffffffff);;
 }
void CFontShader::SetSentence(wchar_t* pFrameStr)
{
	//m_vObjects[0]->
}