#pragma once
#include "stdafx.h"
#include "Object.h"
#include "Transform.h"
#include "resource.h"

class CScene;

class CInspector
{
private:
	ID3D11Device*	m_pd3dDevice;
	CScene* m_pScene;
public:
	CInspector(ID3D11Device *pd3dDevice, CScene* pScene);
	~CInspector();
	bool OnProcessingDlgCommandMessage(HWND hDlg, UINT iMsg,
		WPARAM wParam, LPARAM lParam, CGameObject* pSelectedObject, CGameObject* pOldSelectedObject);
	
	bool LoadTexture(HWND hWnd, CGameObject* pSelectedObject);
	void LoadNormalMap(HWND hWnd, CGameObject* pSelectedObject);
	void RemoveNormalMap(CGameObject* pSelectedObject);
	


};

