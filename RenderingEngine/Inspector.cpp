#include "Inspector.h"
#include "Scene.h"



CInspector::CInspector(ID3D11Device *pd3dDevice, CScene* pScene)
{
	m_pd3dDevice = pd3dDevice;
	m_pScene = pScene;

}


CInspector::~CInspector()
{
}
bool CInspector::LoadTexture(HWND hWnd, CGameObject* pSelectedObject)
{
	OPENFILENAME OFN;
	wchar_t str[300];
	wchar_t lpstrFile[MAX_PATH] = L"";

	memset(&OFN, 0, sizeof(OPENFILENAME));
	OFN.lStructSize = sizeof(OPENFILENAME);
	OFN.hwndOwner = hWnd;
	OFN.nFilterIndex = 2;
	OFN.lpstrFilter = L"텍스쳐 파일(*.jpg;*.bmp;*.png;*.dds)\0*.jpg;*.bmp;*.PNG;*.dds\0\0";
	OFN.lpstrFile = lpstrFile;
	OFN.nMaxFile = MAX_PATH;



	// 그냥 바꾸니깐 문제가 생기는데?

	// 텍스쳐 컨테이너에 저장해서
	// 이미 존재하는 것이라면 그냥 쓰고
	// 그게 아니면 가져와서 쓸까?

	if (GetOpenFileName(&OFN) != 0) 
	{
		wcout << OFN.lpstrFile << endl;
		ID3D11ShaderResourceView *pd3dsrvTexture = nullptr;
		if (FAILED(D3DX11CreateShaderResourceViewFromFile(m_pd3dDevice, OFN.lpstrFile, nullptr, nullptr, &pd3dsrvTexture, nullptr)))
		{
			return false;
		}
	
		
		auto renderer = pSelectedObject->GetComponent<CRenderer>();
		renderer->ReplaceTexture(pd3dsrvTexture);
		pd3dsrvTexture->Release();
		return true;

	}
	return false;
}
void CInspector::LoadNormalMap(HWND hWnd, CGameObject* pSelectedObject)
{
	OPENFILENAME OFN;
	wchar_t str[300];
	wchar_t lpstrFile[MAX_PATH] = L"";

	memset(&OFN, 0, sizeof(OPENFILENAME));
	OFN.lStructSize = sizeof(OPENFILENAME);
	OFN.hwndOwner = hWnd;
	OFN.nFilterIndex = 2;
	OFN.lpstrFilter = L"텍스쳐 파일(*.jpg;*.bmp;*.png;*.dds)\0*.jpg;*.bmp*.png;*.dds\0\0";
	OFN.lpstrFile = lpstrFile;
	OFN.nFilterIndex = 3;
	OFN.nMaxFile = MAX_PATH;


	if (GetOpenFileName(&OFN) != 0)
	{
		ID3D11ShaderResourceView *pd3dsrvTexture = nullptr;
		D3DX11CreateShaderResourceViewFromFile(m_pd3dDevice, OFN.lpstrFile, nullptr, nullptr, &pd3dsrvTexture, nullptr);
		//pSelectedObject->GetTexture()->AddNormalMap(pd3dsrvTexture);
		pd3dsrvTexture->Release();
		
		CheckDlgButton(hWnd, IDC_ACTIVE, BST_CHECKED);
		// 노말맵 셰이더로 오브젝트 옮긴다.
		m_pScene->ActivateNormalMapShader();
	}
}
void CInspector::RemoveNormalMap(CGameObject* pSelectedObject)
{
	m_pScene->ActivateBasicShader();
	
}
bool CInspector::OnProcessingDlgCommandMessage(HWND hDlg, UINT iMsg,
	WPARAM wParam, LPARAM lParam, CGameObject* pSelectedObject, CGameObject* pOldSelectedObject)
{
	CTransform* pTransform;

	
	if (pSelectedObject)
		pTransform = pSelectedObject->GetComponent<CTransform>();
	wchar_t buffer[100];
	switch (LOWORD(wParam))
	{
	case IDOK:
	{
		pSelectedObject = nullptr;
		pOldSelectedObject = nullptr;
		DestroyWindow(hDlg);
		break;
	}

	case IDCANCEL:
	{
		pSelectedObject = nullptr;
		pOldSelectedObject = nullptr;
		DestroyWindow(hDlg);
		hDlg = 0;
		return 1;
	}

	case IDOK_RESET:
	{
		//m_pSelectedObject->ResetRotation();
		break;
	}

	case WM_DROPFILES:
	{
		cout << "파일" << endl;
		break;
	}

	case IDC_LOAD_TEXTURE:
	{
		switch (HIWORD(wParam))
		{
		case BN_CLICKED:
		{
			LoadTexture(hDlg, pSelectedObject);
			break;
		}
		}
		break;
	}
	case IDC_LOAD_NORMAL:
	{
		switch (HIWORD(wParam))
		{
		case BN_CLICKED:
		{
			LoadNormalMap(hDlg, pSelectedObject);
			break;
		}
		}
		break;
	}
	case IDC_ACTIVE:
	{
		if (IsDlgButtonChecked(hDlg, IDC_ACTIVE) == BST_CHECKED)
		{
			LoadNormalMap(hDlg, pSelectedObject);
		}
		else
		{
			RemoveNormalMap(pSelectedObject);
		}
		break;
	}
	
	case IDC_EDIT_POS_X:
	{
		switch (HIWORD(wParam))
		{
		case EN_CHANGE:
		{
			if (pSelectedObject)
			{
				GetDlgItemText(hDlg, IDC_EDIT_POS_X, buffer, 6);
				float fValue = static_cast<float>(_tcstod(buffer, nullptr));
				pTransform->SetPositionX(fValue);
			}
			break;
		}
		}
		break;
	}
	case IDC_EDIT_POS_Y:
	{
		switch (HIWORD(wParam))
		{
		case EN_CHANGE:
		{
			if (pSelectedObject)
			{
				GetDlgItemText(hDlg, IDC_EDIT_POS_Y, buffer, 6);
				float fValue = static_cast<float>(_tcstod(buffer, nullptr));
				pTransform->SetPositionY(fValue);
			}
			break;
		}
		}
		break;
	}
	case IDC_EDIT_POS_Z:
	{
		switch (HIWORD(wParam))
		{
		case EN_CHANGE:
		{
			if (pSelectedObject)
			{
				
				GetDlgItemText(hDlg, IDC_EDIT_POS_Z, buffer,6);
				float fValue = static_cast<float>(_tcstod(buffer, nullptr));
				pTransform->SetPositionZ(fValue);
			}
			break;
		}
		}
		break;
	}
	case IDC_EDIT_SCALE_X:
	{
		switch (HIWORD(wParam))
		{
		case EN_CHANGE:
		{

			if (pSelectedObject)
			{
				GetDlgItemText(hDlg, IDC_EDIT_SCALE_X, buffer, 4);
				float fValue = static_cast<float>(_tcstod(buffer, nullptr));
				cout << "IDC_EDIT_SCALE_X : " << fValue << endl;
				pTransform->ScaleFor(fValue, TRANSFORM::X);
			}
			break;
		}
		}
		break;
	}
	case IDC_EDIT_SCALE_Y:
	{
		switch (HIWORD(wParam))
		{
		case EN_CHANGE:
		{

			if (pSelectedObject)
			{
				GetDlgItemText(hDlg, IDC_EDIT_SCALE_Y, buffer, 4);
				float fValue = static_cast<float>(_tcstod(buffer, nullptr));
				cout << "IDC_EDIT_SCALE_Y : " << fValue << endl;
				pTransform->ScaleFor(fValue, TRANSFORM::Y);
			}
			break;
		}
		}

		break;
	}
	case IDC_EDIT_SCALE_Z:
	{
		switch (HIWORD(wParam))
		{
		case EN_CHANGE:
		{
			if (pSelectedObject)
			{
				GetDlgItemText(hDlg, IDC_EDIT_SCALE_Z, buffer, 4);
				float fValue = static_cast<float>(_tcstod(buffer, nullptr));
				cout << "IDC_EDIT_SCALE_Z : " << fValue << endl;
				pTransform->ScaleFor(fValue, TRANSFORM::Z);
			}
			break;
		}

		}

		break;
	}
	}
	

	return 0;
}
