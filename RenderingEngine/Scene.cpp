//-----------------------------------------------------------------------------
// File: CScene.cpp
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "Scene.h"
#include "resource.h"
#include "Commctrl.h"


CGameObject* CScene::m_pSelectedObject = nullptr;
CGameObject* CScene::m_pOldSelectedObject = nullptr;
CInspector* CScene::m_pInspector = nullptr;



CGameObject* CreateGameObject(int nObjectIndex, ID3D11Device* pd3dDevice, HWND hWnd)
{
	//스태틱 함수를 써도 되는가???
	// CShader로 호출하는 것이 많이 걸린다
	// 셰이더에서 객체를 생성한다고?
	wstring strTexture = L"Image/stone.dds";
	CGameObject* pGameObject = new CGameObject(1);
	pGameObject->AddComponent<CTransform>();
	pGameObject->AddComponent<CRenderer>();
	pGameObject->GetComponent<CTransform>()->SetPosition(XMFLOAT3(255, 50, 255));
	auto renderer = pGameObject->GetComponent<CRenderer>();
	auto transform = pGameObject->GetComponent<CTransform>();
	renderer->Initalize(pd3dDevice);
	renderer->SetMesh(CShader::GetMesh(nObjectIndex));
	renderer->CreateMaterial();
	renderer->CreateTexture(pd3dDevice, strTexture, hWnd);
	renderer->SetTransform(transform);
	


	// 적절히 삭제하거나 함수 변경할것
	//pGameObject->SetMesh(CShader::GetMesh(nObjectIndex));
	//pGameObject->SetMaterial(CShader::CreateMaterial());
	//pGameObject->SetTexture(CShader::CreateTexture(pd3dDevice));


	
	
	return pGameObject;
}

CScene::CScene()
{
	//m_ppShaders = nullptr;
	m_nShaders = 0;

	m_pCamera = nullptr;
	//m_pSelectedObject = nullptr;

	m_pLights = nullptr;
	m_pd3dcbLights = nullptr;

}

CScene::~CScene()
{
}

//DirectX::ScratchImage LoadTextureFromFile(wchar_t* fileName)
//{
//	HRESULT hr;
//	wstring str(fileName);
//	wstring wsTmp(str.begin(), str.end());
//
//	wstring ws = wsTmp;
//	//load the Texture
//	DirectX::ScratchImage image;
//	wchar_t ext[_MAX_EXT];
//	_wsplitpath_s(ws.c_str(), nullptr, 0, nullptr, 0, nullptr,0,  ext, _MAX_EXT);
//
//	if (_wcsicmp(ext, L".dds") == 0)
//	{
//		hr = DirectX::LoadFromDDSFile(ws.c_str(), DirectX::DDS_FLAGS_NONE, nullptr, image);
//
//	}
//	else if (_wcsicmp(ext, L".tga") == 0)
//	{
//		hr = DirectX::LoadFromTGAFile(ws.c_str(), nullptr, image);
//	}
//	else
//	{
//		hr = DirectX::LoadFromDDSFile(ws.c_str(), DirectX::WIC_FLAGS_NONE, nullptr, image);
//	}
//	return image;
//}


void CScene::BuildObjects(ID3D11Device *pd3dDevice, ID3D11DeviceContext *pd3dDeviceContext)
{
	////텍스쳐 맵핑에 사용할 샘플러 상태 객체를 생성한다.
	//ID3D11SamplerState *pd3dSamplerState = nullptr;
	//ID3D11SamplerState *pd3dSamplerState2 = nullptr;
	//D3D11_SAMPLER_DESC d3dSamplerDesc;
	//ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	//d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	//d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	//d3dSamplerDesc.MinLOD = 0;
	//d3dSamplerDesc.MaxLOD = 0;
	//pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dSamplerState);

	//pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dSamplerState2);



	////텍스쳐 리소스를 생성한다.
	//ID3D11ShaderResourceView *pd3dsrvTexture = nullptr; //Stone Brick
	//CTexture *pStoneTexture = new CTexture(2, 1, 0, 0);
	//
	////DirectX::ScratchImage image = LoadTextureFromFile(_T("Image/111.jpg"));
	////CreateShaderResourceView(pd3dDevice, image.GetImages(), image.GetImageCount(), image.GetMetadata(), &pd3dsrvTexture);
	//D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image/floor.dds"), nullptr, nullptr, &pd3dsrvTexture, nullptr);
	//pStoneTexture->SetTexture(0, pd3dsrvTexture);
	//pStoneTexture->SetSampler(0, pd3dSamplerState);
	//pd3dsrvTexture->Release();

	//CTexture *pBrickTexture = new CTexture(1, 1, 0, 0);
	//D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image/Brick1.jpg"), nullptr, nullptr, &pd3dsrvTexture, nullptr);
	////image = LoadTextureFromFile(_T("Image/222.jpg"));
	////CreateShaderResourceView(pd3dDevice, image.GetImages(), image.GetImageCount(), image.GetMetadata(), &pd3dsrvTexture);
	//pBrickTexture->SetTexture(0, pd3dsrvTexture);
	//pBrickTexture->SetSampler(0, pd3dSamplerState);
	//pd3dsrvTexture->Release();

	//CTexture *pWoodTexture = new CTexture(1, 1, 0, 0);
	////image = LoadTextureFromFile(_T("Image/Wood01.jpg"));
	////CreateShaderResourceView(pd3dDevice, image.GetImages(), image.GetImageCount(), image.GetMetadata(), &pd3dsrvTexture);
	//D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image/Brick1.jpg"), nullptr, nullptr, &pd3dsrvTexture, nullptr);
	//pWoodTexture->SetTexture(0, pd3dsrvTexture);
	//pWoodTexture->SetSampler(0, pd3dSamplerState);
	//pd3dsrvTexture->Release();

	////광원 텍스쳐 추가.
	//CTexture *pBlendingTexture= new CTexture(2, 2, 0, 0);
	////image = LoadTextureFromFile(_T("Image/Brick1.jpg"));
	////CreateShaderResourceView(pd3dDevice, image.GetImages(), image.GetImageCount(), image.GetMetadata(), &pd3dsrvTexture);
	//D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image/Brick1.jpg"), nullptr, nullptr, &pd3dsrvTexture, nullptr);
	//pBlendingTexture ->SetTexture(0, pd3dsrvTexture);
	//pBlendingTexture ->SetSampler(0, pd3dSamplerState);
	//pd3dsrvTexture->Release();
	//pd3dSamplerState->Release();
	////벽돌 텍스쳐 추가.

	////image = LoadTextureFromFile(_T("Image/LightMap3.jpg"));
	////CreateShaderResourceView(pd3dDevice, image.GetImages(), image.GetImageCount(), image.GetMetadata(), &pd3dsrvTexture);
	//D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image/LightMap3.jpg"), nullptr, nullptr, &pd3dsrvTexture, nullptr);
	//pBlendingTexture->SetTexture(1, pd3dsrvTexture);
	//pBlendingTexture->SetSampler(1, pd3dSamplerState2);
	//pd3dsrvTexture->Release();

	//pd3dSamplerState->Release();
	//==============================
	CShader::CreateMesh(pd3dDevice);

	//m_nShaders = 2;
	//m_ppShaders = new CShader*[m_nShaders];



	/*m_ppShaders[SCENE::TERRAIN] = new CTerrainShader();
	m_ppShaders[SCENE::TERRAIN]->CreateShader(pd3dDevice);
	m_ppShaders[SCENE::TERRAIN]->BuildObjects(pd3dDevice);*/

	CTerrainShader* pTerrainShader = new CTerrainShader();
	pTerrainShader->CreateShader(pd3dDevice);
	pTerrainShader->BuildObjects(pd3dDevice);
	m_vShaders.push_back(pTerrainShader);


	//Instancing
	CMaterial *pInstancingMaterials[3];
	
	
	//재질을 생성한다.
	CMaterial *pRedMaterial = new CMaterial();
	//pRedMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	//pRedMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	//pRedMaterial->m_Material.m_xmf4Specular = XMFLOAT4(1.0f, 0.0f, 0.0f, 5.0f);
	//pRedMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	//CMaterial *pGreenMaterial = new CMaterial();
	//pGreenMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
	//pGreenMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
	//pGreenMaterial->m_Material.m_xmf4Specular = XMFLOAT4(0.0f, 1.0f, 0.0f, 3.0f);
	//pGreenMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	//CMaterial *pBlueMaterial = new CMaterial();
	//pBlueMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
	//pBlueMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(0.0f, 0.0f, 0.3f, 1.0f);
	//pBlueMaterial->m_Material.m_xmf4Specular = XMFLOAT4(1.0f, 0.0f, 0.0f, 5.0f);
	//pBlueMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);


	//CMaterial *pNormalMaterial = new CMaterial();
	//pNormalMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	//pNormalMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	//pNormalMaterial->m_Material.m_xmf4Specular = XMFLOAT4(0.8f, 0.8f, 0.8f, 5.0f);
	//pNormalMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);





	CTexturedIlluminatedShader* pBasicShader = new CTexturedIlluminatedShader();
	pBasicShader->CreateShader(pd3dDevice);
	pBasicShader->BuildObjects(pd3dDevice);
	m_vShaders.push_back(pBasicShader);
	//m_ppShaders[SHADER_TPYE::BASIC] = pBasicShader;


	CNormalMapShader* pNormalMapShader = new CNormalMapShader();
	pNormalMapShader->CreateShader(pd3dDevice);
	pNormalMapShader->BuildObjects(pd3dDevice);
	m_vShaders.push_back(pNormalMapShader);

	CFontShader* pFontShader = new CFontShader();
	pFontShader->CreateShader(pd3dDevice);
	pFontShader->BuildObjects(pd3dDevice);
	m_vShaders.push_back(pFontShader);



	//m_ppShaders[SHADER_TPYE::NORMALMAP] = pNormalMapShader;

	// 구 평면 큐브

	//CAlphaBlendingShader* pWaterShader = new  CAlphaBlendingShader();
	//pWaterShader->CreateShader(pd3dDevice);
	//pWaterShader->BuildObjects(pd3dDevice);
	//m_ppShaders[5] = pWaterShader;
	//

	//CWaterShader* pWaterObjectShader = new CWaterShader();
	//pWaterObjectShader->CreateShader(pd3dDevice);
	//pWaterObjectShader->BuildObjects(pd3dDevice);
	//m_ppShaders[6] = pWaterObjectShader;

	m_pInspector = new CInspector(pd3dDevice, this);

	CreateShaderVariables(pd3dDevice);
}

void CScene::ReleaseObjects()
{
	ReleaseShaderVariables();


	for (int j = 0; j <  m_vShaders.size(); j++)
	{
		if (m_vShaders[j]) m_vShaders[j]->ReleaseObjects();
		if (m_vShaders[j]) delete m_vShaders[j];
	}
	m_vShaders.clear();
	
}
void MoveRotateBar(HWND hDlg, CTransform* pTransform, int& nXBarPos, int& nYBarPos, int& nZBarPos)
{
	XMFLOAT3 xmf3Axis;
	wchar_t buffer[50];

	xmf3Axis = XMFLOAT3(1.0f, 0.0f, 0.0f);
	XMFLOAT3 xmf3Angle(static_cast<float>(nXBarPos * 3.6), static_cast<float>(nYBarPos * 3.6), static_cast<float>(nZBarPos * 3.6));
	if (pTransform) pTransform->FromEulerAngles(xmf3Angle);
	

	_stprintf(buffer, L"%3.2lf", static_cast<float>(nXBarPos * 3.6));
	SetDlgItemText(hDlg, IDC_STATIC_ANGLE_X, buffer);
	_stprintf(buffer, L"%3.2lf", static_cast<float>(nYBarPos * 3.6));
	SetDlgItemText(hDlg, IDC_STATIC_ANGLE_Y, buffer);
	_stprintf(buffer, L"%3.2lf", static_cast<float>(nZBarPos * 3.6));
	SetDlgItemText(hDlg, IDC_STATIC_ANGLE_Z, buffer);

	pTransform->SetDegree(XMFLOAT3(static_cast<float>(nXBarPos * 3.6), static_cast<float>(nYBarPos * 3.6), static_cast<float>(nZBarPos * 3.6)));
}


void CScene::ActivateNormalMapShader()
{
	if (m_pSelectedObject&& !m_pSelectedObject->HasNormalMap())
	{
		m_pSelectedObject->IncludeNormalMap();
		/*m_ppShaders[SHADER_TPYE::BASIC]->RemoveObject(m_pSelectedObject);
		m_ppShaders[SHADER_TPYE::NORMALMAP]->AddObject(m_pSelectedObject);*/
		
		
		m_vShaders[SHADER_TYPE::BASIC]->RemoveObject(m_pSelectedObject);
		m_vShaders[SHADER_TYPE::NORMALMAP]->AddObject(m_pSelectedObject);
	}
 }

void CScene::ActivateBasicShader()
{
	if (m_pSelectedObject && m_pSelectedObject->HasNormalMap())
	{
		m_pSelectedObject->RemoveNormalMap();
		m_vShaders[SHADER_TYPE::NORMALMAP]->RemoveObject(m_pSelectedObject);
		m_vShaders[SHADER_TYPE::BASIC]->AddObject(m_pSelectedObject);
	}
}



void ReplaceSelectedObject(HWND hDlg, CTransform* pTransform)
{
	wchar_t buffer[100];



	_stprintf(buffer, L"%3.2lf", pTransform->GetPosition().x);
	SetDlgItemText(hDlg, IDC_EDIT_POS_X, buffer);
	_stprintf(buffer, L"%3.2lf", pTransform->GetPosition().y);
	SetDlgItemText(hDlg, IDC_EDIT_POS_Y, buffer);

	_stprintf(buffer, L"%3.2lf", pTransform->GetPosition().z);
	SetDlgItemText(hDlg, IDC_EDIT_POS_Z, buffer);


	float fAangle;
	fAangle = pTransform->ToEulerAngles().x;

	_stprintf(buffer, L"%3.2lf", pTransform->GetDegree().x);
	SetDlgItemText(hDlg, IDC_STATIC_ANGLE_X, buffer);
	SendDlgItemMessage(hDlg, IDC_SLIDER_ROT_X, TBM_SETPOS, TRUE,
		pTransform->GetDegree().x / 360.0 * 100);

	_stprintf(buffer, L"%3.2lf", pTransform->GetDegree().y);
	SetDlgItemText(hDlg, IDC_STATIC_ANGLE_Y, buffer);
	SendDlgItemMessage(hDlg, IDC_SLIDER_ROT_Y, TBM_SETPOS, TRUE,
		pTransform->GetDegree().y / 360.0 * 100);

	_stprintf(buffer, L"%3.2lf", pTransform->GetDegree().z);
	SetDlgItemText(hDlg, IDC_STATIC_ANGLE_Z, buffer);
	SendDlgItemMessage(hDlg, IDC_SLIDER_ROT_Z, TBM_SETPOS, TRUE,
		pTransform->GetDegree().z / 360.0 * 100);




	
	_stprintf(buffer, L"%3.1lf", pTransform->GetScale().x);
	SetDlgItemText(hDlg, IDC_EDIT_SCALE_X, buffer);
	_stprintf(buffer, L"%3.1lf", pTransform->GetScale().y);
	SetDlgItemText(hDlg, IDC_EDIT_SCALE_Y, buffer);
	_stprintf(buffer, L"%3.1lf", pTransform->GetScale().z);
	SetDlgItemText(hDlg, IDC_EDIT_SCALE_Z, buffer);
}



BOOL CALLBACK CScene::Dlg_InspectorPorc(HWND hDlg, UINT iMsg,
	WPARAM wParam, LPARAM lParam)
{

	static int nXBarPos = 0;
	static int nYBarPos = 0;
	static int nZBarPos = 0;

	static int nOldXBarPos = 0;
	static int nOldYBarPos = 0;
	static int nOldZBarPos = 0;

	static float fAngleX = 0.0f;
	static float fAngleY = 0.0f;
	static float fAngleZ = 0.0f;
	
	CTransform* pTransform;
	if(m_pSelectedObject)
		pTransform = m_pSelectedObject->GetComponent<CTransform>();

	switch (iMsg)
	{
	case WM_INITDIALOG:
	{
		if (m_pSelectedObject)
		{
			ReplaceSelectedObject(hDlg, pTransform);
		}
		return true;
	}
		
	case WM_DROPFILES:
	{
		cout << "드락 파일" << endl;
		break;
	}
	case WM_HSCROLL:
	{
		if (m_pSelectedObject)
		{
			nXBarPos = SendDlgItemMessage(hDlg, IDC_SLIDER_ROT_X, TBM_GETPOS, 0, 0);
			nYBarPos = SendDlgItemMessage(hDlg, IDC_SLIDER_ROT_Y, TBM_GETPOS, 0, 0);
			nZBarPos = SendDlgItemMessage(hDlg, IDC_SLIDER_ROT_Z, TBM_GETPOS, 0, 0);


			MoveRotateBar(hDlg, pTransform, nXBarPos, nYBarPos, nZBarPos);
		}
		
		break;
	}
	case WM_REPLACE:
	{
		if (m_pSelectedObject)
		{
			ReplaceSelectedObject(hDlg, pTransform);
		}
		break;
	}
	case WM_COMMAND:
	{
		if (m_pInspector->OnProcessingDlgCommandMessage(hDlg, iMsg,
			wParam, lParam, m_pSelectedObject, m_pOldSelectedObject))
		{
			m_pOldSelectedObject = nullptr;
			m_pSelectedObject = nullptr;

		}
		break;
	}
	
	}
	return 0;
}

bool CScene::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	return(false);
}
bool  CScene::OnProcessingMouseMessage(HWND hWnd, HINSTANCE hInstance, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_LBUTTONDOWN:
	{
		//cout << "마우스 : " << LOWORD(lParam) << "  " << HIWORD(lParam) << endl;
		m_pSelectedObject = PickObjectPointedByCursor(LOWORD(lParam), HIWORD(lParam));
		if (m_pSelectedObject && m_pOldSelectedObject != m_pSelectedObject)
		{
			//m_pSelectedObject->ShowObjectInfo();
			m_pOldSelectedObject = m_pSelectedObject;
			if (!IsWindow(m_hInspector))
			{
				m_hInspector = CreateDialog(hInstance,
					MAKEINTRESOURCE(IDD_DIALOG1), hWnd, Dlg_InspectorPorc);
				ShowWindow(m_hInspector, SW_SHOW);
			}
			else // 새로운 오브젝트 선택했음.
			{
				SendMessage(m_hInspector, WM_REPLACE, (WPARAM)m_pSelectedObject, 0);
			}
		}
		break;
	}

	}
	return(false);
}

void CScene::OnProcessingMenuMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, ID3D11Device* pd3dDevice)
{
	switch (nMessageID)
	{
	case WM_COMMAND:
	{
		int nID = LOWORD(wParam);
		switch (nID)
		{
		case ID_GAMEOBJECT_CUBE:
		{
			m_vShaders[SHADER_TYPE::BASIC]->AddObject(CreateGameObject(OBJECT::CUBE, pd3dDevice, hWnd));
		
			break;
		}
		case ID_GAMEOBJECT_SPHERE:
		{
			m_vShaders[SHADER_TYPE::BASIC]->AddObject(CreateGameObject(OBJECT::SPHERE, pd3dDevice, hWnd));
			break;
		}
		case ID_GAMEOBJECT_PLANE:
		{
			m_vShaders[SHADER_TYPE::BASIC]->AddObject(CreateGameObject(OBJECT::PLANE, pd3dDevice, hWnd));
			break;
		}
		case ID_GAMEOBJECT_PILLAR:
		{
			m_vShaders[SHADER_TYPE::BASIC]->AddObject(CreateGameObject(OBJECT::PILLAR, pd3dDevice, hWnd));
			break;
		}
		case ID_GAMEOBJECT_Terrain:
		{
			if (!m_vShaders[SHADER_TYPE::TERRAIN]->FindObject(0))
			{
				m_vShaders[SHADER_TYPE::TERRAIN]->AddTerrainObject(pd3dDevice);
			}
			break;
		}
		
		}
		break;
	}	
	}
}
bool CScene::ProcessInput()
{
	return(false); 
}
void CScene::AnimateObjects(float fTimeElapsed)
{
	//CHeightMapTerrain *pTerrain = GetTerrain();
	CTerrain* pTerrain = GetTerrain();
	//if (m_pLights && m_pd3dcbLights && pTerrain)
	if (m_pLights && m_pd3dcbLights && pTerrain)
	{
		//현재 카메라의 위치 벡터를 조명을 나타내는 상수 버퍼에 설정한다.
		XMFLOAT3 d3dxvCameraPosition = m_pCamera->GetPosition();
		m_pLights->m_d3dxvCameraPosition = XMFLOAT4(d3dxvCameraPosition.x, d3dxvCameraPosition.y, d3dxvCameraPosition.z, 1.0f);

		//점 조명이 지형의 중앙을 중심으로 회전하도록 설정한다.
		
		//static XMFLOAT3 d3dxvRotated = XMFLOAT3(pTerrain->GetWidth()*0.3f, 0.0f, 0.0f);
		static XMFLOAT3 d3dxvRotated = XMFLOAT3(pTerrain->GetWidth()*0.3f, 0.0f, 0.0f);
		XMMATRIX d3dxmtxRotate;
		

		d3dxmtxRotate = XMMatrixRotationRollPitchYaw(0.0f, (float)XMConvertToRadians(30.0f*fTimeElapsed), 0.0f);
		XMStoreFloat3(&d3dxvRotated, XMVector3TransformCoord( XMLoadFloat3(&d3dxvRotated), d3dxmtxRotate));


		/*XMFLOAT3 d3dxvTerrainCenter = XMFLOAT3(pTerrain->GetWidth()*0.5f, pTerrain->GetPeakHeight() + 10.0f, pTerrain->GetLength()*0.5f); 
		XMStoreFloat3(&m_pLights->m_pLights[0].m_xmf3Position,	XMLoadFloat3(&d3dxvTerrainCenter) + XMLoadFloat3(&d3dxvRotated));
		m_pLights->m_pLights[0].m_fRange = pTerrain->GetPeakHeight();*/


		XMFLOAT3 d3dxvTerrainCenter = XMFLOAT3(pTerrain->GetWidth()*0.5f, pTerrain->GetPeakHeight() + 10.0f, pTerrain->GetLength()*0.5f);
		XMStoreFloat3(&m_pLights->m_pLights[0].m_xmf3Position, XMLoadFloat3(&d3dxvTerrainCenter) + XMLoadFloat3(&d3dxvRotated));
		m_pLights->m_pLights[0].m_fRange = pTerrain->GetPeakHeight();

		/*두 번째 조명은 플레이어가 가지고 있는 손전등(스팟 조명)이다. 그러므로 플레이어의 위치와 방향이 바뀌면 현재 플레이어의 위치와 z-축 방향 벡터를 스팟 조명의 위치와 방향으로 설정한다.*/
		CPlayer *pPlayer = m_pCamera->GetPlayer();
		m_pLights->m_pLights[1].m_xmf3Position = pPlayer->GetPosition();
		m_pLights->m_pLights[1].m_d3dxvDirection = pPlayer->GetLookVector();


		XMStoreFloat3(&m_pLights->m_pLights[3].m_xmf3Position, XMLoadFloat3(&pPlayer->GetPosition()) + XMLoadFloat3(&XMFLOAT3(0.0f, 40.0f, 0.0f)));
	}


	for (int i = 0; i < m_nShaders; i++)
	{
		m_vShaders[i]->AnimateObjects(fTimeElapsed);
	}
}

void CScene::Render(ID3D11DeviceContext*pd3dDeviceContext, CCamera *pCamera)
{
	if (m_pLights && m_pd3dcbLights) UpdateShaderVariable(pd3dDeviceContext, m_pLights);

	for (int i = 0; i <m_vShaders.size(); i++)
	{
		m_vShaders[i]->Render(pd3dDeviceContext, pCamera);
	}
}
CGameObject *CScene::PickObjectPointedByCursor(int xClient, int yClient)
{
	if (!m_pCamera) return(nullptr);

	XMMATRIX xmmtxView = XMLoadFloat4x4(&m_pCamera->GetViewMatrix());
	XMMATRIX xmmtxProjection = XMLoadFloat4x4(&m_pCamera->GetProjectionMatrix());
	D3D11_VIEWPORT d3dViewport = m_pCamera->GetViewport();

	XMFLOAT3 xmf3PickPosition;
	xmf3PickPosition.x = (((2.0f * xClient) / d3dViewport.Width) - 1) / m_pCamera->GetProjectionMatrix()._11;
	xmf3PickPosition.y = -(((2.0f * yClient) / d3dViewport.Height) - 1) / m_pCamera->GetProjectionMatrix()._22;
	xmf3PickPosition.z = 1.0f;

	int nIntersected = 0;
	float fNearHitDistance = FLT_MAX;
	MESHINTERSECTINFO d3dxIntersectInfo;
	CGameObject *pIntersectedObject = nullptr, *pNearestObject = nullptr;
	for (int i = 0; i < m_vShaders.size()-1; i++)
	{
		pIntersectedObject = m_vShaders[i]->PickObjectByRayIntersection(&xmf3PickPosition, &xmmtxView, &d3dxIntersectInfo);
		if (pIntersectedObject && (d3dxIntersectInfo.m_fDistance < fNearHitDistance))
		{
			fNearHitDistance = d3dxIntersectInfo.m_fDistance;
			pNearestObject = pIntersectedObject;
		}
	}
	return(pNearestObject);
}

//CHeightMapTerrain *CScene::GetTerrain()
//{
//	if (m_vShaders[0]->FindObject(0))// 지형 : 강제로 0을 설정하는게 맞는가??? 
//	{
//		CTerrainShader *pTerrainShader = static_cast<CTerrainShader *>(m_vShaders[0]);
//		return(pTerrainShader->GetTerrain());
//	}
//	return nullptr;
//}
CTerrain *CScene::GetTerrain()
{
	//if (m_vShaders[0]->FindObject(0))// 지형 : 강제로 0을 설정하는게 맞는가??? 
	{
		CTerrainShader *pTerrainShader = static_cast<CTerrainShader *>(m_vShaders[0]);
		return(pTerrainShader->GetTerrain());

	}
	return nullptr;
}
void CScene::CreateShaderVariables(ID3D11Device *pd3dDevice)
{
	m_pLights = new LIGHTS;
	::ZeroMemory(m_pLights, sizeof(LIGHTS));
	//게임 월드 전체를 비추는 주변조명을 설정한다.
	m_pLights->m_d3dxcGlobalAmbient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);

	//3개의 조명(점 광원, 스팟 광원, 방향성 광원)을 설정한다.
	m_pLights->m_pLights[0].m_bEnable = 1.0f;
	m_pLights->m_pLights[0].m_nType = POINT_LIGHT;
	m_pLights->m_pLights[0].m_fRange = 300.0f;
	m_pLights->m_pLights[0].m_xmf4Ambient = XMFLOAT4(0.1f, 0.0f, 0.0f, 1.0f);
	m_pLights->m_pLights[0].m_xmf4Diffuse = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	m_pLights->m_pLights[0].m_xmf4Specular = XMFLOAT4(0.1f, 0.1f, 0.1f, 0.0f);
	m_pLights->m_pLights[0].m_xmf3Position = XMFLOAT3(300.0f, 300.0f, 300.0f);
	m_pLights->m_pLights[0].m_d3dxvDirection = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_pLights->m_pLights[0].m_d3dxvAttenuation = XMFLOAT3(1.0f, 0.001f, 0.0001f);

	m_pLights->m_pLights[1].m_bEnable = 1.0f;
	m_pLights->m_pLights[1].m_nType = SPOT_LIGHT;
	m_pLights->m_pLights[1].m_fRange = 100.0f; 
	m_pLights->m_pLights[1].m_xmf4Ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	m_pLights->m_pLights[1].m_xmf4Diffuse = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
	m_pLights->m_pLights[1].m_xmf4Specular = XMFLOAT4(0.1f, 0.1f, 0.1f, 0.0f);
	m_pLights->m_pLights[1].m_xmf3Position = XMFLOAT3(500.0f, 300.0f, 500.0f);
	m_pLights->m_pLights[1].m_d3dxvDirection = XMFLOAT3(0.0f, 0.0f, 1.0f);
	m_pLights->m_pLights[1].m_d3dxvAttenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
	m_pLights->m_pLights[1].m_fFalloff = 8.0f;
	m_pLights->m_pLights[1].m_fPhi = (float)cos(XMConvertToRadians(40.0f));
	m_pLights->m_pLights[1].m_fTheta = (float)cos(XMConvertToRadians(20.0f));

	m_pLights->m_pLights[2].m_bEnable = 1.0f;
	m_pLights->m_pLights[2].m_nType = DIRECTIONAL_LIGHT;
	m_pLights->m_pLights[2].m_xmf4Ambient = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
	m_pLights->m_pLights[2].m_xmf4Diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	m_pLights->m_pLights[2].m_xmf4Specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	//m_pLights->m_pLights[1].m_d3dxvDirection = XMFLOAT3(0.707101 , -0.707101, 0.0f);
	m_pLights->m_pLights[2].m_d3dxvDirection = XMFLOAT3(1.0f, -1.0f, 0.0f);

	m_pLights->m_pLights[3].m_bEnable = 1.0f;
	m_pLights->m_pLights[3].m_nType = VOID; //SPOT_LIGHT;
	m_pLights->m_pLights[3].m_fRange = 60.0f;
	m_pLights->m_pLights[3].m_xmf4Ambient = XMFLOAT4(0.1f, 0.0f, 0.0f, 1.0f);
	m_pLights->m_pLights[3].m_xmf4Diffuse = XMFLOAT4(0.5f, 0.0f, 0.0f, 1.0f);
	m_pLights->m_pLights[3].m_xmf4Specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	m_pLights->m_pLights[3].m_xmf3Position = XMFLOAT3(500.0f, 300.0f, 500.0f);
	m_pLights->m_pLights[3].m_d3dxvDirection = XMFLOAT3(0.0f, -1.0f, 0.0f);
	m_pLights->m_pLights[3].m_d3dxvAttenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
	m_pLights->m_pLights[3].m_fFalloff = 20.0f;
	m_pLights->m_pLights[3].m_fPhi = (float)cos(XMConvertToRadians(40.0f));
	m_pLights->m_pLights[3].m_fTheta = (float)cos(XMConvertToRadians(15.0f));

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(d3dBufferDesc));
	d3dBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	d3dBufferDesc.ByteWidth = sizeof(LIGHTS);
	d3dBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	d3dBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pLights;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dcbLights);
}
void CScene::ReleaseShaderVariables()
{
	if (m_pLights) delete m_pLights;
	if (m_pd3dcbLights) m_pd3dcbLights->Release();
}
void CScene::UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, LIGHTS *pLights)
{
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	pd3dDeviceContext->Map(m_pd3dcbLights, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	LIGHTS *pcbLight = (LIGHTS *)d3dMappedResource.pData;

	memcpy(pcbLight, pLights, sizeof(LIGHTS));
	pd3dDeviceContext->Unmap(m_pd3dcbLights, 0);
	pd3dDeviceContext->PSSetConstantBuffers(PS_SLOT_LIGHT, 1, &m_pd3dcbLights);
}
