#pragma once
#include "stdafx.h"
struct VECTOR2
{
	int x, y;
};

struct COLOR
{
	int r, g, b;
};

class CScroll
{
	VECTOR2 m_vSize;
private:
	

	wchar_t m_cWord[20];
	bool m_bHaveWord = false;
	bool m_bOnMouse = false;
	bool m_bClick = false;
	int m_nMin;
	int m_nMax;
	COLOR m_Green{ 100, 200 ,100 };
public:
	VECTOR2 m_vPos;
	CScroll() {}
	~CScroll() {}
	void BuildObjects(int nXpos, int nYpos, int nWidth, int nHeight, int nMin, int nMax, bool bClick = false);
	bool CheckInputBox(int nXpos, int nYpos);
	void SetOnMouse(bool bOnMouse) { m_bOnMouse = bOnMouse; }
	void SetScrollPos(POINT pt);
	void SetScrollPosY(int nYpos) { m_vPos.y += nYpos; }
	int GetScrollYpos() { return m_vPos.y; }
	void Draw(HDC hDCFrameBuffer);
	void MoveScroll();
};