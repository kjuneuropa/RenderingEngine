#include "stdafx.h"
#include "Player.h"
#include "Camera.h"
using namespace std;

CCamera::CCamera(CCamera *pCamera)
{
	if (pCamera)
	{
		//카메라가 이미 있으면 기존 카메라의 정보를 새로운 카메라에 복사한다. 
		m_xmf3Position = pCamera->GetPosition();
		m_xmf3Right = pCamera->GetRightVector();
		m_xmf3Look = pCamera->GetLookVector();
		m_xmf3Up = pCamera->GetUpVector();
		m_fPitch = pCamera->GetPitch();
		m_fRoll = pCamera->GetRoll();
		m_fYaw = pCamera->GetYaw();
		m_xmmtxView = pCamera->GetViewMatrix();
		m_xmmtxProjection = pCamera->GetProjectionMatrix();
		m_d3dViewport = pCamera->GetViewport();
		m_xmf3LookAtWorld = pCamera->GetLookAtPosition();
		m_xmf3Offset = pCamera->GetOffset();
		m_fTimeLag = pCamera->GetTimeLag();
		m_pPlayer = pCamera->GetPlayer();
		m_pd3dcbCamera = pCamera->GetCameraConstantBuffer();
		if (m_pd3dcbCamera) m_pd3dcbCamera->AddRef();
	}
	else
	{
		//카메라가 없으면 기본 정보를 설정한다. 
		m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		m_xmf3Right = XMFLOAT3(1.0f, 0.0f, 0.0f);
		m_xmf3Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
		m_xmf3Look = XMFLOAT3(0.0f, 0.0f, 1.0f);
		m_fPitch = 0.0f;
		m_fRoll = 0.0f;
		m_fYaw = 0.0f;
		m_fTimeLag = 0.0f;
		m_xmf3LookAtWorld = XMFLOAT3(0.0f, 0.0f, 0.0f);
		m_xmf3Offset = XMFLOAT3(0.0f, 0.0f, 0.0f);
		m_nMode = 0x00;
		m_pPlayer = nullptr;
		//XMMATRIX mtxView = XMLoadFloat4x4(&m_xmmtxView);
		//XMMATRIX mtxProjection = XMLoadFloat4x4(&m_xmmtxProjection);

		//mtxView = XMMatrixIdentity();
		//mtxProjection = XMMatrixIdentity();
		XMStoreFloat4x4(&m_xmmtxView, XMMatrixIdentity());
		XMStoreFloat4x4(&m_xmmtxProjection, XMMatrixIdentity());
		m_pd3dcbCamera = nullptr;
		
	}
}

CCamera::~CCamera()
{
	if (m_pd3dcbCamera) m_pd3dcbCamera->Release();
}
//Direct3D 디바이스의 뷰-포트를 설정하기 위한 함수이다.
void CCamera::SetViewport(ID3D11DeviceContext *pd3dDeviceContext, DWORD xTopLeft, DWORD yTopLeft, DWORD nWidth, DWORD nHeight, float fMinZ, float fMaxZ)
{
	m_d3dViewport.TopLeftX = float(xTopLeft);
	m_d3dViewport.TopLeftY = float(yTopLeft);
	m_d3dViewport.Width = float(nWidth);
	m_d3dViewport.Height = float(nHeight);
	m_d3dViewport.MinDepth = fMinZ;
	m_d3dViewport.MaxDepth = fMaxZ;
	pd3dDeviceContext->RSSetViewports(1, &m_d3dViewport);
}
/*카메라 변환 행렬을 생성하는 함수이다. 카메라의 위치 벡터, 카메라가 바라보는 지점, 카메라의 Up 벡터(로컬 y-축 벡터)를 파라메터로 사용하는 XMMATRIXLookAtLH() 함수를 사용한다.*/
void CCamera::GenerateViewMatrix()
{

	if (m_nMode == SPACESHIP_CAMERA)
	{
		m_xmf3Position.z -= 10.0f;
	}

	XMVECTOR vPosition = XMLoadFloat3(&m_xmf3Position); // 257 100 207
	XMVECTOR vEye = XMLoadFloat3(&m_pPlayer->GetPosition()); // 257 1000 257
	XMVECTOR vUp = XMLoadFloat3(&m_xmf3Up); //0.9284 0.371



	XMMATRIX xmmtxView = XMMatrixLookAtLH(vPosition, vEye, vUp);
	XMStoreFloat4x4(&m_xmmtxView, xmmtxView);
}
// 카메라의 z-축을 기준으로 카메라의 좌표축들이 직교하도록 카메라 변환행렬을 갱신한다.
void CCamera::RegenerateViewMatrix()
{
	XMVECTOR vLook = XMLoadFloat3(&m_xmf3Look);
	XMVECTOR vRight = XMLoadFloat3(&m_xmf3Right);
	XMVECTOR vUp = XMLoadFloat3(&m_xmf3Up);
	XMVECTOR vPosition = XMLoadFloat3(&m_xmf3Position);


	//카메라의 z-축 벡터를 정규화한다.
	vLook = XMVector3Normalize(vLook);


	//카메라의 z-축과 y-축에 수직인 벡터를 x-축으로 설정한다.
	vRight = XMVector3Cross(vUp, vLook);
	//카메라의 x-축 벡터를 정규화한다.
	vRight = XMVector3Normalize(vRight);

	//카메라의 z-축과 x-축에 수직인 벡터를 y-축으로 설정한다.
	vUp = XMVector3Cross(vLook, vRight);

	//카메라의 y-축 벡터를 정규화한다.
	vUp = XMVector3Normalize(vUp);


	XMStoreFloat3(&m_xmf3Look, vLook);
	XMStoreFloat3(&m_xmf3Up, vUp);
	XMStoreFloat3(&m_xmf3Right, vRight);

	m_xmmtxView._11 = m_xmf3Right.x;
	m_xmmtxView._12 = m_xmf3Up.x;
	m_xmmtxView._13 = m_xmf3Look.x;
	m_xmmtxView._21 = m_xmf3Right.y;
	m_xmmtxView._22 = m_xmf3Up.y;
	m_xmmtxView._23 = m_xmf3Look.y;
	m_xmmtxView._31 = m_xmf3Right.z;
	m_xmmtxView._32 = m_xmf3Up.z;
	m_xmmtxView._33 = m_xmf3Look.z;


	m_xmmtxView._41 = -XMVectorGetX(XMVector3Dot(vPosition, vRight));
	m_xmmtxView._42 = -XMVectorGetX(XMVector3Dot(vPosition, vUp));
	m_xmmtxView._43 = -XMVectorGetX(XMVector3Dot(vPosition, vLook));


	//카메라의 위치와 방향이 바뀌면(카메라 변환 행렬이 바뀌면) 절두체 평면을 다시 계산한다.
	CalculateFrustumPlanes();
}


void CCamera::GenerateProjectionMatrix(float fNearPlaneDistance, float fFarPlaneDistance, float fAspectRatio, float fFOVAngle)
{
	XMStoreFloat4x4(&m_xmmtxProjection , XMMatrixPerspectiveFovLH( (float)XMConvertToRadians(fFOVAngle), fAspectRatio, fNearPlaneDistance, fFarPlaneDistance));

}
void CCamera::CreateShaderVariables(ID3D11Device *pd3dDevice)
{
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(VS_CB_CAMERA);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	pd3dDevice->CreateBuffer(&bd, nullptr, &m_pd3dcbCamera);
}
void CCamera::UpdateShaderVariables(ID3D11DeviceContext *pd3dDeviceContext)
{
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	/*상수 버퍼의 메모리 주소를 가져와서 카메라 변환 행렬과 투영 변환 행렬을 복사한다. 
	쉐이더에서 행렬의 행과 열이 바뀌는 것에 주의하라.*/
	pd3dDeviceContext->Map(m_pd3dcbCamera, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	VS_CB_CAMERA *pcbViewProjection = (VS_CB_CAMERA *)d3dMappedResource.pData;
	
	  //XMStoreFloat4x4(&pcbViewProjection->m_xmmtxView,
		 // XMMatrixTranspose(XMLoadFloat4x4(& m_xmmtxView)));
	  //XMStoreFloat4x4(&pcbViewProjection->m_xmmtxProjection,
		 // XMMatrixTranspose(XMLoadFloat4x4(&m_xmmtxProjection)));

	pcbViewProjection->m_xmmtxView = XMMatrixTranspose(XMLoadFloat4x4(&m_xmmtxView));
	pcbViewProjection->m_xmmtxProjection =  XMMatrixTranspose(XMLoadFloat4x4(&m_xmmtxProjection));


	//pcbViewProjection->m_xmmtxProjection = XMMatrixTranspose(m_xmmtxProjection);
	pd3dDeviceContext->Unmap(m_pd3dcbCamera, 0);

	//상수 버퍼를 슬롯(VS_SLOT_CAMERA)에 설정한다.
	pd3dDeviceContext->VSSetConstantBuffers(VS_SLOT_CAMERA, 1, &m_pd3dcbCamera);
}



CFirstPersonCamera::CFirstPersonCamera(CCamera *pCamera) : CCamera(pCamera)
{
	m_nMode = FIRST_PERSON_CAMERA;
	if (pCamera)
	{
		/*1인칭 카메라로 변경하기 이전의 카메라가 스페이스-쉽 카메라이면 카메라의 Up 벡터를 월드좌표의 y-축이 되도록 한다. 이것은 스페이스-쉽 카메라의 로컬 y-축 벡터가 어떤 방향이든지 1인칭 카메라(대부분 사람인 경우)의 로컬 y-축 벡터가 월드좌표의 y-축이 되도록 즉, 똑바로 서있는 형태로 설정한다는 의미이다. 그리고 로컬 x-축 벡터와 로컬 z-축 벡터의 y-좌표가 0.0f가 되도록 한다. 이것은 <그림 8>과 같이 로컬 x-축 벡터와 로컬 z-축 벡터를 xz-평면(지면)으로 투영하는 것을 의미한다. 즉, 1인칭 카메라의 로컬 x-축 벡터와 로컬 z-축 벡터는 xz-평면에 평행하다.*/
		if (pCamera->GetMode() == SPACESHIP_CAMERA)
		{
		

			m_xmf3Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
			m_xmf3Right.y = 0.0f;
			m_xmf3Look.y = 0.0f;
			XMVECTOR vLook = XMLoadFloat3(&m_xmf3Look);
			XMVECTOR vRight = XMLoadFloat3(&m_xmf3Right);

			vLook = XMVector3Normalize(vLook);
			vRight = XMVector3Normalize(vRight);

			XMStoreFloat3(&m_xmf3Look, vLook);
			XMStoreFloat3(&m_xmf3Right, vRight);

		}
	}
}
void CFirstPersonCamera::Rotate(float x, float y, float z)
{
	XMMATRIX mtxRotate;
	XMVECTOR vLook = XMLoadFloat3(&m_xmf3Look);
	XMVECTOR vRight = XMLoadFloat3(&m_xmf3Right);
	XMVECTOR vUp = XMLoadFloat3(&m_xmf3Up);

	if (x != 0.0f)
	{
		cout << "x축 회전" << endl;
		//카메라의 로컬 x-축을 기준으로 회전하는 행렬을 생성한다. 고개를 끄떡이는 동작이다.


		mtxRotate  = XMMatrixRotationAxis( vRight, (float)XMConvertToRadians(x));
		//카메라의 로컬 x-축, y-축, z-축을 회전한다.
		vRight = XMVector3TransformNormal(vRight, mtxRotate);
		vUp = XMVector3TransformNormal(vUp, mtxRotate);
		vLook = XMVector3TransformNormal(vLook, mtxRotate);

		XMStoreFloat3(&m_xmf3Look, vLook);
		XMStoreFloat3(&m_xmf3Right, vRight);
		XMStoreFloat3(&m_xmf3Up, vUp);
	}
	if (m_pPlayer && (y != 0.0f))
	{
		cout << "y축 회전" << endl;
		//플레이어의 로컬 y-축을 기준으로 회전하는 행렬을 생성한다.
		XMVECTOR vPlayerUp = XMLoadFloat3(&m_pPlayer->GetUpVector());
		mtxRotate  = XMMatrixRotationAxis(vPlayerUp, (float)XMConvertToRadians(y));

		//카메라의 로컬 x-축, y-축, z-축을 회전한다.
		vRight = XMVector3TransformNormal(vRight, mtxRotate);
		vUp = XMVector3TransformNormal(vUp, mtxRotate);
		vLook = XMVector3TransformNormal(vLook, mtxRotate);

		XMStoreFloat3(&m_xmf3Look, vLook);
		XMStoreFloat3(&m_xmf3Right, vRight);
		XMStoreFloat3(&m_xmf3Up, vUp);
	}
	if (m_pPlayer && (z != 0.0f))
	{
		cout << "z축 회전" << endl;
		//플레이어의 로컬 z-축을 기준으로 회전하는 행렬을 생성한다.
		XMVECTOR vPlayerLook = XMLoadFloat3(&m_pPlayer->GetLookVector());
		XMVECTOR vPlayerPosition = XMLoadFloat3(&m_pPlayer->GetPosition());
		XMVECTOR vPosition = XMLoadFloat3(&m_xmf3Position);

		mtxRotate = XMMatrixRotationAxis(vPlayerLook, (float)XMConvertToRadians(z));
		//카메라의 위치 벡터를 플레이어 좌표계로 표현한다(오프셋 벡터).
		cout << "카메라 위치" << m_xmf3Position.x << "  " << m_xmf3Position.y << "  " << m_xmf3Position.z << endl;
		cout << "내 위치 : " << m_pPlayer->GetPosition().x << "  " << m_pPlayer->GetPosition().y << ""<<m_pPlayer->GetPosition().z << endl;
		
		vPosition -= vPlayerPosition;
		//오프셋 벡터 벡터를 회전한다.
		vPosition = XMVector3TransformNormal(vPosition, mtxRotate);
		//회전한 카메라의 위치를 월드 좌표계로 표현한다.
		vPosition += vPlayerPosition;

		//카메라의 로컬 x-축, y-축, z-축을 회전한다.
		vRight = XMVector3TransformNormal(vRight, mtxRotate);
		vUp = XMVector3TransformNormal(vUp, mtxRotate);
		vLook = XMVector3TransformNormal(vLook, mtxRotate);

		XMStoreFloat3(&m_xmf3Look, vLook);
		XMStoreFloat3(&m_xmf3Right, vRight);
		XMStoreFloat3(&m_xmf3Up, vUp);
		XMStoreFloat3(&m_xmf3Position, vPosition);
		XMStoreFloat3(&m_xmf3Up, vPlayerPosition);

	}
}

CThirdPersonCamera::CThirdPersonCamera(CCamera *pCamera) : CCamera(pCamera)
{
	m_nMode = THIRD_PERSON_CAMERA;
	if (pCamera)
	{
		/*3인칭 카메라로 변경하기 이전의 카메라가 스페이스-쉽 카메라이면 카메라의 Up 벡터를 월드좌표의 y-축이 되도록 한다. 이것은 스페이스-쉽 카메라의 로컬 y-축 벡터가 어떤 방향이든지 3인칭 카메라(대부분 사람인 경우)의 로컬 y-축 벡터가 월드좌표의 y-축이 되도록 즉, 똑바로 서있는 형태로 설정한다는 의미이다. 그리고 로컬 x-축 벡터와 로컬 z-축 벡터의 y-좌표가 0.0f가 되도록 한다. 이것은 로컬 x-축 벡터와 로컬 z-축 벡터를 xz-평면(지면)으로 투영하는 것을 의미한다. 즉, 3인칭 카메라의 로컬 x-축 벡터와 로컬 z-축 벡터는 xz-평면에 평행하다.*/
		if (pCamera->GetMode() == SPACESHIP_CAMERA)
		{
			m_xmf3Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
			m_xmf3Right.y = 0.0f;
			m_xmf3Look.y = 0.0f;

			XMVECTOR vRight = XMLoadFloat3(&m_xmf3Right);
			XMVECTOR vLook = XMLoadFloat3(&m_xmf3Look);
			vRight = XMVector3Normalize(vRight);
			vLook = XMVector3Normalize(vLook);
			XMStoreFloat3(&m_xmf3Right, vRight);
			XMStoreFloat3(&m_xmf3Look, vLook);
		
		}
	}
}
void CThirdPersonCamera::Update(XMFLOAT3& d3dxvLookAt, float fTimeElapsed)
{
	/*플레이어의 회전에 따라 3인칭 카메라도 회전해야 한다.*/
	if (m_pPlayer)
	{
		XMFLOAT4X4 mtxRotate;
		XMStoreFloat4x4(&mtxRotate, XMMatrixIdentity());
		XMFLOAT3 d3dxvRight = m_pPlayer->GetRightVector();
		XMFLOAT3 d3dxvUp = m_pPlayer->GetUpVector();
		XMFLOAT3 d3dxvLook = m_pPlayer->GetLookVector();
		//플레이어의 로컬 x-축, y-축, z-축 벡터로부터 회전 행렬을 생성한다.
		mtxRotate._11 = d3dxvRight.x; mtxRotate._21 = d3dxvUp.x; mtxRotate._31 = d3dxvLook.x;
		mtxRotate._12 = d3dxvRight.y; mtxRotate._22 = d3dxvUp.y; mtxRotate._32 = d3dxvLook.y;
		mtxRotate._13 = d3dxvRight.z; mtxRotate._23 = d3dxvUp.z; mtxRotate._33 = d3dxvLook.z;

		XMVECTOR d3dxvOffset; //수정
		XMVECTOR vPlayerPosition = XMLoadFloat3(&m_pPlayer->GetPosition());
		d3dxvOffset = XMVector3TransformNormal(XMLoadFloat3(&m_xmf3Offset), XMLoadFloat4x4(&mtxRotate));

		//회전한 카메라의 위치는 플레이어의 위치에 회전한 카메라 오프셋 벡터를 더한 것이다.
		XMVECTOR d3dxvPosition = vPlayerPosition + d3dxvOffset;

		//XMFLOAT3 a;
		//XMStoreFloat3(&a, d3dxvOffset);
		//cout << a.x << " " << a.y << " " << a.z << endl;

		XMVECTOR vOldPosition = XMLoadFloat3(&m_xmf3Position); //수정


		//cout << m_xmf3Position.x << "  " << m_xmf3Position.z << endl; //쓰레기값

		//현재의 카메라의 위치에서 회전한 카메라의 위치까지의 벡터이다.
		XMVECTOR d3dxvDirection = d3dxvPosition - vOldPosition;
		//float fLength = D3DXVec3Length(&d3dxvDirection);
		XMVECTOR vLength = XMVector3Length(d3dxvDirection);


	
		float fLength = XMVectorGetX(vLength);
		//cout << fLength << endl;
		d3dxvDirection = XMVector3Normalize(d3dxvDirection);
		/*3인칭 카메라의 래그(Lag)는 플레이어가 회전하더라도 카메라가 동시에 따라서 회전하지 않고 
		   약간의 시차를 두고 회전하는 효과를 구현하기 위한 것이다. m_fTimeLag가 1보다 크면 fTimeLagScale이 작아지고 
		   실제 회전이 적게 일어날 것이다.*/
		float fTimeLagScale = (m_fTimeLag) ? fTimeElapsed * (1.0f / m_fTimeLag) : 1.0f;
		float fDistance = fLength * fTimeLagScale;
		if (fDistance > fLength) fDistance = fLength;
		if (fLength < 0.01f) fDistance = fLength;
		if (fDistance > 0)
		{
			vOldPosition += d3dxvDirection * fDistance;
			SetLookAt(d3dxvLookAt);
			XMStoreFloat3(&m_xmf3Position, vOldPosition);
		}
	}
}
void CThirdPersonCamera::SetLookAt(XMFLOAT3& d3dxvLookAt)
{
	XMFLOAT4X4 mtxLookAt;
	XMVECTOR vPosition = XMLoadFloat3(&m_xmf3Position);
	XMVECTOR vLookAt = XMLoadFloat3(&d3dxvLookAt);
	XMVECTOR vPlayerUp = XMLoadFloat3(&m_pPlayer->GetUpVector());


	XMStoreFloat4x4(&mtxLookAt, XMMatrixLookAtLH(vPosition, vLookAt, vPlayerUp));
	m_xmf3Right = XMFLOAT3(mtxLookAt._11, mtxLookAt._21, mtxLookAt._31);
	m_xmf3Up = XMFLOAT3(mtxLookAt._12, mtxLookAt._22, mtxLookAt._32);
	m_xmf3Look = XMFLOAT3(mtxLookAt._13, mtxLookAt._23, mtxLookAt._33);
}

// 일반적으로 스페이스-쉽 카메라는 플레이어에 물리적으로 고정된다. 
// 그러므로 카메라의 이동과 회전은 플레이어의 이동과 회전과 일치하게 된다.  
CSpaceShipCamera::CSpaceShipCamera(CCamera *pCamera) : CCamera(pCamera)
{
	m_nMode = SPACESHIP_CAMERA;
}
void CSpaceShipCamera::Rotate(float x, float y, float z)
{
	XMVECTOR vRight = XMLoadFloat3(&m_pPlayer->GetRightVector());
	XMVECTOR vUp = XMLoadFloat3(&m_pPlayer->GetUpVector());
	XMVECTOR vLook = XMLoadFloat3(&m_pPlayer->GetLookVector());

	XMVECTOR vPosition = XMLoadFloat3(&m_xmf3Position);
	XMVECTOR vPlayerPosition = XMLoadFloat3(&m_pPlayer->GetPosition());
	XMMATRIX mtxRotate;
	if (m_pPlayer && (x != 0.0f))
	{
		//플레이어의 로컬 x-축에 대한 x 각도의 회전 행렬을 계산한다.
		mtxRotate = XMMatrixRotationAxis(vRight, (float)XMConvertToRadians(x));



		//카메라의 로컬 x-축, y-축, z-축을 회전한다.
		XMStoreFloat3(&m_xmf3Right, XMVector3TransformNormal(vRight, mtxRotate));
		XMStoreFloat3(&m_xmf3Up, XMVector3TransformNormal(vUp, mtxRotate));
		XMStoreFloat3(&m_xmf3Look, XMVector3TransformNormal(vLook, mtxRotate));



		/*카메라의 위치 벡터에서 플레이어의 위치 벡터를 뺀다. 결과는 플레이어 위치를 기준으로 한 카메라의 위치 벡터이다.*/
		//m_xmf3Position -= m_pPlayer->GetPosition();
		vPosition -= vPlayerPosition;

		//플레이어의 위치를 중심으로 카메라의 위치 벡터(플레이어를 기준으로 한)를 회전한다.
		//D3DXVec3TransformCoord(&m_xmf3Position, &m_xmf3Position, &mtxRotate);
		vPosition = XMVector3TransformNormal(vPosition, mtxRotate);

		//회전시킨 카메라의 위치 벡터에 플레이어의 위치를 더한다.
		//m_xmf3Position += m_pPlayer->GetPosition();
		vPosition+= vPlayerPosition;
		XMStoreFloat3(&m_xmf3Position, vPosition);
	}
	if (m_pPlayer && (y != 0.0f))
	{
		mtxRotate = XMMatrixRotationAxis(vUp, (float)XMConvertToRadians(y));
		XMStoreFloat3(&m_xmf3Right, XMVector3TransformNormal(vRight, mtxRotate));
		XMStoreFloat3(&m_xmf3Up, XMVector3TransformNormal(vUp, mtxRotate));
		XMStoreFloat3(&m_xmf3Look, XMVector3TransformNormal(vLook, mtxRotate));

		vPosition -= vPlayerPosition;
		vPosition = XMVector3TransformNormal(vPosition, mtxRotate);
		vPosition += vPlayerPosition;
		XMStoreFloat3(&m_xmf3Position, vPosition);
	}
	if (m_pPlayer && (z != 0.0f))
	{
		mtxRotate = XMMatrixRotationAxis(vUp, (float)XMConvertToRadians(z));
		XMStoreFloat3(&m_xmf3Right, XMVector3TransformNormal(vRight, mtxRotate));
		XMStoreFloat3(&m_xmf3Up, XMVector3TransformNormal(vUp, mtxRotate));
		XMStoreFloat3(&m_xmf3Look, XMVector3TransformNormal(vLook, mtxRotate));

		vPosition -= vPlayerPosition;
		vPosition = XMVector3TransformNormal(vPosition, mtxRotate);
		vPosition += vPlayerPosition;
		XMStoreFloat3(&m_xmf3Position, vPosition);
	}
}


void CCamera::CalculateFrustumPlanes() //수정
{
	/*카메라 변환 행렬과 원근 투영 변환 행렬을 곱한 행렬을 사용하여 절두체 평면들을 구한다. 즉 월드 좌표계에서 절두체 컬링을 한다.*/
	XMFLOAT4X4 mtxViewProject; //= m_xmmtxView * m_xmmtxProjection;
	XMStoreFloat4x4(&mtxViewProject, XMLoadFloat4x4(&m_xmmtxView) * XMLoadFloat4x4(&m_xmmtxProjection));

	//절두체의 왼쪽 평면
	m_xmf4FrustumPlanes[0].x = -(mtxViewProject._14 + mtxViewProject._11);
	m_xmf4FrustumPlanes[0].y = -(mtxViewProject._24 + mtxViewProject._21);
	m_xmf4FrustumPlanes[0].z = -(mtxViewProject._34 + mtxViewProject._31);
	m_xmf4FrustumPlanes[0].w = -(mtxViewProject._44 + mtxViewProject._41);

	//절두체의 오른쪽 평면
	m_xmf4FrustumPlanes[1].x = -(mtxViewProject._14 - mtxViewProject._11);
	m_xmf4FrustumPlanes[1].y = -(mtxViewProject._24 - mtxViewProject._21);
	m_xmf4FrustumPlanes[1].z = -(mtxViewProject._34 - mtxViewProject._31);
	m_xmf4FrustumPlanes[1].w = -(mtxViewProject._44 - mtxViewProject._41);

	//절두체의 위쪽 평면
	m_xmf4FrustumPlanes[2].x = -(mtxViewProject._14 - mtxViewProject._12);
	m_xmf4FrustumPlanes[2].y = -(mtxViewProject._24 - mtxViewProject._22);
	m_xmf4FrustumPlanes[2].z = -(mtxViewProject._34 - mtxViewProject._32);
	m_xmf4FrustumPlanes[2].w = -(mtxViewProject._44 - mtxViewProject._42);

	//절두체의 아래쪽 평면
	m_xmf4FrustumPlanes[3].x = -(mtxViewProject._14 + mtxViewProject._12);
	m_xmf4FrustumPlanes[3].y = -(mtxViewProject._24 + mtxViewProject._22);
	m_xmf4FrustumPlanes[3].z = -(mtxViewProject._34 + mtxViewProject._32);
	m_xmf4FrustumPlanes[3].w = -(mtxViewProject._44 + mtxViewProject._42);

	//절두체의 근평면
	m_xmf4FrustumPlanes[4].x = -(mtxViewProject._13);
	m_xmf4FrustumPlanes[4].y = -(mtxViewProject._23);
	m_xmf4FrustumPlanes[4].z = -(mtxViewProject._33);
	m_xmf4FrustumPlanes[4].w = -(mtxViewProject._43);

	//절두체의 원평면
	m_xmf4FrustumPlanes[5].x = -(mtxViewProject._14 - mtxViewProject._13);
	m_xmf4FrustumPlanes[5].y = -(mtxViewProject._24 - mtxViewProject._23);
	m_xmf4FrustumPlanes[5].z = -(mtxViewProject._34 - mtxViewProject._33);
	m_xmf4FrustumPlanes[5].w = -(mtxViewProject._44 - mtxViewProject._43);

	/*절두체의 각 평면의 법선 벡터 (a, b. c)의 크기로 a, b, c, d를 나눈다. 즉, 법선 벡터를 정규화하고 원점에서 평면까지의 거리를 계산한다.*/
	//for (int i = 0; i < 6; i++) D3DXPlaneNormalize(&m_xmf4FrustumPlanes[i], &m_xmf4FrustumPlanes[i]);

	for (int i = 0; i < 6; ++i)
	{
		XMVECTOR vPlane = XMPlaneNormalize(XMLoadFloat4(&m_xmf4FrustumPlanes[i]));
		XMStoreFloat4(&m_xmf4FrustumPlanes[i], vPlane);
	}
}

bool CCamera::IsInFrustum(XMFLOAT3& d3dxvMinimum, XMFLOAT3& d3dxvMaximum)
{
	XMFLOAT3 d3dxvNearPoint, d3dxvFarPoint, xmf3Normal;
	for (int i = 0; i < 6; i++)
	{
		/*절두체의 각 평면에 대하여 바운딩 박스의 근접점을 계산한다. 근접점의 x, y, z 좌표는 법선 벡터의 각 요소가 음수이면 바운딩 박스의 최대점의 좌표가 되고 그렇지 않으면 바운딩 박스의 최소점의 좌표가 된다.*/
		xmf3Normal = XMFLOAT3(m_xmf4FrustumPlanes[i].x, m_xmf4FrustumPlanes[i].y, m_xmf4FrustumPlanes[i].z);
		if (xmf3Normal.x >= 0.0f)
		{
			if (xmf3Normal.y >= 0.0f)
			{
				if (xmf3Normal.z >= 0.0f)
				{
					//법선 벡터의 x, y, z 좌표의 부호가 모두 양수이므로 근접점은 바운딩 박스의 최소점이다.
					d3dxvNearPoint.x = d3dxvMinimum.x; d3dxvNearPoint.y = d3dxvMinimum.y; d3dxvNearPoint.z = d3dxvMinimum.z;
				}
				else
				{
					/*법선 벡터의 x, y 좌표의 부호가 모두 양수이므로 근접점의 x, y 좌표는 바운딩 박스의 최소점의 x, y 좌표이고 법선 벡터의 z 좌표가 움수이므로 근접점의 z 좌표는 바운딩 박스의 최대점의 z 좌표이다.*/
					d3dxvNearPoint.x = d3dxvMinimum.x; d3dxvNearPoint.y = d3dxvMinimum.y; d3dxvNearPoint.z = d3dxvMaximum.z;
				}
			}
			else
			{
				if (xmf3Normal.z >= 0.0f)
				{
					/*법선 벡터의 x, z 좌표의 부호가 모두 양수이므로 근접점의 x, z 좌표는 바운딩 박스의 최소점의 x, z 좌표이고 법선 벡터의 y 좌표가 움수이므로 근접점의 y 좌표는 바운딩 박스의 최대점의 y 좌표이다.*/
					d3dxvNearPoint.x = d3dxvMinimum.x; d3dxvNearPoint.y = d3dxvMaximum.y; d3dxvNearPoint.z = d3dxvMinimum.z;
				}
				else
				{
					/*법선 벡터의 y, z 좌표의 부호가 모두 음수이므로 근접점의 y, z 좌표는 바운딩 박스의 최대점의 y, z 좌표이고 법선 벡터의 x 좌표가 양수이므로 근접점의 x 좌표는 바운딩 박스의 최소점의 x 좌표이다.*/
					d3dxvNearPoint.x = d3dxvMinimum.x; d3dxvNearPoint.y = d3dxvMaximum.y; d3dxvNearPoint.z = d3dxvMaximum.z;
				}
			}
		}
		else
		{
			if (xmf3Normal.y >= 0.0f)
			{
				if (xmf3Normal.z >= 0.0f)
				{
					/*법선 벡터의 y, z 좌표의 부호가 모두 양수이므로 근접점의 y, z 좌표는 바운딩 박스의 최소점의 y, z 좌표이고 법선 벡터의 x 좌표가 음수이므로 근접점의 x 좌표는 바운딩 박스의 최대점의 x 좌표이다.*/
					d3dxvNearPoint.x = d3dxvMaximum.x; d3dxvNearPoint.y = d3dxvMinimum.y; d3dxvNearPoint.z = d3dxvMinimum.z;
				}
				else
				{
					/*법선 벡터의 x, z 좌표의 부호가 모두 음수이므로 근접점의 x, z 좌표는 바운딩 박스의 최대점의 x, z 좌표이고 법선 벡터의 y 좌표가 양수이므로 근접점의 y 좌표는 바운딩 박스의 최소점의 y 좌표이다.*/
					d3dxvNearPoint.x = d3dxvMaximum.x; d3dxvNearPoint.y = d3dxvMinimum.y; d3dxvNearPoint.z = d3dxvMaximum.z;
				}
			}
			else
			{
				if (xmf3Normal.z >= 0.0f)
				{
					/*법선 벡터의 x, y 좌표의 부호가 모두 음수이므로 근접점의 x, y 좌표는 바운딩 박스의 최대점의 x, y 좌표이고 법선 벡터의 z 좌표가 양수이므로 근접점의 z 좌표는 바운딩 박스의 최소점의 z 좌표이다.*/
					d3dxvNearPoint.x = d3dxvMaximum.x; d3dxvNearPoint.y = d3dxvMaximum.y; d3dxvNearPoint.z = d3dxvMinimum.z;
				}
				else
				{
					//법선 벡터의 x, y, z 좌표의 부호가 모두 음수이므로 근접점은 바운딩 박스의 최대점이다.
					d3dxvNearPoint.x = d3dxvMaximum.x; d3dxvNearPoint.y = d3dxvMaximum.y; d3dxvNearPoint.z = d3dxvMaximum.z;
				}
			}
		}
		/*근접점이 절두체 평면 중 하나의 평면의 바깥(앞)쪽에 있으면 근접점은 절두체에 포함되지 않는다. 근접점이 어떤 평면의 바깥(앞)쪽에 있으면 근접점을 평면의 방정식에 대입하면 부호가 양수가 된다. 
		각 평면의 방정식에 근접점을 대입하는 것은 근접점 (x, y, z)과 평면의 법선벡터 (a, b, c)의 내적에 원점에서 평면까지의 거리를 더한 것과 같다.*/

		//if ((D3DXVec3Dot(&xmf3Normal, &d3dxvNearPoint) + m_xmf4FrustumPlanes[i].d) > 0.0f) return(false);
		XMVECTOR vNormal = XMLoadFloat3(&xmf3Normal);
		XMVECTOR vNearPoint = XMLoadFloat3(&d3dxvNearPoint);
		XMVECTOR vFrustumPlane = XMLoadFloat4(&m_xmf4FrustumPlanes[i]);
		if(XMVectorGetX(XMVector3Dot(vNormal, vNearPoint)) + XMVectorGetW(vFrustumPlane)>0.0f)return(false);
	}
	return(true);
}

bool CCamera::IsInFrustum(AABB *pAABB)
{
	return(IsInFrustum(pAABB->m_xmf3Minimum, pAABB->m_xmf3Maximum));
}
