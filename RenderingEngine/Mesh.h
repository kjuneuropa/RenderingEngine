#pragma once
#include "stdafx.h"
#include "xnacollision.h"

// 7월 30일 머지 리퀘스트
/*정점의 색상을 무작위로(Random) 설정하기 위해 사용한다. 각 정점의 색상은 난수(Random Number)를 생성하여 지정한다.*/
#define RANDOM_COLOR XMFLOAT4((rand() * 0xFFFFFF) / RAND_MAX)


struct MESHINTERSECTINFO {
	DWORD m_dwFaceIndex;
	float m_fU;
	float m_fV;
	float m_fDistance;
};



struct TempVertexType
{ 
	float x, y, z; float tu, tv; float nx, ny, nz; 
}; 
struct VectorType 
{ 
	float x, y, z;
};

class CCamera;

class AABB
{
public:
	//바운딩 박스의 최소점과 최대점을 나타내는 벡터이다.
	XMFLOAT3 m_xmf3Minimum;
	XMFLOAT3 m_xmf3Maximum;

public:
	AABB() { m_xmf3Minimum = XMFLOAT3(+FLT_MAX, +FLT_MAX, +FLT_MAX); m_xmf3Maximum = XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX); }
	AABB(XMFLOAT3 xmf3Minimum, XMFLOAT3 xmf3Maximum) { m_xmf3Minimum = xmf3Minimum; m_xmf3Maximum = xmf3Maximum; }

	//두 개의 바운딩 박스를 합한다.
	void Union(XMFLOAT3& xmf3Minimum, XMFLOAT3& xmf3Maximum);
	void Union(AABB *pAABB);
	//바운딩 박스의 8개의 꼭지점을 행렬로 변환하고 최소점과 최대점을 다시 계산한다.
	void Update(XMMATRIX *pxmmtxTransform);
	void Print() {  cout << "min : [" << m_xmf3Minimum.x << " " << m_xmf3Minimum.y << " " << m_xmf3Minimum.z << "]" << endl;
	cout << "max : [" << m_xmf3Maximum.x << " " << m_xmf3Maximum.y << " " << m_xmf3Maximum.z << "]" << endl;}
};


class CMesh
{
public:
	CMesh(ID3D11Device *pd3dDevice);
	virtual ~CMesh();

private:
	int m_nReferences;

public:
	void AddRef() { m_nReferences++; }
	void Release() { if (--m_nReferences <= 0) delete this; }
	//Mesh.cpp 파일에서 CMesh::AddRef(), CMesh::Release() 함수의 정의를 삭제한다. 

protected:
	AABB m_bcBoundingCube;

protected:

	/*각 정점의 위치 벡터를 픽킹을 위하여 저장한다
	(정점 버퍼를 DYNAMIC으로 생성하고 Map()을 하지 않아도 되도록).*/
	//XMFLOAT3 *m_vPositions;
	vector<XMFLOAT3> m_vPositions;
	/*메쉬의 인덱스를 저장한다(인덱스 버퍼를 DYNAMIC으로 생성하고 Map()을 하지 않아도 되도록).*/
	UINT *m_pnIndices;
	D3D11_PRIMITIVE_TOPOLOGY m_d3dPrimitiveTopology;

	//정점의 위치 벡터와 색상을 저장하기 위한 버퍼에 대한 인터페이스 포인터이다. 
	ID3D11Buffer *m_pd3dPositionBuffer;
	
	//버퍼들을 입력조립기에 연결하기 위한 시작 슬롯 번호이다. 
	UINT m_nSlot;
	/*인스턴싱을 위한 정점 버퍼는 메쉬의 정점 데이터와 인스턴싱 데이터(객체의 위치와 방향)를 갖는다. 그러므로 인스턴싱을 위한 정점 버퍼는 하나가 아니라 버퍼들의 배열이다. 정점의 요소들을 나타내는 버퍼들을 입력조립기에 전달하기 위한 버퍼이다.*/
	ID3D11Buffer **m_ppd3dVertexBuffers;
	//정점을 조립하기 위해 필요한 버퍼의 개수이다. 
	int m_nBuffers;

	//정점의 개수이다. 
	int m_nVertices;
	UINT m_nStartVertex;
	//정점의 요소들을 나타내는 버퍼들의 원소의 바이트 수를 나타내는 배열이다. 
	UINT *m_pnVertexStrides;
	//정점의 요소들을 나타내는 버퍼들의 시작 위치(바이트 수)를 나타내는 배열이다. 
	UINT *m_pnVertexOffsets;

	//인덱스 버퍼(인덱스의 배열)에 대한 인터페이스 포인터이다. 
	ID3D11Buffer *m_pd3dIndexBuffer;
	//인덱스 버퍼가 포함하는 인덱스의 개수이다. 
	UINT m_nIndices;
	//인덱스 버퍼에서 메쉬를 표현하기 위해 사용되는 시작 인덱스이다. 
	UINT m_nStartIndex;
	//각 인덱스에 더해질 인덱스이다. 
	int m_nBaseVertex;
	UINT m_nIndexOffset;
	//각 인덱스의 형식(DXGI_FORMAT_R32_UINT 또는 DXGI_FORMAT_R16_UINT)이다. 
	DXGI_FORMAT	m_dxgiIndexFormat;

	ID3D11RasterizerState *m_pd3dRasterizerState;

public:
	AABB GetBoundingCube() { return(m_bcBoundingCube); }
	int GetVertexNum() { return m_nVertices; }
	//메쉬의 정점 버퍼들을 배열로 조립한다. 
	void AssembleToVertexBuffer(int nBuffers = 0, ID3D11Buffer **m_pd3dBuffers = nullptr, UINT *pnBufferStrides = nullptr, UINT *pnBufferOffsets = nullptr);

	virtual void CreateRasterizerState(ID3D11Device *pd3dDevice) { }
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext);
	virtual void Render(CCamera* pCamera, ID3D11DeviceContext *pd3dDeviceContext){}
	//인스턴싱을 사용하여 렌더링한다. 
	virtual void RenderInstanced(ID3D11DeviceContext *pd3dDeviceContext, int nInstances = 0, int nStartInstance = 0);
	virtual int GetTextLength() { return -1; }
	ID3D11Buffer** GetVertexBuffer() { return m_ppd3dVertexBuffers; }
	
public:


	int CheckRayIntersection(XMFLOAT3 *pxmf3RayPosition, XMFLOAT3 *pxmf3RayDirection, MESHINTERSECTINFO *pxmf3IntersectInfo);
	int CheckRayIntersectionAABB(XMFLOAT3 *pxmf3RayPosition, XMFLOAT3 *pxmf3RayDirection, MESHINTERSECTINFO *pxmf3IntersectInfo);


};






class CMeshDiffused : public CMesh
{
public:
	CMeshDiffused(ID3D11Device *pd3dDevice);
	virtual ~CMeshDiffused();

protected:
	ID3D11Buffer *m_pd3dColorBuffer;
};
class CMeshIlluminated : public CMesh
{
public:
	CMeshIlluminated(ID3D11Device *pd3dDevice);
	virtual ~CMeshIlluminated();

protected:
	//조명의 영향을 계산하기 위하여 법선벡터가 필요하다.
	ID3D11Buffer *m_pd3dNormalBuffer;

public:
	//정점이 포함된 삼각형의 법선벡터를 계산하는 함수이다.
	XMFLOAT3 CalculateTriAngleNormal(UINT nIndex0, UINT nIndex1, UINT nIndex2);
	void SetTriAngleListVertexNormal(XMFLOAT3 *pxmf3Normals);
	//정점의 법선벡터의 평균을 계산하는 함수이다.
	void SetAverageVertexNormal(XMFLOAT3 *pxmf3Normals, int nPrimitives, int nOffset, bool bStrip);
	void CalculateVertexNormal(XMFLOAT3 *pxmf3Normals);
};
class CCubeMeshIlluminated : public CMeshIlluminated
{
public:
	CCubeMeshIlluminated(ID3D11Device *pd3dDevice, float gScreenWidth = 2.0f, float gScreenHeight = 2.0f, float fDepth = 2.0f);
	virtual ~CCubeMeshIlluminated();
};
class CSphereMeshIlluminated : public CMeshIlluminated
{
public:
	CSphereMeshIlluminated(ID3D11Device *pd3dDevice, float fRadius = 2.0f, int nSlices = 20, int nStacks = 20);
	virtual ~CSphereMeshIlluminated();
};

class CSphereMeshDiffused : public CMeshDiffused
{
public:
	CSphereMeshDiffused(ID3D11Device *pd3dDevice, float fRadius = 2.0f, int nSlices = 20, int nStacks = 20, XMFLOAT4 d3dxColor = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f));
	virtual ~CSphereMeshDiffused();
};

class CMeshTextured : public CMesh
{
public:
	CMeshTextured(ID3D11Device *pd3dDevice);
	virtual ~CMeshTextured();
	
protected:
	//텍스쳐 매핑을 하기 위하여 텍스쳐 좌표가 필요하다.
	ID3D11Buffer *m_pd3dTexCoordBuffer;
};





//========================================================
class CTexture;

class CSkyBoxMesh : public CMeshTextured
{
protected:
	ID3D11DepthStencilState *m_pd3dDepthStencilState;

	CTexture *m_pSkyboxTexture;
	ID3D11ShaderResourceView* m_pd3dsrvCubeMap;
	ID3D11SamplerState* m_pd3dSamplerState;


public:
	CSkyBoxMesh(ID3D11Device *pd3dDevice, float gScreenWidth = 20.0f, float gScreenHeight = 20.0f, float fDepth = 20.0f);
	virtual ~CSkyBoxMesh();

	void OnChangeSkyBoxTextures(ID3D11Device *pd3dDevice, int nIndex = 0);

	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext);
};




//=========================================================

class CMeshDetailTextured : public CMeshTextured
{
public:
	CMeshDetailTextured(ID3D11Device *pd3dDevice);
	virtual ~CMeshDetailTextured();

protected:
	ID3D11Buffer *m_pd3dDetailTexCoordBuffer;
};





/*텍스쳐 매핑을 사용하여 색상을 결정하기 위하여 
   정점이 텍스쳐 좌표를 갖는 직육면체 메쉬 클래스이다.*/
class CCubeMeshTextured : public CMeshTextured
{
public:
	CCubeMeshTextured(ID3D11Device *pd3dDevice, float gScreenWidth = 2.0f, float gScreenHeight = 2.0f, float fDepth = 2.0f);
	virtual ~CCubeMeshTextured();
};

/*텍스쳐 매핑을 사용하여 색상을 결정하기 위하여 
  정점이 텍스쳐 좌표를 갖는 구 메쉬 클래스이다.*/
class CSphereMeshTextured : public CMeshTextured
{
public:
	CSphereMeshTextured(ID3D11Device *pd3dDevice, float fRadius = 2.0f, int nSlices = 20, int nStacks = 20);
	virtual ~CSphereMeshTextured();
};


//======================================================

class CMeshTexturedIlluminated : public CMeshIlluminated
{
public:
	CMeshTexturedIlluminated(ID3D11Device *pd3dDevice);
	virtual ~CMeshTexturedIlluminated();
	void CalculateVectors(XMFLOAT2* pxmf2Tex, XMFLOAT3* pxmf3Normals, XMFLOAT3* pxmf3Binormals, XMFLOAT3* pxmf3Tangents);
	
	void CalculateTangentBinormal(TempVertexType vertex1, TempVertexType vertex2, TempVertexType vertex3, VectorType& tangent, VectorType& binormal);
	void CalculateNormal(VectorType tangent, VectorType binormal, VectorType& normal);

protected:
	ID3D11Buffer *m_pd3dTexCoordBuffer;
	ID3D11Buffer *m_pd3dBinormalBuffer;
	ID3D11Buffer *m_pd3dTengentBuffer;
};

class CMeshDetailTexturedIlluminated : public CMeshIlluminated
{
public:
	CMeshDetailTexturedIlluminated(ID3D11Device *pd3dDevice);
	virtual ~CMeshDetailTexturedIlluminated();

protected:
	ID3D11Buffer *m_pd3dTexCoordBuffer;
	ID3D11Buffer *m_pd3dDetailTexCoordBuffer;
};

class CCubeMeshTexturedIlluminated : public CMeshTexturedIlluminated
{
public:
	CCubeMeshTexturedIlluminated(ID3D11Device *pd3dDevice, float gScreenWidth = 2.0f, float gScreenHeight = 2.0f, float fDepth = 2.0f);
	virtual ~CCubeMeshTexturedIlluminated();
private:

};

class CSphereMeshTexturedIlluminated : public CMeshTexturedIlluminated
{
public:
	CSphereMeshTexturedIlluminated(ID3D11Device *pd3dDevice, float fRadius = 2.0f, int nSlices = 20, int nStacks = 20);
	virtual ~CSphereMeshTexturedIlluminated();
};

class CPlaneMeshTexturedIlluminated : public CMeshTexturedIlluminated
{
public:
	CPlaneMeshTexturedIlluminated(ID3D11Device *pd3dDevice, float gScreenWidth = 2.0f, float fDepth = 2.0f);
	virtual ~CPlaneMeshTexturedIlluminated();
};

/*CHeightMapGridMesh 클래스의 베이스 클래스를 CMeshDetailTextured로 변경한다.
지형 메쉬의 각 정점은 두 개의 텍스쳐 좌표를 갖는다.*/

//CHeightMapGridMesh 클래스의 베이스 클래스를 CMeshDetailTexturedIlluminated로 변경한다.
class CHeightMapGridMesh : public CMeshDetailTexturedIlluminated
{
protected:
	int m_nWidth;
	int m_nLength;
	XMFLOAT3 m_xmvScale;

public:
	//생성자를 다음과 같이 변경한다.
	CHeightMapGridMesh(ID3D11Device *pd3dDevice, int xStart, int zStart, int nWidth, int nLength, XMFLOAT3 pxmf3Scale = XMFLOAT3(1.0f, 1.0f, 1.0f), void *pContext = nullptr);
	virtual ~CHeightMapGridMesh();

	XMFLOAT3 GetScale() { return(m_xmvScale); }
	int GetWidth() { return(m_nWidth); }
	int GetLength() { return(m_nLength); }

	virtual float OnGetHeight(int x, int z, void *pContext);
};







class CAlphaBlendingMeshTextured : public CMeshTextured
{
public:
	CAlphaBlendingMeshTextured(ID3D11Device *pd3dDevice);
	virtual ~CAlphaBlendingMeshTextured();
};

class CAlphaBlendingWaterMeshTextured : public CAlphaBlendingMeshTextured
{
public:
	CAlphaBlendingWaterMeshTextured(ID3D11Device *pd3dDevice, float gScreenWidth = 2.0f, float gScreenHeight = 2.0f, float fDepth = 2.0f);
	virtual ~CAlphaBlendingWaterMeshTextured();
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext);
	void CreateBlendingState(ID3D11Device *pd3dDevice);
protected:
	CTexture *m_pWaterTexture;
	ID3D11DepthStencilState *m_pd3dDepthStencilState;
	ID3D11BlendState* m_pd3dBlendingState;
	ID3D11Buffer *m_pd3dTimeBuffer;

	XMMATRIX m_d3dxTextureMatrix;

	//public:
	//CWaterMesh(ID3D11Device *pd3dDevice, float gScreenWidth = 2.0f, float gScreenHeight = 2.0f, float fDepth = 2.0f);
};

class CScreenQuadMesh : public CMesh
{
public:
	CScreenQuadMesh(ID3D11Device *pd3dDevice);
	virtual ~CScreenQuadMesh();
	ID3D11Buffer *m_pd3dTexCoordBuffer;
};

class CAxisMesh : public CMeshDiffused
{
public:
	CAxisMesh(ID3D11Device *pd3dDevice, XMFLOAT3 xmf3Center, XMFLOAT3 xmf3Extent);
	virtual ~CAxisMesh();
};

class CFixedMesh : public CMeshTexturedIlluminated
{
public:
	CFixedMesh(ID3D11Device *pd3dDevice, string strFileName);
	virtual ~CFixedMesh();
	//virtual  void Render(ID3D11DeviceContext *pd3dDeviceContext);
};

class CSentenceMesh :  public CMeshTextured
{
private:
	
	int m_nMaxLength;
	VertexType* m_pVertexs;
public:
	CSentenceMesh(ID3D11Device *pd3dDevice, int nMaxLength);
	virtual  ~CSentenceMesh();
	virtual int GetTextLength() { return m_nMaxLength; }
	//void UpdateText(char* text, int positionX, int positionY, float red, float green, float blue,
	//	ID3D11DeviceContext* deviceContext);
};