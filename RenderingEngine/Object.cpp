#include "Object.h"
#include "Shader.h"
#include "GameFramework.h"
using namespace std;

ID3D11Buffer *CSentenceObject::m_pd3dcbColor = nullptr;
CFont* CSentenceObject::m_pFont = nullptr;

CGameObject::CGameObject(int nMeshes)
{
	m_nObjectType = -1;
	m_bActive = true;
	m_nReferences = 0;
}

CGameObject::~CGameObject()
{
	
}


void CGameObject::AddRef()
{
	m_nReferences++;
}
void CGameObject::Release()
{
	if (m_nReferences > 0) m_nReferences--;
	if (m_nReferences <= 0) delete this;
}

void CGameObject::Animate(float fTimeElapsed)
{
}




XMFLOAT3 CGameObject::GetPosition()
{
	return GetComponent<CTransform>()->GetPosition();
}


void CGameObject::GenerateRayForPicking(XMFLOAT3 *pd3dxvPickPosition, XMMATRIX *pd3dxmtxWorld, XMMATRIX *pd3dxmtxView, XMFLOAT3 *pd3dxvPickRayPosition, XMFLOAT3 *pd3dxvPickRayDirection)
{
	//pd3dxvPickPosition: 카메라 좌표계의 점(화면 좌표계에서 마우스를 클릭한 점을 역변환한 점)
	//pd3dxmtxWorld: 월드 변환 행렬, pd3dxmtxView: 카메라 변환 행렬
	//pd3dxvPickRayPosition: 픽킹 광선의 시작점, pd3dxvPickRayDirection: 픽킹 광선 벡터
	/*객체의 월드 변환 행렬이 주어지면 객체의 월드 변환 행렬과 카메라 변환 행렬을 곱하고 역행렬을 구한다. 이것은 카메라 변환 행렬의 역행렬과 객체의 월드 변환 행렬의 역행렬의 곱과 같다. 객체의 월드 변환 행렬이 주어지지 않으면 카메라 변환 행렬의 역행렬을 구한다. 객체의 월드 변환 행렬이 주어지면 모델 좌표계의 픽킹 광선을 구하고 그렇지 않으면 월드 좌표계의 픽킹 광선을 구한다.*/
	XMMATRIX xmmtxInverse;
	XMMATRIX xmmtxWorldView = *pd3dxmtxView;
	if (pd3dxmtxWorld) xmmtxWorldView = XMMatrixMultiply(*pd3dxmtxWorld, *pd3dxmtxView);

	XMVECTOR xmvDeterminant;
	xmmtxInverse = XMMatrixInverse(&xmvDeterminant, xmmtxWorldView);
	//d3dxmtxInverse = XMMatrixInverse(nullptr, d3dxmtxWorldView);
	
	/*카메라 좌표계의 원점 (0, 0, 0)을 위에서 구한 역행렬로 변환한다. 변환의 결과는 카메라 좌표계의 원점에 대응되는 모델 좌표계의 점 또는 월드 좌표계의 점이다.*/
	XMVECTOR xmvCameraOrigin = XMVectorZero();
	XMVECTOR xmvPickRayPosition = XMLoadFloat3(pd3dxvPickRayPosition);
	xmvPickRayPosition = XMVector3TransformCoord(xmvCameraOrigin, xmmtxInverse);
	/*카메라 좌표계의 점을 위에서 구한 역행렬로 변환한다. 변환의 결과는 마우스를 클릭한 점에 대응되는 모델 좌표계의 점 또는 월드 좌표계의 점이다.*/
	// D3DXVec3TransformCoord(pd3dxvPickRayDirection, pd3dxvPickPosition, &d3dxmtxInverse);
	XMVECTOR xmvPickRayDirection = XMLoadFloat3(pd3dxvPickRayDirection);
	XMVECTOR xmvPickPosition = XMLoadFloat3(pd3dxvPickPosition);
	xmvPickRayDirection = XMVector3TransformCoord(xmvPickPosition, xmmtxInverse);
	//픽킹 광선의 방향 벡터를 구한다.
	xmvPickRayDirection = xmvPickRayDirection - xmvPickRayPosition;
	//*pd3dxvPickRayDirection = *pd3dxvPickRayDirection - *pd3dxvPickRayPosition;
	XMStoreFloat3(pd3dxvPickRayPosition, xmvPickRayPosition);
	XMStoreFloat3(pd3dxvPickRayDirection, xmvPickRayDirection);

}

int CGameObject::PickObjectByRayIntersection(XMFLOAT3 *pd3dxvPickPosition, XMMATRIX *pd3dxmtxView, MESHINTERSECTINFO *pd3dxIntersectInfo)
{
	XMFLOAT3 xmf3PickRayPosition, xmf3PickRayDirection;
	XMMATRIX xmmtxWorld = XMLoadFloat4x4(&GetComponent<CTransform>()->GetWorldMtx());
	int nIntersected = 0;
	if (m_bActive && GetComponent<CRenderer>()->GetMesh())
	{
		GenerateRayForPicking(pd3dxvPickPosition, &xmmtxWorld, pd3dxmtxView, &xmf3PickRayPosition, &xmf3PickRayDirection);
		CMesh** ppMeshes = GetComponent<CRenderer>()->GetMesh();
		int nMeshes = GetComponent<CRenderer>()->GetMeshNum();
		for (int i = 0; i < nMeshes; i++)
		{
		    //nIntersected = m_ppMeshes[i]->CheckRayIntersection(&xmf3PickRayPosition, &xmf3PickRayDirection, pd3dxIntersectInfo);
			nIntersected = ppMeshes[i]->CheckRayIntersection(&xmf3PickRayPosition, &xmf3PickRayDirection, pd3dxIntersectInfo);
			//nIntersected = m_ppMeshes[i]->CheckRayIntersectionAABB(&xmf3PickRayPosition, &xmf3PickRayDirection, pd3dxIntersectInfo);
			if (nIntersected > 0) break;
		}
	}
	return(nIntersected);
}

bool CGameObject::IsVisible(CCamera *pCamera)
{
	OnPrepareRender();
	XMMATRIX xmmtxWorld = XMLoadFloat4x4(&GetComponent<CTransform>()->GetWorldMtx());
	bool bIsVisible = false;
	if (m_bActive)
	{
		AABB bcBoundingCube = *GetComponent<CRenderer>()->GetAABB();
		bcBoundingCube.Update(&xmmtxWorld);
		XMStoreFloat4x4(&GetComponent<CTransform>()->GetWorldMtx(), xmmtxWorld);
		if (pCamera) bIsVisible = pCamera->IsInFrustum(&bcBoundingCube);
	}
	return(bIsVisible);
}
// *************
void CGameObject::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	for (auto& component : m_Components)
	{
		component.second->Update(pd3dDeviceContext, pCamera);
	}
}


void CGameObject::RemoveNormalMap()
{
	m_bNormalMap = false;

}





CFont::CFont(ID3D11Device* pd3dDevice, wchar_t* pFontFile, wchar_t* pTextureFile)
{
	LoadFontData(pFontFile);
	LoadTexture(pd3dDevice, pTextureFile);

}
CFont::~CFont()
{
	if (m_pFontType) delete[]m_pFontType;
	if (m_pTexture) m_pTexture->Release();
}
void CFont::LoadFontData(wchar_t* pFontFile)
{
	ifstream fin;
	char temp;

	// Create the font spacing buffer.
	m_pFontType = new FontType[95];

	// Read in the font size and spacing between chars.
	fin.open(pFontFile);
	
	// Read in the 95 used ascii characters for text.
	for (int i = 0; i<95; i++)
	{
		fin.get(temp);
		while (temp != ' ')
		{
			fin.get(temp);
		}
		fin.get(temp);
		while (temp != ' ')
		{
			fin.get(temp);
		}

		fin >> m_pFontType[i].left;
		fin >> m_pFontType[i].right;
		fin >> m_pFontType[i].size;
		
	}

	fin.close();
}
void CFont::LoadTexture(ID3D11Device* pd3dDevice, wchar_t* pTextureFile)
{
	ID3D11SamplerState *pd3dBaseSamplerState = nullptr;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;

	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dBaseSamplerState);

	m_pTexture = new CTexture(1, 1, 0, 0);
	ID3D11ShaderResourceView *pd3dsrvTexture = nullptr; // dirt01d.tga
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, pTextureFile, nullptr, nullptr, &pd3dsrvTexture, nullptr);

	m_pTexture->SetTexture(0, pd3dsrvTexture);
	m_pTexture->SetSampler(0, pd3dBaseSamplerState);

	pd3dsrvTexture->Release();
	pd3dBaseSamplerState->Release();
}
void  CFont::UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext)
{
	if (m_pTexture)m_pTexture->UpdateShaderVariable(pd3dDeviceContext);
}
void CFont::BuildVertexAarry(void* pVertexs, const wchar_t* pText, float fPosX, float fPosY)
{
	VertexType* pVertexPtr;
	int nLetterNum, nIndex, nLetter;


	// Coerce the input vertices into a VertexType structure.
	pVertexPtr = static_cast<VertexType*>(pVertexs);


	// Get the number of nLetters in the sentence.
	nLetterNum = wcslen(pText);

	// Initialize the nIndex to the vertex array.
	nIndex = 0;

	// Draw each nLetter onto a quad.
	for (int i = 0; i<nLetterNum; i++)
	{
		nLetter = static_cast<int>(pText[i] - 32);

		// If the nLetter is a space then just move over three pixels.
		if (nLetter == 0)
		{
			fPosX = fPosX + 3.0f;
		}
		else
		{
			
			// First triangle in quad.
			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX), FromToNDC_Y(fPosY), 0.0f);  // Top left.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_pFontType[nLetter].left, 0.0f);
			nIndex++;

			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX + m_pFontType[nLetter].size), FromToNDC_Y(fPosY + 16), 0.0f);  // Bottom right.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_pFontType[nLetter].right, 1.0f);
			nIndex++;

			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX), FromToNDC_Y(fPosY + 16), 0.0f);  // Bottom left.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_pFontType[nLetter].left, 1.0f);
			nIndex++;
			// Second triangle in quad.
			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX), FromToNDC_Y(fPosY), 0.0f);  // Top left.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_pFontType[nLetter].left, 0.0f);
			nIndex++;

			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX + m_pFontType[nLetter].size), FromToNDC_Y(fPosY), 0.0f);  // Top right.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_pFontType[nLetter].right, 0.0f);
			nIndex++;

			pVertexPtr[nIndex].xmf3Pos = XMFLOAT3(FromToNDC_X(fPosX + m_pFontType[nLetter].size), FromToNDC_Y(fPosY + 16), 0.0f);  // Bottom right.
			pVertexPtr[nIndex].xmf2Tex = XMFLOAT2(m_pFontType[nLetter].right, 1.0f);
			nIndex++;


			// Update the x location for drawing by the size of the nLetter and one pixel.
			fPosX = fPosX + m_pFontType[nLetter].size + 1.0f;
		}
	}
}
float CFont::FromToNDC_X(float fScreenX) 
{ 
	int nWindowWidth = CGameFramework::GetInstance().GetWindowWidth();
	return static_cast<float>(2.0 / nWindowWidth * fScreenX - 1.0f);
}
float CFont::FromToNDC_Y(float fScreenY) 
{ 
	int nWindowHeight = CGameFramework::GetInstance().GetWindowHeight();
	return static_cast<float>(-2.0 / nWindowHeight * fScreenY + 1.0f);
}
CSentenceObject::CSentenceObject(ID3D11Device *pd3dDevice, int nScreenPosX, int nScreenPosY, int nMaxString, XMFLOAT4 xmf4Color) : CGameObject(1)
{
	m_pFont = new CFont(pd3dDevice, L"Font/fontdata.txt", L"Font/font.dds");
	m_nScreenPosX = nScreenPosX;
	m_nScreenPosY = nScreenPosY;
	m_xmf4Color = xmf4Color;
	CSentenceMesh* pSentenceMesh = new CSentenceMesh(pd3dDevice, nMaxString);
	//SetMesh(pSentenceMesh, 0);
	CreateShaderVariables(pd3dDevice);
	
}
CSentenceObject::~CSentenceObject()
{

}

void  CSentenceObject::UpdateSentence(CMesh* pTextMesh, const wchar_t* text, int positionX, int positionY,
	ID3D11DeviceContext* pd3dDeviceContext)
{
	int nNumLetters;

	int nVertexNum = pTextMesh->GetVertexNum();
	XMFLOAT3* pxmf3Positions;
	XMFLOAT2* pxmf2Texcoords;

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	VertexType* pMapPtr;
	VertexType* pVertexs;

	// 색을 상수 버퍼에 전달.
	UpdateShaderVariable(pd3dDeviceContext);


	nNumLetters = wcslen(text);
	if (nNumLetters > pTextMesh->GetTextLength())
	{
		return;
	}

	pVertexs = new VertexType[nVertexNum];
	memset(pVertexs, 0, (sizeof(VertexType) * nVertexNum));

	// 문장에 필요한 폰트를 가져온다.
	m_pFont->BuildVertexAarry(static_cast<void*>(pVertexs), text, m_nScreenPosX, m_nScreenPosY);

	// 정점 버퍼 수정.
	pd3dDeviceContext->Map(*pTextMesh->GetVertexBuffer(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	pMapPtr = (VertexType*)mappedResource.pData;
	memcpy(pMapPtr, pVertexs, (sizeof(VertexType) * nVertexNum));
	pd3dDeviceContext->Unmap(*pTextMesh->GetVertexBuffer(), 0);

	delete[] pVertexs;
}
void CSentenceObject::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera , wstring& strSentence)
{
	/*UpdateSentence(m_ppMeshes[0], strSentence.c_str(), 0, 0, pd3dDeviceContext);
	m_pFont->UpdateShaderVariable(pd3dDeviceContext);
	for (int i = 0; i < 1; i++)
	{
		m_ppMeshes[i]->Render(pd3dDeviceContext);
	}*/
}
void CSentenceObject::CreateShaderVariables(ID3D11Device *pd3dDevice)
{
	//월드 변환 행렬을 위한 상수 버퍼를 생성한다.
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(XMFLOAT4);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	pd3dDevice->CreateBuffer(&bd, nullptr, &m_pd3dcbColor);
}
void CSentenceObject::UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext)
{
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	pd3dDeviceContext->Map(m_pd3dcbColor, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	XMFLOAT4 *pcbColor= (XMFLOAT4 *)d3dMappedResource.pData;
	pcbColor->x = m_xmf4Color.x;
	pcbColor->y = m_xmf4Color.y;
	pcbColor->z = m_xmf4Color.z;
	pcbColor->w = m_xmf4Color.w;
	pd3dDeviceContext->Unmap(m_pd3dcbColor, 0);

	//상수 버퍼를 디바이스의 슬롯(VS_SLOT_WORLD_MATRIX)에 연결한다.
	pd3dDeviceContext->PSSetConstantBuffers(0x00, 1, &m_pd3dcbColor);
}




CRevolvingObject::CRevolvingObject(int nMeshes) : CGameObject(nMeshes)
{
	m_xmvRevolutionAxis = XMFLOAT3(1.0f, 0.0f, 0.0f);
	m_fRevolutionSpeed = 0.0f;
}
CRevolvingObject::~CRevolvingObject()
{
}
void CRevolvingObject::Animate(float fTimeElapsed)
{
	//공전을 나타내기 위해 회전 행렬을 오른쪽에 곱한다.
	XMMATRIX xmmtxRotate;
	//XMMATRIX xmmtxWorld = XMLoadFloat4x4(&m_xmmtxWorld);
	XMMATRIX xmmtxWorld = XMLoadFloat4x4(&GetComponent<CTransform>()->GetWorldMtx());
	XMVECTOR xmvRevolutionAxis = XMLoadFloat3(&m_xmvRevolutionAxis);
	xmmtxRotate = XMMatrixRotationAxis(xmvRevolutionAxis, (float)XMConvertToRadians(m_fRevolutionSpeed * fTimeElapsed));
	xmmtxWorld = xmmtxWorld  * xmmtxRotate;

	//XMStoreFloat4x4(&m_xmmtxWorld, xmmtxWorld);
	XMStoreFloat4x4(&GetComponent<CTransform>()->GetWorldMtx(), xmmtxWorld);
}





CAlphaBlendingObject::CAlphaBlendingObject(ID3D11Device *pd3dDevice) : CGameObject(1)
{
	CAlphaBlendingWaterMeshTextured* pWaterMesh = new CAlphaBlendingWaterMeshTextured(pd3dDevice , 1000.0f, 10.0f, 1000.0f);
	//SetMesh(pWaterMesh, 0);

	//월드 변환 행렬을 위한 상수 버퍼를 생성한다.
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(XMFLOAT4);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	pd3dDevice->CreateBuffer(&bd, nullptr, &m_pd3dTimeBuffer);



	//m_pd3dxUVPosition = XMFLOAT3()

}
CAlphaBlendingObject::~CAlphaBlendingObject()
{
}
void CAlphaBlendingObject::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	//스카이 박스 객체의 위치를 카메라의 위치로 변경한다.
	XMFLOAT3 xmf3CameraPos = pCamera->GetPosition();
	//m_pTransform->SetPosition(XMFLOAT3(1028.0f, 120.0f, 978.0f));
	//SetPosition(d3dxvCameraPos.x, d3dxvCameraPos.y, d3dxvCameraPos.z);


	//CShader::UpdateShaderVariable(pd3dDeviceContext, &m_xmmtxWorld);
	CShader::UpdateShaderVariable(pd3dDeviceContext, &GetComponent<CTransform>()->GetWorldMtx());


	//스카이 박스 메쉬(6개의 사각형)를 렌더링한다.
	/*if (m_ppMeshes)
	{
		m_ppMeshes[0]->Render(pd3dDeviceContext);
	}*/
}

void CAlphaBlendingObject::Animate(float fTimeElapsed)
{
	XMMATRIX UVMatrix = XMLoadFloat4x4(&m_pd3dxUVMatrix);
	UVMatrix = XMMatrixIdentity();
	XMStoreFloat4x4(&m_pd3dxUVMatrix, UVMatrix);

	m_pd3dxUVMatrix._41 = fTimeElapsed*0.25;
	m_pd3dxUVMatrix._42 = 0.0;
}


