#pragma once
#include "stdafx.h"

#pragma comment(lib, "pdh.lib")
#include <pdh.h>
class CCpu
{
private:
	bool m_bCanReadCpu;
	HQUERY m_queryHandle;
	HCOUNTER m_counterHandle;
	unsigned long m_lastSampleTime;
	long m_cpuUsage;
public:
	CCpu();
	~CCpu();
	int GetCpuPercentage();
	void Frame();
};

