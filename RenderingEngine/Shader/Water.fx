cbuffer cbViewProjectionMatrix : register(b0)
{
	matrix gmtxView : packoffset(c0);
	matrix gmtxProjection : packoffset(c4);
};

//월드 변환 행렬을 위한 쉐이더 변수를 선언한다(슬롯 1을 사용). 
cbuffer cbWorldMatrix : register(b1)
{
	matrix gmtxWorld : packoffset(c0);
};


//시간 전달 구조체
cbuffer cbWTime : register(b2)
{
	matrix gfTime : packoffset(c0);
};




//텍스쳐를 사용하는 경우 정점 쉐이더의 입력을 위한 구조체이다.
struct VS_TEXTURED_INPUT
{
	float3 position : POSITION;
	float2 texCoord1 : TEXCOORD0;
};

//텍스쳐를 사용하는 경우 정점 쉐이더의 출력을 위한 구조체이다.
struct VS_TEXTURED_OUTPUT
{
	float4 position : SV_POSITION;
	float2 texCoord1 : TEXCOORD0;
};



//텍스쳐를 사용하는 경우 정점 쉐이더의 입력을 위한 구조체이다.
struct VS_TEXTURED_COLOR_INPUT
{
	float3 position : POSITION;
	float2 texCoord1 : TEXCOORD0;
	float2 texCoord2 : TEXCOORD1;
};

//텍스쳐를 사용하는 경우 정점 쉐이더의 출력을 위한 구조체이다.
struct VS_TEXTURED_COLOR_OUTPUT
{
	float4 position : SV_POSITION;
	float2 texCoord1 : TEXCOORD0;
	float2 texCoord2 : TEXCOORD1;
};


Texture2D gtxtTexture1: register(t0);
Texture2D gtxtTexture2 : register(t1);
SamplerState gSamplerState1 : register(s0);
SamplerState gSamplerState2 : register(s1);

//각 픽셀에 대하여 텍스쳐 샘플링을 하기 위한 픽셀 쉐이더 함수이다.


VS_TEXTURED_COLOR_OUTPUT VSTexturedColor(VS_TEXTURED_COLOR_INPUT input)
{
	VS_TEXTURED_COLOR_OUTPUT output = (VS_TEXTURED_COLOR_OUTPUT)0;
	output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
	output.texCoord1 = input.texCoord1;
	output.texCoord2 = input.texCoord2;
	return(output);
}

float4 PSTexturedColor(VS_TEXTURED_COLOR_OUTPUT input) : SV_Target
{
	float4         cColor = gtxtTexture1.Sample(gSamplerState1, input.texCoord1);
	float4  fDestColor = gtxtTexture2.Sample(gSamplerState1, input.texCoord1);
	float4 fResult = cColor * fDestColor;

	/*float4 black = { 0.0f, 0.0f, 0.0f, 1.0f };
	*/
	/*if (fResult.r>0.1f)
		discard;*/
	return fResult;

	//float4(0.1, 0.1, 0.1, 1.0))
	//return(cColor);
	//return fDestColor;
	//return float4(1.0f, 1.0f, 0.0f, 1.0f);
}


VS_TEXTURED_OUTPUT VSTextured(VS_TEXTURED_INPUT input)
{
	VS_TEXTURED_OUTPUT output = (VS_TEXTURED_OUTPUT)0;
	output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
	
	
	float time = mul(float4(1.0f, 0.0f, 0.0f, 1.0f), gfTime). x;
	float cosTime = 3 * cos(gfTime._14 * 2 + input.texCoord1.x * 10);
	input.position.y += cosTime;

	
	
	
	input.texCoord1 += mul(float4(input.texCoord1, 0.0f, 1.0f), gfTime).xy;
	output.texCoord1 = input.texCoord1;
	return(output);
}

//각 픽셀에 대하여 텍스쳐 샘플링을 하기 위한 픽셀 쉐이더 함수이다.
float4 PSTextured(VS_TEXTURED_OUTPUT input) : SV_Target
{
	//float time = mul(float4(1.0f, 0.0f, 0.0f, 1.0f), gfTime).x;
	//float cosTime  = 3 * cos(time * 2 + input.texCoord1.x * 10);
	//input.position.y += cosTime;

	//문제점
	//input.texCoord1 = mul(float4(input.texCoord1, 0.0f, 1.0f), gfTime).xy;
	//input.texCoord1 += mul(float4(input.texCoord1, 0.0f, 1.0f), gfTime).xy;

	float4 cColor = gtxtTexture1.Sample(gSamplerState1, input.texCoord1);

	cColor.a = 0.5;
	return(cColor);
}


struct VS_SKYBOX_CUBEMAP_INPUT
{
	float3 position : POSITION;
};
struct VS_SKYBOX_CUBEMAP_OUTPUT
{
	float3 position : POSITION;
	float4 positionH : SV_POSITION;
};

TextureCube gtxtCubeMapSkyBox : register(t0);
SamplerState gssSkyBox : register(s0);

VS_SKYBOX_CUBEMAP_OUTPUT VSSkyBox(VS_SKYBOX_CUBEMAP_INPUT input)
{
	VS_SKYBOX_CUBEMAP_OUTPUT output = (VS_SKYBOX_CUBEMAP_OUTPUT)0;
	output.positionH = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);

	output.position = input.position;
	return output;
}

float4 PSSkyBox(VS_SKYBOX_CUBEMAP_OUTPUT input) : SV_Target
{
	float4 cColor = gtxtCubeMapSkyBox.Sample(gssSkyBox, input.position);
	return cColor;
}