

//--------------------------------------------------------------------------------------
//카메라 변환 행렬과 투영 변환 행렬을 위한 쉐이더 변수를 선언한다(슬롯 0을 사용).
#include "Light.fx"

cbuffer cbViewProjectionMatrix : register(b0)
{
	matrix gmtxView : packoffset(c0);
	matrix gmtxProjection : packoffset(c4);
};

//월드 변환 행렬을 위한 쉐이더 변수를 선언한다(슬롯 1을 사용). 
cbuffer cbWorldMatrix : register(b1)
{
	matrix gmtxWorld : packoffset(c0);
};


Texture2D gtxtTexture : register(t0);
Texture2D gtxtNormalTexture : register(t1);
SamplerState gSamplerState : register(s0);





struct VS_TEXTURED_LIGHTING_COLOR_INPUT
{
	float3 position : POSITION;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
	float2 texCoord : TEXCOORD0;
};


struct VS_TEXTURED_LIGHTING_COLOR_OUTPUT
{
	float4 position : SV_POSITION;
	float3 positionW : POSITION;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
	float2 texCoord : TEXCOORD0;
};



VS_TEXTURED_LIGHTING_COLOR_OUTPUT VSTexturedLightingColor(VS_TEXTURED_LIGHTING_COLOR_INPUT input)
{
	VS_TEXTURED_LIGHTING_COLOR_OUTPUT output = (VS_TEXTURED_LIGHTING_COLOR_OUTPUT)0;
	output.normal = mul(input.normal, (float3x3)gmtxWorld);
	output.tangent = mul(input.tangent, (float3x3)gmtxWorld);
	output.binormal = mul(input.binormal, (float3x3)gmtxWorld);

	output.positionW = mul(float4(input.position, 1.0f), gmtxWorld).xyz;
	output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
	output.texCoord = input.texCoord;

	return(output);
}

float4 PSTexturedLightingColor(VS_TEXTURED_LIGHTING_COLOR_OUTPUT input) : SV_Target
{
	float4 cNormalMap = gtxtNormalTexture.Sample(gSamplerState, input.texCoord);
	float3 normal;

	input.normal = normalize(input.normal);
    input.tangent = normalize(input.tangent);
    input.binormal = normalize(input.binormal);



	// Expand the range of the normal value from (0, +1) to (-1, +1).
	cNormalMap = (cNormalMap * 2.0f) - 1.0f;

	// Calculate the normal from the data in the bump map.
	normal = (cNormalMap.x * input.tangent) + (cNormalMap.y * input.binormal) + (cNormalMap.z * input.normal);

	// Normalize the resulting bump normal.
	normal = normalize(normal);


	float4 cIllumination = Lighting(input.positionW, normal);
	float4 cColor = gtxtTexture.Sample(gSamplerState, input.texCoord) * cIllumination;


		return(cColor);
}

