Texture2D gtxtTexture : register(t0);
SamplerState gSamplerState : register(s0);

cbuffer cbPostProcessIndex : register(b2)
{
	float gPostProcessIndex;
	float gScreenWidth;
	float gScreenHeight;
	float gIntensity;
}


struct VS_SCREEN_INPUT
{
	float3 position : POSITION;
	float2 texCoord : TEXCOORD;
};

struct VS_SCREEN_OUTPUT
{
	float4 position : SV_POSITION;
	float2 texCoord : TEXCOORD;
};



// 화면에 따료 표기한다.
VS_SCREEN_OUTPUT VS_ScreenQuad(VS_SCREEN_INPUT input)
{
	VS_SCREEN_OUTPUT output = (VS_SCREEN_OUTPUT)0;

	output.position = float4(input.position, 1.0f);
	output.texCoord = input.texCoord;

	return output;
}


float3x3 Kx = { -1, 0, 1,
				-2, 0, 2,
				-1, 0, 1 };

float3x3 Ky = { 1, 2, 1,
				0, 0, 0,
				-1, -2, -1 };


float3x3 K = { -2, -1, 0,
			-1, 0, 1,
			0, 1, 2 };





float4 PS_ScreenQuad(VS_SCREEN_OUTPUT input) : SV_TARGET
{
	/*float2 gPixelOffset = { 586.0,  466.0 };
	float Lx = 0;
	float Ly = 0;
	for (int y = -1; y <= 1; ++y)
	{
	for (int x = -1; x <= 1; ++x)
	{
	float2 offset = float2(x, y) / gPixelOffset;
	float3 tex = gtxtTexture.Sample(gSamplerState, input.texCoord + offset).rgb;
	float luminance = dot(tex, float3(0.3, 0.59, 0.11));

	Lx += luminance * Kx[y + 1][x + 1];
	Ly += luminance * Ky[y + 1][x + 1];

	}
	}
	float L = sqrt((Lx*Lx) + (Ly*Ly));
	return float4(L.xxx, 1);*/


	float4 cColor = gtxtTexture.Sample(gSamplerState, input.texCoord);

	if (gPostProcessIndex.x == 0.0f)
	{
		//return cColor;
	}
	else if (gPostProcessIndex == 1.0f) //Black
	{
		cColor.rgb = dot(cColor.rgb, float3(0.3, 0.59, 0.11));
		//return cColor;
	}
	else if (gPostProcessIndex == 2.0f) //Sepia
	{
		float4 cSepia;
		cSepia.a = cColor.a;
		cSepia.r = dot(cColor.rgb, float3(0.393f, 0.769f, 0.189f));
		cSepia.g = dot(cColor.rgb, float3(0.349f, 0.686f, 0.168f));
		cSepia.b = dot(cColor.rgb, float3(0.272f, 0.534f, 0.131f));
		cColor = cSepia;

	}
	else if (gPostProcessIndex == 3.0f) //ouput edge
	{
		float4 lum = float4(0.30, 0.59, 0.11, 1);


		// s11 s12 s13
		// s21     s23
		// s31 s32 s33

		// top row
		float s11 = dot(gtxtTexture.Sample(gSamplerState, input.texCoord + float2(-1.0f / gScreenWidth, -1.0f / gScreenHeight)), lum);   // LEFT
		float s12 = dot(gtxtTexture.Sample(gSamplerState, input.texCoord + float2(0, -1.0f / gScreenHeight)), lum);             // MIDDLE
		float s13 = dot(gtxtTexture.Sample(gSamplerState, input.texCoord + float2(1.0f / gScreenWidth, -1.0f / gScreenHeight)), lum);    // RIGHT

																											  // MIDDLE ROW
		float s21 = dot(gtxtTexture.Sample(gSamplerState, input.texCoord + float2(-1.0f / gScreenWidth, 0)), lum);                // LEFT
																											  // Omit center
		float s23 = dot(gtxtTexture.Sample(gSamplerState, input.texCoord + float2(-1.0f / gScreenWidth, 0)), lum);                // RIGHT

		// last row
		float s31 = dot(gtxtTexture.Sample(gSamplerState, input.texCoord + float2(-1.0f / gScreenWidth, 1.0f / gScreenHeight)), lum);    // LEFT
		float s32 = dot(gtxtTexture.Sample(gSamplerState, input.texCoord + float2(0, 1.0f / gScreenHeight)), lum);              // MIDDLE
		float s33 = dot(gtxtTexture.Sample(gSamplerState, input.texCoord + float2(1.0f / gScreenWidth, 1.0f / gScreenHeight)), lum); // RIGHT
																																																						 																																																									 
		 // Filter ... thanks internet <img class="emoji" draggable="false" alt="??" 
		// src="https://s0.wp.com/wp-content/mu-plugins/wpcom-smileys/twemoji/2/svg/1f642.svg">
		float t1 = s13 + s33 + (2 * s23) - s11 - (2 * s21) - s31;
		float t2 = s31 + (2 * s32) + s33 - s11 - (2 * s12) - s13;

		float4 col;

		if (((t1 * t1) + (t2 * t2)) > 0.05) {
			col = float4(0, 0, 0, 1);
		}
		else {
			col = float4(1, 1, 1, 1);
		}

		cColor = col;

	}
	else if (gPostProcessIndex == 4.0f) //Emboss
	{
		float4 s22 = gtxtTexture.Sample(gSamplerState, input.texCoord); // center
		float4 s11 = gtxtTexture.Sample(gSamplerState, input.texCoord + float2(-1.0f / 1024.0f, -1.0f / 768.0f));
		float4 s33 = gtxtTexture.Sample(gSamplerState, input.texCoord + float2(1.0f / 1024.0f, 1.0f / 768.0f));

		s11.rgb = (s11.r + s11.g + s11.b);
		s22.rgb = (s22.r + s22.g + s22.b) * -0.5;
		s33.rgb = (s22.r + s22.g + s22.b) * 0.2;

		cColor = (s11 + s22 + s33);


	}
	else if (gPostProcessIndex == 5.0f) // blur
	{
		float2 pixelOffset = { gScreenWidth , gScreenHeight };
		int intensity = gIntensity;
		float4 cSum = { 0.0, 0.0 , 0.0, 0.0 };
		for (int y = -intensity; y <= intensity; ++y)
		{
			for (int x = -intensity; x <= intensity; ++x)
			{
				float2 offset = float2(x, y) / pixelOffset;
				float3 tex = gtxtTexture.Sample(gSamplerState, input.texCoord + offset).rgb;
				cSum.rgb += tex;
			}
		}
		int num = (intensity * 2 + 1) *(intensity * 2 + 1);
		cColor = cSum / num;

	}
	return cColor;

}