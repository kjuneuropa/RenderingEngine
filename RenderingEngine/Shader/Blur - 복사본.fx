﻿Texture2D gtxtTexture : register(t0);
SamplerState gSamplerState : register(s0);

cbuffer cbPostProcessIndex : register(b2)
{
	float4 gPostProcessIndex;
}


struct VS_SCREEN_INPUT
{
	float3 position : POSITION;
	float2 texCoord : TEXCOORD;
};

struct VS_SCREEN_OUTPUT
{
	float4 position : SV_POSITION;
	float2 texCoord : TEXCOORD;
};



// 화면에 따료 표기한다.
VS_SCREEN_OUTPUT VS_ScreenQuad(VS_SCREEN_INPUT input)
{
	VS_SCREEN_OUTPUT output = (VS_SCREEN_OUTPUT)0;

	output.position = float4(input.position, 1.0f);
	output.texCoord = input.texCoord;

	return output;
}


float3x3 Kx = { -1, 0, 1,
				-2, 0, 2,
				-1, 0, 1 };

float3x3 Ky = { 1, 2, 1,
				0, 0, 0,
				-1, -2, -1 };


float3x3 K = { -2, -1, 0,
				-1, 0, 1,
				0, 1, 2 };




//float2 gPixelOffset;
float4 PS_ScreenQuad(VS_SCREEN_OUTPUT input) : SV_TARGET
{
	/*float2 gPixelOffset = { 586.0,  466.0 };
	float Lx = 0;
	float Ly = 0;
	for (int y = -1; y <= 1; ++y)
	{
		for (int x = -1; x <= 1; ++x)
		{
			float2 offset = float2(x, y) / gPixelOffset;
			float3 tex = gtxtTexture.Sample(gSamplerState, input.texCoord + offset).rgb;
			float luminance = dot(tex, float3(0.3, 0.59, 0.11));

			Lx += luminance * Kx[y + 1][x + 1];
			Ly += luminance * Ky[y + 1][x + 1];
		
		}
	}
	float L = sqrt((Lx*Lx) + (Ly*Ly));
	return float4(L.xxx, 1);*/


	float4 cColor = gtxtTexture.Sample(gSamplerState, input.texCoord);

	if (gPostProcessIndex.x == 0.0f)
	{
		//return cColor;
	}
	else if (gPostProcessIndex.x == 1.0f) //Black
	{
		cColor.rgb = dot(cColor.rgb, float3(0.3, 0.59, 0.11));
		//return cColor;
	}
	else if (gPostProcessIndex.x == 2.0f) //Sepia
	{
		float4 sepia;
		sepia.a = cColor.a;
		sepia.r = dot(cColor.rgb, float3(0.393f, 0.769f, 0.189f));
		sepia.g = dot(cColor.rgb, float3(0.349f, 0.686f, 0.168f));
		sepia.b = dot(cColor.rgb, float3(0.272f, 0.534f, 0.131f));
		cColor = sepia;

		//return sepia;
	}
	else if (gPostProcessIndex.x == 3.0f) 
	{

		float2 gPixelOffset = { 1.0f / 586.0, 1.0f / 466.0 };
		float res = 0;
		for (int y = -1; y <= 1; ++y)
		{
			for (int x = -1; x <= 1; ++x)
			{
				float2 offset = float2(x, y) * gPixelOffset;
				float3 tex = gtxtTexture.Sample(gSamplerState, input.texCoord + offset).rgb;
				float luminance = dot(tex, float3(0.3, 0.59, 0.11));

				res += luminance * K[y + 1][x + 1];
			}
		}
		//res += 0.5f;
		//cColor.rgb = res;
		cColor.r = res;
		cColor.g = res;
		cColor.b = res;
		//return float4(res.xxx, 1);
	}
	else if (gPostProcessIndex.x == 4.0f)
	{
		float fWidth = 586.0;
		float fHeight = 466.0;

		float4 s11 = gtxtTexture.Sample(gSamplerState, input.texCoord + float2(-1.0f / fWidth, -1.0f / fHeight));    // LEFT
		float4 s12 = gtxtTexture.Sample(gSamplerState, input.texCoord + float2(0, -1.0f / 768.0f));              // MIDDLE
		float4 s13 = gtxtTexture.Sample(gSamplerState, input.texCoord + float2(1.0f / fWidth, -1.0f / fHeight)); // RIGHT

																								  // MIDDLE ROW
		float4 s21 = gtxtTexture.Sample(gSamplerState, input.texCoord + float2(-1.0f / fWidth, 0));             // LEFT
		float4 col = gtxtTexture.Sample(gSamplerState, input.texCoord);                                          // DEAD CENTER
		float4 s23 = gtxtTexture.Sample(gSamplerState, input.texCoord + float2(-1.0f / fWidth, 0));                 // RIGHT

																									  // LAST ROW
		float4 s31 = gtxtTexture.Sample(gSamplerState, input.texCoord + float2(-1.0f / fWidth, 1.0f / fHeight)); // LEFT
		float4 s32 = gtxtTexture.Sample(gSamplerState, input.texCoord + float2(0, 1.0f / fHeight));                   // MIDDLE
		float4 s33 = gtxtTexture.Sample(gSamplerState, input.texCoord + float2(1.0f / fWidth, 1.0f / fHeight));  // RIGHT

																								  // Average the color with surrounding samples
		col = (col + s11 + s12 + s13 + s21 + s23 + s31 + s32 + s33) / 9;
		cColor = col;
	

	}
	return cColor;

}