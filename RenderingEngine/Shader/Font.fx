

//--------------------------------------------------------------------------------------
//카메라 변환 행렬과 투영 변환 행렬을 위한 쉐이더 변수를 선언한다(슬롯 0을 사용).
#include "Light.fx"

cbuffer cbViewProjectionMatrix : register(b0)
{
	matrix gmtxView : packoffset(c0);
	matrix gmtxProjection : packoffset(c4);
};

//월드 변환 행렬을 위한 쉐이더 변수를 선언한다(슬롯 1을 사용). 
cbuffer cbWorldMatrix : register(b1)
{
	matrix gmtxWorld : packoffset(c0);
};


Texture2D gtxtTexture : register(t0);
SamplerState gSamplerState : register(s0);

cbuffer PixelBuffer: register(b0)
{
	float4 pixelColor;
};




struct VS_TEXTURED_LIGHTING_COLOR_INPUT
{
	float3 position : POSITION;
	float2 texCoord : TEXCOORD0;
};


struct VS_TEXTURED_LIGHTING_COLOR_OUTPUT
{
	float4 position : SV_POSITION;
	float2 texCoord : TEXCOORD0;
};



VS_TEXTURED_LIGHTING_COLOR_OUTPUT VSFont(VS_TEXTURED_LIGHTING_COLOR_INPUT input)
{
	VS_TEXTURED_LIGHTING_COLOR_OUTPUT output = (VS_TEXTURED_LIGHTING_COLOR_OUTPUT)0;

	//output.position = mul(mul(float4(input.position, 1.0f), gmtxView), gmtxProjection);
	output.position = float4(input.position, 1.0f);
	output.texCoord = input.texCoord;

	return(output);

}

float4 PSFont(VS_TEXTURED_LIGHTING_COLOR_OUTPUT input) : SV_Target
{
	float4 cColor;

// Sample the texture pixel at this location.
    cColor = gtxtTexture.Sample(gSamplerState, input.texCoord);

// If the color is black on the texture then treat this pixel as transparent.
    if (cColor.r == 0.0f)
    {
	  cColor.a = 0.0f;
    }

// If the color is other than black on the texture then this is a pixel in the font so draw it using the font pixel color.
    else
	{
	 cColor.a = 1.0f;
	 cColor = cColor * pixelColor;
	}

	// = float4(1.0f, 0.0f, 0.0f, 1.0f);

		return(cColor);
}

