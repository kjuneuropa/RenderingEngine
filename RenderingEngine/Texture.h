#pragma once
#include "stdafx.h"
//게임 객체는 하나 이상의 텍스쳐를 가질 수 있다. 
//CTexture는 텍스쳐를 관리하기 위한 클래스이다.
class CTexture
{
public:
	CTexture(int nTextures = 1, int nSamplers = 1, int nTextureStartSlot = 0, int nSamplerStartSlot = 0);
	~CTexture();

private:
	int m_nReferences;

public:
	void AddRef() { m_nReferences++; }
	void Release();

private:
	//텍스쳐 리소스의 개수이다.
	int m_nTextures;
	ID3D11ShaderResourceView **m_ppd3dsrvTextures;
	//텍스쳐 리소스를 연결할 시작 슬롯이다.
	int m_nTextureStartSlot;
	//샘플러 상태 객체의 개수이다.
	int m_nSamplers;
	ID3D11SamplerState **m_ppd3dSamplerStates;
	//샘플러 상태 객체를 연결할 시작 슬롯이다.
	int m_nSamplerStartSlot;
public:
	void		SetTexture(int nIndex, ID3D11ShaderResourceView *pd3dsrvTexture);
	void		SetSampler(int nIndex, ID3D11SamplerState *pd3dSamplerState);

	void		UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext);
	void		UpdateTextureShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, int nIndex = 0, int nSlot = 0);
	void		UpdateSamplerShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, int nIndex = 0, int nSlot = 0);

	void		AddNormalMap(ID3D11ShaderResourceView *pd3dsrvTexture);
	void		ReplaceTexture(ID3D11ShaderResourceView *pd3dsrvTexture);
};
