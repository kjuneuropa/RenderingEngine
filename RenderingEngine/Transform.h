#pragma once
#include "stdafx.h"
#include "Component.h"
#include "Camera.h"
class CTransform : public CComponent
{
public:
	static ComponentType  getComponentId(void) { return ComponentType_Transform; }
	typedef _component_identifier<CTransform, CComponent, true> component_identifier_t;
private:
	static component_identifier_t identifier;
private:
	XMFLOAT3		m_xmf3Pos;
	XMFLOAT3		m_xmf3Scale;
	XMFLOAT4		m_xmf4Quaternion;
	XMFLOAT3		m_xmf3Degree;
	XMFLOAT4X4		m_xmmtxWorld;
public:
	CTransform();
	~CTransform();
	
	void Initalize(ID3D11Device *pd3dDevice);
	virtual void Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera) override;


	void		SetPositionX(float x) { m_xmf3Pos.x = x; UpdateTransform(); }
	void		SetPositionY(float y) { m_xmf3Pos.y = y; UpdateTransform(); }
	void		SetPositionZ(float z) { m_xmf3Pos.z = z; UpdateTransform(); }
	void		SetPosition(XMFLOAT3 xmf3Pos) { m_xmf3Pos = xmf3Pos; UpdateTransform(); }
	void		SetPosition(float x, float y, float z);

	void		SetDegree(XMFLOAT3 xmf3Degree) { m_xmf3Degree = xmf3Degree; }
	void		ScaleFor(float fScale, int nIndex);
	
	XMFLOAT3	GetDegree() { return m_xmf3Degree; }
	XMFLOAT3	GetPosition() const { return m_xmf3Pos; }
	XMFLOAT3	GetScale() { return  m_xmf3Scale; }
	XMFLOAT4X4& GetWorldMtx() { return m_xmmtxWorld; }

	//객체의 위치, 로컬 x-축, y-축, z-축 방향 벡터를 반환한다.
	XMFLOAT3	GetLookAt();
	XMFLOAT3	GetUp();
	XMFLOAT3	GetRight();


	void		FromEulerAngles(XMFLOAT3 xmf3Angles);
	XMFLOAT3	ToEulerAngles();
	void		UpdateTransform();
	XMFLOAT4X4	CreateRotation(XMFLOAT4 rotation);
	void		OnPrepareRender(XMFLOAT3& xmf3Right, XMFLOAT3& xmf3Up, XMFLOAT3& xmf3Look, XMFLOAT3& xmf3Position);

	//로컬 x-축, y-축, z-축 방향으로 회전한다.
	void		Rotate(float fPitch = 10.0f, float fYaw = 10.0f, float fRoll = 10.0f);
	void		Rotate(XMFLOAT3 *pxmf3Axis, float fAngle);

	//로컬 x-축, y-축, z-축 방향으로 이동한다.
	void		MoveStrafe(float fDistance = 1.0f);
	void		MoveUp(float fDistance = 1.0f);
	void		MoveForward(float fDistance = 1.0f);
};

