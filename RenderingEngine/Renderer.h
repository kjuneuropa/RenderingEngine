#pragma once
#include "Component.h"
#include "Mesh.h"
#include "Material.h"
#include "Camera.h"
#include "Transform.h"

class CRenderer : public CComponent
{
public:
	static ComponentType getComponentId(void) { return ComponentType_Renderable; }
	typedef _component_identifier<CRenderer, CComponent, true> component_identifier_t;
private:
	static component_identifier_t identifier;


protected:
	int				m_nMeshes=1;
	CMesh**			m_ppMeshes;
	AABB			m_bcMeshBoundingCube;
private:
	CTexture*		m_pTexture;
	CMaterial*		m_pMaterial;

	// Transform 컴포넌트 포인터를 가지고 있음
	CTransform*		m_pTransform;

public:
	CRenderer();
	~CRenderer();
	virtual void Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera) override;
	void		Initalize(ID3D11Device *pd3dDevice);
	void		SetTransform(CTransform* pTransform) { m_pTransform = pTransform; }
	void		SetMesh(CMesh *pMesh, int nIndex = 0);
	
	void		ReplaceTexture(ID3D11ShaderResourceView* pd3dsrvTexture);
	

	void		CreateMaterial(XMFLOAT4 xmf4Diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f), XMFLOAT4 xmf4Ambient= XMFLOAT4(0.9f, 0.9f, 0.9f, 1.0f),
							   XMFLOAT4 xmf4Specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f), XMFLOAT4 xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	void		CreateMaterial(CMaterial* pMaterial);
	void		CreateTexture(ID3D11Device *pd3dDevice, wstring strTextureName);
	void		CreateTexture(ID3D11Device *pd3dDevice, wstring strTextureName,  HWND hWnd);
	void		UpdateMaterial(ID3D11DeviceContext* pd3dDeviceContext);
	void		UpdateTexture(ID3D11DeviceContext* pd3dDeviceContext);
	void		Render(ID3D11DeviceContext* pd3dDeviceContext, CCamera *pCamera);

	CMesh**		GetMesh() { return m_ppMeshes; }
	CTransform*	GetTransform() { return m_pTransform; }
	AABB*		GetAABB() { return &m_bcMeshBoundingCube; }
	int			GetMeshNum() { return m_nMeshes; }
};

