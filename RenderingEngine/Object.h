#pragma once
#include "Mesh.h"
#include "QuadTreeMesh.h"

#include "stdafx.h"
#include "Camera.h"
#include "HeightMap.h"
#include "Transform.h"
#include "Texture.h"
#include <fstream>
#include "Material.h"
#include "Component.h"


class CGameObject
{
public:
	CGameObject(int nMeshes = 0);
	virtual ~CGameObject();
private:
	std::map<size_t, CComponent*> m_Components;
	int			m_nObjectType;
	int			m_nDrawMeshs = 0;
	int			m_nDrawVertexs = 0;
	int			m_nReferences = 0;
	bool		m_bNormalMap = false;
	bool		 m_bActive;

public:
	void AddRef();
	void Release();
	template<class component_t>
	void AddComponent()
	{
		ComponentType  nType = CComponent::Type_To_Enum<component_t>();

		// 
		//ComponentType nType = get_component<component_t>::type::getComponentId();


		if (HasComponent(nType))
			return;
		auto newComponent = new component_t;
		newComponent->SetType(nType);

		//--------------------------------------------------------
		// 컴포넌트를 컨테이너에 삽입
		//--------------------------------------------------------
		m_Components[nType] = newComponent;
	}

	template<class component_t>
	component_t* GetComponent()
	{
		//--------------------------------------------------------
		// 컴포넌트 식별자 ID 취득
		//--------------------------------------------------------
		ComponentType nType = CComponent::Type_To_Enum<component_t>();
	
		//ComponentType nType = get_component<component_t>::type::getComponentId();

		//--------------------------------------------------------
		// 컴포넌트가 컨테이너에 존재하지 않다면 nullptr 리턴
		//--------------------------------------------------------
		if (m_Components.find(nType) == m_Components.end())
			return nullptr;
			

		//--------------------------------------------------------
		// 컴포넌트가 존재한다면 컴포넌트 리턴
		//--------------------------------------------------------
		return reinterpret_cast<component_t*>(m_Components[nType]);
	}
	bool HasComponent(ComponentType type) { return m_Components.find(type) != m_Components.end(); }

	virtual void		Animate(float fTimeElapsed);
	virtual void		Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
	virtual void		Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera, wstring& strSentence){}
	
	// 디게 특이한 함수
	
	

	//월드 좌표계의 픽킹 광선을 생성한다.
	void		GenerateRayForPicking(XMFLOAT3 *pxmf3PickPosition, XMMATRIX *pxmmtxWorld, XMMATRIX *pd3dxmtxView, XMFLOAT3 *pd3dxvPickRayPosition, XMFLOAT3 *pd3dxvPickRayDirection);
	//월드 좌표계의 픽킹 광선을 생성한다.
	int			PickObjectByRayIntersection(XMFLOAT3 *pxmf3PickPosition, XMMATRIX *pxmmtxView, MESHINTERSECTINFO *pd3dxIntersectInfo);
	//객체가 카메라의 절두체 내부에 있는 가를 판단한다. 
	bool		IsVisible(CCamera *pCamera = nullptr);
	void		SetActive(bool bActive = false) { m_bActive = bActive; }

	//객체를 렌더링하기 전에 호출되는 함수이다.
	virtual void OnPrepareRender() { }


public:
	// **수정해야할 함수
	void		RemoveNormalMap();
	bool		HasNormalMap() { return m_bNormalMap; }
	void		IncludeNormalMap() { m_bNormalMap = true; }
	int			GetObjectType() { return m_nObjectType; }
	XMFLOAT3	GetPosition();
	int			GetDrawVertexNum() { return m_nDrawVertexs; }
};







class CFont
{
private:
	struct FontType
	{
		float left, right;
		int size;
	};
public:
	CFont(ID3D11Device* pd3dDevice, wchar_t* pFontFile, wchar_t* pTextureFile);
	~CFont();
	void LoadFontData(wchar_t* pFontFile);
	void LoadTexture(ID3D11Device* pd3dDevice, wchar_t* pTextureFile);
	void BuildVertexAarry(void* pVertexs, const wchar_t* pText, float fPosX, float fPosY);
	void  UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext);
	float FromToNDC_X(float fScreenX); 
	float FromToNDC_Y(float fScreenY); 
private:
	FontType* m_pFontType;
	CTexture* m_pTexture;
};

class CSentenceObject: public CGameObject
{
private:
	static CFont* m_pFont;
	int m_nScreenPosX;
	int m_nScreenPosY;
	XMFLOAT4 m_xmf4Color;
public:
	CSentenceObject(ID3D11Device *pd3dDevice, int nScreenPosX, int nScreenPosY, int nMaxString, XMFLOAT4 xmf4Color);
	virtual ~CSentenceObject();
	void UpdateSentence(CMesh* pTextMesh, const wchar_t* text, int positionX, int positionY,
			ID3D11DeviceContext* pd3dDeviceContext);
	//void SetSentence
	void UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext);
	void CreateShaderVariables(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera, wstring& strSentence);
	
	
	static void ReleaseFont() { if (m_pFont) delete m_pFont; }
	static ID3D11Buffer* m_pd3dcbColor;
};



class CRevolvingObject : public CGameObject
{
public:
	CRevolvingObject(int nMeshes = 1);
	virtual ~CRevolvingObject();

	virtual void Animate(float fTimeElapsed);

private:
	// 공전 회전축과 회전 속력을 나타낸다.
	XMFLOAT3 m_xmvRevolutionAxis;
	float m_fRevolutionSpeed;

public:
	// 공전 속력을 설정한다.
	void SetRevolutionSpeed(float fRevolutionSpeed) { m_fRevolutionSpeed = fRevolutionSpeed; }
	// 공전을 위한 회전축을 설정한다.
	void SetRevolutionAxis(XMFLOAT3 xmf3RevolutionAxis) { m_xmvRevolutionAxis = xmf3RevolutionAxis; }

};


class CAlphaBlendingObject : public CGameObject
{
public:
	CAlphaBlendingObject(ID3D11Device *pd3dDevice);
	virtual ~CAlphaBlendingObject();
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
	virtual void Animate(float fTimeElapsed);
protected:
	XMFLOAT4X4 m_pd3dxUVMatrix;
	XMFLOAT3 m_pd3dxUVPosition;

	ID3D11Buffer *m_pd3dTimeBuffer;
};

