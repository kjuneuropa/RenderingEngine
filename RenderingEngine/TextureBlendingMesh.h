#pragma once
#include "stdafx.h"
#include "Mesh.h"
#include "Object.h"

class CTextureBlendingMesh : public CMesh
{
public:
	CTextureBlendingMesh(ID3D11Device *pd3dDevice, float gScreenWidth, float gScreenHeight, float fDepth);
	virtual ~CTextureBlendingMesh();
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext);
protected:
	//텍스쳐 매핑을 하기 위하여 텍스쳐 좌표가 필요하다.
	ID3D11Buffer *m_pd3dTexCoordBuffer1;
	ID3D11Buffer *m_pd3dTexCoordBuffer2;

	CTexture *m_pBlendingTexture;


};

