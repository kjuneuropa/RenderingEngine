#pragma once
#include "stdafx.h"
#include "Component.h"
#include "HeightMap.h"
#include "Mesh.h"
#include "Material.h"
#include "Transform.h"
#include "Renderer.h"
class CTerrain : public CRenderer
{
public:
	static ComponentType getComponentId(void) { return ComponentType_Terrain1; }
	typedef _component_identifier<CTerrain, CRenderer, false> component_identifier_t;
private:
	static component_identifier_t identifier;

private:
	// 공통적인 사항.
	//int				m_nMeshes;
	//CMesh**			m_ppMeshes;
	//AABB			m_bcMeshBoundingCube;
	//CTexture*		m_pTexture;
	//CMaterial*		m_pMaterial;
	//// Transform 컴포넌트 포인터를 가지고 있음
	



	CHeightMap *m_pHeightMap;
	//지형의 가로와 세로 크기이다.
	int m_nWidth;
	int m_nLength;
	//지형을 실제로 몇 배 확대할 것인가를 나타내는 스케일 벡터이다.
	XMFLOAT3 m_xmvScale;


public:
	CTerrain();
	~CTerrain();
	virtual void Update(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera) override;
	

	void		Initalize(ID3D11Device *pd3dDevice, wstring& strHeightMap);
	//지형의 실제 높이를 반환한다. 높이 맵의 높이에 스케일을 곱한 값이다.
	
	
	float GetHeight(float x, float z, bool bReverseQuad = false) { return(m_pHeightMap->GetHeight(x, z, bReverseQuad) * m_xmvScale.y); }
	XMFLOAT3 GetNormal(float x, float z) { return(m_pHeightMap->GetHeightMapNormal(int(x / m_xmvScale.x), int(z / m_xmvScale.z))); }

	int GetHeightMapWidth() { return(m_pHeightMap->GetHeightMapWidth()); }
	int GetHeightMapLength() { return(m_pHeightMap->GetHeightMapLength()); }

	XMFLOAT3 GetScale() { return(m_xmvScale); }
	//지형의 실제 크기(가로/세로)를 반환한다. 높이 맵의 크기에 스케일을 곱한 값이다.
	float GetWidth() { return(m_nWidth * m_xmvScale.x); }
	float GetLength() { return(m_nLength * m_xmvScale.z); }

	float GetPeakHeight() { return(m_bcMeshBoundingCube.m_xmf3Maximum.y); }

};

