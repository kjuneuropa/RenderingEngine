// 높이 맵 지형 클래스
class CHeightMapTerrain : public CGameObject
{
public:
	struct QuadTreeNode
	{
		QuadTreeNode* pNodes[4];
		CHeightMapGridMesh* pTerrainMesh;
		float fPosX, fPosZ, fWidth;
		AABB bcBoundingCube;
	};
	CHeightMapTerrain(ID3D11Device *pd3dDevice, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale);
	virtual ~CHeightMapTerrain();
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
	void RenderQuadTree(ID3D11DeviceContext *pd3dDeviceContext, QuadTreeNode* pNode, CCamera *pCamera);
	
	void CreateQuadTree();
	void CreateTreeNode(ID3D11Device *pd3dDevice, QuadTreeNode* pNode, int nPosX, int nPosZ, int nTerrainWidth, int nBolckWidth, int nTreeDepth);
	void CalculateBouningBox(QuadTreeNode* pNode);
private:
	//지형의 높이 맵으로 사용할 이미지이다.
	CHeightMap *m_pHeightMap;
	QuadTreeNode* m_pParentNode;

	//지형의 가로와 세로 크기이다.
	int m_nWidth;
	int m_nLength;

	//지형을 실제로 몇 배 확대할 것인가를 나타내는 스케일 벡터이다.
	XMFLOAT3 m_xmvScale;


public:
	//지형의 실제 높이를 반환한다. 높이 맵의 높이에 스케일을 곱한 값이다.
	float GetHeight(float x, float z, bool bReverseQuad = false) { return(m_pHeightMap->GetHeight(x, z, bReverseQuad) * m_xmvScale.y); }
	XMFLOAT3 GetNormal(float x, float z) { return(m_pHeightMap->GetHeightMapNormal(int(x / m_xmvScale.x), int(z / m_xmvScale.z))); }

	int GetHeightMapWidth() { return(m_pHeightMap->GetHeightMapWidth()); }
	int GetHeightMapLength() { return(m_pHeightMap->GetHeightMapLength()); }

	XMFLOAT3 GetScale() { return(m_xmvScale); }
	//지형의 실제 크기(가로/세로)를 반환한다. 높이 맵의 크기에 스케일을 곱한 값이다.
	float GetWidth() { return(m_nWidth * m_xmvScale.x); }
	float GetLength() { return(m_nLength * m_xmvScale.z); }

//	float GetPeakHeight() { return(m_bcMeshBoundingCube.m_xmf3Maximum.y); }
};










CHeightMapTerrain::CHeightMapTerrain(ID3D11Device *pd3dDevice, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale) : CGameObject(0)
{
	m_nObjectType = 0; //지형
	m_nWidth = nWidth;
	m_nLength = nLength;

	int cxQuadsPerBlock = nBlockWidth - 1;
	int czQuadsPerBlock = nBlockLength - 1;

	m_xmvScale = xmf3Scale;

	m_pHeightMap = new CHeightMap(pFileName, nWidth, nLength, xmf3Scale);

	long cxBlocks = (m_nWidth - 1) / cxQuadsPerBlock;
	long czBlocks = (m_nLength - 1) / czQuadsPerBlock;
	m_nMeshes = cxBlocks * czBlocks;
	//m_ppMeshes = new CMesh*[m_nMeshes];
	//for (int i = 0; i < m_nMeshes; i++)m_ppMeshes[i] = nullptr;

	CHeightMapGridMesh *pHeightMapGridMesh = nullptr;

	m_pParentNode = new QuadTreeNode;

	int nTreeDepth = log2(cxBlocks*czBlocks) / 2;
	
	

	//CreateTreeNode(pd3dDevice, m_pParentNode, 0, 0, m_nWidth -1, nBlockWidth, nTreeDepth);

	// 커밋용


	for (int z = 0, zStart = 0; z < czBlocks; z++)
	{
		for (int x = 0, xStart = 0; x < cxBlocks; x++)
		{
			xStart = x * (nBlockWidth - 1);
			zStart = z * (nBlockLength - 1);
			pHeightMapGridMesh = new CHeightMapGridMesh(pd3dDevice, xStart, zStart, nBlockWidth, nBlockLength, xmf3Scale, m_pHeightMap);
			//SetMesh(pHeightMapGridMesh, x + (z*cxBlocks));
		}
	}
}
void CHeightMapTerrain::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	
	/*for (auto& component : m_Components)
	{
		component.second->Update(pd3dDeviceContext, pCamera);
	}*/
	
	//m_pTransform->UpdateTransform();


	//CShader::UpdateShaderVariable(pd3dDeviceContext, &GetComponent<CTransform>()->GetWorldMtx());
	/*if (m_pMaterial) CIlluminatedShader::UpdateShaderVariable(pd3dDeviceContext, &m_pMaterial->m_Material);
	if (m_pTexture) m_pTexture->UpdateShaderVariable(pd3dDeviceContext);
	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i])
			{
				bool bIsVisible = true;
				if (pCamera)
				{
					AABB bcBoundingCube = m_ppMeshes[i]->GetBoundingCube();
					bcBoundingCube.Update(&XMLoadFloat4x4(&GetComponent<CTransform>()->GetWorldMtx()));
					bIsVisible = pCamera->IsInFrustum(&bcBoundingCube);
				}
				if (bIsVisible) m_ppMeshes[i]->Render(pd3dDeviceContext);
			}
		}
	}*/






	/*m_nDrawMeshs = 0;
	m_nDrawVertexs = 0;
	if(m_pParentNode)
		RenderQuadTree(pd3dDeviceContext,  m_pParentNode, pCamera);
	
	wchar_t temp[40];
	swprintf(temp, L"Draw VertexNum :  %d", m_nDrawVertexs);
	CGameFramework::GetInstance().GetTextManager()->SetSentence(DRAW_VERTEX, temp);*/
	
}
void  CHeightMapTerrain::RenderQuadTree(ID3D11DeviceContext *pd3dDeviceContext, QuadTreeNode* pNode, CCamera *pCamera)
{
	int nCount = 0;
	bool bResult = pCamera->IsInFrustum(&pNode->bcBoundingCube);

	if (bResult)
	{
		for (int i = 0; i < 4; ++i)
		{
			if (pNode->pNodes[i] != nullptr)
			{
				nCount++;
				RenderQuadTree(pd3dDeviceContext, pNode->pNodes[i], pCamera);
			}
		}

	}
	else
		return;
	

	if (nCount != 0)
	{
		return;
	}
	m_nDrawMeshs++;
	m_nDrawVertexs += pNode->pTerrainMesh->GetVertexNum();
	pNode->pTerrainMesh->Render(pd3dDeviceContext);

}
void CHeightMapTerrain::CreateQuadTree()
{

}
void  CHeightMapTerrain::CreateTreeNode(ID3D11Device *pd3dDevice, QuadTreeNode* pNode, int nPosX, int nPosZ, int nTerrainWidth, int nBlockWidth, int nTreeDepth)
{
	for (int i = 0; i < 4; i++)
		pNode->pNodes[i] = nullptr;
	float xStart, zStart;

	// 알수 있는 정보 : 2049 x 2049
	// 나누는 블록 개수 : 1025 1025

	int arrX[4] = { nPosX, nPosX + nTerrainWidth/2, nPosX, nPosX + nTerrainWidth / 2 };
	int arrZ[4] = { nPosZ, nPosZ, nPosZ + nTerrainWidth / 2, nPosZ + nTerrainWidth / 2 };




	// 트리 깊이가 1보다 클 경우 계속 탐색을 한다.
	if (nTreeDepth > 0)
	{
		for (int i = 0; i < 4; ++i) //0 1 2 3 
		{

			pNode->pNodes[i] = new QuadTreeNode;
			// 노드를 확장한다.
			CreateTreeNode(pd3dDevice, pNode->pNodes[i], arrX[i], arrZ[i], nTerrainWidth / 2, nBlockWidth, nTreeDepth - 1);

		}
		//pNode->bcBoundingCube.m_xmf3Minimum = XMFLOAT3(nPosX * m_xmvScale.x, 0.0f, nPosZ * m_xmvScale.z);
		//pNode->bcBoundingCube.m_xmf3Maximum = XMFLOAT3((nPosX + nTerrainWidth + 1)*  m_xmvScale.x, 0.0f, (nPosZ + nTerrainWidth + 1)*  m_xmvScale.z);
		CalculateBouningBox(pNode);

		return;
	}
	int nStartX = nPosX, nStartZ = nPosZ;

	pNode->pTerrainMesh = new CHeightMapGridMesh(pd3dDevice, nStartX, nStartZ, nBlockWidth, nBlockWidth, m_xmvScale, m_pHeightMap);
	pNode->bcBoundingCube = pNode->pTerrainMesh->GetBoundingCube();

}

void CHeightMapTerrain::CalculateBouningBox(QuadTreeNode* pNode)
{
	//AABB bcBoundingBox;
	pNode->bcBoundingCube.m_xmf3Minimum = XMFLOAT3(+FLT_MAX, +FLT_MAX, +FLT_MAX);
	pNode->bcBoundingCube.m_xmf3Maximum = XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	
	
	for (int i = 0; i < 4; i++)
	{
		// 최소값 설정
		if (pNode->bcBoundingCube.m_xmf3Minimum.x > pNode->pNodes[i]->bcBoundingCube.m_xmf3Minimum.x)
			pNode->bcBoundingCube.m_xmf3Minimum.x = pNode->pNodes[i]->bcBoundingCube.m_xmf3Minimum.x;

		if (pNode->bcBoundingCube.m_xmf3Minimum.y > pNode->pNodes[i]->bcBoundingCube.m_xmf3Minimum.y)
			pNode->bcBoundingCube.m_xmf3Minimum.y = pNode->pNodes[i]->bcBoundingCube.m_xmf3Minimum.y;

		if (pNode->bcBoundingCube.m_xmf3Minimum.z > pNode->pNodes[i]->bcBoundingCube.m_xmf3Minimum.z)
			pNode->bcBoundingCube.m_xmf3Minimum.z = pNode->pNodes[i]->bcBoundingCube.m_xmf3Minimum.z;

		//최대값 설정
		if (pNode->bcBoundingCube.m_xmf3Maximum.x < pNode->pNodes[i]->bcBoundingCube.m_xmf3Maximum.x)
			pNode->bcBoundingCube.m_xmf3Maximum.x = pNode->pNodes[i]->bcBoundingCube.m_xmf3Maximum.x;

		if (pNode->bcBoundingCube.m_xmf3Maximum.y < pNode->pNodes[i]->bcBoundingCube.m_xmf3Maximum.y)
			pNode->bcBoundingCube.m_xmf3Maximum.y = pNode->pNodes[i]->bcBoundingCube.m_xmf3Maximum.y;

		if (pNode->bcBoundingCube.m_xmf3Maximum.z < pNode->pNodes[i]->bcBoundingCube.m_xmf3Maximum.z)
			pNode->bcBoundingCube.m_xmf3Maximum.z = pNode->pNodes[i]->bcBoundingCube.m_xmf3Maximum.z;

		//pNode->bcBoundingCube.m_xmf3Maximum.x
	}
}
CHeightMapTerrain::~CHeightMapTerrain()
{
	if (m_pHeightMap) delete m_pHeightMap;
}


//Shader...........................



class CTerrainShader : public CDetailTexturedIlluminatedShader
{
public:
	CTerrainShader();
	virtual ~CTerrainShader();

	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void AddTerrainObject(ID3D11Device* pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
	//CHeightMapTerrain *GetTerrain();
	CTerrain1* GetTerrain1();
public:

	bool CreateTeraain(ID3D11Device *pd3dDevice);
	ID3D11Texture2D* m_texture;
};




void CTerrainShader::BuildObjects(ID3D11Device *pd3dDevice)
{
	//m_nObjects = 1;
	//m_ppObjects = new CGameObject*[m_nObjects];

	//지형은 텍스쳐가 2개이므로 2개의 샘플러 객체가 필요하다.

	//m_nShaderType = SHADER_TYPE::TERRAIN;

	//ID3D11SamplerState *pd3dBaseSamplerState = nullptr;
	//D3D11_SAMPLER_DESC d3dSamplerDesc;
	//ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	//d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	//d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	//d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	//d3dSamplerDesc.MinLOD = 0;
	//d3dSamplerDesc.MaxLOD = 0;
	//pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dBaseSamplerState);

	////ID3D11SamplerState *pd3dDetailSamplerState = nullptr;
	////d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	////d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	////d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	////pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dDetailSamplerState);

	//CTexture *pTerrainTexture = new CTexture(1, 1, 0, 0);
	//ID3D11ShaderResourceView *pd3dsrvBaseTexture = nullptr;
	//D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image/floor.dds"), nullptr, nullptr, &pd3dsrvBaseTexture, nullptr);
	//pTerrainTexture->SetTexture(0, pd3dsrvBaseTexture);
	//pTerrainTexture->SetSampler(0, pd3dBaseSamplerState);

	//pd3dsrvBaseTexture->Release();
	//pd3dBaseSamplerState->Release();

	////ID3D11ShaderResourceView *pd3dsrvDetailTexture = nullptr;
	////D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Detail_Texture_7.jpg"), nullptr, nullptr, &pd3dsrvDetailTexture, nullptr);
	////pTerrainTexture->SetTexture(1, pd3dsrvDetailTexture);
	////pTerrainTexture->SetSampler(1, pd3dDetailSamplerState);
	////pd3dsrvDetailTexture->Release();
	////pd3dDetailSamplerState->Release();


	////XMFLOAT3 xmf3Scale(16.0f, 2.0f, 16.0f);

	////XMFLOAT3 xmf3Scale(8.0f, 2.0f, 8.0f);
	//XMFLOAT3 xmf3Scale(2.0f, 2.0f, 2.0f);



	////m_ppObjects[0] = new CHeightMapTerrain(pd3dDevice, _T("Image/PlaneMap.raw"), 257, 257, 257, 257, xmf3Scale);
	////m_ppObjects[0]->SetTexture(pTerrainTexture);

	//CGameObject* pObjects = new CHeightMapTerrain(pd3dDevice, _T("Image/PlaneMap.raw"), 257, 257, 257, 257, xmf3Scale);



	////CGameObject* pObjects = new CHeightMapTerrain(pd3dDevice, _T("Image/heightmap01.bmp"), 257, 257, 257, 257, xmf3Scale);
	//pObjects->SetTexture(pTerrainTexture);



	//CMaterial *pTerrainMaterial = new CMaterial();
	//pTerrainMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	//pTerrainMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(0.9f, 0.9f, 0.9f, 1.0f);
	//pTerrainMaterial->m_Material.m_xmf4Specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f);
	//pTerrainMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	//pObjects->SetMaterial(pTerrainMaterial);

	//m_vObjects.push_back(pObjects);
	CreateTeraain(pd3dDevice);
}