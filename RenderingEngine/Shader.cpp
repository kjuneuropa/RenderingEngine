#include "Shader.h"
#include "Component.h"
#include "Renderer.h"
#include "Terrain.h"
#include "SkyBox.h"

//월드 변환 행렬을 위한 상수 버퍼는 쉐이더 객체의 정적(static) 데이터 멤버이다.
ID3D11Buffer *CShader::m_pd3dcbWorldMatrix = nullptr;
ID3D11Buffer *CIlluminatedShader::m_pd3dcbMaterial = nullptr;
ID3D11ShaderResourceView* CShader::m_d3dDefaultTexture = nullptr;
vector<CMesh*> CShader::m_vMeshes;



CShader::CShader()
{
	m_pd3dVertexShader = nullptr;
	m_pd3dVertexLayout = nullptr;
	m_pd3dPixelShader = nullptr;
	m_nShaderType = -1;
	m_bActive = false;
}

CShader::~CShader()
{
	if (m_pd3dVertexShader) m_pd3dVertexShader->Release();
	if (m_pd3dVertexLayout) m_pd3dVertexLayout->Release();
	if (m_pd3dPixelShader) m_pd3dPixelShader->Release();

}

void CShader::BuildObjects(ID3D11Device *pd3dDevice)
{

}
void CShader::RemoveObject(CGameObject* pSelectedObect)
{
	if(pSelectedObect)
		m_vObjects.erase(remove(m_vObjects.begin(), m_vObjects.end(), pSelectedObect),
		m_vObjects.end());
}
bool CShader::FindObject(int nObjectType)
{
	for (auto object : m_vObjects)
	{
		if (object->GetObjectType() == nObjectType)
			return true;
	}
	return false;
}
CTexture*  CShader::CreateTexture(ID3D11Device* pd3dDevice)
{
	ID3D11SamplerState *pd3dSamplerState = nullptr;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	HRESULT hResult;

	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;

	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dSamplerState);


	CTexture* pTexture = new CTexture(2, 1, 0, 0);

	//ID3D11ShaderResourceView *pd3dsrvTexture = nullptr; 
	//hResult = D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image\\222.jpg"), nullptr, nullptr, &pd3dsrvTexture, nullptr);
	
	pTexture->SetTexture(0, m_d3dDefaultTexture);
	pTexture->SetSampler(0, pd3dSamplerState);

	// 왜 여기서 죽지????
	//if (pd3dsrvTexture) pd3dsrvTexture->Release();
	//if (pd3dSamplerState) pd3dSamplerState->Release();

	return pTexture;
}
CMaterial*  CShader::CreateMaterial()
{
	CMaterial *pMaterial = new CMaterial();
	pMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	pMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	pMaterial->m_Material.m_xmf4Specular = XMFLOAT4(0.8f, 0.8f, 0.8f, 5.0f);
	pMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	
	return pMaterial;
}

void CShader::ReleaseObjects()
{
	if (m_vObjects.size() != 0)
	{
		for (int j = 0; j < m_vObjects.size(); j++) if (m_vObjects[j]) delete m_vObjects[j];
	}
}

void CShader::AnimateObjects(float fTimeElapsed)
{
	for (int j = 0; j <  m_vObjects.size(); j++)
	{
		m_vObjects[j]->Animate(fTimeElapsed);
	}
}

void CShader::OnPrepareRender(ID3D11DeviceContext *pd3dDeviceContext)
{
	pd3dDeviceContext->IASetInputLayout(m_pd3dVertexLayout);
	pd3dDeviceContext->VSSetShader(m_pd3dVertexShader, nullptr, 0);
	pd3dDeviceContext->PSSetShader(m_pd3dPixelShader, nullptr, 0);
}
void CShader::CreateMesh(ID3D11Device *pd3dDevice)
{
	string strFile01 = "Data/NewRockTall01_Vertex.txt";
	CCubeMeshTexturedIlluminated* pCubeMesh = new CCubeMeshTexturedIlluminated(pd3dDevice, 12.0f, 12.0f, 12.0f);
	CSphereMeshTexturedIlluminated* pSphereMesh = new CSphereMeshTexturedIlluminated(pd3dDevice, 12.0f, 12.0f, 12.0f);
	CPlaneMeshTexturedIlluminated* pPlaneMesh = new CPlaneMeshTexturedIlluminated(pd3dDevice, 12.0f, 12.0f);
	CFixedMesh* pPillarMesh = new CFixedMesh(pd3dDevice, strFile01);


	m_vMeshes.push_back(pCubeMesh);
	m_vMeshes.push_back(pSphereMesh);
	m_vMeshes.push_back(pPlaneMesh);
	m_vMeshes.push_back(pPillarMesh);


	 D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image\\222.jpg"), nullptr, nullptr, &m_d3dDefaultTexture, nullptr);
}

void CShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	OnPrepareRender(pd3dDeviceContext);

	/*for_each(m_vObjects.begin(), m_vObjects.end(),
		[&](CGameObject* pGameObject) {
		if (pGameObject->IsVisible(pCamera))
			pGameObject->Render(pd3dDeviceContext, pCamera); 
	});*/

	for (auto object : m_vObjects)
	{
		if(object->IsVisible(pCamera))
			object->Render(pd3dDeviceContext, pCamera);
	}

	//if (m_vObjects[j])
}


void CShader::CreateVertexShaderFromFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderModel, 
	    ID3D11VertexShader **ppd3dVertexShader, D3D11_INPUT_ELEMENT_DESC *pd3dInputLayout, UINT nElements, ID3D11InputLayout **ppd3dVertexLayout)
{
	HRESULT hResult; 

	
	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif


	ID3DBlob *pd3dShaderBlob = nullptr, *pd3dErrorBlob = nullptr;
	/*파일(pszFileName)에서 쉐이더 함수(pszShaderName)를 컴파일하여 
	  컴파일된 쉐이더 코드의 메모리 주소(pd3dShaderBlob)를 반환한다.*/
	if (SUCCEEDED(hResult = D3DX11CompileFromFile(pszFileName, nullptr, nullptr, pszShaderName, pszShaderModel, dwShaderFlags, 0, nullptr, &pd3dShaderBlob, &pd3dErrorBlob, nullptr)))
	//if (SUCCEEDED(hResult = D3DCompile(pszFileName, nullptr, nullptr, pszShaderName, pszShaderModel, dwShaderFlags, 0, nullptr, &pd3dShaderBlob, &pd3dErrorBlob, nullptr)))
	{
		//컴파일된 쉐이더 코드의 메모리 주소에서 정점-쉐이더를 생성한다. 
		pd3dDevice->CreateVertexShader(pd3dShaderBlob->GetBufferPointer(), pd3dShaderBlob->GetBufferSize(), nullptr, ppd3dVertexShader);
		//컴파일된 쉐이더 코드의 메모리 주소와 입력 레이아웃에서 정점 레이아웃을 생성한다. 
		pd3dDevice->CreateInputLayout(pd3dInputLayout, nElements, pd3dShaderBlob->GetBufferPointer(), pd3dShaderBlob->GetBufferSize(), ppd3dVertexLayout);
		pd3dShaderBlob->Release();
	}
	if (pd3dErrorBlob != 0)
	{
		MessageBoxA(0, (char*)pd3dErrorBlob->GetBufferPointer(), 0, 0);
	}
}
void CShader::CreatePixelShaderFromFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderModel, ID3D11PixelShader **ppd3dPixelShader)
{
	HRESULT hResult;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

	ID3DBlob *pd3dShaderBlob = nullptr, *pd3dErrorBlob = nullptr;
	/*파일(pszFileName)에서 쉐이더 함수(pszShaderName)를 컴파일하여 컴파일된 쉐이더 코드의 메모리 주소(pd3dShaderBlob)를 반환한다.*/
	if (SUCCEEDED(hResult = D3DX11CompileFromFile(pszFileName, nullptr, nullptr, pszShaderName, pszShaderModel, dwShaderFlags, 0, nullptr, &pd3dShaderBlob, &pd3dErrorBlob, nullptr)))
	{
		//컴파일된 쉐이더 코드의 메모리 주소에서 픽셀-쉐이더를 생성한다. 
		pd3dDevice->CreatePixelShader(pd3dShaderBlob->GetBufferPointer(), pd3dShaderBlob->GetBufferSize(), nullptr, ppd3dPixelShader);
		pd3dShaderBlob->Release();
	}
	if (pd3dErrorBlob != 0)
	{
		MessageBoxA(0, (char*)pd3dErrorBlob->GetBufferPointer(), 0, 0);
	}
}
void CShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputLayout);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "VS", "vs_5_0", &m_pd3dVertexShader, d3dInputLayout, nElements, &m_pd3dVertexLayout);
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "PS", "ps_5_0", &m_pd3dPixelShader);
}
void CShader::CreateShaderVariables(ID3D11Device *pd3dDevice)
{
	//월드 변환 행렬을 위한 상수 버퍼를 생성한다.
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(VS_CB_WORLD_MATRIX);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	pd3dDevice->CreateBuffer(&bd, nullptr, &m_pd3dcbWorldMatrix);
}
void CShader::ReleaseShaderVariables()
{
	//월드 변환 행렬을 위한 상수 버퍼를 반환한다.
	if (m_pd3dcbWorldMatrix) 
		m_pd3dcbWorldMatrix->Release();
}
void CShader::UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, XMFLOAT4X4 *pxm4x4World)
{
	//월드 변환 행렬을 상수 버퍼에 복사한다.
	
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	pd3dDeviceContext->Map(m_pd3dcbWorldMatrix, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	VS_CB_WORLD_MATRIX *pcbWorldMatrix = (VS_CB_WORLD_MATRIX *)d3dMappedResource.pData;
	pcbWorldMatrix->m_xmmtxWorld = XMMatrixTranspose(XMLoadFloat4x4(pxm4x4World));
	pd3dDeviceContext->Unmap(m_pd3dcbWorldMatrix, 0);
	
	//상수 버퍼를 디바이스의 슬롯(VS_SLOT_WORLD_MATRIX)에 연결한다.
	pd3dDeviceContext->VSSetConstantBuffers(VS_SLOT_WORLD_MATRIX, 1, &m_pd3dcbWorldMatrix);
}
CGameObject *CShader::PickObjectByRayIntersection(XMFLOAT3 *pxmf3vPickPosition, XMMATRIX *pxmmtxView, MESHINTERSECTINFO *pIntersectInfo)
{
	int nIntersected = 0;
	float fNearHitDistance = FLT_MAX;
	CGameObject *pSelectedObject = nullptr;
	MESHINTERSECTINFO IntersectInfo;
	/*쉐이더 객체에 포함되는 모든 객체들에 대하여 픽킹 광선을 생성하고 
	객체의 바운딩 박스와의 교차를 검사한다.
	교차하는 객체들 중에 카메라와 가장 가까운 객체의 정보와 객체를 반환한다.*/
	for (int i = 0; i <  m_vObjects.size(); i++)
	{
		nIntersected =  m_vObjects[i]->PickObjectByRayIntersection(pxmf3vPickPosition, pxmmtxView, &IntersectInfo);
		if ((nIntersected > 0) && (IntersectInfo.m_fDistance < fNearHitDistance))
		{
			fNearHitDistance = IntersectInfo.m_fDistance;
			pSelectedObject = m_vObjects[i];
			if (pIntersectInfo) *pIntersectInfo = IntersectInfo;
		}
	}
	return(pSelectedObject);
}

CSceneShader::CSceneShader()
{
}

CSceneShader::~CSceneShader()
{
}
void CSceneShader::CreateShader(ID3D11Device *pd3dDevice)
{
	CShader::CreateShader(pd3dDevice);
}

void CSceneShader::BuildObjects(ID3D11Device *pd3dDevice)
{
}


CPlayerShader::CPlayerShader()
{
}

CPlayerShader::~CPlayerShader()
{
}


void CPlayerShader::CreateShader(ID3D11Device *pd3dDevice)
{
	CShader::CreateShader(pd3dDevice);
}

void CPlayerShader::BuildObjects(ID3D11Device *pd3dDevice)
{
	CMaterial *pPlayerMaterial = new CMaterial();
	pPlayerMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(0.6f, 0.2f, 0.2f, 1.0f);
	pPlayerMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	pPlayerMaterial->m_Material.m_xmf4Specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f);
	pPlayerMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.2f, 1.0f);

	//CCubeMeshIlluminated *pCubeMesh = new CCubeMeshIlluminated(pd3dDevice, 4.0f, 12.0f, 4.0f);
	//CTerrainPlayer *pTerrainPlayer = new CTerrainPlayer(1);
	//pTerrainPlayer->SetMesh(pCubeMesh);
	//pTerrainPlayer->CreateShaderVariables(pd3dDevice);
	//pTerrainPlayer->ChangeCamera(pd3dDevice, SPACESHIP_CAMERA, 0.0f);
	//pTerrainPlayer->SetMaterial(pPlayerMaterial);
	//AddObject(pTerrainPlayer);
	//GetGameObject(0)->AddComponent<CTransform>();


	CCubeMeshIlluminated *pCubeMesh = new CCubeMeshIlluminated(pd3dDevice, 4.0f, 12.0f, 4.0f);
	CGameObject* pGameObject = new CGameObject;
	pGameObject->AddComponent<CTransform>();
	pGameObject->AddComponent<CTerrainPlayer>();
	auto pTerrainPlayer = pGameObject->GetComponent<CTerrainPlayer>();
	pTerrainPlayer->Initalize(pd3dDevice);
	pTerrainPlayer->SetMesh(pCubeMesh);
	pTerrainPlayer->CreateMaterial(pPlayerMaterial);
	pTerrainPlayer->SetTransform(pGameObject->GetComponent<CTransform>());
	pTerrainPlayer->CreateShaderVariables(pd3dDevice);
	pTerrainPlayer->ChangeCamera(pd3dDevice, THIRD_PERSON_CAMERA, 0.0f);
	
	
	AddObject(pGameObject);


}

void CPlayerShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	//3인칭 카메라일 때 플레이어를 렌더링한다.
	DWORD nCameraMode = (pCamera) ? pCamera->GetMode() : 0x00;
	if (nCameraMode == THIRD_PERSON_CAMERA)
	{
		CShader::Render(pd3dDeviceContext,  pCamera);
	}
}



CInstancingShader::CInstancingShader()
{
	m_pd3dCubeInstanceBuffer = nullptr;
	m_pd3dSphereInstanceBuffer = nullptr;
	m_pMaterial = nullptr;
	m_pTexture = nullptr;
}

CInstancingShader::~CInstancingShader()
{
	if (m_pd3dCubeInstanceBuffer) m_pd3dCubeInstanceBuffer->Release();
	if (m_pd3dSphereInstanceBuffer) m_pd3dSphereInstanceBuffer->Release();
	if (m_pMaterial) m_pMaterial->Release();
	if (m_pTexture) m_pTexture->Release();
}


void CInstancingShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "INSTANCEPOS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 3, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCEPOS", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 3, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCEPOS", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 3, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCEPOS", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 3, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
	};
	UINT nElements = ARRAYSIZE(d3dInputLayout);

	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "VSInstancedTexturedLightingColor", "vs_5_0", &GetVertexShader(), d3dInputLayout, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "PSInstancedTexturedLightingColor", "ps_5_0", &GetPixelShader());

}

ID3D11Buffer *CInstancingShader::CreateInstanceBuffer(ID3D11Device *pd3dDevice, int nObjects, UINT nBufferStride, void *pBufferData)
{
	ID3D11Buffer *pd3dInstanceBuffer = nullptr;
	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	/*버퍼의 초기화 데이터가 없으면 동적 버퍼로 생성한다. 즉, 나중에 매핑을 하여 내용을 채우거나 변경한다.*/
	d3dBufferDesc.Usage = (pBufferData) ? D3D11_USAGE_DEFAULT : D3D11_USAGE_DYNAMIC;
	d3dBufferDesc.ByteWidth = nBufferStride * nObjects;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = (pBufferData) ? 0 : D3D11_CPU_ACCESS_WRITE;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = pBufferData;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, (pBufferData) ? &d3dBufferData : nullptr, &pd3dInstanceBuffer);
	return(pd3dInstanceBuffer);
}

void CInstancingShader::BuildObjects(ID3D11Device *pd3dDevice, CTerrain *pHeightMapTerrain, CMaterial *pMaterial, CTexture *pTexture, int k)
{
	m_pMaterial = pMaterial;
	if (pMaterial) pMaterial->AddRef();
	m_pTexture = pTexture;
	if (pTexture) pTexture->AddRef();


	//에러의 원인
	//m_nInstanceBufferStride = sizeof(XMMATRIX);
	m_nInstanceBufferStride = sizeof(VS_VB_INSTANCE);
	m_nInstanceBufferOffset = 0;

	CCubeMeshTexturedIlluminated *pCubeMesh = new CCubeMeshTexturedIlluminated(pd3dDevice, 12.0f, 12.0f, 12.0f);
	CSphereMeshTexturedIlluminated *pSphereMesh = new CSphereMeshTexturedIlluminated(pd3dDevice, 12.0f, 20.0f, 20.0f);
	float fxPitch = 12.0f * 3.5f;
	float fyPitch = 12.0f * 3.5f;
	float fzPitch = 12.0f * 3.5f;

	float fTerrainWidth = pHeightMapTerrain->GetWidth();
	float fTerrainLength = pHeightMapTerrain->GetLength();

	/*두 가지(직육면체와 구) 객체들을 지형에 일정한 간격으로 배치한다. 지형의 표면에 직육면체를 배치하고 직육면체 위에 구가 배치된다. 직육면체와 구는 빨강색, 녹색, 파랑색이 반복되도록 배치된다.*/
	int xObjects = int(fTerrainWidth / (fxPitch * 3.0f)), yObjects = 2, zObjects = int(fTerrainLength / (fzPitch * 3.0f)), i = 0;
	//m_nObjects = xObjects * yObjects * zObjects;
	//m_ppObjects = new CGameObject*[m_nObjects];
	int nObjects = xObjects * yObjects * zObjects;

	XMFLOAT3 xmf3RotateAxis;
	CRotatingObject *pRotatingObject = nullptr;

	/*구는 3가지 종류(재질에 따라)이다. 다른 재질의 구들이 번갈아 나열되도록 한다. 재질의 종류에 따라 k가 0, 1, 2의 값을 가지고 k에 따라 객체의 위치를 다르게 설정한다.*/
	for (int x = 0; x < xObjects; x++)
	{
		for (int z = 0; z < zObjects; z++)
		{
			pRotatingObject = new CRotatingObject();
			//pRotatingObject->SetMesh(pSphereMesh);
			float xPosition = (k * fxPitch) + (x * fxPitch * 3.0f);
			float zPosition = (k * fzPitch) + (z * fxPitch * 3.0f);
			float gScreenHeight = pHeightMapTerrain->GetHeight(xPosition, zPosition);
			//pRotatingObject->GetTransform()->SetPosition(xPosition, gScreenHeight + (fyPitch * 4), zPosition);
			pRotatingObject->SetRotationAxis(XMFLOAT3(0.0f, 1.0f, 0.0f));
			pRotatingObject->SetRotationSpeed(36.0f * (i % 10) + 36.0f);
			//m_vObjects.push_back(pRotatingObject);
		}
	}
	m_pd3dSphereInstanceBuffer = CreateInstanceBuffer(pd3dDevice, xObjects * zObjects, m_nInstanceBufferStride, nullptr);
	pSphereMesh->AssembleToVertexBuffer(1, &m_pd3dSphereInstanceBuffer, &m_nInstanceBufferStride, &m_nInstanceBufferOffset);

	for (int x = 0; x < xObjects; x++)
	{
		for (int z = 0; z < zObjects; z++)
		{
			pRotatingObject = new CRotatingObject();
			//pRotatingObject->SetMesh(pCubeMesh);
			float xPosition = (k * fxPitch) + (x * fxPitch * 3.0f);
			float zPosition = (k * fzPitch) + (z * fxPitch * 3.0f);
			float gScreenHeight = pHeightMapTerrain->GetHeight(xPosition, zPosition);
			//pRotatingObject->GetTransform()->SetPosition(xPosition, gScreenHeight + 6.0f, zPosition);
			XMFLOAT3 xmf3SurfaceNormal = pHeightMapTerrain->GetNormal(xPosition, zPosition);
			//&d3dxvRotateAxis = XMVector3Cross(&XMFLOAT3(0.0f, 1.0f, 0.0f), &d3dxvSurfaceNormal);
			XMStoreFloat3(&xmf3RotateAxis, XMVector3Cross(XMLoadFloat3(&XMFLOAT3(0.0f, 1.0f, 0.0f)), XMLoadFloat3(&xmf3SurfaceNormal)));
			
			//float fAngle = acos(D3DXVec3Dot(&XMFLOAT3(0.0f, 1.0f, 0.0f), &d3dxvSurfaceNormal));
			
			float fAngle = acos(XMVectorGetX(XMVector3Dot(XMLoadFloat3(&XMFLOAT3(0.0f, 1.0f, 0.0f)), XMLoadFloat3(&xmf3SurfaceNormal))));
		

			pRotatingObject->SetRotationAxis(XMFLOAT3(0.0f, 1.0f, 0.0f));
			pRotatingObject->SetRotationSpeed(18.0f * (i % 10) + 10.0f);
			//m_vObjects.push_back(pRotatingObject);
		}
	}
	m_pd3dCubeInstanceBuffer = CreateInstanceBuffer(pd3dDevice, xObjects * zObjects, m_nInstanceBufferStride, nullptr);
	pCubeMesh->AssembleToVertexBuffer(1, &m_pd3dCubeInstanceBuffer, &m_nInstanceBufferStride, &m_nInstanceBufferOffset);
}


void CInstancingShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	//OnPrepareRender(pd3dDeviceContext);
	//if (m_pMaterial) 
	//	CIlluminatedShader::UpdateShaderVariable(pd3dDeviceContext, &m_pMaterial->m_Material);
	//
	//if (m_pTexture) m_pTexture->UpdateShaderVariable(pd3dDeviceContext);
	//

	//int nSphereObjects = m_vObjects.size() / 2;

	//int nSphereInstances = 0;
	//D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	//pd3dDeviceContext->Map(m_pd3dSphereInstanceBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	//VS_VB_INSTANCE *pnSphereInstances = (VS_VB_INSTANCE *)d3dMappedResource.pData;
	//for (int j = 0; j < nSphereObjects; j++)
	//{
	//	if (m_vObjects[j]->IsVisible(pCamera))
	//	{
	//		if (m_vObjects[j]->IsVisible(pCamera))
	//		{
	//			//pnSphereInstances[nSphereInstances].m_d3dxTransform = XMMatrixTranspose(XMLoadFloat4x4(&m_vObjects[j]->m_xmmtxWorld));
	//		
	//		}
	//	}
	//}
	//pd3dDeviceContext->Unmap(m_pd3dSphereInstanceBuffer, 0);

	//CMesh *pSphereMesh = m_vObjects[0]->GetMesh();
	//pSphereMesh->RenderInstanced(pd3dDeviceContext, nSphereInstances, 0);

	//int nCubeInstances = 0;
	//pd3dDeviceContext->Map(m_pd3dCubeInstanceBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	//VS_VB_INSTANCE *pCubeInstances = (VS_VB_INSTANCE *)d3dMappedResource.pData;
	//for (int j = nSphereObjects; j < m_vObjects.size(); j++)
	//{
	//	if (m_vObjects[j])
	//	{
	//		if (m_vObjects[j]->IsVisible(pCamera))
	//		{
	//			
	//			//pCubeInstances[nCubeInstances].m_d3dxTransform = XMMatrixTranspose(XMLoadFloat4x4(&m_vObjects[j]->m_xmmtxWorld));

	//		}
	//	}
	//}
	//pd3dDeviceContext->Unmap(m_pd3dCubeInstanceBuffer, 0);

	//CMesh *pCubeMesh = m_vObjects[m_vObjects.size() - 1]->GetMesh();
	//pCubeMesh->RenderInstanced(pd3dDeviceContext, nCubeInstances, 0);
}

CTerrainShader::CTerrainShader()
{
}

CTerrainShader::~CTerrainShader()
{
}

void CTerrainShader::BuildObjects(ID3D11Device *pd3dDevice)
{
	//m_nObjects = 1;
	//m_ppObjects = new CGameObject*[m_nObjects];

	//지형은 텍스쳐가 2개이므로 2개의 샘플러 객체가 필요하다.

	//m_nShaderType = SHADER_TYPE::TERRAIN;

	//ID3D11SamplerState *pd3dBaseSamplerState = nullptr;
	//D3D11_SAMPLER_DESC d3dSamplerDesc;
	//ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	//d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	//d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	//d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	//d3dSamplerDesc.MinLOD = 0;
	//d3dSamplerDesc.MaxLOD = 0;
	//pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dBaseSamplerState);

	////ID3D11SamplerState *pd3dDetailSamplerState = nullptr;
	////d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	////d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	////d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	////pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dDetailSamplerState);

	//CTexture *pTerrainTexture = new CTexture(1, 1, 0, 0);
	//ID3D11ShaderResourceView *pd3dsrvBaseTexture = nullptr;
	//D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image/floor.dds"), nullptr, nullptr, &pd3dsrvBaseTexture, nullptr);
	//pTerrainTexture->SetTexture(0, pd3dsrvBaseTexture);
	//pTerrainTexture->SetSampler(0, pd3dBaseSamplerState);

	//pd3dsrvBaseTexture->Release();
	//pd3dBaseSamplerState->Release();

	////ID3D11ShaderResourceView *pd3dsrvDetailTexture = nullptr;
	////D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Detail_Texture_7.jpg"), nullptr, nullptr, &pd3dsrvDetailTexture, nullptr);
	////pTerrainTexture->SetTexture(1, pd3dsrvDetailTexture);
	////pTerrainTexture->SetSampler(1, pd3dDetailSamplerState);
	////pd3dsrvDetailTexture->Release();
	////pd3dDetailSamplerState->Release();


	////XMFLOAT3 xmf3Scale(16.0f, 2.0f, 16.0f);

	////XMFLOAT3 xmf3Scale(8.0f, 2.0f, 8.0f);
	//XMFLOAT3 xmf3Scale(2.0f, 2.0f, 2.0f);



	////m_ppObjects[0] = new CHeightMapTerrain(pd3dDevice, _T("Image/PlaneMap.raw"), 257, 257, 257, 257, xmf3Scale);
	////m_ppObjects[0]->SetTexture(pTerrainTexture);

	//CGameObject* pObjects = new CHeightMapTerrain(pd3dDevice, _T("Image/PlaneMap.raw"), 257, 257, 257, 257, xmf3Scale);



	////CGameObject* pObjects = new CHeightMapTerrain(pd3dDevice, _T("Image/heightmap01.bmp"), 257, 257, 257, 257, xmf3Scale);
	//pObjects->SetTexture(pTerrainTexture);



	//CMaterial *pTerrainMaterial = new CMaterial();
	//pTerrainMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	//pTerrainMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(0.9f, 0.9f, 0.9f, 1.0f);
	//pTerrainMaterial->m_Material.m_xmf4Specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f);
	//pTerrainMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	//pObjects->SetMaterial(pTerrainMaterial);

	//m_vObjects.push_back(pObjects);
	CreateTeraain(pd3dDevice);
}
void CTerrainShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	CShader::Render(pd3dDeviceContext, pCamera);
}
//CHeightMapTerrain *CTerrainShader::GetTerrain()
//{
//	return((CHeightMapTerrain *)GetGameObject(0));
//}
CTerrain *CTerrainShader::GetTerrain()
{
	
	return GetGameObject(0)->GetComponent<CTerrain>();
}

void CTerrainShader::AddTerrainObject(ID3D11Device* pd3dDevice)
{
	ID3D11SamplerState *pd3dBaseSamplerState = nullptr;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;
	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dBaseSamplerState);

	//ID3D11SamplerState *pd3dDetailSamplerState = nullptr;
	//d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	//pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dDetailSamplerState);

	CTexture *pTerrainTexture = new CTexture(1, 1, 0, 0);
	ID3D11ShaderResourceView *pd3dsrvBaseTexture = nullptr;
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image/floor.dds"), nullptr, nullptr, &pd3dsrvBaseTexture, nullptr);
	pTerrainTexture->SetTexture(0, pd3dsrvBaseTexture);
	pTerrainTexture->SetSampler(0, pd3dBaseSamplerState);

	pd3dsrvBaseTexture->Release();
	pd3dBaseSamplerState->Release();

	//ID3D11ShaderResourceView *pd3dsrvDetailTexture = nullptr;
	//D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Detail_Texture_7.jpg"), nullptr, nullptr, &pd3dsrvDetailTexture, nullptr);
	//pTerrainTexture->SetTexture(1, pd3dsrvDetailTexture);
	//pTerrainTexture->SetSampler(1, pd3dDetailSamplerState);
	//pd3dsrvDetailTexture->Release();
	//pd3dDetailSamplerState->Release();


	//XMFLOAT3 xmf3Scale(16.0f, 2.0f, 16.0f);

	//XMFLOAT3 xmf3Scale(8.0f, 2.0f, 8.0f);
	XMFLOAT3 xmf3Scale(2.0f, 2.0f, 2.0f);



	//m_ppObjects[0] = new CHeightMapTerrain(pd3dDevice, _T("Image/PlaneMap.raw"), 257, 257, 257, 257, xmf3Scale);
	//m_ppObjects[0]->SetTexture(pTerrainTexture);

	//CGameObject* pObjects = new CHeightMapTerrain(pd3dDevice, _T("Image/heightmap.r16"), 1025, 1025, 1025, 1025, xmf3Scale);



	//CGameObject* pObjects = new CHeightMapTerrain(pd3dDevice, _T("Image/HeightMap.raw"), 257, 257, 257, 257, xmf3Scale);
	
	//pObjects->AddComponent<CTransform>();
	//pObjects->SetTexture(pTerrainTexture);



	CMaterial *pTerrainMaterial = new CMaterial();
	pTerrainMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	pTerrainMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(0.9f, 0.9f, 0.9f, 1.0f);
	pTerrainMaterial->m_Material.m_xmf4Specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f);
	pTerrainMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	//pObjects->SetMaterial(pTerrainMaterial);

	//m_vObjects.push_back(pObjects);;
	
}


bool CTerrainShader::CreateTeraain(ID3D11Device *pd3dDevice)
{
	
	ID3D11SamplerState *pd3dBaseSamplerState = nullptr;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;


	/*d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	d3dSamplerDesc.MipLODBias = 0.0f;
	d3dSamplerDesc.MaxAnisotropy = 1;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	d3dSamplerDesc.BorderColor[0] = 0;
	d3dSamplerDesc.BorderColor[1] = 0;
	d3dSamplerDesc.BorderColor[2] = 0;
	d3dSamplerDesc.BorderColor[3] = 0;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = D3D11_FLOAT32_MAX;*/


	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dBaseSamplerState);

	//ID3D11SamplerState *pd3dDetailSamplerState = nullptr;
	//d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	//d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	//pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dDetailSamplerState);

	CTexture *pTerrainTexture = new CTexture(1, 1, 0, 0);
	ID3D11ShaderResourceView *pd3dsrvBaseTexture = nullptr; // dirt01d.tga
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("Image/floor.dds"), nullptr, nullptr, &pd3dsrvBaseTexture, nullptr);
	

	
	pTerrainTexture->SetTexture(0, pd3dsrvBaseTexture);
	pTerrainTexture->SetSampler(0, pd3dBaseSamplerState);

	pd3dsrvBaseTexture->Release();
	pd3dBaseSamplerState->Release();



	XMFLOAT3 xmf3Scale(2.0f, 2.0f, 2.0f);
	//XMFLOAT3 xmf3Scale(2.0f, 2.0f, 2.0f);
	//CGameObject* pObjects = new CQuadTree(pd3dDevice, _T("Image/heightmap.r16"), 1025, 1025, 0, 0, xmf3Scale);
	//CGameObject* pObjects = new CQuadTree(pd3dDevice, _T("Image/HeightMap.raw"), 129, 129, 0, 0, xmf3Scale);
	
	//CGameObject* pObjects = new CNewHeightMapTerrain(pd3dDevice, _T("Image/heightmap.r16"), 1025, 1025, 0, 0, xmf3Scale);
	//CGameObject* pObjects = new CHeightMapTerrain(pd3dDevice, _T("Image/HeightMap.raw"), 257, 257, 257, 257, xmf3Scale);




	//CGameObject* pObjects = new CQuadTree(pd3dDevice, _T("Image/heightmap01.bmp"), 257, 257, 257, 257, xmf3Scale);
	
	// 최근
	//CGameObject* pObject = new CHeightMapTerrain(pd3dDevice, _T("Image/PlaneMap.raw"), 129, 129, 129, 129, xmf3Scale);
	CGameObject* pObject = new CGameObject(1);
	wstring strHeightMap = L"Image/PlaneMap.raw";
	wstring strTexture = L"Image/floor.dds";

	//CGameObject* pObjects = new CHeightMapTerrain(pd3dDevice, _T("Image/terrain.raw"), 2049, 2049, 2049, 2049, xmf3Scale);
	pObject->AddComponent<CTransform>();
	pObject->AddComponent<CTerrain>();
	CTransform* pTransform = pObject->GetComponent<CTransform>();
	pObject->GetComponent<CTerrain>()->SetTransform(pTransform);
	pObject->GetComponent<CTerrain>()->Initalize(pd3dDevice, strHeightMap);
	pObject->GetComponent<CTerrain>()->CreateMaterial();
	pObject->GetComponent<CTerrain>()->CreateTexture(pd3dDevice, strTexture);
	


	/*pObject->AddComponent<CRenderer>();
	CTransform* pTransform = pObject->GetComponent<CTransform>();
	pObject->GetComponent<CRenderer>()->SetTransform(pTransform);*/



	// 최근
	//CMaterial *pTerrainMaterial = new CMaterial();
	//pTerrainMaterial->m_Material.m_xmf4Diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	//pTerrainMaterial->m_Material.m_xmf4Ambient = XMFLOAT4(0.9f, 0.9f, 0.9f, 1.0f);
	//pTerrainMaterial->m_Material.m_xmf4Specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f);
	//pTerrainMaterial->m_Material.m_xmf4Emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);


	//pObject->SetTexture(pTerrainTexture);
	//pObject->SetMaterial(pTerrainMaterial);


	AddObject(pObject);
	
	return true;
}


CIlluminatedShader::CIlluminatedShader()
{
}

CIlluminatedShader::~CIlluminatedShader()
{
}
void CIlluminatedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "VSLightingColor", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "PSLightingColor", "ps_5_0", &GetPixelShader());
}
void CIlluminatedShader::CreateShaderVariables(ID3D11Device *pd3dDevice)
{
	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	d3dBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	d3dBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	d3dBufferDesc.ByteWidth = sizeof(MATERIAL);
	pd3dDevice->CreateBuffer(&d3dBufferDesc, nullptr, &m_pd3dcbMaterial);
}

void CIlluminatedShader::ReleaseShaderVariables()
{
	if (m_pd3dcbMaterial) m_pd3dcbMaterial->Release();
}

void CIlluminatedShader::UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, MATERIAL *pMaterial)
{
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	pd3dDeviceContext->Map(m_pd3dcbMaterial, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	MATERIAL *pcbMaterial = (MATERIAL *)d3dMappedResource.pData;
	memcpy(pcbMaterial, pMaterial, sizeof(MATERIAL));
	pd3dDeviceContext->Unmap(m_pd3dcbMaterial, 0);
	pd3dDeviceContext->PSSetConstantBuffers(PS_SLOT_MATERIAL, 1, &m_pd3dcbMaterial);
}
CDiffusedShader::CDiffusedShader()
{
}

CDiffusedShader::~CDiffusedShader()
{
}

void CDiffusedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "VSDiffusedColor", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "PSDiffusedColor", "ps_5_0", &GetPixelShader());
}

CTexturedShader::CTexturedShader()
{
}

CTexturedShader::~CTexturedShader()
{
}

void CTexturedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "VSTexturedColor", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "PSTexturedColor", "ps_5_0", &GetPixelShader());
}

void CTexturedShader::BuildObjects(ID3D11Device *pd3dDevice, CTexture *pTexture)
{
}

CDetailTexturedShader::CDetailTexturedShader()
{
}

CDetailTexturedShader::~CDetailTexturedShader()
{
}

void CDetailTexturedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,     1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT,     2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "VSDetailTexturedColor", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "PSDetailTexturedColor", "ps_5_0", &GetPixelShader());
}

CSkyBoxShader::CSkyBoxShader()
{
}

CSkyBoxShader::~CSkyBoxShader()
{
}


void CSkyBoxShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Water.fx", "VSSkyBox", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Water.fx", "PSSkyBox", "ps_5_0", &GetPixelShader());
}

void CSkyBoxShader::BuildObjects(ID3D11Device *pd3dDevice)
{
	
	CGameObject* pGameObject = new CGameObject;
	wstring strSkyBox = L"Image/environment.dds";
	CSkyBoxMesh *pSkyBoxMesh = new CSkyBoxMesh(pd3dDevice, 20.0f, 20.0f, 20.0f);
	pGameObject->AddComponent<CTransform>();
	pGameObject->AddComponent<CSkyBox>();
	auto skybox = pGameObject->GetComponent<CSkyBox>();
	skybox->CreateTexture(pd3dDevice, strSkyBox);
	skybox->SetMesh(pSkyBoxMesh);
	skybox->SetTransform(pGameObject->GetComponent<CTransform>());
	AddObject(pGameObject);

	//AddObject(new CSkyBox(pd3dDevice));
	//GetGameObject(0)->AddComponent<CTransform>();
}

void CSkyBoxShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	CShader::OnPrepareRender(pd3dDeviceContext);
	GetGameObject(0)->Render(pd3dDeviceContext, pCamera);
}




CTexturedIlluminatedShader::CTexturedIlluminatedShader()
{
}

CTexturedIlluminatedShader::~CTexturedIlluminatedShader()
{
}

void CTexturedIlluminatedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "VSTexturedLightingColor", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "PSTexturedLightingColor", "ps_5_0", &GetPixelShader());
}
void CTexturedIlluminatedShader::BuildObjects(ID3D11Device *pd3dDevice)
{
	//m_nShaderType = SHADER_TYPE::BASIC;
	//m_bActive = true;


	//m_pMaterial = pMaterial;
	//if (pMaterial) pMaterial->AddRef();
	//m_pTexture = pTexture;
	//if (pTexture) pTexture->AddRef();


	//CCubeMeshTexturedIlluminated *pCubeMesh = new CCubeMeshTexturedIlluminated(pd3dDevice, 12.0f, 12.0f, 12.0f);
	//CAxisMesh* pAxisMesh = new CAxisMesh(pd3dDevice, 12.0f);

	//m_nObjects = 1;
	//m_ppObjects = new CGameObject*[m_nObjects];
	//m_ppObjects[0] = new CGameObject(1);


	//m_vObjects.push_back(new CGameObject(1));

	//m_vObjects[0]->SetMesh(m_pSphereMesh);


	//m_vObjects[0]->SetTexture(pTexture);
	//m_vObjects[0]->SetMaterial(pMaterial);
	//m_vObjects[0]->GetTransform()->SetPosition(XMFLOAT3(255, 15, 335));

}

void CTexturedIlluminatedShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	CShader::Render(pd3dDeviceContext, pCamera);
}

//void  CTexturedIlluminatedShader::AddObject(CGameObject* pSelectedObject)
//{
//	m_vObjects.push_back(pSelectedObject);
//}
//결국 다형성 부여한 이상
// 이러한 형태의 코드는 꼭 있어야한다.
// 오브젝트 인덱스를 다른 방법으로 넣을 방안이 없다...?
void AddObject(int nIndex, ID3D11Device* pd3dDevice)
{
	//m_vObjects.push_back(new CGameObject(1));

	//switch (nIndex)
	//{
	//case OBJECT::CUBE:
	//{
	//	m_vObjects.back()->SetMesh(m_pCubeMesh);
	//



	//	break;
	//}
	//case OBJECT::SPHERE:
	//{
	//	m_vObjects.back()->SetMesh(m_pSphereMesh);
	//	break;
	//}
	//case OBJECT::PLANE:
	//{
	//	m_vObjects.back()->SetMesh(m_pPlaneMesh);
	//	break;
	//}
	//case OBJECT::PILLAR:
	//{
	//	m_vObjects.back()->SetMesh(m_pPillarMesh);

	//
	//	float fCenterX = (m_vObjects.back()->GetMesh()->GetBoundingCube().m_xmf3Maximum.x
	//		+ m_vObjects.back()->GetMesh()->GetBoundingCube().m_xmf3Minimum.x) / 2;
	//	float fCenterY = (m_vObjects.back()->GetMesh()->GetBoundingCube().m_xmf3Maximum.y
	//		+ m_vObjects.back()->GetMesh()->GetBoundingCube().m_xmf3Minimum.y) / 2;
	//	float fCenterZ = (m_vObjects.back()->GetMesh()->GetBoundingCube().m_xmf3Maximum.z
	//		+ m_vObjects.back()->GetMesh()->GetBoundingCube().m_xmf3Minimum.z) / 2;

	//	XMFLOAT3 xmf3Center(fCenterX, fCenterY, fCenterZ);
	//	XMFLOAT3 xmf3Extent(m_vObjects.back()->GetMesh()->GetBoundingCube().m_xmf3Maximum.x - fCenterX,
	//		m_vObjects.back()->GetMesh()->GetBoundingCube().m_xmf3Maximum.y - fCenterY,
	//		m_vObjects.back()->GetMesh()->GetBoundingCube().m_xmf3Maximum.z - fCenterZ);
	//	CAxisMesh* pAxisMesh = new CAxisMesh(pd3dDevice, xmf3Center, xmf3Extent);
	//	m_vObjects.back()->SetMesh(pAxisMesh, 1);
	//	break;
	//}
	//}
	//
	//
	//m_vObjects.back()->SetTexture(CreateTexture(pd3dDevice));
	//m_vObjects.back()->SetMaterial(CreateMaterial());
	//m_vObjects.back()->GetTransform()->SetPosition(XMFLOAT3(255, 50, 255));
}


CNormalMapShader::CNormalMapShader()
{
}

CNormalMapShader::~CNormalMapShader()
{
}

void CNormalMapShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 3, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 4, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/NormalMap.fx", "VSTexturedLightingColor", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/NormalMap.fx", "PSTexturedLightingColor", "ps_5_0", &GetPixelShader());
}
void CNormalMapShader::BuildObjects(ID3D11Device *pd3dDevice)
{
	
	//m_nShaderType = SHADER_TYPE::NORMALMAP;
	//m_bActive = true;
}

//void CNormalMapShader::AddObject(CGameObject* pSelectedObject)
//{
//	m_vObjects.push_back(pSelectedObject);
//}
void CNormalMapShader::AddObject(int nIndex)
{
	/*switch (nIndex)
	{
	case 0:
	{
		m_vObjects.push_back(new CGameObject(1));
		m_vObjects.back()->SetMesh(m_pCubeMesh);
		m_vObjects.back()->SetTexture(m_pTexture);
		m_vObjects.back()->SetMaterial(m_pMaterial);
		m_vObjects.back()->GetTransform()->SetPosition(XMFLOAT3(255, 15, 255));
		break;
	}
	case 1:
	{
		m_vObjects.push_back(new CGameObject(1));
		m_vObjects.back()->SetMesh(m_pSphereMesh);
		m_vObjects.back()->SetTexture(m_pTexture);
		m_vObjects.back()->SetMaterial(m_pMaterial);
		m_vObjects.back()->GetTransform()->SetPosition(XMFLOAT3(255, 15, 255));
		break;
	}
	case 2:
	{
		m_vObjects.push_back(new CGameObject(1));
		m_vObjects.back()->SetMesh(m_pPlaneMesh);
		m_vObjects.back()->SetTexture(m_pTexture);
		m_vObjects.back()->SetMaterial(m_pMaterial);
		m_vObjects.back()->GetTransform()->SetPosition(XMFLOAT3(255, 15, 255));
		break;
	}
	}*/
}



CDetailTexturedIlluminatedShader::CDetailTexturedIlluminatedShader()
{
}

CDetailTexturedIlluminatedShader::~CDetailTexturedIlluminatedShader()
{
}

void CDetailTexturedIlluminatedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	//	{ "TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT, 3, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "VSDetailTexturedLightingColor", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Effect.fx", "PSDetailTexturedLightingColor", "ps_5_0", &GetPixelShader());
}





CRenderToTextureShader::CRenderToTextureShader()
{
}

CRenderToTextureShader ::~CRenderToTextureShader()
{
	if(m_pQuadMesh) m_pQuadMesh->Release();
}
void  CRenderToTextureShader::BuildObjects(ID3D11Device *pd3dDevice)// ID3D11ShaderResourceView* pd3dsrvRTT)
{
	m_pQuadMesh = new CScreenQuadMesh(pd3dDevice);
	//CGameObject* pGameObject = new CGameObject(1);
	//pGameObject->SetMesh(pQuadMesh);
	//AddObject(pGameObject);



	/*CScreenQuadMesh *pQuadMesh = new CScreenQuadMesh(pd3dDevice);
	CScreenQuadObject* pScreenQuadObject = new CScreenQuadObject(pd3dDevice);

	pScreenQuadObject->SetMesh(pQuadMesh);
	m_vObjects.push_back(pScreenQuadObject);*/

}

void  CRenderToTextureShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Blur.fx", "VS_ScreenQuad", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Blur.fx", "PS_ScreenQuad", "ps_5_0", &GetPixelShader());
}


void CRenderToTextureShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	OnPrepareRender(pd3dDeviceContext);
	m_pQuadMesh->Render(pd3dDeviceContext);
	//GetGameObject(0)->Render(pd3dDeviceContext, pCamera);
}





CAlphaBlendingShader::CAlphaBlendingShader()
{

}
CAlphaBlendingShader::~CAlphaBlendingShader()
{

}
void CAlphaBlendingShader::BuildObjects(ID3D11Device *pd3dDevice)
{
	AddObject(new CAlphaBlendingObject(pd3dDevice));
}
void CAlphaBlendingShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Shader/Water.fx", "VSTextured", "vs_5_0", &GetVertexShader(), d3dInputElements, nElements, &GetInputLayout());
	CreatePixelShaderFromFile(pd3dDevice, L"Shader/Water.fx", "PSTextured", "ps_5_0", &GetPixelShader());
}
void CAlphaBlendingShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	CShader::OnPrepareRender(pd3dDeviceContext);

	//m_vObjects[0]->Render(pd3dDeviceContext, pCamera);
}