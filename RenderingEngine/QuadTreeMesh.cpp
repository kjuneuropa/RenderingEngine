#include "QuadTreeMesh.h"
#include "Object.h"



CQuadTreeMesh::CQuadTreeMesh(ID3D11Device *pd3dDevice, int nWidth, int nLength, XMFLOAT3 xmf3Scale, void *pContext) : CMeshDetailTexturedIlluminated(pd3dDevice)
{
	// Calculate the number of vertices in the 3D terrain model.
	m_nDrawCount = 0;
	m_nVertices = (nWidth - 1) * (nLength - 1) * 6;
	//m_nIndices = m_nVertices;
	//m_pnIndices = new UINT[m_nIndices];

	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	//unsigned short *pHeightMap = (unsigned short *)pContext;

	CNewHeightMap *pHeightMap = (CNewHeightMap *)pContext;


	m_nWidth = nWidth;
	m_nLength = nLength;
	m_xmvScale = xmf3Scale;


	m_vPositions.resize(m_nVertices);
	m_pxmf3Normals = new XMFLOAT3[m_nVertices];
	m_pxmf2TexCoords = new XMFLOAT2[m_nVertices];

	// 일단 어떤 한 곳에 저장해두어야 한다.


	// Initialize the nIndex into the height map array.
	int nIndex = 0, nIndex1 = 0, nIndex2 = 0, nIndex3 = 0, nIndex4 = 0;
	float tu, tv;

	// Load the 3D terrain model with the height map terrain data.
	//// We will be creating 2 triangles for each of the four points in a quad.
	//for (int j = 0; j<(nLength - 1); j++)
	//{
	//	for (int i = 0; i<(nWidth - 1); i++)
	//	{
	//		// Get the nIndexes to the four points of the quad.
	//		nIndex1 = (nWidth * j) + i;          // Upper left.
	//		nIndex2 = (nWidth * j) + (i + 1);      // Upper right.
	//		nIndex3 = (nWidth * (j + 1)) + i;      // Bottom left.
	//		nIndex4 = (nWidth * (j + 1)) + (i + 1);  // Bottom right.

	//		// Now create two triangles for that quad.
	//		// Triangle 1 - Upper left.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].z;

	//		m_pxmf2TexCoords[nIndex].x = 0.0f;
	//		m_pxmf2TexCoords[nIndex].y = 0.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].nz;

	//		nIndex++;

	//		// Triangle 1 - Upper right.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].z;

	//		m_pxmf2TexCoords[nIndex].x = 1.0f;
	//		m_pxmf2TexCoords[nIndex].y = 0.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].nz;

	//		nIndex++;

	//		// Triangle 1 - Bottom left.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].z;

	//		m_pxmf2TexCoords[nIndex].x = 0.0f;
	//		m_pxmf2TexCoords[nIndex].y = 1.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].nz;
	//		nIndex++;

	//		// Triangle 2 - Bottom left.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].z;

	//		m_pxmf2TexCoords[nIndex].x = 0.0f;
	//		m_pxmf2TexCoords[nIndex].y = 1.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].nz;
	//		nIndex++;

	//		// Triangle 2 - Upper right.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].z;

	//		m_pxmf2TexCoords[nIndex].x = 1.0f;
	//		m_pxmf2TexCoords[nIndex].y = 0.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].nz;
	//		nIndex++;

	//		// Triangle 2 - Bottom right.
	//		m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].x;
	//		m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].y;
	//		m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].z;

	//		m_pxmf2TexCoords[nIndex].x = 1.0f;
	//		m_pxmf2TexCoords[nIndex].y = 1.0f;

	//		m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].nx;
	//		m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].ny;
	//		m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].nz;
	//		nIndex++;
	//	}
	//}
	



	
	for (int j = 0; j<(nLength - 1); j++)
	{
		for (int i = 0; i<(nWidth - 1); i++)
		{
			// Get the nIndexes to the four points of the quad.
			nIndex1 = (nLength * j) + i;          // Upper left.
			nIndex2 = (nLength * j) + (i + 1);      // Upper right.
			nIndex3 = (nLength * (j + 1)) + i;      // Bottom left.
			nIndex4 = (nLength * (j + 1)) + (i + 1);  // Bottom right.

													 // Now create two triangles for that quad.
													 // Upper left.
			tv = pHeightMap->m_pHeightMap[nIndex3].tv;

			// Modify the texture coordinates to cover the top edge.
			if (tv == 1.0f) { tv = 0.0f; }
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].z;

			m_pxmf2TexCoords[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].tu;
			m_pxmf2TexCoords[nIndex].y = tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex3].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex3].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex3].nz;

			nIndex++;

			// Triangle 1 - Upper right.
			// Upper right.
			tu = pHeightMap->m_pHeightMap[nIndex4].tu;
			tv = pHeightMap->m_pHeightMap[nIndex4].tv;

			// Modify the texture coordinates to cover the top and right edge.
			if (tu == 0.0f) { tu = 1.0f; }
			if (tv == 1.0f) { tv = 0.0f; }

			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].z;

			m_pxmf2TexCoords[nIndex].x = tu;
			m_pxmf2TexCoords[nIndex].y = tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].nz;

			nIndex++;

			// Triangle 1 - Bottom left.
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].z;

			m_pxmf2TexCoords[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].tu;
			m_pxmf2TexCoords[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].nz;
			nIndex++;

			// Triangle 2 - Bottom left.
			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].z;

			m_pxmf2TexCoords[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].tu;
			m_pxmf2TexCoords[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex1].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex1].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex1].nz;
			nIndex++;

			// Triangle 2 - Upper right.
			// Upper right.
			tu = pHeightMap->m_pHeightMap[nIndex4].tu;
			tv = pHeightMap->m_pHeightMap[nIndex4].tv;

			// Modify the texture coordinates to cover the top and right edge.
			if (tu == 0.0f) { tu = 1.0f; }
			if (tv == 1.0f) { tv = 0.0f; }


			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].z;

			m_pxmf2TexCoords[nIndex].x = tu;
			m_pxmf2TexCoords[nIndex].y = tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex4].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex4].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex4].nz;
			nIndex++;

			// Triangle 2 - Bottom right.
			// Bottom right.
			tu = pHeightMap->m_pHeightMap[nIndex2].tu;

			// Modify the texture coordinates to cover the right edge.
			if (tu == 0.0f) { tu = 1.0f; }

			m_vPositions[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].x;
			m_vPositions[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].y;
			m_vPositions[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].z;

			m_pxmf2TexCoords[nIndex].x = tu;
			m_pxmf2TexCoords[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].tv;

			m_pxmf3Normals[nIndex].x = pHeightMap->m_pHeightMap[nIndex2].nx;
			m_pxmf3Normals[nIndex].y = pHeightMap->m_pHeightMap[nIndex2].ny;
			m_pxmf3Normals[nIndex].z = pHeightMap->m_pHeightMap[nIndex2].nz;
			nIndex++;
		}
	}
	


	/*for (int i = 0; i < m_nVertices; ++i)
		m_pnIndices[i] = i;*/



	CreateQuadTree(pd3dDevice);



	/*D3D11_RASTERIZER_DESC d3dRasterizerDesc;
	d3dRasterizerDesc.AntialiasedLineEnable = false;
	d3dRasterizerDesc.CullMode = D3D11_CULL_BACK;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = true;
	d3dRasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
	d3dRasterizerDesc.FrontCounterClockwise = false;
	d3dRasterizerDesc.MultisampleEnable = false;
	d3dRasterizerDesc.ScissorEnable = false;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;

	pd3dDevice->CreateRasterizerState(&d3dRasterizerDesc, &m_pd3dRasterizerState);*/



	XNA::AxisAlignedBox pBoundingBox;
	XNA::ComputeBoundingAxisAlignedBoxFromPoints(&pBoundingBox, m_nVertices, &m_vPositions[0], sizeof(XMFLOAT3));


	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(pBoundingBox.Center.x - pBoundingBox.Extents.x,
		pBoundingBox.Center.y - pBoundingBox.Extents.y, pBoundingBox.Center.z - pBoundingBox.Extents.z);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(pBoundingBox.Center.x + pBoundingBox.Extents.x,
		pBoundingBox.Center.y + pBoundingBox.Extents.y, pBoundingBox.Center.z + pBoundingBox.Extents.z);

	

}

CQuadTreeMesh::~CQuadTreeMesh()
{
	if (m_pParentNode)
	{
		ReleaseNode(m_pParentNode);
		delete m_pParentNode;
	}
}
void CQuadTreeMesh::ReleaseNode(NodeType* pParentNode)
{
	// Recursively go down the tree and release the bottom nodes first.
	for (int i = 0; i<4; i++)
	{
		if (pParentNode->pNodes[i] != 0)
		{
			ReleaseNode(pParentNode->pNodes[i]);
		}
	}

	// Release the vertex buffer for this node.
	if (pParentNode->ppd3dVertexBuffers)
	{
		delete[]pParentNode->ppd3dVertexBuffers;
	}

	// Release the nIndex buffer for this node.
	if (pParentNode->nIndexBuffer)
	{
		pParentNode->nIndexBuffer->Release();
	}

	// Release the four child nodes.
	for (int i = 0; i<4; i++)
	{
		if (pParentNode->pNodes[i])
		{
			delete pParentNode->pNodes[i];
		}
	}

}

void CQuadTreeMesh::Render(CCamera* pCamera, ID3D11DeviceContext *pd3dDeviceContext)
{
	m_nDrawCount = 0;

	// Render each node that is visible starting at the parent node and moving down the tree.
	
	RenderNode(pCamera, m_pParentNode, pd3dDeviceContext);
	cout << "DrawCount : " << m_nDrawCount << endl;
}
void CQuadTreeMesh::RenderNode(CCamera* pCamera, NodeType* pNode, ID3D11DeviceContext *pd3dDeviceContext)
{
	bool result;
	int count, nIndexCount;
	unsigned int stride, offset;


	// Check to see if the node can be viewed, height doesn't matter in a quad tree.
	AABB bcBoundingCube;
	bcBoundingCube.m_xmf3Maximum = XMFLOAT3(pNode->positionX + pNode->width / 2, 0.0f, pNode->positionZ + pNode->width / 2);
	bcBoundingCube.m_xmf3Minimum = XMFLOAT3(pNode->positionX - pNode->width / 2, 0.0f, pNode->positionZ - pNode->width / 2);
	if (pCamera->IsInFrustum(&bcBoundingCube))
	{
		// If it can be seen then check all four child nodes to see if they can also be seen.
		count = 0;
		for (int i = 0; i < 4; i++)
		{
			if (pNode->pNodes[i] != 0)
			{
				count++;
				RenderNode(pCamera, pNode->pNodes[i], pd3dDeviceContext);
			}
		}
	}
	else
		return;




	// If there were any children nodes then there is no need to continue as parent nodes won't contain any triangles to render.
	if (count != 0)
	{
		return;
	}


	//메쉬의 정점은 여러 개의 정점 버퍼로 표현된다.
	pd3dDeviceContext->IASetVertexBuffers(m_nSlot, 3, pNode->ppd3dVertexBuffers, pNode->pnVertexStrides, pNode->pnVertexOffsets);
	pd3dDeviceContext->IASetIndexBuffer(pNode->nIndexBuffer, m_dxgiIndexFormat, m_nIndexOffset);
	pd3dDeviceContext->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	pd3dDeviceContext->RSSetState(m_pd3dRasterizerState);

	//if (pNode->nIndexBuffer)
	pd3dDeviceContext->DrawIndexed(pNode->triangleCount*3, 0, 0);

		


	// Increase the count of the number of polygons that have been rendered during this frame.
	m_nDrawCount += pNode->triangleCount;
}
void CQuadTreeMesh::CreateQuadTree(ID3D11Device *pd3dDevice)
{
	float fCenterX, fCenterZ, fMeshWidth;
	m_nTriangleCount = m_nVertices / 3;
	CalculateMeshDimensions( fCenterX,  fCenterZ,  fMeshWidth);

	// Create the parent node for the quad tree.
	m_pParentNode = new NodeType;


	// Recursively build the quad tree based on the vertex list data and mesh dimensions.
	CreateTreeNode(m_pParentNode, fCenterX, fCenterZ, fMeshWidth, pd3dDevice);

}
void CQuadTreeMesh::CalculateMeshDimensions(float& fCenterX, float& fCenterZ, float& fMeshWidth)
{
	int i;
	float maxWidth, maxDepth, minWidth, minDepth, width, depth, maxX, maxZ;


	// Initialize the center position of the mesh to zero.
	fCenterX = 0.0f;
	fCenterZ = 0.0f;

	// Sum all the vertices in the mesh.
	for (i = 0; i<m_nVertices; i++)
	{
		fCenterX += m_vPositions[i].x;
		fCenterZ += m_vPositions[i].z;
	}

	// And then divide it by the number of vertices to find the mid-point of the mesh.
	fCenterX = fCenterX / (float)m_nVertices;
	fCenterZ = fCenterZ / (float)m_nVertices;

	// Initialize the maximum and minimum size of the mesh.
	maxWidth = 0.0f;
	maxDepth = 0.0f;

	minWidth = fabsf(m_vPositions[i].x - fCenterX);
	minDepth = fabsf(m_vPositions[i].z - fCenterZ);

	// Go through all the vertices and find the maximum and minimum width and depth of the mesh.
	for (i = 0; i<m_nVertices; i++)
	{
		width = fabsf(m_vPositions[i].x - fCenterX);
		depth = fabsf(m_vPositions[i].z - fCenterZ);

		if (width > maxWidth) { maxWidth = width; }
		if (depth > maxDepth) { maxDepth = depth; }
		if (width < minWidth) { minWidth = width; }
		if (depth < minDepth) { minDepth = depth; }
	}

	// Find the absolute maximum value between the min and max depth and width.
	maxX = (float)max(fabs(minWidth), fabs(maxWidth));
	maxZ = (float)max(fabs(minDepth), fabs(maxDepth));

	// Calculate the maximum diameter of the mesh.
	fMeshWidth = max(maxX, maxZ) * 2.0f;

	return;
}


void CQuadTreeMesh::CreateTreeNode(NodeType* pNode, float positionX, float positionZ, float width, ID3D11Device*  pd3dDevice)
{
	int nNumTriangles, nCount,nVertexCount, nIndex, nVertexIndex;
	float fOffsetX, fOffsetZ;

	unsigned int* indices;
	bool result;



	// Store the node position and size.
	pNode->positionX = positionX;
	pNode->positionZ = positionZ;
	pNode->width = width;
	pNode->pnVertexOffsets = nullptr;
	pNode->nIndexBuffer = nullptr;
	pNode->pnVertexStrides = nullptr;
	pNode->ppd3dVertexBuffers = nullptr;
	

	// Initialize the triangle nCount to zero for the node.
	pNode->triangleCount = 0;

	// Initialize the vertex and nIndex buffer to null.

	pNode->nIndexBuffer = 0;

	// Initialize the children nodes of this node to null.
	for(int i=0; i<4; ++i)
		pNode->pNodes[i] = nullptr;

	// Count the number of triangles that are inside this node.
	nNumTriangles = CountTriangles(positionX, positionZ, width);

	// Case 1: If there are no triangles in this node then return as it is empty and requires no processing.
	if (nNumTriangles == 0)
	{
		return;
	}

	// Case 2: If there are too many triangles in this node then split it into four equal sized smaller tree nodes.
	if (nNumTriangles > MAX_TRIANGLES)
	{
		for (int i = 0; i<4; i++)
		{
			// Calculate the position offsets for the new child node.
			fOffsetX = (((i % 2) < 1) ? -1.0f : 1.0f) * (width / 4.0f);
			fOffsetZ = (((i % 4) < 2) ? -1.0f : 1.0f) * (width / 4.0f);

			// See if there are any triangles in the new node.
			nCount = CountTriangles((positionX + fOffsetX), (positionZ + fOffsetZ), (width / 2.0f));
			if (nCount > 0)
			{
				// If there are triangles inside where this new node would be then create the child node.
				pNode->pNodes[i] = new NodeType;

				// Extend the tree starting from this new child node now.
				CreateTreeNode(pNode->pNodes[i], (positionX + fOffsetX), (positionZ + fOffsetZ), (width / 2.0f), pd3dDevice);
			}
		}

		return;
	}

	// Case 3: If this node is not empty and the triangle nCount for it is less than the max then 
	// this node is at the bottom of the tree so create the list of triangles to store in it.
	pNode->triangleCount = nNumTriangles;

	// Calculate the number of vertices.
	nVertexCount = nNumTriangles * 3;

	// Create the vertex array.
	XMFLOAT3 *pxmf3Positions = new XMFLOAT3[nVertexCount];
	XMFLOAT3 *pxmf3Normals = new XMFLOAT3[nVertexCount];
	XMFLOAT2 *pxmf2TexCoords = new XMFLOAT2[nVertexCount];

	// Create the nIndex array.
	indices = new unsigned int[nVertexCount];

	// Initialize the nIndex for this new vertex and nIndex array.
	nIndex = 0;

	// Go through all the triangles in the vertex list.
	for (int i = 0; i<m_nTriangleCount; i++)
	{
		// If the triangle is inside this node then add it to the vertex array.
		result = IsTriangleContained(i, positionX, positionZ, width);
		if (result == true)
		{
			// Calculate the nIndex into the terrain vertex list.
			nVertexIndex = i * 3;

			// Get the three vertices of this triangle from the vertex list.
			pxmf3Positions[nIndex] = m_vPositions[nVertexIndex];
			pxmf2TexCoords[nIndex] = m_pxmf2TexCoords[nVertexIndex];
			pxmf3Normals[nIndex] = m_pxmf3Normals[nVertexIndex];
			indices[nIndex] = nIndex;
			nIndex++;

			nVertexIndex++;
			pxmf3Positions[nIndex] = m_vPositions[nVertexIndex];
			pxmf2TexCoords[nIndex] = m_pxmf2TexCoords[nVertexIndex];
			pxmf3Normals[nIndex] = m_pxmf3Normals[nVertexIndex];
			indices[nIndex] = nIndex;
			nIndex++;

			nVertexIndex++;
			pxmf3Positions[nIndex] = m_vPositions[nVertexIndex];
			pxmf2TexCoords[nIndex] = m_pxmf2TexCoords[nVertexIndex];
			pxmf3Normals[nIndex] = m_pxmf3Normals[nVertexIndex];
			indices[nIndex] = nIndex;
			nIndex++;
		}
	}



	// Now finally create the vertex buffer.

	D3D11_BUFFER_DESC d3dBufferDesc;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ID3D11Buffer *pd3dPositionBuffer = nullptr;
	ID3D11Buffer *pd3dNormalBuffer = nullptr;
	ID3D11Buffer *pd3dTexCoordBuffer = nullptr;

	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * nVertexCount;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = pxmf3Positions;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &pd3dPositionBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * nVertexCount;
	d3dBufferData.pSysMem = pxmf3Normals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &pd3dNormalBuffer);

	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * nVertexCount;
	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &pd3dTexCoordBuffer);


	ID3D11Buffer *pd3dBuffers[3] = { pd3dPositionBuffer, pd3dNormalBuffer, pd3dTexCoordBuffer };
	UINT pnBufferStrides[3] = { sizeof(XMFLOAT3), sizeof(XMFLOAT3), sizeof(XMFLOAT2) };
	UINT pnBufferOffsets[3] = { 0, 0, 0 };


	pNode->ppd3dVertexBuffers = new ID3D11Buffer*[3];
	pNode->pnVertexStrides = new UINT[3];
	pNode->pnVertexOffsets = new UINT[3];
	pNode->nBuffers = 3;

	for (int i = 0; i < 3; i++)
	{
		pNode->ppd3dVertexBuffers[i] = pd3dBuffers[i];
		pNode->pnVertexStrides[i] = pnBufferStrides[i];
		pNode->pnVertexOffsets[i] = pnBufferOffsets[i];
	}



	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	// Set up the description of the nIndex buffer.
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(unsigned int) *nVertexCount;
	d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	d3dBufferDesc.MiscFlags = 0;
	d3dBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the nIndex data.
	d3dBufferData.pSysMem = indices;
	d3dBufferData.SysMemPitch = 0;
	d3dBufferData.SysMemSlicePitch = 0;

	// Create the nIndex buffer.
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &pNode->nIndexBuffer);

	// Release the vertex and nIndex arrays now that the data is stored in the buffers in the node.
	delete[] pxmf3Positions;
	delete[] pxmf2TexCoords;
	delete[] pxmf3Normals;



	delete[] indices;
	indices = 0;

	return;
}
int CQuadTreeMesh::CountTriangles(float positionX, float positionZ, float width)
{
	int count, i;
	bool result;


	// Initialize the count to zero.
	count = 0;

	// Go through all the triangles in the entire mesh and check which ones should be inside this node.
	for (i = 0; i<m_nTriangleCount; i++)
	{
		// If the triangle is inside the node then increment the count by one.
		result = IsTriangleContained(i, positionX, positionZ, width);
		if (result == true)
		{
			count++;
		}
	}

	return count;
}
bool CQuadTreeMesh::IsTriangleContained(int nIndex, float positionX, float positionZ, float width)
{
	float radius;
	int vertexIndex;
	float x1, z1, x2, z2, x3, z3;
	float minimumX, maximumX, minimumZ, maximumZ;


	// Calculate the radius of this node.
	radius = width / 2.0f;

	// Get the nIndex into the vertex list.
	vertexIndex = nIndex * 3;

	// Get the three vertices of this triangle from the vertex list.
	x1 = m_vPositions[vertexIndex].x;
	z1 = m_vPositions[vertexIndex].z;
	vertexIndex++;

	x2 = m_vPositions[vertexIndex].x;
	z2 = m_vPositions[vertexIndex].z;
	vertexIndex++;

	x3 = m_vPositions[vertexIndex].x;
	z3 = m_vPositions[vertexIndex].z;

	// Check to see if the minimum of the x coordinates of the triangle is inside the node.
	minimumX = min(x1, min(x2, x3));
	if (minimumX > (positionX + radius))
	{
		return false;
	}

	// Check to see if the maximum of the x coordinates of the triangle is inside the node.
	maximumX = max(x1, max(x2, x3));
	if (maximumX < (positionX - radius))
	{
		return false;
	}

	// Check to see if the minimum of the z coordinates of the triangle is inside the node.
	minimumZ = min(z1, min(z2, z3));
	if (minimumZ >(positionZ + radius))
	{
		return false;
	}

	// Check to see if the maximum of the z coordinates of the triangle is inside the node.
	maximumZ = max(z1, max(z2, z3));
	if (maximumZ < (positionZ - radius))
	{
		return false;
	}

	return true;
}