#include "Mesh.h"
#include "Object.h"
using namespace std;
//using namespace DirectX;



CMesh::CMesh(ID3D11Device *pd3dDevice)
{
	m_nBuffers = 0;
	m_pd3dPositionBuffer = nullptr;
	m_ppd3dVertexBuffers = nullptr;

	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	m_nSlot = 0;
	m_nStartVertex = 0;

	m_pd3dIndexBuffer = nullptr;
	m_nIndices = 0;
	m_nStartIndex = 0;
	m_nBaseVertex = 0;
	m_nIndexOffset = 0;
	m_dxgiIndexFormat = DXGI_FORMAT_R32_UINT;

	m_pd3dRasterizerState = nullptr;

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(+FLT_MAX, +FLT_MAX, +FLT_MAX);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	m_nReferences = 0;

	m_pnIndices = nullptr;

	D3D11_RASTERIZER_DESC d3dRasterizerDesc;
	//d3dRasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
	//d3dRasterizerDesc.CullMode = D3D11_CULL_BACK;
	//d3dRasterizerDesc.FrontCounterClockwise = false;



	//d3dRasterizerDesc.AntialiasedLineEnable = false;
	//d3dRasterizerDesc.CullMode = D3D11_CULL_BACK;
	//d3dRasterizerDesc.DepthBias = 0;
	//d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	//d3dRasterizerDesc.DepthClipEnable = true;
	//d3dRasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
	//d3dRasterizerDesc.FrontCounterClockwise = false;
	//d3dRasterizerDesc.MultisampleEnable = false;
	//d3dRasterizerDesc.ScissorEnable = false;
	//d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;



	//pd3dDevice->CreateRasterizerState(&d3dRasterizerDesc, &m_pd3dRasterizerState);




}

CMesh::~CMesh()
{
	if (m_pd3dRasterizerState) m_pd3dRasterizerState->Release();
	if (m_pd3dPositionBuffer) m_pd3dPositionBuffer->Release();
	if (m_pd3dIndexBuffer) m_pd3dIndexBuffer->Release();

	if (m_ppd3dVertexBuffers) delete[] m_ppd3dVertexBuffers;
	if (m_pnVertexStrides) delete[] m_pnVertexStrides;
	if (m_pnVertexOffsets) delete[] m_pnVertexOffsets;

	//if (m_vPositions) delete[] m_vPositions;
	m_vPositions.clear();
	if (m_pnIndices) delete[] m_pnIndices;
}
// 메쉬를 그린다. 메쉬 클래스의 정점 버퍼를 디바이스 컨텍스트에 연결하고 프리미티브 유형을 설정한다.  
void CMesh::Render(ID3D11DeviceContext *pd3dDeviceContext)
{
	//메쉬의 정점은 여러 개의 정점 버퍼로 표현된다.
	pd3dDeviceContext->IASetVertexBuffers(m_nSlot, m_nBuffers, m_ppd3dVertexBuffers, m_pnVertexStrides, m_pnVertexOffsets);
	pd3dDeviceContext->IASetIndexBuffer(m_pd3dIndexBuffer, m_dxgiIndexFormat, m_nIndexOffset);
	pd3dDeviceContext->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	pd3dDeviceContext->RSSetState(m_pd3dRasterizerState);

	if (m_pd3dIndexBuffer)
		pd3dDeviceContext->DrawIndexed(m_nIndices, m_nStartIndex, m_nBaseVertex);
	else
		pd3dDeviceContext->Draw(m_nVertices, m_nStartVertex);
}
void CMesh::RenderInstanced(ID3D11DeviceContext *pd3dDeviceContext, int nInstances, int nStartInstance)
{
	
	//인스턴싱의 경우 입력 조립기에 메쉬의 정점 버퍼와 인스턴스 정점 버퍼가 연결된다.
	pd3dDeviceContext->IASetVertexBuffers(m_nSlot, m_nBuffers, m_ppd3dVertexBuffers, m_pnVertexStrides, m_pnVertexOffsets);
	pd3dDeviceContext->IASetIndexBuffer(m_pd3dIndexBuffer, m_dxgiIndexFormat, m_nIndexOffset);
	
	
	pd3dDeviceContext->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	
	
	pd3dDeviceContext->RSSetState(m_pd3dRasterizerState);

	//객체들의 인스턴스들을 렌더링한다. 
	if (m_pd3dIndexBuffer)
		pd3dDeviceContext->DrawIndexedInstanced(m_nIndices, nInstances, m_nStartIndex, m_nBaseVertex, nStartInstance);
	else
		pd3dDeviceContext->DrawInstanced(m_nVertices, nInstances, m_nStartVertex, nStartInstance);
}
void CMesh::AssembleToVertexBuffer(int nBuffers, ID3D11Buffer **ppd3dBuffers, UINT *pnBufferStrides, UINT *pnBufferOffsets)
{
	ID3D11Buffer **ppd3dNewVertexBuffers = new ID3D11Buffer*[m_nBuffers + nBuffers];
	UINT *pnNewVertexStrides = new UINT[m_nBuffers + nBuffers];
	UINT *pnNewVertexOffsets = new UINT[m_nBuffers + nBuffers];

	if (m_nBuffers > 0)
	{
		for (int i = 0; i < m_nBuffers; i++)
		{
			ppd3dNewVertexBuffers[i] = m_ppd3dVertexBuffers[i];
			pnNewVertexStrides[i] = m_pnVertexStrides[i];
			pnNewVertexOffsets[i] = m_pnVertexOffsets[i];
		}
		if (m_ppd3dVertexBuffers) delete[] m_ppd3dVertexBuffers;
		if (m_pnVertexStrides) delete[] m_pnVertexStrides;
		if (m_pnVertexOffsets) delete[] m_pnVertexOffsets;
	}

	for (int i = 0; i < nBuffers; i++)
	{
		ppd3dNewVertexBuffers[m_nBuffers + i] = ppd3dBuffers[i];
		pnNewVertexStrides[m_nBuffers + i] = pnBufferStrides[i];
		pnNewVertexOffsets[m_nBuffers + i] = pnBufferOffsets[i];
	}

	m_nBuffers += nBuffers;
	m_ppd3dVertexBuffers = ppd3dNewVertexBuffers;
	m_pnVertexStrides = pnNewVertexStrides;
	m_pnVertexOffsets = pnNewVertexOffsets;
}


int CMesh::CheckRayIntersection(XMFLOAT3 *pxmf3RayPosition, XMFLOAT3 *pxmf3RayDirection, MESHINTERSECTINFO *pIntersectInfo)
{
	//모델 좌표계의 광선의 시작점(pd3dxvRayPosition)과 방향이 주어질 때 메쉬와의 충돌 검사를 한다.
	int nIntersections = 0;

	// 임시 주석
	//BYTE *pbPositions;
	//BYTE *pbPositions = reinterpret_cast<BYTE*>(m_vPositions) + m_pnVertexOffsets[0];
	BYTE *pbPositions = reinterpret_cast<BYTE*>(&m_vPositions[0]) + m_pnVertexOffsets[0];

	int nOffset = (m_d3dPrimitiveTopology == D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST) ? 3 : 1;
	// 메쉬의 프리미티브(삼각형)들의 개수이다. 
	// 삼각형 리스트인 경우 (정점의 개수 / 3) 또는 (인덱스의 개수 / 3), 
	// 삼각형 스트립의 경우 (정점의 개수 - 2) 또는 (인덱스의 개수 - 2)이다.
	int nPrimitives = (m_d3dPrimitiveTopology == D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST) ? (m_nVertices / 3) : (m_nVertices - 2);
	if (m_nIndices > 0) nPrimitives = (m_d3dPrimitiveTopology == D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST) ? (m_nIndices / 3) : (m_nIndices - 2);

	XMFLOAT3 v0, v1, v2;
	float fuHitBaryCentric, fvHitBaryCentric, fHitDistance, fNearHitDistance = FLT_MAX;
	D3DXVECTOR3 d3dxv0;
	D3DXVECTOR3 d3dxv1;
	D3DXVECTOR3 d3dxv2;
	D3DXVECTOR3 rayPosition(pxmf3RayPosition->x, pxmf3RayPosition->y, pxmf3RayPosition->z);
	D3DXVECTOR3 rayDirection(pxmf3RayDirection->x, pxmf3RayDirection->y, pxmf3RayDirection->z); 


	// 메쉬의 모든 프리미티브(삼각형)들에 대하여 픽킹 광선과의 충돌을 검사한다. 
	// 충돌하는 모든 삼각형을 찾아 광선의 시작점(실제로는 카메라 좌표계의 원점)에 가장 가까운 삼각형을 찾는다.
	//XMPlaneIntersectLine
	for (int i = 0; i < nPrimitives; i++)
	{
		v0 = *(XMFLOAT3 *)(pbPositions + ((m_pnIndices) ? (m_pnIndices[(i*nOffset) + 0]) : ((i*nOffset) + 0)) * m_pnVertexStrides[0]);
		v1 = *(XMFLOAT3 *)(pbPositions + ((m_pnIndices) ? (m_pnIndices[(i*nOffset) + 1]) : ((i*nOffset) + 1)) * m_pnVertexStrides[0]);
		v2 = *(XMFLOAT3 *)(pbPositions + ((m_pnIndices) ? (m_pnIndices[(i*nOffset) + 2]) : ((i*nOffset) + 2)) * m_pnVertexStrides[0]);
		d3dxv0.x = v0.x, d3dxv0.y = v0.y, d3dxv0.z = v0.z;
		d3dxv1.x = v1.x, d3dxv1.y = v1.y, d3dxv1.z = v1.z;
		d3dxv2.x = v2.x, d3dxv2.y = v2.y, d3dxv2.z = v2.z;
	
			//IntersectRayTriangle()
		/*D3DXVECTOR3 d3dxv0(v0.x, v1.y, v2.z);
		D3DXVECTOR3 d3dxv1(v0.x, v1.y, v2.z);
		D3DXVECTOR3 d3dxv2(v0.x, v1.y, v2.z);
		D3DXVECTOR3 rayPosition(pxmf3RayPosition->x, pxmf3RayPosition->y, pxmf3RayPosition->z);
		D3DXVECTOR3 rayDirection(pxmf3RayDirection->x, pxmf3RayDirection->y, pxmf3RayDirection->z);*/
		

	/*	if (XNA::IntersectRayTriangle(XMLoadFloat3(pxmf3RayPosition), XMLoadFloat3(pxmf3RayDirection),
			XMLoadFloat3(&v0), XMLoadFloat3(&v1), XMLoadFloat3(&v2), &fuHitBaryCentric, &fvHitBaryCentric, &fHitDistance))*/
				if (D3DXIntersectTri(&d3dxv0, &d3dxv1, &d3dxv2, &rayPosition, &rayDirection,
			&fuHitBaryCentric, &fvHitBaryCentric, &fHitDistance))
		{
			if (fHitDistance < fNearHitDistance)
			{
				fNearHitDistance = fHitDistance;
				if (pIntersectInfo)
				{
					pIntersectInfo->m_dwFaceIndex = i;
					pIntersectInfo->m_fU = fuHitBaryCentric;
					pIntersectInfo->m_fV = fvHitBaryCentric;
					pIntersectInfo->m_fDistance = fHitDistance;
				}
			}
			nIntersections++;
		}
	}
	return(nIntersections);
}


int CMesh::CheckRayIntersectionAABB(XMFLOAT3 *pxmf3RayPosition, XMFLOAT3 *pxmf3RayDirection, MESHINTERSECTINFO *pIntersectInfo)
{

	XNA::AxisAlignedBox MeshBox;
	int nIntersections = 0;
	float fHitDistance, fNearHitDistance = FLT_MAX;
	XMStoreFloat3(&MeshBox.Center, 0.5f*
		(XMLoadFloat3(&m_bcBoundingCube.m_xmf3Minimum) + XMLoadFloat3(&m_bcBoundingCube.m_xmf3Maximum)));
	XMStoreFloat3(&MeshBox.Extents, 0.5f*
		(XMLoadFloat3(&m_bcBoundingCube.m_xmf3Maximum) - XMLoadFloat3(&m_bcBoundingCube.m_xmf3Minimum)));


	if (XNA::IntersectRayAxisAlignedBox(XMLoadFloat3(pxmf3RayPosition), XMLoadFloat3(pxmf3RayDirection), &MeshBox, &fHitDistance))
	{
		if (fHitDistance < fNearHitDistance)
		{
			fNearHitDistance = fHitDistance;
			if (pIntersectInfo)
			{
				pIntersectInfo->m_fDistance = fHitDistance;
			}
		}
		nIntersections++;
	}
	return(nIntersections);
}



CMeshDiffused::CMeshDiffused(ID3D11Device *pd3dDevice) : CMesh(pd3dDevice)
{
	m_pd3dColorBuffer = nullptr;
}

CMeshDiffused::~CMeshDiffused()
{
	if (m_pd3dColorBuffer) m_pd3dColorBuffer->Release();
}
CSphereMeshDiffused::CSphereMeshDiffused(ID3D11Device *pd3dDevice, float fRadius, int nSlices, int nStacks, XMFLOAT4 d3dxColor) : CMeshDiffused(pd3dDevice)
{
	m_nVertices = (nSlices * nStacks) * 3 * 2;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	//구 메쉬는 2개의 정점 버퍼(위치 벡터 버퍼와 색상 버퍼)로 구성된다.
	//구 메쉬의 정점 버퍼(위치 벡터 버퍼)를 생성한다.

	//m_vPositions = new XMFLOAT3[m_nVertices];
	m_vPositions.resize(m_nVertices);

	float theta_i, theta_ii, phi_j, phi_jj, fRadius_j, fRadius_jj, y_j, y_jj;
	for (int j = 0, k = 0; j < nStacks; j++)
	{
		phi_j = float(D3DX_PI / nStacks) * j;
		phi_jj = float(D3DX_PI / nStacks) * (j + 1);
		fRadius_j = fRadius * sinf(phi_j);
		fRadius_jj = fRadius * sinf(phi_jj);
		y_j = fRadius*cosf(phi_j);
		y_jj = fRadius*cosf(phi_jj);
		for (int i = 0; i < nSlices; i++)
		{
			theta_i = float(2 * D3DX_PI / nSlices) * i;
			theta_ii = float(2 * D3DX_PI / nSlices) * (i + 1);
			m_vPositions[k++] = XMFLOAT3(fRadius_j*cosf(theta_i), y_j, fRadius_j*sinf(theta_i));
			m_vPositions[k++] = XMFLOAT3(fRadius_jj*cosf(theta_i), y_jj, fRadius_jj*sinf(theta_i));
			m_vPositions[k++] = XMFLOAT3(fRadius_j*cosf(theta_ii), y_j, fRadius_j*sinf(theta_ii));
			m_vPositions[k++] = XMFLOAT3(fRadius_jj*cosf(theta_i), y_jj, fRadius_jj*sinf(theta_i));
			m_vPositions[k++] = XMFLOAT3(fRadius_jj*cosf(theta_ii), y_jj, fRadius_jj*sinf(theta_ii));
			m_vPositions[k++] = XMFLOAT3(fRadius_j*cosf(theta_ii), y_j, fRadius_j*sinf(theta_ii));
		}
	}

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	//delete[] m_vPositions;

	//구 메쉬의 정점 버퍼(색상 버퍼)를 생성한다.
	XMFLOAT4 *pd3dxColors = new XMFLOAT4[m_nVertices];
	for (int i = 0; i < m_nVertices; i++) pd3dxColors[i] = d3dxColor;// +RANDOM_COLOR;

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT4) * m_nVertices;
	d3dBufferData.pSysMem = pd3dxColors;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dColorBuffer);

	delete[] pd3dxColors;

	ID3D11Buffer *pd3dBuffers[2] = { m_pd3dPositionBuffer, m_pd3dColorBuffer };
	UINT pnBufferStrides[2] = { sizeof(XMFLOAT3), sizeof(XMFLOAT4) };
	UINT pnBufferOffsets[2] = { 0, 0 };
	AssembleToVertexBuffer(2, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fRadius, -fRadius, -fRadius);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fRadius, +fRadius, +fRadius);
}

CSphereMeshDiffused::~CSphereMeshDiffused()
{
}

void AABB::Union(XMFLOAT3& d3dxvMinimum, XMFLOAT3& d3dxvMaximum)
{
	if (d3dxvMinimum.x < m_xmf3Minimum.x) m_xmf3Minimum.x = d3dxvMinimum.x;
	if (d3dxvMinimum.y < m_xmf3Minimum.y) m_xmf3Minimum.y = d3dxvMinimum.y;
	if (d3dxvMinimum.z < m_xmf3Minimum.z) m_xmf3Minimum.z = d3dxvMinimum.z;
	if (d3dxvMaximum.x > m_xmf3Maximum.x) m_xmf3Maximum.x = d3dxvMaximum.x;
	if (d3dxvMaximum.y > m_xmf3Maximum.y) m_xmf3Maximum.y = d3dxvMaximum.y;
	if (d3dxvMaximum.z > m_xmf3Maximum.z) m_xmf3Maximum.z = d3dxvMaximum.z;
}

void AABB::Union(AABB *pAABB)
{
	Union(pAABB->m_xmf3Minimum, pAABB->m_xmf3Maximum);
}

void AABB::Update(XMMATRIX *pmtxTransform)
{
	/*바운딩 박스의 최소점과 최대점은 회전을 하면 더 이상 최소점과 최대점이 되지 않는다. 
	그러므로 바운딩 박스의 최소점과 최대점에서 8개의 정점을 구하고 변환(회전)을 한 다음 최소점과 최대점을 다시 계산한다.*/

	XMFLOAT3 vVertices[8];
	vVertices[0] = XMFLOAT3(m_xmf3Minimum.x, m_xmf3Minimum.y, m_xmf3Minimum.z);
	vVertices[1] = XMFLOAT3(m_xmf3Minimum.x, m_xmf3Minimum.y, m_xmf3Maximum.z);
	vVertices[2] = XMFLOAT3(m_xmf3Maximum.x, m_xmf3Minimum.y, m_xmf3Maximum.z);
	vVertices[3] = XMFLOAT3(m_xmf3Maximum.x, m_xmf3Minimum.y, m_xmf3Minimum.z);
	vVertices[4] = XMFLOAT3(m_xmf3Minimum.x, m_xmf3Maximum.y, m_xmf3Minimum.z);
	vVertices[5] = XMFLOAT3(m_xmf3Minimum.x, m_xmf3Maximum.y, m_xmf3Maximum.z);
	vVertices[6] = XMFLOAT3(m_xmf3Maximum.x, m_xmf3Maximum.y, m_xmf3Maximum.z);
	vVertices[7] = XMFLOAT3(m_xmf3Maximum.x, m_xmf3Maximum.y, m_xmf3Minimum.z);
	m_xmf3Minimum = XMFLOAT3(+FLT_MAX, +FLT_MAX, +FLT_MAX);
	m_xmf3Maximum = XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	//8개의 정점에서 x, y, z 좌표의 최소값과 최대값을 구한다.
	for (int i = 0; i < 8; i++)
	{
		//정점을 변환한다.
		//D3DXVec3TransformCoord(&vVertices[i], &vVertices[i], pmtxTransform);
		XMStoreFloat3(&vVertices[i], XMVector3TransformCoord(XMLoadFloat3(&vVertices[i]), *pmtxTransform));
		if (vVertices[i].x < m_xmf3Minimum.x) m_xmf3Minimum.x = vVertices[i].x;
		if (vVertices[i].y < m_xmf3Minimum.y) m_xmf3Minimum.y = vVertices[i].y;
		if (vVertices[i].z < m_xmf3Minimum.z) m_xmf3Minimum.z = vVertices[i].z;
		if (vVertices[i].x > m_xmf3Maximum.x) m_xmf3Maximum.x = vVertices[i].x;
		if (vVertices[i].y > m_xmf3Maximum.y) m_xmf3Maximum.y = vVertices[i].y;
		if (vVertices[i].z > m_xmf3Maximum.z) m_xmf3Maximum.z = vVertices[i].z;
	}
}

CHeightMapGridMesh::CHeightMapGridMesh(ID3D11Device *pd3dDevice, int xStart, int zStart, int nWidth, int nLength, XMFLOAT3 xmf3Scale, void *pContext) : CMeshDetailTexturedIlluminated(pd3dDevice)
{
	m_nVertices = nWidth * nLength;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;

	//m_vPositions = new XMFLOAT3[m_nVertices];
	m_vPositions.resize(m_nVertices);
	XMFLOAT3 *pxmf3Normals = new XMFLOAT3[m_nVertices];
	XMFLOAT2 *pxmf2TexCoords = new XMFLOAT2[m_nVertices];
	XMFLOAT2 *pxmf2DetailTexCoords = new XMFLOAT2[m_nVertices];

	m_nWidth = nWidth;
	m_nLength = nLength;
	m_xmvScale = xmf3Scale;

	CHeightMap *pHeightMap = (CHeightMap *)pContext;
	int cxHeightMap = pHeightMap->GetHeightMapWidth();
	int czHeightMap = pHeightMap->GetHeightMapLength();
	float gScreenHeight = 0.0f, fMinHeight = +FLT_MAX, fMaxHeight = -FLT_MAX;
	for (int i = 0, z = zStart; z < (zStart + nLength); z++)
	{
		for (int x = xStart; x < (xStart + nWidth); x++, i++)
		{
			gScreenHeight = OnGetHeight(x, z, pContext);
			m_vPositions[i] = XMFLOAT3((x*m_xmvScale.x), gScreenHeight, (z*m_xmvScale.z));
			pxmf3Normals[i] = pHeightMap->GetHeightMapNormal(x, z);
			pxmf2TexCoords[i] = XMFLOAT2(float(x) / float(cxHeightMap - 1), float(czHeightMap - 1 - z) / float(czHeightMap - 1));
			
			//pxmf2DetailTexCoords[i] = XMFLOAT2(float(x) / float(m_xmvScale.x*0.125f), float(z) / float(m_xmvScale.z*0.125f));
			if (gScreenHeight < fMinHeight) fMinHeight = gScreenHeight;
			if (gScreenHeight > fMaxHeight) fMaxHeight = gScreenHeight;
		}
	}



	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferData.pSysMem = pxmf3Normals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dNormalBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * m_nVertices;
	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);

	//d3dBufferData.pSysMem = pxmf2DetailTexCoords;
	//pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dDetailTexCoordBuffer);

	if (pxmf3Normals) delete[] pxmf3Normals;
	if (pxmf2TexCoords) delete[] pxmf2TexCoords;
	//if (pxmf2DetailTexCoords) delete[] pxmf2DetailTexCoords;

	//정점은 위치 벡터, 법선 벡터, 텍스쳐 좌표, 디테일 텍스쳐 좌표를 갖는다.
	ID3D11Buffer *pd3dBuffers[3] = { m_pd3dPositionBuffer, m_pd3dNormalBuffer, m_pd3dTexCoordBuffer };
	UINT pnBufferStrides[3] = { sizeof(XMFLOAT3), sizeof(XMFLOAT3), sizeof(XMFLOAT2) };
	UINT pnBufferOffsets[3] = {  0, 0, 0 };
	AssembleToVertexBuffer(3, pd3dBuffers, pnBufferStrides, pnBufferOffsets);
	
	/*격자는 사각형들의 집합이고 사각형은 두 개의 삼각형으로 구성되므로 격자는 <그림 18>과 같이 삼각형들의 집합이라고 할 수 있다. 격자를 표현하기 위하여 격자의 삼각형들을 인덱스로 표현해야 한다. 삼각형들은 사각형의 줄 단위로 아래에서 위쪽 방향으로(z-축) 나열한다. 첫 번째 사각형 줄의 삼각형들은 왼쪽에서 오른쪽으로(x-축) 나열한다. 두 번째 줄의 삼각형들은 오른쪽에서 왼쪽 방향으로 나열한다. 즉, 사각형의 줄이 바뀔 때마다 나열 순서가 바뀌도록 한다. <그림 18>의 격자에 대하여 삼각형 스트립을 사용하여 삼각형들을 표현하기 위한 인덱스의 나열은 다음과 같이 격자의 m번째 줄과 (m+1)번째 줄의 정점 번호를 사각형의 나열 방향에 따라 번갈아 아래, 위, 아래, 위, ... 순서로 나열하면 된다.
	0, 6, 1, 7, 2, 8, 3, 9, 4, 10, 5, 11, 11, 17, 10, 16, 9, 15, 8, 14, 7, 13, 6, 12
	이렇게 인덱스를 나열하면 삼각형 스트립을 사용할 것이므로 실제 그려지는 삼각형들의 인덱스는 다음과 같다.
	(0, 6, 1), (1, 6, 7), (1, 7, 2), (2, 7, 8), (2, 8, 3), (3, 8, 9), ...
	그러나 이러한 인덱스를 사용하면 첫 번째 줄을 제외하고 삼각형들이 제대로 그려지지 않는다. 왜냐하면 삼각형 스트립에서는 마지막 2개의 정점과 새로운 하나의 정점을 사용하여 새로운 삼각형을 그린다. 그리고 홀수 번째 삼각형의 정점 나열 순서(와인딩 순서)는 시계방향이고 짝수 번째 삼각형의 와인딩 순서는 반시계방향이어야 한다. 격자의 사각형이 한 줄에서 몇 개가 있던지 상관없이 한 줄의 마지막 삼각형은 짝수 번째 삼각형이고 와인딩 순서는 반시계 방향이다. 왜냐하면 사각형은 두 개의 삼각형으로 나누어지기 때문이다. 첫 번째 줄에서 두 번째 줄의 인덱스 나열과 실제 그려지는 삼각형들의 인덱스를 살펴보자.
	..., 4, 10, 5, 11, 11, 17, 10, 16, 9, 15, 8, 14, 7, 13, 6, 12, ...
	..., (4, 10, 5), (5, 10, 11), (5, 11, 11), (11, 11, 17), (11, 17, 10), ...
	삼각형 (5, 10, 11)은 첫 번째 줄의 마지막 삼각형이고 짝수 번째이다. 삼각형 (11, 17, 10)은 두 번째 줄의 첫 번째 삼각형이고 홀수 번째이다. 홀수 번째이므로 와인딩 순서가 시계방향이어야 하는데 실제 와인딩 순서는 반시계방향이므로 그려지지 않을 것이다. 당연히 다음 삼각형도 와인딩 순서가 맞지 않으므로 그려지지 않을 것이다. 삼각형 (11, 17, 10)의 와인딩 순서가 반시계방향이므로 그려지도록 하려면 이 삼각형이 짝수 번째 삼각형이 되도록 해야 한다. 이를 위해서 줄이 바뀔 때마다 마지막 정점의 인덱스를 추가하도록 하자. 그러면 줄이 바뀐 첫 번째 삼각형은 짝수 번째 삼각형이 된다. 다음의 예에서는 11이 추가된 마지막 정점의 인덱스이다. 이렇게 하면 삼각형을 구성할 수 없어서 그려지지 않는 삼각형이 각 줄마다 3개씩 생기게 된다.
	..., 4, 10, 5, 11, 11, 11, 17, 10, 16, 9, 15, 8, 14, 7, 13, 6, 12, ...
	..., (5, 10, 11), (5, 11, 11), (11, 11, 11), (11, 11, 17), (11, 17, 10), ...
	세 개의 삼각형 (5, 11, 11), (11, 11, 11), (11, 11, 17)은 삼각형을 구성할 수 없으므로 실제로 그려지지 않는다.
	이렇게 인덱스를 나열하면 인덱스 버퍼는 ((nWidth*2)*(nLength-1))+((nLength-1)-1)개의 인덱스를 갖는다. 사각형 줄의 개수는 (nLength-1)이고 한 줄에서 (nWidth*2)개의 인덱스를 갖는다. 그리고 줄이 바뀔 때마다 인덱스를 하나 추가하므로 (nLength-1)-1개의 인덱스가 추가로 필요하다.*/
	m_nIndices = ((nWidth * 2)*(nLength - 1)) + ((nLength - 1) - 1);
	m_pnIndices = new UINT[m_nIndices];
	for (int j = 0, z = 0; z < nLength - 1; z++)
	{
		if ((z % 2) == 0)
		{
			//홀수 번째 줄이므로(z = 0, 2, 4, ...) 인덱스의 나열 순서는 왼쪽에서 오른쪽 방향이다.
			for (int x = 0; x < nWidth; x++)
			{
				//첫 번째 줄을 제외하고 줄이 바뀔 때마다(x == 0) 첫 번째 인덱스를 추가한다.
				if ((x == 0) && (z > 0)) m_pnIndices[j++] = (UINT)(x + (z * nWidth));
				//아래, 위의 순서로 인덱스를 추가한다.
				m_pnIndices[j++] = (UINT)(x + (z * nWidth));
				m_pnIndices[j++] = (UINT)((x + (z * nWidth)) + nWidth);
			}
		}
		else
		{
			//짝수 번째 줄이므로(z = 1, 3, 5, ...) 인덱스의 나열 순서는 오른쪽에서 왼쪽 방향이다.
			for (int x = nWidth - 1; x >= 0; x--)
			{
				//줄이 바뀔 때마다(x == (nWidth-1)) 첫 번째 인덱스를 추가한다.
				if (x == (nWidth - 1)) m_pnIndices[j++] = (UINT)(x + (z * nWidth));
				//아래, 위의 순서로 인덱스를 추가한다.
				m_pnIndices[j++] = (UINT)(x + (z * nWidth));
				m_pnIndices[j++] = (UINT)((x + (z * nWidth)) + nWidth);
			}
		}
	}

	/*D3D11_RASTERIZER_DESC d3dRasterizerDesc;
	d3dRasterizerDesc.AntialiasedLineEnable = false;
	d3dRasterizerDesc.CullMode = D3D11_CULL_BACK;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = true;
	d3dRasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
	d3dRasterizerDesc.FrontCounterClockwise = false;
	d3dRasterizerDesc.MultisampleEnable = false;
	d3dRasterizerDesc.ScissorEnable = false;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;

	pd3dDevice->CreateRasterizerState(&d3dRasterizerDesc, &m_pd3dRasterizerState);*/

	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(UINT) * m_nIndices;
	d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pnIndices;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(xStart*m_xmvScale.x, fMinHeight, zStart*m_xmvScale.z);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3((xStart + nWidth)*m_xmvScale.x, fMaxHeight, (zStart + nLength)*m_xmvScale.z);
	
	//cout << "vertex : " << m_nVertices << endl;
	//m_bcBoundingCube.Print();
	//cout << endl;

}


CHeightMapGridMesh::~CHeightMapGridMesh()
{
}

float CHeightMapGridMesh::OnGetHeight(int x, int z, void *pContext)
{
	//높이 맵 객체의 높이 맵 이미지의 픽셀 값을 지형의 높이로 반환한다. 
	CHeightMap *pHeightMap = (CHeightMap *)pContext;
	BYTE *pHeightMapImage = pHeightMap->GetHeightMapImage();
	XMFLOAT3 xmf3Scale = pHeightMap->GetScale();
	int cxTerrain = pHeightMap->GetHeightMapWidth();
	float gScreenHeight = pHeightMapImage[x + (z*cxTerrain)] * xmf3Scale.y;
	return(gScreenHeight);
}



CMeshIlluminated::CMeshIlluminated(ID3D11Device *pd3dDevice) : CMesh(pd3dDevice)
{
	m_pd3dNormalBuffer = nullptr;
}

CMeshIlluminated::~CMeshIlluminated()
{
	if (m_pd3dNormalBuffer) m_pd3dNormalBuffer->Release();
}
void CMeshIlluminated::CalculateVertexNormal(XMFLOAT3 *pxmf3Normals)
{
	switch (m_d3dPrimitiveTopology)
	{
		/*프리미티브가 삼각형 리스트일 때 인덱스 버퍼가 있는 경우와 없는 경우를 구분하여 정점의 법선 벡터를 계산한다. 인덱스 버퍼를 사용하지 않는 경우 각 정점의 법선 벡터는 그 정점이 포함된 삼각형의 법선 벡터로 계산한다. 인덱스 버퍼를 사용하는 경우 각 정점의 법선 벡터는 그 정점이 포함된 삼각형들의 법선 벡터의 평균으로(더하여) 계산한다.*/
	case D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST:
		if (m_pnIndices)
			SetAverageVertexNormal(pxmf3Normals, (m_nIndices / 3), 3, false);
		else
			SetTriAngleListVertexNormal(pxmf3Normals);
		break;
		/*프리미티브가 삼각형 스트립일 때 각 정점의 법선 벡터는 그 정점이 포함된 삼각형들의 법선 벡터의 평균으로(더하여) 계산한다.*/
	case D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP:
		SetAverageVertexNormal(pxmf3Normals, (m_nIndices) ? (m_nIndices - 2) : (m_nVertices - 2), 1, true);
		break;
	default:
		break;
	}
}
void CMeshIlluminated::SetTriAngleListVertexNormal(XMFLOAT3 *pxmf3Normals)
{
	/*삼각형(프리미티브)의 개수를 구하고 각 삼각형의 법선 벡터를 계산하고 삼각형을 구성하는 각 정점의 법선 벡터로 지정한다.*/
	int nPrimitives = m_nVertices / 3;
	for (int i = 0; i < nPrimitives; i++)
	{
		XMFLOAT3 xmf3Normal = CalculateTriAngleNormal((i * 3 + 0), (i * 3 + 1), (i * 3 + 2));
		pxmf3Normals[i * 3 + 0] = pxmf3Normals[i * 3 + 1] = pxmf3Normals[i * 3 + 2] = xmf3Normal;
	}
}
XMFLOAT3 CMeshIlluminated::CalculateTriAngleNormal(UINT nIndex0, UINT nIndex1, UINT nIndex2)
{
	XMFLOAT3 xmf3Normal;
	XMFLOAT3 xmf3P0 = m_vPositions[nIndex0];
	XMFLOAT3 xmf3P1 = m_vPositions[nIndex1];
	XMFLOAT3 xmf3P2 = m_vPositions[nIndex2];
	XMFLOAT3 xmf3Edge1;
	//XMStoreFloat3(&d3dxvEdge1, XMLoadFloat3(&d3dxvP1) - XMLoadFloat3(&d3dxvP0));
	XMFLOAT3 xmf3Edge2;
	//XMStoreFloat3(&d3dxvEdge2, XMLoadFloat3(&d3dxvP2) - XMLoadFloat3(&d3dxvP0));

	XMStoreFloat3(&xmf3Normal, XMVector3Normalize(XMVector3Cross(XMLoadFloat3(&xmf3P1) - XMLoadFloat3(&xmf3P0)
		                                                        , XMLoadFloat3(&xmf3P2) - XMLoadFloat3(&xmf3P0))));
	
	//D3DXVec3Normalize(&xmf3Normal, &xmf3Normal);
	return(xmf3Normal);
}
void CMeshIlluminated::SetAverageVertexNormal(XMFLOAT3 *pxmf3Normals, int nPrimitives, int nOffset, bool bStrip)
{
	for (int j = 0; j < m_nVertices; j++)
	{
		//XMFLOAT3 xmf3SumOfNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
		XMVECTOR xmf3SumOfNormal = XMVectorZero();
		for (int i = 0; i < nPrimitives; i++)
		{
			UINT nIndex0 = (bStrip) ? (((i % 2) == 0) ? (i*nOffset + 0) : (i*nOffset + 1)) : (i*nOffset + 0);
			if (m_pnIndices) nIndex0 = m_pnIndices[nIndex0];
			UINT nIndex1 = (bStrip) ? (((i % 2) == 0) ? (i*nOffset + 1) : (i*nOffset + 0)) : (i*nOffset + 1);
			if (m_pnIndices) nIndex1 = m_pnIndices[nIndex1];
			UINT nIndex2 = (m_pnIndices) ? m_pnIndices[i*nOffset + 2] : (i*nOffset + 2);
			if ((nIndex0 == j) || (nIndex1 == j) || (nIndex2 == j)) 
				xmf3SumOfNormal += XMLoadFloat3(&CalculateTriAngleNormal(nIndex0, nIndex1, nIndex2));
		
		}
		xmf3SumOfNormal = XMVector3Normalize(xmf3SumOfNormal);
		XMStoreFloat3(&pxmf3Normals[j],xmf3SumOfNormal);
	}
}
CCubeMeshIlluminated::CCubeMeshIlluminated(ID3D11Device *pd3dDevice, float gScreenWidth, float gScreenHeight, float fDepth) : CMeshIlluminated(pd3dDevice)
{
	m_nVertices = 8;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float fx = gScreenWidth*0.5f, fy = gScreenHeight*0.5f, fz = fDepth*0.5f;

	m_vPositions.resize(m_nVertices);

	m_vPositions[0] = XMFLOAT3(-fx, +fy, -fz);
	m_vPositions[1] = XMFLOAT3(+fx, +fy, -fz);
	m_vPositions[2] = XMFLOAT3(+fx, +fy, +fz);
	m_vPositions[3] = XMFLOAT3(-fx, +fy, +fz);
	m_vPositions[4] = XMFLOAT3(-fx, -fy, -fz);
	m_vPositions[5] = XMFLOAT3(+fx, -fy, -fz);
	m_vPositions[6] = XMFLOAT3(+fx, -fy, +fz);
	m_vPositions[7] = XMFLOAT3(-fx, -fy, +fz);

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	m_nIndices = 36;
	m_pnIndices = new UINT[m_nIndices];

	m_pnIndices[0] = 3; m_pnIndices[1] = 1; m_pnIndices[2] = 0;
	m_pnIndices[3] = 2; m_pnIndices[4] = 1; m_pnIndices[5] = 3;
	m_pnIndices[6] = 0; m_pnIndices[7] = 5; m_pnIndices[8] = 4;
	m_pnIndices[9] = 1; m_pnIndices[10] = 5; m_pnIndices[11] = 0;
	m_pnIndices[12] = 3; m_pnIndices[13] = 4; m_pnIndices[14] = 7;
	m_pnIndices[15] = 0; m_pnIndices[16] = 4; m_pnIndices[17] = 3;
	m_pnIndices[18] = 1; m_pnIndices[19] = 6; m_pnIndices[20] = 5;
	m_pnIndices[21] = 2; m_pnIndices[22] = 6; m_pnIndices[23] = 1;
	m_pnIndices[24] = 2; m_pnIndices[25] = 7; m_pnIndices[26] = 6;
	m_pnIndices[27] = 3; m_pnIndices[28] = 7; m_pnIndices[29] = 2;
	m_pnIndices[30] = 6; m_pnIndices[31] = 4; m_pnIndices[32] = 5;
	m_pnIndices[33] = 7; m_pnIndices[34] = 4; m_pnIndices[35] = 6;

	XMFLOAT3 pxmf3Normals[8];
	CalculateVertexNormal(pxmf3Normals);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferData.pSysMem = pxmf3Normals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dNormalBuffer);

	ID3D11Buffer *pd3dBuffers[2] = { m_pd3dPositionBuffer, m_pd3dNormalBuffer };
	UINT pnBufferStrides[2] = { sizeof(XMFLOAT3), sizeof(XMFLOAT3) };
	UINT pnBufferOffsets[2] = { 0, 0 };
	AssembleToVertexBuffer(2, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(UINT)* m_nIndices;
	d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pnIndices;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fx, -fy, -fz);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fx, +fy, +fz);
}

CCubeMeshIlluminated::~CCubeMeshIlluminated()
{
}
#define _WITH_INDEX_BUFFER

CSphereMeshIlluminated::CSphereMeshIlluminated(ID3D11Device *pd3dDevice, float fRadius, int nSlices, int nStacks) : CMeshIlluminated(pd3dDevice)
{
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float fDeltaPhi = float(D3DX_PI / nStacks);
	float fDeltaTheta = float((2.0f * D3DX_PI) / nSlices);
	int k = 0;

#ifdef _WITH_INDEX_BUFFER
	m_nVertices = 2 + (nSlices * (nStacks - 1));
	m_vPositions.resize(m_nVertices);
	XMFLOAT3 *pxmf3Normals = new XMFLOAT3[m_nVertices];

	m_vPositions[k] = XMFLOAT3(0.0f, +fRadius, 0.0f);
	//D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;

	XMStoreFloat3(&pxmf3Normals[k], XMVector3Normalize(XMLoadFloat3(&m_vPositions[k]))); k++;
	float theta_i, phi_j;
	for (int j = 1; j < nStacks; j++)
	{
		phi_j = fDeltaPhi * j;
		for (int i = 0; i < nSlices; i++)
		{
			theta_i = fDeltaTheta * i;
			m_vPositions[k] = XMFLOAT3(fRadius*sinf(phi_j)*cosf(theta_i), fRadius*cosf(phi_j), fRadius*sinf(phi_j)*sinf(theta_i));
			XMStoreFloat3(&pxmf3Normals[k], XMVector3Normalize(XMLoadFloat3(&m_vPositions[k]))); k++;
			//D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
		}
	}
	m_vPositions[k] = XMFLOAT3(0.0f, -fRadius, 0.0f);
	XMStoreFloat3(&pxmf3Normals[k], XMVector3Normalize(XMLoadFloat3(&m_vPositions[k]))); k++;
	//D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferData.pSysMem = pxmf3Normals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dNormalBuffer);

	if (pxmf3Normals) delete[] pxmf3Normals;

	ID3D11Buffer *pd3dBuffers[2] = { m_pd3dPositionBuffer, m_pd3dNormalBuffer };
	UINT pnBufferStrides[2] = { sizeof(XMFLOAT3), sizeof(XMFLOAT3) };
	UINT pnBufferOffsets[2] = { 0, 0 };
	AssembleToVertexBuffer(2, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	k = 0;
	m_nIndices = (nSlices * 3) * 2 + (nSlices * (nStacks - 2) * 3 * 2);
	m_pnIndices = new UINT[m_nIndices];
	for (int i = 0; i < nSlices; i++)
	{
		m_pnIndices[k++] = 0;
		m_pnIndices[k++] = 1 + ((i + 1) % nSlices);
		m_pnIndices[k++] = 1 + i;
	}
	for (int j = 0; j < nStacks - 2; j++)
	{
		for (int i = 0; i < nSlices; i++)
		{
			m_pnIndices[k++] = 1 + (i + (j * nSlices));
			m_pnIndices[k++] = 1 + (((i + 1) % nSlices) + (j * nSlices));
			m_pnIndices[k++] = 1 + (i + ((j + 1) * nSlices));
			m_pnIndices[k++] = 1 + (i + ((j + 1) * nSlices));
			m_pnIndices[k++] = 1 + (((i + 1) % nSlices) + (j * nSlices));
			m_pnIndices[k++] = 1 + (((i + 1) % nSlices) + ((j + 1) * nSlices));
		}
	}
	for (int i = 0; i < nSlices; i++)
	{
		m_pnIndices[k++] = (m_nVertices - 1);
		m_pnIndices[k++] = ((m_nVertices - 1) - nSlices) + i;
		m_pnIndices[k++] = ((m_nVertices - 1) - nSlices) + ((i + 1) % nSlices);
	}

	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(UINT)* m_nIndices;
	d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pnIndices;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);
#else
	m_nVertices = (nSlices * 3) * 2 + (nSlices * (nStacks - 2) * 3 * 2);
	m_vPositions = new XMFLOAT3[m_nVertices];
	XMFLOAT3 *pxmf3Normals = new XMFLOAT3[m_nVertices];

	float theta_i, theta_ii, phi_j = 0.0f, phi_jj = fDeltaPhi;
	for (int i = 0; i < nSlices; i++)
	{
		theta_i = fDeltaTheta * i;
		theta_ii = fDeltaTheta * (i + 1);
		m_vPositions[k] = XMFLOAT3(0.0f, +fRadius, 0.0f);
		D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
		m_vPositions[k] = XMFLOAT3(fRadius*cosf(theta_i)*sinf(phi_jj), fRadius*cosf(phi_jj), fRadius*sinf(theta_i)*sinf(phi_jj));
		D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
		m_vPositions[k] = XMFLOAT3(fRadius*cosf(theta_ii)*sinf(phi_jj), fRadius*cosf(phi_jj), fRadius*sinf(theta_ii)*sinf(phi_jj));
		D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
	}

	for (int j = 1; j < nStacks - 1; j++)
	{
		phi_j = fDeltaPhi * j;
		phi_jj = fDeltaPhi * (j + 1);
		for (int i = 0; i < nSlices; i++)
		{
			theta_i = fDeltaTheta * i;
			theta_ii = fDeltaTheta * (i + 1);
			m_vPositions[k] = XMFLOAT3(fRadius*cosf(theta_i)*sinf(phi_j), fRadius*cosf(phi_j), fRadius*sinf(theta_i)*sinf(phi_j));
			D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
			m_vPositions[k] = XMFLOAT3(fRadius*cosf(theta_i)*sinf(phi_jj), fRadius*cosf(phi_jj), fRadius*sinf(theta_i)*sinf(phi_jj));
			D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
			m_vPositions[k] = XMFLOAT3(fRadius*cosf(theta_ii)*sinf(phi_j), fRadius*cosf(phi_j), fRadius*sinf(theta_ii)*sinf(phi_j));
			D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
			m_vPositions[k] = XMFLOAT3(fRadius*cosf(theta_i)*sinf(phi_jj), fRadius*cosf(phi_jj), fRadius*sinf(theta_i)*sinf(phi_jj));
			D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
			m_vPositions[k] = XMFLOAT3(fRadius*cosf(theta_ii)*sinf(phi_jj), fRadius*cosf(phi_jj), fRadius*sinf(theta_ii)*sinf(phi_jj));
			D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
			m_vPositions[k] = XMFLOAT3(fRadius*cosf(theta_ii)*sinf(phi_j), fRadius*cosf(phi_j), fRadius*sinf(theta_ii)*sinf(phi_j));
			D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
		}
	}
	phi_j = fDeltaPhi * (nStacks - 1);
	for (int i = 0; i < nSlices; i++)
	{
		theta_i = fDeltaTheta * i;
		theta_ii = fDeltaTheta * (i + 1);
		m_vPositions[k] = XMFLOAT3(0.0f, -fRadius, 0.0f);
		D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
		m_vPositions[k] = XMFLOAT3(fRadius*cosf(theta_ii)*sinf(phi_j), fRadius*cosf(phi_j), fRadius*sinf(theta_ii)*sinf(phi_j));
		D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
		m_vPositions[k] = XMFLOAT3(fRadius*cosf(theta_i)*sinf(phi_j), fRadius*cosf(phi_j), fRadius*sinf(theta_i)*sinf(phi_j));
		D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
	}

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_vPositions;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferData.pSysMem = pxmf3Normals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dNormalBuffer);

	if (pxmf3Normals) delete[] pxmf3Normals;

	ID3D11Buffer *pd3dBuffers[2] = { m_pd3dPositionBuffer, m_pd3dNormalBuffer };
	UINT pnBufferStrides[2] = { sizeof(XMFLOAT3), sizeof(XMFLOAT3) };
	UINT pnBufferOffsets[2] = { 0, 0 };
	AssembleToVertexBuffer(2, pd3dBuffers, pnBufferStrides, pnBufferOffsets);
#endif

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fRadius, -fRadius, -fRadius);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fRadius, +fRadius, +fRadius);
}

CSphereMeshIlluminated::~CSphereMeshIlluminated()
{
}



CMeshTextured::CMeshTextured(ID3D11Device *pd3dDevice) : CMesh(pd3dDevice)
{
	m_pd3dTexCoordBuffer = nullptr;
}

CMeshTextured::~CMeshTextured()
{
	if (m_pd3dTexCoordBuffer) m_pd3dTexCoordBuffer->Release();
}
void CMeshTexturedIlluminated::CalculateVectors(XMFLOAT2* pxmf2Tex, XMFLOAT3* pxmf3Normals, XMFLOAT3* pxmf3Binormals, XMFLOAT3* pxmf3Tangents)
{
	int faceCount, i, nIndex;
	TempVertexType vertex1, vertex2, vertex3;
	VectorType  tangent, binormal, normal;


	// Calculate the number of faces in the model.
	faceCount = m_nVertices / 3;
	
	// Initialize the nIndex to the model data.
	nIndex = 0;

	// Go through all the faces and calculate the the tangent, binormal, and normal vectors.
	for (i = 0; i<faceCount; i++)
	{
		// Get the three vertices for this face from the model.
		vertex1.x = m_vPositions[nIndex].x;
		vertex1.y = m_vPositions[nIndex].y;
		vertex1.z = m_vPositions[nIndex].z;
		vertex1.tu = pxmf2Tex[nIndex].x;
		vertex1.tv = pxmf2Tex[nIndex].y;

		vertex1.nx = pxmf3Normals[nIndex].x;
		vertex1.ny = pxmf3Normals[nIndex].y;
		vertex1.nz = pxmf3Normals[nIndex].z;
		nIndex++;

		vertex2.x = m_vPositions[nIndex].x;
		vertex2.y = m_vPositions[nIndex].y;
		vertex2.z = m_vPositions[nIndex].z;
		vertex2.tu = pxmf2Tex[nIndex].x;
		vertex2.tv = pxmf2Tex[nIndex].y;
		vertex2.nx = pxmf3Normals[nIndex].x;
		vertex2.ny = pxmf3Normals[nIndex].y;
		vertex2.nz = pxmf3Normals[nIndex].z;
		nIndex++;

		vertex3.x = m_vPositions[nIndex].x;
		vertex3.y = m_vPositions[nIndex].y;
		vertex3.z = m_vPositions[nIndex].z;
		vertex3.tu = pxmf2Tex[nIndex].x;
		vertex3.tv = pxmf2Tex[nIndex].y;
		vertex3.nx = pxmf3Normals[nIndex].x;
		vertex3.ny = pxmf3Normals[nIndex].y;
		vertex3.nz = pxmf3Normals[nIndex].z;
		nIndex++;

		// Calculate the tangent and binormal of that face.
		CalculateTangentBinormal(vertex1, vertex2, vertex3, tangent, binormal);

		// Calculate the new normal using the tangent and binormal.
		CalculateNormal(tangent, binormal, normal);

		// Store the normal, tangent, and binormal for this face back in the model structure.
		pxmf3Normals[nIndex - 1].x = normal.x;
		pxmf3Normals[nIndex - 1].y = normal.y;
		pxmf3Normals[nIndex - 1].z = normal.z;

		pxmf3Tangents[nIndex - 1].x = tangent.x;
		pxmf3Tangents[nIndex - 1].y = tangent.y;
		pxmf3Tangents[nIndex - 1].z = tangent.z;
		pxmf3Binormals[nIndex - 1].x = binormal.x;
		pxmf3Binormals[nIndex - 1].y = binormal.y;
		pxmf3Binormals[nIndex - 1].z = binormal.z;

		pxmf3Normals[nIndex - 2].x = normal.x;
		pxmf3Normals[nIndex - 2].y = normal.y;
		pxmf3Normals[nIndex - 2].z = normal.z;
		pxmf3Tangents[nIndex - 2].x = tangent.x;
		pxmf3Tangents[nIndex - 2].y = tangent.y;
		pxmf3Tangents[nIndex - 2].z = tangent.z;
		pxmf3Binormals[nIndex - 2].x = binormal.x;
		pxmf3Binormals[nIndex - 2].y = binormal.y;
		pxmf3Binormals[nIndex - 2].z = binormal.z;

		pxmf3Normals[nIndex - 3].x = normal.x;
		pxmf3Normals[nIndex - 3].y = normal.y;
		pxmf3Normals[nIndex - 3].z = normal.z;
		pxmf3Tangents[nIndex - 3].x = tangent.x;
		pxmf3Tangents[nIndex - 3].y = tangent.y;
		pxmf3Tangents[nIndex - 3].z = tangent.z;
		pxmf3Binormals[nIndex - 3].x = binormal.x;
		pxmf3Binormals[nIndex - 3].y = binormal.y;
		pxmf3Binormals[nIndex - 3].z = binormal.z;
	}

}
void CMeshTexturedIlluminated::CalculateNormal(VectorType tangent, VectorType binormal, VectorType& normal)
{
	float length;


	// Calculate the cross product of the tangent and binormal which will give the normal vector.
	normal.x = (tangent.y * binormal.z) - (tangent.z * binormal.y);
	normal.y = (tangent.z * binormal.x) - (tangent.x * binormal.z);
	normal.z = (tangent.x * binormal.y) - (tangent.y * binormal.x);

	// Calculate the length of the normal.
	length = sqrt((normal.x * normal.x) + (normal.y * normal.y) + (normal.z * normal.z));

	// Normalize the normal.
	normal.x = normal.x / length;
	normal.y = normal.y / length;
	normal.z = normal.z / length;

	return;
}
void CMeshTexturedIlluminated::CalculateTangentBinormal(TempVertexType vertex1, TempVertexType vertex2, TempVertexType vertex3, VectorType& tangent, VectorType& binormal)
{
	float vector1[3], vector2[3]; float tuVector[2], tvVector[2]; float den; float length; 
	// Calculate the two vectors for this face. 
	vector1[0] = vertex2.x - vertex1.x; 
	vector1[1] = vertex2.y - vertex1.y; 
	vector1[2] = vertex2.z - vertex1.z; 
	vector2[0] = vertex3.x - vertex1.x; 
	vector2[1] = vertex3.y - vertex1.y; 
	vector2[2] = vertex3.z - vertex1.z;
	// Calculate the tu and tv texture space vectors. 
	tuVector[0] = vertex2.tu - vertex1.tu; 
	tvVector[0] = vertex2.tv - vertex1.tv; 
	tuVector[1] = vertex3.tu - vertex1.tu; 
	tvVector[1] = vertex3.tv - vertex1.tv;
	// Calculate the denominator of the tangent/binormal equation. 
	den = 1.0f / (tuVector[0] * tvVector[1] - tuVector[1] * tvVector[0]); 
	// Calculate the cross products and multiply by the coefficient to get the tangent and binormal. 
	tangent.x = (tvVector[1] * vector1[0] - tvVector[0] * vector2[0]) * den;
	tangent.y = (tvVector[1] * vector1[1] - tvVector[0] * vector2[1]) * den;
	tangent.z = (tvVector[1] * vector1[2] - tvVector[0] * vector2[2]) * den; 
	binormal.x = (tuVector[0] * vector2[0] - tuVector[1] * vector1[0]) * den; 
	binormal.y = (tuVector[0] * vector2[1] - tuVector[1] * vector1[1]) * den; 
	binormal.z = (tuVector[0] * vector2[2] - tuVector[1] * vector1[2]) * den; 
	// Calculate the length of this normal. 
	length = sqrt((tangent.x * tangent.x) + (tangent.y * tangent.y) + (tangent.z * tangent.z)); 
	// Normalize the normal and then store it 
	tangent.x = tangent.x / length; tangent.y = tangent.y / length; tangent.z = tangent.z / length; 
	// Calculate the length of this normal. 
	length = sqrt((binormal.x * binormal.x) + (binormal.y * binormal.y) + (binormal.z * binormal.z)); 
	// Normalize the normal and then store it 
	binormal.x = binormal.x / length; 
	binormal.y = binormal.y / length;
	binormal.z = binormal.z / length;

	return;
}


CCubeMeshTextured::CCubeMeshTextured(ID3D11Device *pd3dDevice, float gScreenWidth, float gScreenHeight, float fDepth) : CMeshTextured(pd3dDevice)
{
	m_nVertices = 36;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float fx = gScreenWidth*0.5f, fy = gScreenHeight*0.5f, fz = fDepth*0.5f;

	m_vPositions.resize(m_nVertices);
	XMFLOAT2 pxmf2TexCoords[36];
	int i = 0;

	//직육면체의 각 면(삼각형 2개)에 하나의 텍스쳐 이미지 전체가 맵핑되도록 텍스쳐 좌표를 설정한다.
	//
	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);


	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);

	ID3D11Buffer *pd3dBuffers[2] = { m_pd3dPositionBuffer, m_pd3dTexCoordBuffer };
	UINT pnBufferStrides[2] = { sizeof(XMFLOAT3), sizeof(XMFLOAT2) };
	UINT pnBufferOffsets[2] = { 0, 0 };
	AssembleToVertexBuffer(2, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	/*직육면체의 각 면에 텍스쳐를 맵핑하려면 인덱스를 사용할 수 없으므로 
	   인덱스 버퍼는 생성하지 않는다.*/

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fx, -fy, -fz);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fx, +fy, +fz);
}

CCubeMeshTextured::~CCubeMeshTextured()
{
}

CSphereMeshTextured::CSphereMeshTextured(ID3D11Device *pd3dDevice, float fRadius, int nSlices, int nStacks) : CMeshTextured(pd3dDevice)
{
	m_nVertices = (nSlices * nStacks) * 3 * 2;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	m_vPositions.resize(m_nVertices);
	XMFLOAT2 *xmf2TexCoords = new XMFLOAT2[m_nVertices];

	float theta_i, theta_ii, phi_j, phi_jj, fRadius_j, fRadius_jj, y_j, y_jj;
	for (int j = 0, k = 0; j < nStacks; j++)
	{
		phi_j = float(D3DX_PI / nStacks) * j;
		phi_jj = float(D3DX_PI / nStacks) * (j + 1);
		fRadius_j = fRadius * sinf(phi_j);
		fRadius_jj = fRadius * sinf(phi_jj);
		y_j = fRadius * cosf(phi_j);
		y_jj = fRadius * cosf(phi_jj);
		for (int i = 0; i < nSlices; i++)
		{
			theta_i = float(2 * D3DX_PI / nSlices) * i;
			theta_ii = float(2 * D3DX_PI / nSlices) * (i + 1);
			m_vPositions[k] = XMFLOAT3(fRadius_j*cosf(theta_i), y_j, fRadius_j*sinf(theta_i));
			xmf2TexCoords[k++] = XMFLOAT2(float(i) / float(nSlices), float(j) / float(nStacks));
			m_vPositions[k] = XMFLOAT3(fRadius_jj*cosf(theta_i), y_jj, fRadius_jj*sinf(theta_i));
			xmf2TexCoords[k++] = XMFLOAT2(float(i) / float(nSlices), float(j + 1) / float(nStacks));
			m_vPositions[k] = XMFLOAT3(fRadius_j*cosf(theta_ii), y_j, fRadius_j*sinf(theta_ii));
			xmf2TexCoords[k++] = XMFLOAT2(float(i + 1) / float(nSlices), float(j) / float(nStacks));
			m_vPositions[k] = XMFLOAT3(fRadius_jj*cosf(theta_i), y_jj, fRadius_jj*sinf(theta_i));
			xmf2TexCoords[k++] = XMFLOAT2(float(i) / float(nSlices), float(j + 1) / float(nStacks));
			m_vPositions[k] = XMFLOAT3(fRadius_jj*cosf(theta_ii), y_jj, fRadius_jj*sinf(theta_ii));
			xmf2TexCoords[k++] = XMFLOAT2(float(i + 1) / float(nSlices), float(j + 1) / float(nStacks));
			m_vPositions[k] = XMFLOAT3(fRadius_j*cosf(theta_ii), y_j, fRadius_j*sinf(theta_ii));
			xmf2TexCoords[k++] = XMFLOAT2(float(i + 1) / float(nSlices), float(j) / float(nStacks));
		}
	}

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * m_nVertices;
	d3dBufferData.pSysMem = xmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);

	delete[] xmf2TexCoords;

	ID3D11Buffer *pd3dBuffers[2] = { m_pd3dPositionBuffer, m_pd3dTexCoordBuffer };
	UINT pnBufferStrides[2] = { sizeof(XMFLOAT3), sizeof(XMFLOAT2) };
	UINT pnBufferOffsets[2] = { 0, 0 };
	AssembleToVertexBuffer(2, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fRadius, -fRadius, -fRadius);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fRadius, +fRadius, +fRadius);
}

CSphereMeshTextured::~CSphereMeshTextured()
{
}


CMeshDetailTextured::CMeshDetailTextured(ID3D11Device *pd3dDevice) : CMeshTextured(pd3dDevice)
{
	m_pd3dDetailTexCoordBuffer = nullptr;
}

CMeshDetailTextured::~CMeshDetailTextured()
{
	if (m_pd3dDetailTexCoordBuffer) m_pd3dDetailTexCoordBuffer->Release();
}

CSkyBoxMesh::CSkyBoxMesh(ID3D11Device *pd3dDevice, float gScreenWidth, float gScreenHeight, float fDepth) : CMeshTextured(pd3dDevice)
{
	//스카이 박스는 6개의 면(사각형), 사각형은 정점 4개, 그러므로 24개의 정점이 필요하다.
	m_nVertices = 24;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;

	m_vPositions.resize(m_nVertices);
	XMFLOAT2 *pxmf2TexCoords = new XMFLOAT2[m_nVertices];

	int i = 0;
	float fx = gScreenWidth*0.5f, fy = gScreenHeight*0.5f, fz = fDepth*0.5f;
	// Front Quad 
	m_vPositions[i] = XMFLOAT3(-fx, +fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);
	// Back Quad
	m_vPositions[i] = XMFLOAT3(+fx, +fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(-fx, +fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);
	// Left Quad
	m_vPositions[i] = XMFLOAT3(-fx, +fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(-fx, +fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);
	// Right Quad
	m_vPositions[i] = XMFLOAT3(+fx, +fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);
	// Top Quad
	m_vPositions[i] = XMFLOAT3(-fx, +fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, +fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);
	// Bottom Quad
	m_vPositions[i] = XMFLOAT3(-fx, -fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fx, +fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fx, -fx);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);

	D3D11_BUFFER_DESC d3dBufferDesc;
	::ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	::ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * m_nVertices;
	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);

	delete[] pxmf2TexCoords;

	ID3D11Buffer *pd3dBuffers[2] = { m_pd3dPositionBuffer, m_pd3dTexCoordBuffer };
	UINT pnBufferStrides[2] = { sizeof(XMFLOAT3), sizeof(XMFLOAT2) };
	UINT pnBufferOffsets[2] = { 0, 0 };
	AssembleToVertexBuffer(2, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	//삼각형 스트립으로 사각형 1개를 그리기 위해 인덱스는 4개가 필요하다.
	m_nIndices = 4;
	m_pnIndices = new UINT[m_nIndices];

	m_pnIndices[0] = 0;
	m_pnIndices[1] = 1;
	m_pnIndices[2] = 3;
	m_pnIndices[3] = 2;

	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(UINT) * m_nIndices;
	d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pnIndices;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);

	D3D11_DEPTH_STENCIL_DESC d3dDepthStencilDesc;
	ZeroMemory(&d3dDepthStencilDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	//스카이 박스 사각형들은 깊이 버퍼 알고리즘을 적용하지 않고 깊이 버퍼를 변경하지 않는다.
	d3dDepthStencilDesc.DepthEnable = false;
	d3dDepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	d3dDepthStencilDesc.DepthFunc = D3D11_COMPARISON_NEVER;
	d3dDepthStencilDesc.StencilEnable = false;
	d3dDepthStencilDesc.StencilReadMask = 0xFF;
	d3dDepthStencilDesc.StencilWriteMask = 0xFF;
	d3dDepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	d3dDepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	d3dDepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	d3dDepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	pd3dDevice->CreateDepthStencilState(&d3dDepthStencilDesc, &m_pd3dDepthStencilState);

	ID3D11SamplerState *pd3dSamplerState = nullptr;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;
	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dSamplerState);

	//m_pSkyboxTexture = new CTexture(6, 1, 0, 0);
	//m_pSkyboxTexture->SetSampler(0, pd3dSamplerState);
	//pd3dSamplerState->Release();
	//m_pSkyboxTexture->AddRef();

	//
	////왠지 중요할거 같은부분이다.
	//OnChangeSkyBoxTextures(pd3dDevice, 4);


	//m_pSkyboxTexture = new CTexture(1, 1, 0, 0);

	//m_pSkyboxTexture->AddRef();


	//D3DX11CreateShaderResourceViewFromFile(pd3dDevice, L"Image/environment.dds", nullptr, nullptr, &m_pd3dsrvCubeMap, nullptr);
	//pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &m_pd3dSamplerState);
	//
	//m_pSkyboxTexture->SetTexture(0, m_pd3dsrvCubeMap);
	//m_pSkyboxTexture->SetSampler(0, m_pd3dSamplerState);
	////m_pd3dsrvCubeMap->Release();
	//pd3dSamplerState->Release();
	
}

CSkyBoxMesh::~CSkyBoxMesh()
{
	if (m_pd3dDepthStencilState) m_pd3dDepthStencilState->Release();
	if (m_pSkyboxTexture) m_pSkyboxTexture->Release();
}

void CSkyBoxMesh::OnChangeSkyBoxTextures(ID3D11Device *pd3dDevice, int nIndex)
{
	//6개의 스카이 박스 텍스쳐를 생성하여 CTexture 객체에 연결한다.
	_TCHAR pstrTextureName[80];
	ID3D11ShaderResourceView *pd3dsrvTexture = nullptr;
	_stprintf_s(pstrTextureName, _T("Image/front.jpg"));
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, pstrTextureName, nullptr, nullptr, &pd3dsrvTexture, nullptr);
	m_pSkyboxTexture->SetTexture(0, pd3dsrvTexture);
	pd3dsrvTexture->Release();
	
	_stprintf_s(pstrTextureName, _T("Image/back.jpg"));
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, pstrTextureName, nullptr, nullptr, &pd3dsrvTexture, nullptr);
	m_pSkyboxTexture->SetTexture(1, pd3dsrvTexture);
	pd3dsrvTexture->Release();
	
	_stprintf_s(pstrTextureName, _T("Image/left.jpg"), nIndex, 80);
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, pstrTextureName, nullptr, nullptr, &pd3dsrvTexture, nullptr);
	m_pSkyboxTexture->SetTexture(2, pd3dsrvTexture);
	pd3dsrvTexture->Release();
	
	_stprintf_s(pstrTextureName, _T("Image/right.jpg"), nIndex, 80);
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, pstrTextureName, nullptr, nullptr, &pd3dsrvTexture, nullptr);
	m_pSkyboxTexture->SetTexture(3, pd3dsrvTexture);
	pd3dsrvTexture->Release();
	
	_stprintf_s(pstrTextureName, _T("Image/top.jpg"), nIndex, 80);
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, pstrTextureName, nullptr, nullptr, &pd3dsrvTexture, nullptr);
	m_pSkyboxTexture->SetTexture(4, pd3dsrvTexture);
	pd3dsrvTexture->Release();
	
	_stprintf_s(pstrTextureName, _T("Image/bottom.jpg"), nIndex, 80);
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, pstrTextureName, nullptr, nullptr, &pd3dsrvTexture, nullptr);
	m_pSkyboxTexture->SetTexture(5, pd3dsrvTexture);
	pd3dsrvTexture->Release();
}

void CSkyBoxMesh::Render(ID3D11DeviceContext *pd3dDeviceContext)
{
	pd3dDeviceContext->IASetVertexBuffers(m_nSlot, m_nBuffers, m_ppd3dVertexBuffers, m_pnVertexStrides, m_pnVertexOffsets);
	pd3dDeviceContext->IASetIndexBuffer(m_pd3dIndexBuffer, m_dxgiIndexFormat, m_nIndexOffset);
	pd3dDeviceContext->IASetPrimitiveTopology(m_d3dPrimitiveTopology);

	//스카이 박스를 그리기 위한 샘플러 상태 객체와 깊이 스텐실 상태 객체를 설정한다.
	//pd3dDeviceContext->OMSetDepthStencilState(m_pd3dDepthStencilState, 1);
	//m_pSkyboxTexture->UpdateSamplerShaderVariable(pd3dDeviceContext, 0, 0);

	////스카이 박스의 6개 면(사각형)을 순서대로 그린다.
	//for (int i = 0; i < 6; i++)
	//{
	//	//스카이 박스의 각 면(사각형)을 그릴 때 사용할 텍스쳐를 설정한다.
	//	m_pSkyboxTexture->UpdateTextureShaderVariable(pd3dDeviceContext, i, 0);
	//	pd3dDeviceContext->DrawIndexed(4, 0, i * 4);
	//}
	pd3dDeviceContext->OMSetDepthStencilState(m_pd3dDepthStencilState, 1);
	//pd3dDeviceContext->PSSetSamplers(3, 1, &m_pd3dSamplerState);
	//pd3dDeviceContext->PSSetShaderResources(2, 1, &m_pd3dsrvCubeMap);


	//m_pSkyboxTexture->UpdateShaderVariable(pd3dDeviceContext);


	for (int i = 0; i < 6; i++)
		pd3dDeviceContext->DrawIndexed(4, 0, i * 4);



	pd3dDeviceContext->OMSetDepthStencilState(nullptr, 1);
}




CMeshTexturedIlluminated::CMeshTexturedIlluminated(ID3D11Device *pd3dDevice) : CMeshIlluminated(pd3dDevice)
{
	m_pd3dTexCoordBuffer = nullptr;
}

CMeshTexturedIlluminated::~CMeshTexturedIlluminated()
{
	if (m_pd3dTexCoordBuffer) m_pd3dTexCoordBuffer->Release();
}

CMeshDetailTexturedIlluminated::CMeshDetailTexturedIlluminated(ID3D11Device *pd3dDevice) : CMeshIlluminated(pd3dDevice)
{
	m_pd3dTexCoordBuffer = nullptr;
	m_pd3dDetailTexCoordBuffer = nullptr;
}

CMeshDetailTexturedIlluminated::~CMeshDetailTexturedIlluminated()
{
	if (m_pd3dTexCoordBuffer) m_pd3dTexCoordBuffer->Release();
	if (m_pd3dDetailTexCoordBuffer) m_pd3dDetailTexCoordBuffer->Release();
}

CCubeMeshTexturedIlluminated::CCubeMeshTexturedIlluminated(ID3D11Device *pd3dDevice, float gScreenWidth, float gScreenHeight, float fDepth) : CMeshTexturedIlluminated(pd3dDevice)
{
	m_nVertices = 36;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float fx = gScreenWidth*0.5f, fy = gScreenHeight*0.5f, fz = fDepth*0.5f;

	m_vPositions.resize(m_nVertices);
	//생략된 부분은 LabProject13-2의 CCubeMeshTextured 클래스의 생성자 부분과 동일하다.
	XMFLOAT2 pxmf2TexCoords[36];
	int i = 0;

	//직육면체의 각 면(삼각형 2개)에 하나의 텍스쳐 이미지 전체가 맵핑되도록 텍스쳐 좌표를 설정한다.
	//
	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);


	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(-fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[i] = XMFLOAT3(+fx, -fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);


	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);




	//법선 벡터를 생성하기 위한 다음 코드를 추가한다.
	XMFLOAT3 pxmf3Normals[36], pxmf3Binormals[36], pxmf3Tangents[36];
	CalculateVertexNormal(pxmf3Normals);
	CalculateVectors(pxmf2TexCoords, pxmf3Normals, pxmf3Binormals, pxmf3Tangents);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferData.pSysMem = pxmf3Normals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dNormalBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferData.pSysMem = pxmf3Binormals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dBinormalBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferData.pSysMem = pxmf3Tangents;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTengentBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * m_nVertices;
	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);

	//정점은 위치 벡터, 법선 벡터, 텍스쳐 좌표를 갖는다.
	ID3D11Buffer *pd3dBuffers[5] = { m_pd3dPositionBuffer, m_pd3dTexCoordBuffer, m_pd3dNormalBuffer, m_pd3dTengentBuffer, m_pd3dBinormalBuffer };
	UINT pnBufferStrides[5] = { sizeof(XMFLOAT3),  sizeof(XMFLOAT2), sizeof(XMFLOAT3), sizeof(XMFLOAT3), sizeof(XMFLOAT3) };
	UINT pnBufferOffsets[5] = { 0, 0, 0, 0, 0};
	AssembleToVertexBuffer(5, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fx, -fy, -fz);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fx, +fy, +fz);
}

CCubeMeshTexturedIlluminated::~CCubeMeshTexturedIlluminated()
{
}


CSphereMeshTexturedIlluminated::CSphereMeshTexturedIlluminated(ID3D11Device *pd3dDevice, float fRadius, int nSlices, int nStacks) : CMeshTexturedIlluminated(pd3dDevice)
{
	m_nVertices = (nSlices * nStacks) * 4;
	//m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;

	m_vPositions.resize(m_nVertices);
	XMFLOAT3 *pxmf3Normals = new XMFLOAT3[m_nVertices];
	XMFLOAT2 *xmf2TexCoords = new XMFLOAT2[m_nVertices];
	XMFLOAT3* pxmf3Binormals = new XMFLOAT3[m_nVertices];
	XMFLOAT3* pxmf3Tangents = new XMFLOAT3[m_nVertices];

	float theta_i, theta_ii, phi_j, phi_jj, fRadius_j, fRadius_jj, y_j, y_jj;
	for (int j = 0, k = 0; j < nStacks; j++)
	{
		phi_j = float(D3DX_PI / nStacks) * j;
		phi_jj = float(D3DX_PI / nStacks) * (j + 1);
		fRadius_j = fRadius * sinf(phi_j);
		fRadius_jj = fRadius * sinf(phi_jj);
		y_j = fRadius*cosf(phi_j);
		y_jj = fRadius*cosf(phi_jj);
		for (int i = 0; i < nSlices; i++)
		{
			theta_i = float(2 * D3DX_PI / nSlices) * i;
			theta_ii = float(2 * D3DX_PI / nSlices) * (i + 1);

			m_vPositions[k] = XMFLOAT3(fRadius_j*cosf(theta_i), y_j, fRadius_j*sinf(theta_i));
			xmf2TexCoords[k] = XMFLOAT2(float(i) / float(nSlices), float(j) / float(nStacks));
			XMStoreFloat3(&pxmf3Normals[k], XMVector3Normalize(-XMLoadFloat3(&m_vPositions[k]))); k++;
			
			// Partial derivative of P with respect to theta
		/*	pxmf3Tangents[k].x = -fRadius_j*sinf(phi)*sinf(theta);
			pxmf3Tangents[k].y = 0.0f;
			pxmf3Tangents[k].z = +radius*sinf(phi)*cosf(theta);*/


			m_vPositions[k] = XMFLOAT3(fRadius_jj*cosf(theta_i), y_jj, fRadius_jj*sinf(theta_i));
			xmf2TexCoords[k] = XMFLOAT2(float(i) / float(nSlices), float(j + 1) / float(nStacks));
			XMStoreFloat3(&pxmf3Normals[k], XMVector3Normalize(-XMLoadFloat3(&m_vPositions[k]))); k++;
			
			m_vPositions[k] = XMFLOAT3(fRadius_j*cosf(theta_ii), y_j, fRadius_j*sinf(theta_ii));
			xmf2TexCoords[k] = XMFLOAT2(float(i + 1) / float(nSlices), float(j) / float(nStacks));
			XMStoreFloat3(&pxmf3Normals[k], XMVector3Normalize(-XMLoadFloat3(&m_vPositions[k]))); k++;
		
			m_vPositions[k] = XMFLOAT3(fRadius_jj*cosf(theta_ii), y_jj, fRadius_jj*sinf(theta_ii));
			xmf2TexCoords[k] = XMFLOAT2(float(i + 1) / float(nSlices), float(j + 1) / float(nStacks));
			XMStoreFloat3(&pxmf3Normals[k], XMVector3Normalize(-XMLoadFloat3(&m_vPositions[k]))); k++;
			
			
			
		/*	
			m_vPositions[k] = XMFLOAT3(fRadius_jj*cosf(theta_i), y_jj, fRadius_jj*sinf(theta_i));
			xmf2TexCoords[k] = XMFLOAT2(float(i) / float(nSlices), float(j + 1) / float(nStacks));
			D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
			
			m_vPositions[k] = XMFLOAT3(fRadius_jj*cosf(theta_ii), y_jj, fRadius_jj*sinf(theta_ii));
			xmf2TexCoords[k] = XMFLOAT2(float(i + 1) / float(nSlices), float(j + 1) / float(nStacks));
			D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;
			
			m_vPositions[k] = XMFLOAT3(fRadius_j*cosf(theta_ii), y_j, fRadius_j*sinf(theta_ii));
			xmf2TexCoords[k] = XMFLOAT2(float(i + 1) / float(nSlices), float(j) / float(nStacks));
			D3DXVec3Normalize(&pxmf3Normals[k], &m_vPositions[k]); k++;*/
		}
	}


	//CalculateVectors(xmf2TexCoords, pxmf3Normals, pxmf3Binormals, pxmf3Tangents);

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferData.pSysMem = pxmf3Normals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dNormalBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * m_nVertices;
	d3dBufferData.pSysMem = xmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferData.pSysMem = pxmf3Binormals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dBinormalBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferData.pSysMem = pxmf3Tangents;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTengentBuffer);


	delete[] pxmf3Normals;
	delete[] xmf2TexCoords;
	delete[] pxmf3Binormals;
	delete[] pxmf3Tangents;

	//정점은 위치 벡터, 법선 벡터, 텍스쳐 좌표를 갖는다.
	//ID3D11Buffer *pd3dBuffers[3] = { m_pd3dPositionBuffer, m_pd3dTexCoordBuffer, m_pd3dNormalBuffer };
	//UINT pnBufferStrides[3] = { sizeof(XMFLOAT3),  sizeof(XMFLOAT2), sizeof(XMFLOAT3) };
	//UINT pnBufferOffsets[3] = { 0, 0, 0 };
	//AssembleToVertexBuffer(3, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	ID3D11Buffer *pd3dBuffers[5] = { m_pd3dPositionBuffer, m_pd3dTexCoordBuffer, m_pd3dNormalBuffer, m_pd3dTengentBuffer, m_pd3dBinormalBuffer };
	UINT pnBufferStrides[5] = { sizeof(XMFLOAT3),  sizeof(XMFLOAT2), sizeof(XMFLOAT3), sizeof(XMFLOAT3), sizeof(XMFLOAT3) };
	UINT pnBufferOffsets[5] = { 0, 0, 0, 0, 0 };
	AssembleToVertexBuffer(5, pd3dBuffers, pnBufferStrides, pnBufferOffsets);


	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fRadius, -fRadius, -fRadius);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fRadius, +fRadius, +fRadius);
}

CSphereMeshTexturedIlluminated::~CSphereMeshTexturedIlluminated()
{
}

CPlaneMeshTexturedIlluminated::CPlaneMeshTexturedIlluminated(ID3D11Device *pd3dDevice, float gScreenWidth,  float fDepth) : CMeshTexturedIlluminated(pd3dDevice)
{
	m_nVertices = 4;
	m_nIndices = 6;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float fx = gScreenWidth*0.5f,  fz = fDepth*0.5f;

	m_vPositions.resize(m_nVertices);
	m_pnIndices = new UINT[m_nIndices];
	//생략된 부분은 LabProject13-2의 CCubeMeshTextured 클래스의 생성자 부분과 동일하다.
	XMFLOAT2 pxmf2TexCoords[4];


	//직육면체의 각 면(삼각형 2개)에 하나의 텍스쳐 이미지 전체가 맵핑되도록 텍스쳐 좌표를 설정한다.
	//
	m_vPositions[0] = XMFLOAT3(-fx, 0, -fz);
	pxmf2TexCoords[0] = XMFLOAT2(0.0f, 1.0f);
	m_vPositions[1] = XMFLOAT3(+fx, 0, -fz);
	pxmf2TexCoords[1] = XMFLOAT2(1.0f, 1.0f);
	m_vPositions[2] = XMFLOAT3(+fx, 0, +fz);
	pxmf2TexCoords[2] = XMFLOAT2(1.0f, 0.0f);
	m_vPositions[3] = XMFLOAT3(-fx, 0, +fz);
	pxmf2TexCoords[3] = XMFLOAT2(0.0f, 0.0f);


	

	m_pnIndices[0] = 3; m_pnIndices[1] = 1; m_pnIndices[2] = 0;
	m_pnIndices[3] = 2; m_pnIndices[4] = 1; m_pnIndices[5] = 3;

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);




	//법선 벡터를 생성하기 위한 다음 코드를 추가한다.
	XMFLOAT3 pxmf3Normals[4];
	CalculateVertexNormal(pxmf3Normals);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferData.pSysMem = pxmf3Normals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dNormalBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * m_nVertices;
	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);

	//정점은 위치 벡터, 법선 벡터, 텍스쳐 좌표를 갖는다.
	ID3D11Buffer *pd3dBuffers[3] = { m_pd3dPositionBuffer, m_pd3dTexCoordBuffer,  m_pd3dNormalBuffer };
	UINT pnBufferStrides[3] = { sizeof(XMFLOAT3), sizeof(XMFLOAT2), sizeof(XMFLOAT3) };
	UINT pnBufferOffsets[3] = { 0, 0, 0 };
	AssembleToVertexBuffer(3, pd3dBuffers, pnBufferStrides, pnBufferOffsets);



	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(UINT)* m_nIndices;
	d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pnIndices;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);



	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fx, 0, -fz);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fx, 0, +fz);
}

CPlaneMeshTexturedIlluminated::~CPlaneMeshTexturedIlluminated()
{
}
CAlphaBlendingMeshTextured::CAlphaBlendingMeshTextured(ID3D11Device *pd3dDevice) : CMeshTextured(pd3dDevice)
{
}
CAlphaBlendingMeshTextured::~CAlphaBlendingMeshTextured()
{}
CAlphaBlendingWaterMeshTextured::CAlphaBlendingWaterMeshTextured(ID3D11Device *pd3dDevice, float gScreenWidth, float gScreenHeight, float fDepth)
	:CAlphaBlendingMeshTextured(pd3dDevice)
{

	m_nVertices = 6;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;


	float fx = gScreenWidth*0.5f, fy = gScreenHeight*0.5f, fz = fDepth*0.5f;

	m_vPositions.resize(m_nVertices);
	//생략된 부분은 LabProject13-2의 CCubeMeshTextured 클래스의 생성자 부분과 동일하다.
	XMFLOAT2 pxmf2TexCoords[6];
	int i = 0;

	//직육면체의 각 면(삼각형 2개)에 하나의 텍스쳐 이미지 전체가 맵핑되도록 텍스쳐 좌표를 설정한다.
	//
	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, +fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 0.0f);

	m_vPositions[i] = XMFLOAT3(+fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(1.0f, 1.0f);

	m_vPositions[i] = XMFLOAT3(-fx, +fy, -fz);
	pxmf2TexCoords[i++] = XMFLOAT2(0.0f, 1.0f);


	//시간을 담는 상수 버퍼
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(XMFLOAT4);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	pd3dDevice->CreateBuffer(&bd, nullptr, &m_pd3dTimeBuffer);


	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3) * m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);



	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * m_nVertices;
	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);


	D3D11_DEPTH_STENCIL_DESC d3dDepthStencilDesc;
	ZeroMemory(&d3dDepthStencilDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	//스카이 박스 사각형들은 깊이 버퍼 알고리즘을 적용하지 않고 깊이 버퍼를 변경하지 않는다.
	d3dDepthStencilDesc.DepthEnable = false;
	d3dDepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	d3dDepthStencilDesc.DepthFunc = D3D11_COMPARISON_NEVER;
	d3dDepthStencilDesc.StencilEnable = false;
	d3dDepthStencilDesc.StencilReadMask = 0xFF;
	d3dDepthStencilDesc.StencilWriteMask = 0xFF;
	d3dDepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	d3dDepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	d3dDepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	d3dDepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	pd3dDevice->CreateDepthStencilState(&d3dDepthStencilDesc, &m_pd3dDepthStencilState);


	//정점은 위치 벡터, 법선 벡터, 텍스쳐 좌표를 갖는다.
	ID3D11Buffer *pd3dBuffers[2] = { m_pd3dPositionBuffer, m_pd3dTexCoordBuffer };
	UINT pnBufferStrides[2] = { sizeof(XMFLOAT3), sizeof(XMFLOAT2) };
	UINT pnBufferOffsets[2] = { 0, 0 };
	AssembleToVertexBuffer(2, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	//m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(-fx, -fy, -fz);
	//m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fx, +fy, +fz);



	ID3D11SamplerState *pd3dSamplerState = nullptr;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	d3dSamplerDesc.MipLODBias = 0.0;
	d3dSamplerDesc.MaxAnisotropy = 1;
	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dSamplerState);

	m_pWaterTexture = new CTexture(1, 1, 0, 0);
	m_pWaterTexture->SetSampler(0, pd3dSamplerState);
	pd3dSamplerState->Release();
	m_pWaterTexture->AddRef();

	ID3D11ShaderResourceView *pd3dsrvTexture = nullptr;
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, L"Image/water.dds", nullptr, nullptr, &pd3dsrvTexture, nullptr);
	m_pWaterTexture->SetTexture(0, pd3dsrvTexture);
	pd3dsrvTexture->Release();


	CreateBlendingState(pd3dDevice);
}
void CAlphaBlendingWaterMeshTextured::CreateBlendingState(ID3D11Device *pd3dDevice)
{
	D3D11_BLEND_DESC d3dBlendDesc;
	ZeroMemory(&d3dBlendDesc, sizeof(d3dBlendDesc));
	d3dBlendDesc.AlphaToCoverageEnable = false;
	d3dBlendDesc.IndependentBlendEnable = false;
	d3dBlendDesc.RenderTarget[0].BlendEnable = true;
	d3dBlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	d3dBlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	d3dBlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	D3D11_COLOR_WRITE_ENABLE_ALPHA;
	D3D11_COLOR_WRITE_ENABLE_ALL;

	pd3dDevice->CreateBlendState(&d3dBlendDesc, &m_pd3dBlendingState);
}
CAlphaBlendingWaterMeshTextured::~CAlphaBlendingWaterMeshTextured()
{
	if (m_pd3dBlendingState) m_pd3dBlendingState->Release();
	if (m_pd3dTimeBuffer) m_pd3dTimeBuffer->Release();
	
}
void  CAlphaBlendingWaterMeshTextured::Render(ID3D11DeviceContext *pd3dDeviceContext)
{
	pd3dDeviceContext->RSSetState(m_pd3dRasterizerState);
	pd3dDeviceContext->IASetVertexBuffers(m_nSlot, m_nBuffers, m_ppd3dVertexBuffers, m_pnVertexStrides, m_pnVertexOffsets);
	pd3dDeviceContext->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	

	// 깊이 버퍼 관련
	//pd3dDeviceContext->OMSetDepthStencilState(m_pd3dDepthStencilState, 1);

	if (m_pWaterTexture)
		m_pWaterTexture->UpdateShaderVariable(pd3dDeviceContext);


	float blendFactors[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	pd3dDeviceContext->OMSetBlendState(m_pd3dBlendingState, blendFactors, 0xfffffff);
	pd3dDeviceContext->Draw(m_nVertices, 0);

	pd3dDeviceContext->OMSetDepthStencilState(nullptr, 1);
	pd3dDeviceContext->OMSetBlendState(0, nullptr, 0xffffffff);

}


CScreenQuadMesh::CScreenQuadMesh(ID3D11Device *pd3dDevice) : CMesh(pd3dDevice)
{
	m_nVertices = 4;
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float fx = 1.0 * 0.5, fy = 1.0f *0.5f;
	D3DXVECTOR2 pd3dxvTexCoords[4];
	m_vPositions.resize(m_nVertices);

	
	// 원본
	m_vPositions[0] = XMFLOAT3(-1.0, +1.0, 0);
	m_vPositions[1] = XMFLOAT3(+1.0, +1.0, 0);
	m_vPositions[2] = XMFLOAT3(-1.0, -1.0, 0);
	m_vPositions[3] = XMFLOAT3(+1.0, -1.0, 0);



	//m_vPositions[0] = XMFLOAT3(-1.0, +1.0, 0);
	//m_vPositions[1] = XMFLOAT3(+0.0, +1.0, 0);
	//m_vPositions[2] = XMFLOAT3(-1.0, -0.0, 0);
	//m_vPositions[3] = XMFLOAT3(-0.0, -0.0, 0);




	// 원본
	pd3dxvTexCoords[0] = D3DXVECTOR2(0.0f, 0.0f);
	pd3dxvTexCoords[1] = D3DXVECTOR2(1.0f, 0.0f);
	pd3dxvTexCoords[2] = D3DXVECTOR2(0.0f, 1.0f);
	pd3dxvTexCoords[3] = D3DXVECTOR2(1.0f, 1.0f);


	//pd3dxvTexCoords[0] = D3DXVECTOR2(0.0f, 1.0f);
	//pd3dxvTexCoords[1] = D3DXVECTOR2(0.0f, 0.0f);
	//pd3dxvTexCoords[2] = D3DXVECTOR2(1.0f, 0.0f);
	//pd3dxvTexCoords[3] = D3DXVECTOR2(1.0f, 1.0f);


	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(D3DXVECTOR3)* m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	m_nIndices = 6;
	m_pnIndices = new UINT[m_nIndices];

	// 원본
	m_pnIndices[0] = 0; m_pnIndices[1] = 1; m_pnIndices[2] = 2;
	m_pnIndices[3] = 1; m_pnIndices[4] = 3; m_pnIndices[5] = 2;

	//m_pnIndices[0] = 0; m_pnIndices[1] = 1; m_pnIndices[2] = 2;
	//m_pnIndices[3] = 0; m_pnIndices[4] = 2; m_pnIndices[5] = 3;



	d3dBufferDesc.ByteWidth = sizeof(D3DXVECTOR2)* m_nVertices;
	d3dBufferData.pSysMem = pd3dxvTexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);


	//정점은 위치 벡터, 법선 벡터, 텍스쳐 좌표를 갖는다.
	ID3D11Buffer *pd3dBuffers[2] = { m_pd3dPositionBuffer, m_pd3dTexCoordBuffer };
	UINT pnBufferStrides[2] = { sizeof(D3DXVECTOR3), sizeof(D3DXVECTOR2) };
	UINT pnBufferOffsets[2] = { 0, 0 };
	AssembleToVertexBuffer(2, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(UINT)* m_nIndices;
	d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pnIndices;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(0, -fy, -1);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(+fx, +fy, +1);


	
}
CScreenQuadMesh::~CScreenQuadMesh()
{
}


CAxisMesh::CAxisMesh(ID3D11Device *pd3dDevice, XMFLOAT3 xmf3Center, XMFLOAT3 xmf3Extent) : CMeshDiffused(pd3dDevice)
{
	m_nVertices = 8;


	m_vPositions.resize(m_nVertices);


	//float f1 = xmf3Center.x + xmf3Extent.x;
	//float f2 = xmf3Center.y + xmf3Extent.y;
	//float f3 = xmf3Center.z + xmf3Extent.z;

	//float f4 = xmf3Center.x - xmf3Extent.x;
	//float f5 = xmf3Center.y - xmf3Extent.y;
	//float f6 = xmf3Center.z - xmf3Extent.z;


	//m_vPositions[0] = XMFLOAT3(f4, f2, f6);
	//m_vPositions[1] = XMFLOAT3(f1, f2, f6);
	//m_vPositions[2] = XMFLOAT3(f1, f2, f3);
	//m_vPositions[2].x = f1, m_vPositions[2].y = f2, m_vPositions[2].z = f3;


	//m_vPositions[3] = XMFLOAT3(f4, f2, f3);


	//m_vPositions[4] = XMFLOAT3(f4, f5, f6);
	//m_vPositions[5] = XMFLOAT3(f1, f5, f6);
	//m_vPositions[6] = XMFLOAT3(f1, f5, f3);
	//m_vPositions[7] = XMFLOAT3(f4, f5, f3);




	m_vPositions[0] = XMFLOAT3(xmf3Center.x - xmf3Extent.x, xmf3Center.y + xmf3Extent.y, xmf3Center.z - xmf3Extent.z);
	m_vPositions[1] = XMFLOAT3(xmf3Center.x + xmf3Extent.x, xmf3Center.y + xmf3Extent.y, xmf3Center.z - xmf3Extent.z);
	m_vPositions[2] = XMFLOAT3(xmf3Center.x + xmf3Extent.x, xmf3Center.y + xmf3Extent.y, xmf3Center.z + xmf3Extent.z);
	m_vPositions[3] = XMFLOAT3(xmf3Center.x - xmf3Extent.x, xmf3Center.y + xmf3Extent.y, xmf3Center.z + xmf3Extent.z);
	m_vPositions[4] = XMFLOAT3(xmf3Center.x - xmf3Extent.x, xmf3Center.y - xmf3Extent.y, xmf3Center.z - xmf3Extent.z);
	m_vPositions[5] = XMFLOAT3(xmf3Center.x + xmf3Extent.x, xmf3Center.y - xmf3Extent.y, xmf3Center.z - xmf3Extent.z);
	m_vPositions[6] = XMFLOAT3(xmf3Center.x + xmf3Extent.x, xmf3Center.y - xmf3Extent.y, xmf3Center.z + xmf3Extent.z);
	m_vPositions[7] = XMFLOAT3(xmf3Center.x - xmf3Extent.x, xmf3Center.y - xmf3Extent.y, xmf3Center.z + xmf3Extent.z);



	

	m_nIndices = 36;
	m_pnIndices = new UINT[m_nIndices];

	m_pnIndices[0] = 3; m_pnIndices[1] = 1; m_pnIndices[2] = 0;
	m_pnIndices[3] = 2; m_pnIndices[4] = 1; m_pnIndices[5] = 3;
	m_pnIndices[6] = 0; m_pnIndices[7] = 5; m_pnIndices[8] = 4;
	m_pnIndices[9] = 1; m_pnIndices[10] = 5; m_pnIndices[11] = 0;
	m_pnIndices[12] = 3; m_pnIndices[13] = 4; m_pnIndices[14] = 7;
	m_pnIndices[15] = 0; m_pnIndices[16] = 4; m_pnIndices[17] = 3;
	m_pnIndices[18] = 1; m_pnIndices[19] = 6; m_pnIndices[20] = 5;
	m_pnIndices[21] = 2; m_pnIndices[22] = 6; m_pnIndices[23] = 1;
	m_pnIndices[24] = 2; m_pnIndices[25] = 7; m_pnIndices[26] = 6;
	m_pnIndices[27] = 3; m_pnIndices[28] = 7; m_pnIndices[29] = 2;
	m_pnIndices[30] = 6; m_pnIndices[31] = 4; m_pnIndices[32] = 5;
	m_pnIndices[33] = 7; m_pnIndices[34] = 4; m_pnIndices[35] = 6;


	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);


	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(UINT)* m_nIndices;
	d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pnIndices;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);






	ID3D11Buffer *pd3dBuffers[1] = { m_pd3dPositionBuffer };
	UINT pnBufferStrides[1] = { sizeof(XMFLOAT3) };
	UINT pnBufferOffsets[1] = { 0 };
	AssembleToVertexBuffer(1, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(xmf3Center.x - xmf3Extent.x, xmf3Center.y - xmf3Extent.y, xmf3Center.z - xmf3Extent.z);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(xmf3Center.x + xmf3Extent.x, xmf3Center.y + xmf3Extent.y, xmf3Center.z + xmf3Extent.z);


	D3D11_RASTERIZER_DESC d3dRasterizerDesc;
	d3dRasterizerDesc.AntialiasedLineEnable = false;
	d3dRasterizerDesc.CullMode = D3D11_CULL_BACK;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = true;
	d3dRasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
	d3dRasterizerDesc.FrontCounterClockwise = false;
	d3dRasterizerDesc.MultisampleEnable = false;
	d3dRasterizerDesc.ScissorEnable = false;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;

	pd3dDevice->CreateRasterizerState(&d3dRasterizerDesc, &m_pd3dRasterizerState);


}
CAxisMesh::~CAxisMesh()
{
}

CFixedMesh::CFixedMesh(ID3D11Device *pd3dDevice, string strFileName) : CMeshTexturedIlluminated(pd3dDevice)
{
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	FILE* pFile = NULL;
	string fileName = strFileName;
	//string fileName = "Data/";
	//fileName += "_pos.txt";
	::fopen_s(&pFile, (fileName.c_str()), "rb,ccs = UNICODE");
	// 정점의 개수를 읽는다.
	::fread(&m_nVertices, sizeof(int), 1, pFile);
	m_vPositions.resize(m_nVertices);
	XMFLOAT3 *pd3dxvNormals = new XMFLOAT3[m_nVertices];
	XMFLOAT2 *pd3dxvTexCoords = new XMFLOAT2[m_nVertices];


	::fread(&m_vPositions[0], sizeof(XMFLOAT3), m_nVertices, pFile);
	::fread(pd3dxvNormals, sizeof(XMFLOAT3), m_nVertices, pFile);
	::fread(pd3dxvTexCoords, sizeof(XMFLOAT2), m_nVertices, pFile);
	::fclose(pFile);



	XMFLOAT3* pxmf3Binormals = new XMFLOAT3[m_nVertices];
	XMFLOAT3* pxmf3Tangents = new XMFLOAT3[m_nVertices];


	//CalculateVertexNormal(pd3dxvNormals);
	//CalculateVectors(pd3dxvTexCoords, pd3dxvNormals, pxmf3Binormals, pxmf3Tangents);




	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;


	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));

	d3dBufferData.pSysMem = &m_vPositions[0];
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferData.pSysMem = pd3dxvNormals;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dNormalBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2)* m_nVertices;
	d3dBufferData.pSysMem = pd3dxvTexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferData.pSysMem = pd3dxvTexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dBinormalBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferData.pSysMem = pd3dxvTexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTengentBuffer);

	UINT BufferCount = 5;
	ID3D11Buffer *pd3dBuffers[5] = {
		m_pd3dPositionBuffer,
		m_pd3dTexCoordBuffer,
		m_pd3dNormalBuffer,
		m_pd3dTengentBuffer,
		m_pd3dBinormalBuffer
	
	};
	UINT pnBufferStrides[5] = {
		sizeof(XMFLOAT3),
		sizeof(XMFLOAT2),
		sizeof(XMFLOAT3),
		sizeof(XMFLOAT3),
		sizeof(XMFLOAT3),

	};
	UINT pnBufferOffsets[5] = { 0, 0, 0, 0, 0 }; // 5
	AssembleToVertexBuffer(BufferCount, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	//ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	//d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	//d3dBufferDesc.ByteWidth = sizeof(UINT)* m_nIndices;
	//d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	//d3dBufferDesc.CPUAccessFlags = 0;
	//ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	//d3dBufferData.pSysMem = m_pnIndices;
	//pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);

	XMFLOAT3 min = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 max = XMFLOAT3(0.0f, 0.0f, 0.0f);


	//for (int i = 0; i < m_nVertices; i++)
	//	cout << m_pd3dxvPositions[i].x << "   " << m_pd3dxvPositions[i].y << endl;

	for (int i = 0; i < m_nVertices; ++i)
	{
		if (m_vPositions[i].x < min.x) min.x = m_vPositions[i].x;
		if (m_vPositions[i].y < min.y) min.y = m_vPositions[i].y;
		if (m_vPositions[i].z < min.z) min.z = m_vPositions[i].z;

		if (m_vPositions[i].x > max.x) max.x = m_vPositions[i].x;
		if (m_vPositions[i].y > max.y) max.y = m_vPositions[i].y;
		if (m_vPositions[i].z > max.z) max.z = m_vPositions[i].z;
	}

	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(min.x, min.y, min.z);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(max.x, max.y, max.z);


}


CFixedMesh::~CFixedMesh()
{
}
CSentenceMesh::CSentenceMesh(ID3D11Device *pd3dDevice, int nMaxLength) : CMeshTextured(pd3dDevice)
{
	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	m_nMaxLength = nMaxLength;
	m_nVertices = nMaxLength * 6;
	//m_nVertices = 6;
	m_nIndices = m_nVertices;



	// Create the vertex array.
	//m_vPositions = new XMFLOAT3[m_nVertices];
	//XMFLOAT2 *pxmf2TexCoords = new XMFLOAT2[m_nVertices];
	m_pVertexs = new VertexType[m_nVertices];
	

	//m_pnIndices = new UINT[m_nIndices];


	// Create the nIndex array.


	// Initialize vertex array to zeros at first.
	//memset(m_vPositions, 0, (sizeof(XMFLOAT3) * m_nVertices));
	memset(m_pVertexs, 0, (sizeof(VertexType) * m_nVertices));

	// Initialize the nIndex array.
	/*for (int i = 0; i < m_nIndices; i++)
	{
		m_pnIndices[i] = i;
	}
*/


	
	//int nIndex = 0;
	//m_vPositions[nIndex] = XMFLOAT3(-1.0f, 1.0f, 0.0f);  // Top left.											   
	//pxmf2TexCoords[nIndex] = XMFLOAT2(0.0f, 0.0f);
	//nIndex++;

	//m_vPositions[nIndex] = XMFLOAT3(1.0f, 0.92, 0.0f);  // Bottom right.													 
	//pxmf2TexCoords[nIndex] = XMFLOAT2(0.6f, 1.0f);
	//nIndex++;

	//m_vPositions[nIndex] = XMFLOAT3(-1.0f, 0.92f, 0.0f);  // Bottom left.												 
	//pxmf2TexCoords[nIndex] = XMFLOAT2(0.0f, 1.0f);
	//nIndex++;

	//// Second triangle in quad.
	//m_vPositions[nIndex] = XMFLOAT3(-1.0f, 1.0f, 0.0f);  // Top left.
	//pxmf2TexCoords[nIndex] = XMFLOAT2(0.0f, 0.0f);
	//nIndex++;

	//m_vPositions[nIndex] = XMFLOAT3(1.0f, 1.0f, 0.0f);  // Top right.												
	//pxmf2TexCoords[nIndex] = XMFLOAT2(0.6f, 0.0f);
	//nIndex++;

	//m_vPositions[nIndex] = XMFLOAT3(1.0f, 0.92, 0.0f);  // Bottom right.												
	//pxmf2TexCoords[nIndex] = XMFLOAT2(0.6f, 1.0f);
	//nIndex++;




	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	d3dBufferDesc.ByteWidth = sizeof(VertexType)* m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	D3D11_SUBRESOURCE_DATA d3dBufferData;

	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pVertexs;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);




	/*d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	D3D11_SUBRESOURCE_DATA d3dBufferData;


	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_vPositions;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2) * m_nVertices;
	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);*/






	//ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	//d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	//d3dBufferDesc.ByteWidth = sizeof(UINT)* m_nIndices;
	//d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	//d3dBufferDesc.CPUAccessFlags = 0;
	//ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	//d3dBufferData.pSysMem = m_pnIndices;
	//pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);


	
	ID3D11Buffer *pd3dBuffers[1] = {
		m_pd3dPositionBuffer
	

	};
	UINT pnBufferStrides[1] = {
		sizeof(VertexType)

	};
	UINT pnBufferOffsets[1] = { 0}; // 5
	AssembleToVertexBuffer(1, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	XMFLOAT3 min = XMFLOAT3(FLT_MIN, FLT_MIN, FLT_MIN);
	XMFLOAT3 max = XMFLOAT3(FLT_MAX, FLT_MAX, FLT_MAX);
	
	m_bcBoundingCube.m_xmf3Minimum = XMFLOAT3(min.x, min.y, min.z);
	m_bcBoundingCube.m_xmf3Maximum = XMFLOAT3(max.x, max.y, max.z);
}



CSentenceMesh::~CSentenceMesh()
{
}